Pan has a five-day code freeze before every stable release to give
translators a chance to update translations.  To be notified of
freezes, add your email here or mail the Pan team at pan@rebelbase.com.

String bug reports are encouraged.

Thanks for translating Pan!
the Pan team

---

TO BE NOTIFIED ON FREEZE:

Benjamin Greiner <nadaschauher@gmx.de>
Christian Rose <menthos@menthos.com>
Andras Timar <timar@gnome.hu>
Christophe Merlet <redfox@redfoxcenter.org>
Marcel Telka <marcel@telka.sk>
Pablo Saratxaga <pablo@mandrakesoft.com>
Zipeco <zipeco@btamail.net.cn>
Duarte Loreto <happyguy_pt@hotmail.com>
GNOME PL Team <translators@gnome.pl>
Abel Cheung <deaddog@deaddog.org>
Vincent van Adrighem <V.vanAdrighem@dirck.mine.nu>
Aleksandar Urosevic <urke@users.sourceforge.net>
Miloslav Trmac <mitr@volny.cz>
Flammie Pirinen <flammie@gnome-fi.org>
Alina Valea <avalea@home.ro>
Ilkka Tuohela <hile@iki.fi>
Yavor Doganov <yavor@doganov.org>

