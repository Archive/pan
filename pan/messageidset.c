/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/messageidset.h>

struct MsgIdSet
{
	GHashTable * hash;
};

MessageIdSet*
messageidset_new (void)
{
	MessageIdSet * mset = g_new0 (MessageIdSet, 1);
	mset->hash = g_hash_table_new_full (pstring_hash, pstring_equal, (GDestroyNotify)pstring_free, NULL);
	return mset;
}

void
messageidset_free (MessageIdSet * mset)
{
	g_return_if_fail (mset != NULL);

	g_hash_table_destroy (mset->hash);
	g_free (mset);
}

void
messageidset_clear (MessageIdSet * mset)
{
	g_return_if_fail (mset != NULL);

	g_hash_table_destroy (mset->hash);
	mset->hash = g_hash_table_new_full (pstring_hash, pstring_equal, (GDestroyNotify)pstring_free, NULL);
}

static void
maybe_add (MessageIdSet * mset, const PString * message_id)
{
	g_return_if_fail (pstring_is_set (message_id));

	g_hash_table_insert (mset->hash, pstring_dup(message_id), GINT_TO_POINTER(1));
}

void
messageidset_add_message_ids  (MessageIdSet         * mset,
                               const PString       ** message_ids,
                               int                    message_id_qty)
{
	int i;

	g_return_if_fail (mset != NULL);
	g_return_if_fail (message_ids != NULL);
	g_return_if_fail (message_id_qty >= 1);

	for (i=0; i<message_id_qty; ++i)
		maybe_add (mset, message_ids[i]);
}

void
messageidset_add_articles (MessageIdSet            * mset,
                           const Article          ** articles,
                           int                       article_qty)
{
	int i;

	g_return_if_fail (mset != NULL);
	g_return_if_fail (article_qty >= 1);
	g_return_if_fail (articles_are_valid (articles, article_qty));

	for (i=0; i<article_qty; ++i)
		maybe_add (mset, &articles[i]->message_id);
}

void
messageidset_add_articles_and_ancestors  (MessageIdSet            * mset,
                                          const Article          ** articles,
                                          int                       article_qty)
{
	int i;

	/* sanity clause */
	g_return_if_fail (mset != NULL);
	g_return_if_fail (article_qty >= 1);
	g_return_if_fail (articles_are_valid (articles, article_qty));

	/* add the messages */
	for (i=0; i<article_qty; ++i) {
		const Article * a;
		for (a=articles[i]; a!=NULL; a=a->parent)
			maybe_add (mset, &a->message_id);
	}
}

static void
add_articles (Article ** articles, guint article_qty, gpointer mset_gpointer)
{
	MessageIdSet * mset = (MessageIdSet*) mset_gpointer;
	messageidset_add_articles (mset, (const Article**)articles, article_qty);
}

void
messageidset_add_articles_and_threads (MessageIdSet        * mset,
                                       const Article      ** articles,
                                       int                   article_qty)
{
	int i;

	/* sanity clause */
	g_return_if_fail (mset != NULL);
	g_return_if_fail (article_qty >= 1);
	g_return_if_fail (articles_are_valid (articles, article_qty));

	for (i=0; i<article_qty; ++i)
		article_forall_in_thread ((Article*)articles[i], add_articles, mset);
}


static void
messageidset_get_all_ghfunc (gpointer key, gpointer val, gpointer data)
{
	g_ptr_array_add ((GPtrArray*)data, pstring_dup(key));
}


GPtrArray*
messageidset_get_ids (const MessageIdSet  * mset)
{
	GPtrArray * message_ids;

	g_return_val_if_fail (mset!=NULL, NULL);

	message_ids = g_ptr_array_sized_new (g_hash_table_size (mset->hash));
	g_hash_table_foreach (mset->hash, messageidset_get_all_ghfunc, message_ids);

	return message_ids;
}
