/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GUI_HEADERS_H__
#define __GUI_HEADERS_H__

#include <glib.h>
#include <gtk/gtkwidget.h>
#include <gmime/gmime-message.h>
#include <pan/base/article.h>

/**
 * These are the flgs passed in via the "header_fields" argument in
 * the function gui_set_headers().  For each flag, that header will
 * be displayed.
 */
enum {
	UI_HEADER_SUBJECT	= 1<<0,
	UI_HEADER_AUTHOR	= 1<<1,
	UI_HEADER_MESSAGE_ID	= 1<<2,
	UI_HEADER_REFERENCES	= 1<<3,
	UI_HEADER_REPLY_TO	= 1<<4,
	UI_HEADER_FOLLOWUP_TO	= 1<<5,
	UI_HEADER_DATE		= 1<<6,
	UI_HEADER_NEWSGROUPS	= 1<<7,
	UI_HEADER_NEWSREADER	= 1<<8
};

/**
 * This variable is initialized in prefs_init() and defines the user-settable
 * default headers to be displayed.  This is an or'ed list of items from the
 * above enumeration.
 */
extern gulong header_flags;

#endif /* __GUI_HEADERS_H__ */
