/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>
#include <pan/base/debug.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/serverlist.h>
#include <pan/flagset.h>
#include <pan/queue.h>
#include <pan/task-bodies.h>

static GHashTable * _id_to_mid = NULL;

void
flagset_init (void)
{
	_id_to_mid = g_hash_table_new_full (pstring_hash, pstring_equal, NULL, g_object_unref);
}

gboolean
flagset_has_article (const Article * article)
{
	g_return_val_if_fail (article_is_valid(article), FALSE);

	return g_hash_table_lookup (_id_to_mid, &article->message_id) != NULL;
}

static void
flagset_add_messages (MessageIdentifier  ** mids,
                      int                   qty)
{
	int i;

	/* sanity clause */
	g_return_if_fail (qty>=1);
	g_return_if_fail (message_identifiers_are_valid ((const MessageIdentifier**)mids, qty));

	/* add these message-ids to the set */
	for (i=0; i<qty; ++i) {
		g_object_ref (mids[i]);
		g_hash_table_replace (_id_to_mid, &mids[i]->message_id, mids[i]);
	}
}

void
flagset_add_articles (const Article   ** articles,
                      int                qty)
{
	int i;
	MessageIdentifier ** mids;

	/* sanity clause */
	g_return_if_fail (qty>=1);
	g_return_if_fail (articles_are_valid(articles, qty));

	/* convert to message-identifiers */
	mids = g_newa (MessageIdentifier*, qty);
	for (i=0; i<qty; ++i)
		mids[i] = message_identifier_new_from_article (articles[i]);

	/* add */
	flagset_add_messages (mids, qty);

	/* cleanup */
	for (i=0; i<qty; ++i)
		g_object_unref (mids[i]);
}

static void
flagset_remove_messages (MessageIdentifier  ** mids,
                         int                   qty)
{
	int i;

	/* sanity clause */
	g_return_if_fail (qty>=1);
	g_return_if_fail (message_identifiers_are_valid ((const MessageIdentifier**)mids, qty));

	/* remove these message-ids from the set */
	for (i=0; i<qty; ++i)
		g_hash_table_remove (_id_to_mid, &mids[i]->message_id);
}

void
flagset_remove_articles (const Article   ** articles,
                         int                qty)
{
	int i;
	MessageIdentifier ** mids;

	/* sanity clause */
	g_return_if_fail (qty>=1);
	g_return_if_fail (articles_are_valid(articles, qty));

	/* convert to message-identifiers */
	mids = g_newa (MessageIdentifier*, qty);
	for (i=0; i<qty; ++i)
		mids[i] = message_identifier_new_from_article (articles[i]);

	/* add */
	flagset_remove_messages (mids, qty);

	/* cleanup */
	for (i=0; i<qty; ++i)
		g_object_unref (mids[i]);
}

void
flagset_flush (void)
{
	Server * server = serverlist_get_active_server ();
	if (server != NULL)
	{
		/* get an array of the identifiers and ref them so they
		 * can be used after the hash is destroyed */
		GPtrArray * mids = g_ptr_array_new ();
		pan_hash_to_ptr_array (_id_to_mid, mids);
		pan_g_ptr_array_foreach (mids, (GFunc)g_object_ref, NULL);

		/* clear the flagset's hash table */
		g_hash_table_destroy (_id_to_mid);
		_id_to_mid = g_hash_table_new_full (pstring_hash, pstring_equal, NULL, g_object_unref);

		/* if any messages were flagged, queue them */
		if (mids->len)
			queue_add (TASK (task_bodies_new (server, (MessageIdentifier**)mids->pdata, mids->len)));

		/* cleanup */
		pan_g_ptr_array_foreach (mids, (GFunc)g_object_unref, NULL);
		g_ptr_array_free (mids, TRUE);
	}
}
