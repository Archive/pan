/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _GROUP_ACTIONS__H_
#define _GROUP_ACTIONS__H_

void group_action_subscribed_download_new          (void);
void group_action_subscribed_download_new_and_bodies (void);
void group_action_selected_download_new            (void);
void group_action_selected_download_new_and_bodies (void);
void group_action_selected_download_dialog         (void);
void group_action_selected_mark_read               (void);
void group_action_selected_update_count_info       (void);

void group_action_selected_subscribe               (void);
void group_action_selected_unsubscribe             (void);
void group_action_selected_properties              (void);
void group_action_selected_empty                   (void);
void group_action_selected_destroy                 (void);

#endif /* _GROUP_ACTIONS__H_ */
