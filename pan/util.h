/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __UTIL_H__
#define __UTIL_H__

#include <time.h> /* for time_t */

#include <glib.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkitemfactory.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkmenushell.h>

#include <pan/base/pan-glib-extensions.h> /* for PRETTY_FUNCTION */

/**
***  Dialogs
**/

void pan_info_dialog (const char * fmt, ...);

void pan_error_dialog (const char * fmt, ...);

void pan_error_dialog_parented (gpointer gtkwindow, const char * fmt, ...);


/**
***  Menus
**/

GtkWidget* menubar_create     (GtkWidget             * window,
                               GtkItemFactoryEntry   * entries,
                               unsigned int            entries_qty,
                               const char            * path,
                               gpointer                data);

GtkWidget* menu_create_items  (GtkItemFactoryEntry  * entries,
                               unsigned int           entries_qty,
                               const char           * path,
                               GtkItemFactory      ** factory,
                               gpointer               data);

void menu_set_sensitive       (GtkItemFactory       * ifactory,
                               const char           * path,
                               gboolean               sensitive);

void menu_set_checked         (GtkItemFactory       * ifactory,
                               const char           * path,
                               gboolean               checked);

/**
***  Column Widths and Window Geometry
**/

void     gui_save_column_widths               (GtkWidget    * clist,
                                               const char   * key);

void     gui_save_column_widths_tree_view     (GtkWidget    * tree_view,
                                               const char   * key);

void     gui_save_window_size                 (GtkWidget    * window,
                                               const char   * key);

void     gui_restore_column_widths            (GtkWidget    * clist,
                                               const char   * key);

void     gui_restore_column_widths_tree_view  (GtkWidget    * tree_view,
                                               const char   * key);

gboolean gui_restore_window_size              (GtkWidget    * window,
                                               const char   * key);


/**
***
**/

void launch_external_editor (const char    * edit_command,
                             const char    * filename,
                             int             line_number,
                             GSourceFunc     finished_callback,
                             gpointer        finished_user_data);


char * get_date_display_string (time_t date, const char * strftime_fmt);

void pan_widget_set_font (GtkWidget* w, const char* font_name);

void pan_url_show (const char * url);

void pan_gtk_entry_set_text   (GtkWidget            * w,
                               const char           * text);

GtkWidget*  pan_gtk_image_new_from_inline_text (const guint8    * inline_text,
                                                GtkIconSize       icon_size);

/**
***
**/

GtkWidget *     pan_hig_workarea_create                 (void);

void            pan_hig_workarea_add_section_divider    (GtkWidget   * table,
                                                         int         * row);

void            pan_hig_workarea_add_section_title      (GtkWidget   * table,
                                                         int         * row,
                                                         const char  * section_title);

void            pan_hig_workarea_add_section_spacer     (GtkWidget   * table,
                                                         int           row,
                                                         int           items_in_section);

GtkWidget *     pan_hig_workarea_add_wide_checkbutton   (GtkWidget   * table,
                                                         int         * row,
                                                         const char  * mnemonic_string,
                                                         gboolean      is_active);

GtkWidget *     pan_hig_workarea_add_label              (GtkWidget   * table,
                                                         int           row,
                                                         const char  * mnemonic_string);

void            pan_hig_workarea_add_control            (GtkWidget   * table,
                                                         int           row,
                                                         GtkWidget   * control);

GtkWidget*      pan_hig_workarea_add_row                (GtkWidget   * table,
                                                         int         * row,
                                                         const char  * mnemonic_string,
                                                         GtkWidget   * control,
                                                         GtkWidget   * mnemonic_or_null_if_control_is_mnemonic);

/**
***
**/

enum
{
	GUI_PAD_SMALL = 3,
	GUI_PAD = 6,
	GUI_PAD_BIG = 12,
	GUI_PAD_LARGE = 12
};

#endif /* __UTIL_H__ */
