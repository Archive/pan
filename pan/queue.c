/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Author: Charles Kerr <charles@rebelbase.com>
 *
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * A note on implementation
 * The queue defers all public tasks (like insert, remove, etc.)
 * to the queue thread.  This ensures that all tasks are handled
 * in sequence, rather than concurrently.  This ensures that callbacks
 * will be invoked in sync with the queue, so the queue's state won't
 * change during the callback.
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <errno.h>

#include <glib.h>

#include <pan/base/base-prefs.h>
#include <pan/base/debug.h>
#include <pan/base/log.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/server.h>

#include <pan/queue.h>
#include <pan/socket-pool.h>
#include <pan/task-xml.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

static void fire_tasks_added (const GSList * tasks, int index);
static void fire_tasks_moved (const GSList * tasks, int moveto_index);
static void fire_tasks_removed (const GSList * tasks);
static void fire_queue_size_changed (int running_qty, int total_qty);
static void fire_connection_size_changed (int increment);
static void fire_online_status_changed (gboolean online);
static void fire_message_id_status_changed (const PString ** mids, int qty);

static gboolean queue_upkeep (gpointer);

static void queue_set_task_status (Task* task, QueueTaskStatus status);

/*********************
**********************  Constants
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

static GSList* task_list = NULL;

static GHashTable * server_to_pool = NULL;
static GHashTable * task_to_status = NULL;
static GHashTable * message_id_hash = NULL;

static guint running_tasks = 0;

static gboolean _online = TRUE;
static gboolean _remove_failed_tasks = TRUE;
static gboolean _dirty = FALSE;

static guint queue_upkeep_timer_id = 0u;

/*********************
**********************  BEGINNING OF SOURCE
*********************/

#define SECONDS_BETWEEN_QUEUE_UPKEEP 15

/************
*************  PUBLIC ROUTINES
************/

void
queue_init (gboolean online, gboolean remove_failed_tasks)
{
	guint msec;

	debug_enter ("queue_init");

	server_to_pool = g_hash_table_new (g_direct_hash, g_direct_equal);
	task_to_status = g_hash_table_new (g_direct_hash, g_direct_equal);
	message_id_hash = g_hash_table_new_full (pstring_hash,
	                                         pstring_equal,
	                                         (GDestroyNotify)pstring_free,
	                                         NULL);

	_online = online;

	_remove_failed_tasks = remove_failed_tasks;

	msec = SECONDS_BETWEEN_QUEUE_UPKEEP * 1000;
	queue_upkeep_timer_id = g_timeout_add (msec,
	                                       queue_upkeep,
	                                       GINT_TO_POINTER(1));

	debug_exit ("queue_init");
}

/***
**** SOCKET POOL
***/

static void
pool_count_ghfunc (gpointer key,
                   gpointer value,
                   gpointer user_data)
{
	int * pi = (int *) user_data;
	const SocketPool * pool = (const SocketPool*) value;
	(*pi) += socket_pool_get_connection_qty (pool);
}

static void
pool_socket_avail_cb (gpointer call_object,
                      gpointer call_arg,
                      gpointer user_data)
{
	/* update the connection count */
	int qty = 0;
	g_hash_table_foreach (server_to_pool, pool_count_ghfunc, &qty);
	fire_connection_size_changed (qty);

	/* wakeup the queue */
	queue_wakeup ();
}

static SocketPool*
get_socket_pool_from_server (Server * server)
{
	SocketPool * pool =
		(SocketPool*) g_hash_table_lookup (server_to_pool, server);
	if (pool == NULL) {
		pool = socket_pool_new (server);
		socket_pool_set_online (pool, _online);
		pan_callback_add (socket_pool_get_available_callback(pool),
		                  pool_socket_avail_cb,
		                  NULL);
		g_hash_table_insert (server_to_pool, server, pool);
	}
	return pool;
}

/***
****
***/

int
queue_get_message_id_status (const PString * message_id)
{
	return GPOINTER_TO_INT (
		g_hash_table_lookup (message_id_hash, message_id));
}
static void
queue_add_message_id_state (const PString    ** mids,
                            int                 mid_qty,
                            int                 state)
{
	int i;

	/* sanity clause */
	g_return_if_fail (state);
	g_return_if_fail (mids!=NULL);
	g_return_if_fail (mid_qty>0);
	g_return_if_fail (pstring_is_set(mids[0]));

	/* add the state */
	for (i=0; i<mid_qty; ++i)
	{
		int new_state = state | queue_get_message_id_status(mids[i]);
		g_hash_table_insert (message_id_hash,
		                     pstring_dup(mids[i]),
		                     GINT_TO_POINTER(new_state));
	}

	fire_message_id_status_changed (mids, mid_qty);
}

static void
queue_remove_message_id_state (const PString   ** mids,
                               int                mid_qty,
                               int                state)
{
	int i;

	/* sanity clause */
	g_return_if_fail (state);
	g_return_if_fail (mids!=NULL);
	g_return_if_fail (mid_qty>0);
	g_return_if_fail (pstring_is_set(mids[0]));

	/* remove the state */
	for (i=0; i<mid_qty; ++i)
	{
		const PString * id = mids[i];
		const int new_state = queue_get_message_id_status(id) & ~state;
		if (!new_state)
			g_hash_table_remove (message_id_hash, id);
		else
			g_hash_table_insert (message_id_hash,
			                     pstring_dup(id),
			                     GINT_TO_POINTER(new_state));
	}

	fire_message_id_status_changed (mids, mid_qty);
}

static int
task_get_type_state (const Task * task)
{
	int state = 0;

	g_return_val_if_fail (task!=NULL, 0);

	switch (task->type)
	{
		case TASK_TYPE_BODY:
		case TASK_TYPE_BODIES:
			state |= QUEUE_MESSAGE_ID_DOWNLOAD;
			break;
		case TASK_TYPE_SAVE:
			state |= QUEUE_MESSAGE_ID_SAVE;
			break;
		case TASK_TYPE_HEADERS:
			break;
		default:
			state = 0;
			break;
	}

	return state;
}

static void
queue_add_message_ids_forall_func (Task                 * task,
                                   MessageIdentifier   ** mids,
                                   int                    mid_qty,
                                   gpointer               state_gpointer)
{
	int i;
	const int state = GPOINTER_TO_INT(state_gpointer);
	const PString ** pstrs = g_newa (const PString*, mid_qty);
	for (i=0; i<mid_qty; ++i)
		pstrs[i] = &mids[i]->message_id;
	queue_add_message_id_state (pstrs, mid_qty, state);
}

static void
queue_add_message_ids (Task * task)
{
	const int state = task_get_type_state (task);
	if (state)
		task_forall_identifiers (task,
		                         queue_add_message_ids_forall_func, 
		                         GINT_TO_POINTER(state));
}

static void
queue_remove_message_ids_forall_func (Task               * task,
                                      MessageIdentifier ** mids,
                                      int                  mid_qty,
                                      gpointer             state_gpointer)
{
	int i;
	const int state = GPOINTER_TO_INT(state_gpointer);
	const PString ** pstrs = g_newa (const PString*, mid_qty);
	for (i=0; i<mid_qty; ++i)
		pstrs[i] = &mids[i]->message_id;
	queue_remove_message_id_state (pstrs, mid_qty, state);
}

static void
queue_remove_message_ids (Task * task)
{
	const int state = task_get_type_state (task);
	if (state)
		task_forall_identifiers (task,
		                         queue_remove_message_ids_forall_func,
		                         GINT_TO_POINTER(state));
}

/***
****
***/

gboolean
queue_is_online (void)
{
	return _online;
}

static void
queue_set_online_ghfunc (gpointer key, gpointer value, gpointer user_data)
{
	socket_pool_set_online ((SocketPool*)value, _online);
}

void
queue_set_online (gboolean p)
{
	if (_online != p)
	{
		_online = p;

		/* set the socket pools offline */
		g_hash_table_foreach (server_to_pool,
		                      queue_set_online_ghfunc,
		                      NULL);

		/* let listeners know */
		fire_online_status_changed (_online);
	}

	if (_online)
		queue_wakeup ();
}


void
queue_shutdown (void)
{
	/* clear the status hashtable */
	g_hash_table_destroy (task_to_status);
	task_to_status = NULL;

	/* optionally save the tasks */

	/* unref the tasks */

	/* close the sockets */
	/* FIXME: need to clear the socket pools */
}


void
queue_add (Task * task)
{
	GSList * l;

	g_return_if_fail (task!=NULL);

	l = g_slist_append (NULL, task);
	queue_insert_tasks (l, task->high_priority ? 0 : -1);
	g_slist_free (l);
}

gboolean
queue_is_empty (void)
{
	return task_list == NULL;
}

/**
***
**/

void
queue_forall_tasks (QueueForallTaskFunc    forall_func,
                    gpointer               forall_func_user_data,
                    gboolean               run_even_if_no_tasks)
{
	int i;
	int task_qty;
	GSList * l;
	Task ** tasks;

	task_qty = g_slist_length (task_list);
 	tasks = g_newa (Task*, task_qty);
	for (l=task_list, i=0; l!=NULL; l=l->next, ++i)
		tasks[i] = (Task*) l->data;

	if (task_qty>0 || run_even_if_no_tasks)
		(*forall_func)(tasks, task_qty, forall_func_user_data);
}

static void
queue_save_tasks_forall (Task       ** tasks,
                         int           task_qty,
                         gpointer      filename_gpointer)
{
	task_xml_write ((const char*)filename_gpointer,
	                (const Task**)tasks,
	                task_qty);
}

/**
***
**/


void
queue_stop_tasks (const GSList * tasks)
{
	const GSList * l;
	for (l=tasks; l!=NULL; l=l->next)
	{
		Task * task = TASK(l->data);
		const QueueTaskStatus status = queue_get_task_status (task);

		if (status == QUEUE_TASK_STATUS_RUNNING)
		{
			/* ask a running task to stop */
			task->abort_flag = TRUE;
			queue_set_task_status (task, QUEUE_TASK_STATUS_STOPPING);
		}
		else
		{
			/* if the task was stopped because we went offline,
			   set it to queued instead of stopped. */
			queue_set_task_status (task, task->abort_flag && !queue_is_online()
					? QUEUE_TASK_STATUS_QUEUED
					: QUEUE_TASK_STATUS_STOPPED);
		}
	}
}

/**
***
**/

void
queue_insert_tasks (const GSList * tasks, int index)
{
	const GSList * tmp;
	GSList * insertme;
	debug_enter ("queue_insert_tasks");
	debug1 (DEBUG_QUEUE, "queue_insert_tasks: inserting at position %d", index);

	/* sanity clause */
	g_return_if_fail (tasks!=NULL);
	g_return_if_fail (index==-1 || (guint)index<=g_slist_length(task_list));

	/* make our own GSList nodes for inserting */
	insertme = g_slist_copy ((GSList*)tasks);

	/**
	***  Add to the list
	**/
	if (task_list == NULL) /* no previous list */
		task_list = insertme;
	else if (index == 0)
		task_list = g_slist_concat (insertme, task_list);
	else if (index == -1)
		task_list = g_slist_concat (task_list, insertme);
	else {
		GSList * nth = g_slist_nth (task_list, index);
		if (nth == NULL) { /* index out of range, append to end */
			index = g_slist_length (task_list);
			nth = g_slist_last (task_list);
		}
		g_slist_last (insertme)->next = nth->next;
		nth->next = insertme;
	}

	/**
	***  Mark the new tasks as queued
	**/
	for (tmp=tasks; tmp!=NULL; tmp=tmp->next) {
		Task * task = TASK(tmp->data);
		queue_add_message_ids (task);
		g_hash_table_insert (task_to_status, task, GINT_TO_POINTER(QUEUE_TASK_STATUS_QUEUED));
	}

	/* we need this for fire_queue_size_changed */
	_dirty = TRUE;

	/* cleanup */
	fire_queue_size_changed (running_tasks, g_slist_length(task_list));
	fire_tasks_added (tasks, index);
	queue_wakeup ();
	debug_exit ("queue_insert_tasks");
}

/**
***
**/

void
queue_remove_last_task (void)
{
	GSList * last = g_slist_last (task_list);
	if (last != NULL)
	{
		Task * task = TASK(last->data);
		GSList * l = g_slist_append (NULL, task);
		queue_remove_tasks (l);
		g_slist_free (l);
        }
}

void
queue_remove_tasks (const GSList * tasks)
{
	const GSList * l;
	GSList * removed = NULL;
	debug_enter ("queue_remove_tasks");

	for (l=tasks; l!=NULL; l=l->next)
	{
		Task * task = TASK(l->data);
		const gboolean is_in_task_list = g_slist_find (task_list, task) != NULL;

		if (is_in_task_list)
		{
			const gboolean is_running = task->thread_qty > 0;

			if (!is_running) /* we can remove it straight away */
			{
				task_list = g_slist_remove (task_list, task);
				removed = g_slist_prepend (removed, task);
				_dirty = TRUE;
			}
			else /* try to abort it before removing.
                                'abort' should cause queue_task_work() to fail,
			        then desc_task_work() will call
				queue_remove_tasks() with the aborted task. */
			{
				task->abort_flag = TRUE;
				queue_set_task_status (task, QUEUE_TASK_STATUS_REMOVING);
			}
		}
	}

	if (removed != NULL)
	{
		removed = g_slist_reverse (removed);
		fire_queue_size_changed (running_tasks,
		                         g_slist_length(task_list));
		fire_tasks_removed (removed);

		for (l=removed; l!=NULL; l=l->next)
		{
			Task * task = TASK(l->data);

			pan_callback_call (task->task_ran_callback,
			                   task,
			                   GINT_TO_POINTER(task->state.health));
			g_hash_table_remove (task_to_status, task);
			queue_remove_message_ids (task);
			pan_object_unref (PAN_OBJECT(task));
		}

		g_slist_free (removed);
	}

	debug_exit ("queue_remove_tasks");
}

/**
***
**/

void
queue_move_tasks (const GSList * tasks, int moveto_index)
{
	GSList * l;
	GSList * ref;

	/* sanity clause */
	g_return_if_fail (tasks!=NULL);
	g_return_if_fail (task_list!=NULL);

	/* find the point of reference */
	if (moveto_index == 0)
		ref = NULL;
	else {
		ref = g_slist_nth (task_list, moveto_index-1);
		while (ref != NULL) {
			if (g_slist_find ((GSList*)tasks, ref->data) == NULL)
				break;
			ref = ref->next;
		}
		if (ref == NULL)
			ref = g_slist_last (task_list);
	}

	/* remove the tasks */
	for (l=(GSList*)tasks; l!=NULL; l=l->next)
		task_list = g_slist_remove (task_list, l->data);

	/* add them back in */
	l = g_slist_copy ((GSList*)tasks);
	if (ref == NULL) {
		g_slist_last(l)->next = task_list;
		task_list = l;
		moveto_index = 0;
	} else {
		if (task_list != NULL) {
			g_slist_last(l)->next = ref->next;
			ref->next = l;
		} else
			task_list = l;	
		moveto_index = g_slist_index (task_list, tasks->data);
	}

	/* let everyone know */
	_dirty = TRUE;
	fire_tasks_moved (tasks, moveto_index);
	debug_exit ("queue_move_tasks");
}

/**
***
**/

void
queue_requeue_failed_tasks (const GSList * tasks)
{
	const GSList * l;

	for (l=tasks; l!=NULL; l=l->next)
	{
		Task * task = TASK(l->data);
		if (queue_get_task_status(task) == QUEUE_TASK_STATUS_STOPPED)
		{
			task->abort_flag = FALSE;
			task_state_set_health (&task->state, TASK_OK);
			queue_set_task_status (task, QUEUE_TASK_STATUS_QUEUED);
		}
	}

	queue_wakeup ();
}

/**
***
**/

gboolean
queue_get_remove_failed_tasks (void)
{
	return _remove_failed_tasks;
}

void
queue_set_remove_failed_tasks (gboolean b)
{
	_remove_failed_tasks = b;
}

/**
***
**/

QueueTaskStatus
queue_get_task_status (const Task* task)
{
	QueueTaskStatus status = QUEUE_TASK_STATUS_NOT_QUEUED;

	if (task_to_status != NULL) {
		gpointer p = g_hash_table_lookup (task_to_status, task);
		if (p != NULL)
			status = GPOINTER_TO_INT (p);
	}

	return status;
}

static void
queue_set_task_status (Task* task, QueueTaskStatus status)
{
	g_return_if_fail (task!=NULL);
	g_return_if_fail (g_slist_index(task_list, task) != -1);

	if (task_to_status != NULL)
		g_hash_table_insert (task_to_status,
		                     task,
		                     GINT_TO_POINTER(status));
}

/************
*************  PRIVATE ROUTINES
************/

/***
****
***/

static void
increment_running_task_qty (int inc)
{
	running_tasks += inc;
	fire_queue_size_changed (running_tasks, g_slist_length(task_list));
}

static void
inc_task_work (Task * task)
{
	if (++task->thread_qty == 1)
	{
		/* This is mostly for the benefit of the GUI */
		queue_set_task_status (task, QUEUE_TASK_STATUS_RUNNING);
		status_item_set_active (STATUS_ITEM(task), TRUE);
		increment_running_task_qty (1);
	}
}

static void
dec_task_work (Task * task)
{
	if (!--task->thread_qty)
	{
		GSList * l;
		const QueueTaskStatus qts = queue_get_task_status (task);

		/* let the GUI know what's going on */
		status_item_set_active (STATUS_ITEM(task), FALSE);
		increment_running_task_qty (-1);

		/* if the task is slated for removal or stopping,
		 * the queue is waiting for the task's threads to finish. */
		switch (qts) {
			case QUEUE_TASK_STATUS_REMOVING:
				l = g_slist_append (NULL, task);
				queue_remove_tasks (l);
				g_slist_free (l);
				break;
			case QUEUE_TASK_STATUS_STOPPING:
				l = g_slist_append (NULL, task);
				queue_stop_tasks (l);
				g_slist_free (l);
				break;
			default:
				queue_set_task_status (
					task, QUEUE_TASK_STATUS_QUEUED);
				break;
		}
	}
}

/***
****
***/

static void
queue_task_work (Task * task)
{
	inc_task_work (task);
	(*task->state.work_func)(task);
	dec_task_work (task);

	queue_wakeup ();
}

/***
****
***/

static void
queue_task_checkin_channel (Task         * task,
                            GIOChannel   * channel,
                            gboolean       channel_ok,
                            gpointer       user_data)
{
	dec_task_work (task);
	socket_pool_checkin (channel, channel_ok);
	queue_wakeup ();
}

static void
queue_task_give_socket (Task                       * task,
                        GIOChannel                 * channel)
{
	inc_task_work (task);

	task_give_socket (task,
	                  channel,
	                  queue_task_checkin_channel,
	                  NULL);
}

/***
****
***/

static void
queue_run_tasks_foreach (gpointer task_gpointer, gpointer user_data)
{
	Task * task = TASK(task_gpointer);
	GSList ** cull = (GSList**) user_data;
	TaskState state = task->state;
	QueueTaskStatus qts = queue_get_task_status (task); 
	char desc[512];

	status_item_describe (STATUS_ITEM(task), desc, sizeof(desc));
	debug4 (DEBUG_QUEUE,
		"tasks_foreach: task %p (%s) task-state %d queue-state %d",
		task, desc, (int)state.health, (int)qts);

	switch (state.health)
	{
		case TASK_FAIL:
		{
			if (qts != QUEUE_TASK_STATUS_STOPPED)
			{
				if (_remove_failed_tasks)
					*cull = g_slist_prepend (*cull, task);
				else
					queue_set_task_status (
					      task, QUEUE_TASK_STATUS_STOPPED);
			}
			break;
		}
		case TASK_FAIL_NETWORK:
		{
			/* for now, just retry.
			   maybe at some point in the future
			   we can have the task sleep a bit first.
			   note the intentional fall-through. */
			task_state_set_health (&task->state, TASK_OK);
		}
		case TASK_OK:
		{
			const gboolean runnable =
				qts==QUEUE_TASK_STATUS_QUEUED ||
				qts==QUEUE_TASK_STATUS_RUNNING;

			switch (state.work)
			{
				case TASK_NEED_SOCKET:
					if (runnable) {
						GIOChannel * channel;
						SocketPool * pool = get_socket_pool_from_server (state.server);
						socket_pool_request_connection (pool);
						channel = socket_pool_checkout (pool);
						if (channel != NULL)
							queue_task_give_socket (task, channel);
					}
					break;

				case TASK_NEED_WORK:
				case TASK_NEED_WORK_SUBTHREAD:
					if (runnable)
						queue_task_work (task);
					break;

				case TASK_WORKING:
					/* no-op; the task is busy */
					break;

				case TASK_COMPLETED:
					*cull = g_slist_prepend (*cull, task);
					break;

				default:
					g_warning ("unhandled state");
					break;
			}
		}
	}
}

char*
queue_get_tasks_filename (void)
{
	return g_build_filename (get_data_dir(), "tasks.xml", NULL);
}

static guint queue_quick_timer_id = 0;

static gboolean
queue_upkeep (gpointer do_loop)
{
	/* try to process the queue */
	GSList * cull = NULL;
	g_slist_foreach (task_list, queue_run_tasks_foreach, &cull);
	if (cull != NULL) {
		queue_remove_tasks (cull);
		g_slist_free (cull);
	}

	/* maybe backup the tasks */
	if (_dirty)
	{
		char * filename = queue_get_tasks_filename ();
		queue_forall_tasks (queue_save_tasks_forall, filename, TRUE);
		g_free (filename);

		_dirty = FALSE;
	}

	/* maybe invoke again in awhile */
	queue_quick_timer_id = 0u;
	return do_loop != NULL;
}

void
queue_wakeup (void)
{
	if (queue_quick_timer_id == 0u)
		queue_quick_timer_id = g_idle_add (queue_upkeep, NULL);
}

/***
****  Callbacks
***/

PanCallback*
queue_get_online_status_changed_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb==NULL) cb = pan_callback_new ();
	return cb;
}

static void
fire_online_status_changed (gboolean online)
{
	pan_callback_call (queue_get_online_status_changed_callback(),
	                   GINT_TO_POINTER(online), NULL);
}

PanCallback*
queue_get_message_id_status_changed (void)
{
	static PanCallback * cb = NULL;
	if (cb==NULL) cb = pan_callback_new ();
	return cb;
}

static void
fire_message_id_status_changed (const PString     ** message_ids,
                                int                  message_id_qty)
{
	pan_callback_call (queue_get_message_id_status_changed(),
	                   message_ids,
	                   GINT_TO_POINTER(message_id_qty));
}

PanCallback*
queue_get_connection_size_changed_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb==NULL) cb = pan_callback_new ();
	return cb;
}

static void
fire_connection_size_changed (int increment)
{
	pan_callback_call (queue_get_connection_size_changed_callback(),
	                   GINT_TO_POINTER(increment),
	                   NULL);
}

PanCallback*
queue_get_size_changed_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb==NULL) cb = pan_callback_new ();
	return cb;
}

static void
fire_queue_size_changed (int running_qty, int total_qty)
{
	pan_callback_call (queue_get_size_changed_callback(),
	                   GINT_TO_POINTER(running_qty),
	                   GINT_TO_POINTER(total_qty));
}

PanCallback*
queue_get_tasks_added_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb==NULL) cb = pan_callback_new ();
	return cb;
}

static void
fire_tasks_added (const GSList * tasks, int index)
{
	pan_callback_call (queue_get_tasks_added_callback (),
	                   (GSList*)tasks,
	                   GINT_TO_POINTER(index));
}

PanCallback*
queue_get_tasks_removed_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb==NULL) cb = pan_callback_new ();
	return cb;
}

static void
fire_tasks_removed (const GSList * tasks)
{
	pan_callback_call (queue_get_tasks_removed_callback (),
	                   (GSList*)tasks,
	                   NULL);
}

PanCallback*
queue_get_tasks_moved_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb==NULL) cb = pan_callback_new ();
	return cb;
}

static void
fire_tasks_moved (const GSList * tasks, int moveto_index)
{
	pan_callback_call (queue_get_tasks_moved_callback (),
	                   (GSList*)tasks,
	                   GINT_TO_POINTER(moveto_index));
}
