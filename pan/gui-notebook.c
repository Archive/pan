/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/debug.h>
#include <pan/base/pan-i18n.h>

#include <pan/articlelist.h>
#include <pan/globals.h>
#include <pan/grouplist.h>
#include <pan/gui.h>
#include <pan/gui-notebook.h>
#include <pan/text.h>
#include <pan/util.h>


static GtkWidget * notebook = NULL;

/*---[ gui_notebook_construct ]---------------------------------------
 * Build and pack the notebook GUI layout into Pan's main vbox
 *--------------------------------------------------------------------*/
void
gui_notebook_construct (void)
{
	debug_enter ("gui_notebook_construct");

	if (Pan.viewmode != GUI_NOTEBOOK)
	{
		Pan.viewmode = GUI_NOTEBOOK;

		gui_layout_clear_workarea ();

		/* build a new UI */
		notebook = gtk_notebook_new ();
		gtk_notebook_append_page (GTK_NOTEBOOK (notebook), groups_vbox, gtk_label_new (_("Groups")));
		gtk_notebook_append_page (GTK_NOTEBOOK (notebook), articlelist_ctree, gtk_label_new (_("Headers")));
		gtk_notebook_append_page (GTK_NOTEBOOK (notebook), text_box, gtk_label_new (_("Body")));
		gtk_notebook_set_tab_border (GTK_NOTEBOOK (notebook), 2);
		gui_restore_column_widths (Pan.group_tree, "group");
		gtk_box_pack_start (GTK_BOX(Pan.workarea), notebook, TRUE, TRUE, 0);
		gtk_widget_show_all (GTK_WIDGET(Pan.workarea));
	}

	debug_exit ("gui_notebook_construct");
}

void
gui_notebook_page_set_nolock (int page, GtkWidget * focus_item)
{
	GtkNotebook * nb = GTK_NOTEBOOK(notebook);
	const int cur_page_num = gtk_notebook_get_current_page (nb);
	if (page != cur_page_num)
	{
		gtk_notebook_set_page (nb, page);
		gtk_widget_grab_focus (focus_item);
	}
}

int
gui_notebook_get_current_pane_nolock (void)
{
	return gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook));
}

