/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GLOBALS_H_
#define __GLOBALS_H_

#include <gtk/gtkwidget.h>
#include <pan/base/article.h>

#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 255
#endif

#ifndef PATH_MAX
#define PATH_MAX 255
#endif

typedef struct
{
	GtkWidget     * window;	        /* main window for Pan */
	GtkWidget     * workarea;	/* workarea */
	GtkWidget     * group_tree;	/* ??? */
	GtkWidget     * article_ctree;	/* ??? */
	GtkWidget     * widget;		/* ??? */
	GtkWidget     * text;		/* ??? */
	int             viewmode;
	gboolean        mute;		/* if true, outgoing
					   posts/mail are sent to
					   stdout instead of actually
					   being sent.  This is a
					   debugging tool. */
}
PanApp;

extern PanApp Pan;


#endif /* __PAN_H__ */
