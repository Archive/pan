/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include <glib.h>

#include <gmime/gmime-stream-file.h>

#include <pan/base/acache.h>
#include <pan/base/article.h>
#include <pan/base/base-prefs.h>
#include <pan/base/debug.h>
#include <pan/base/decode.h>
#include <pan/base/log.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/util-file.h>

#include <pan/globals.h>
#include <pan/nntp.h>
#include <pan/queue.h>
#include <pan/task-save.h>

/*********************
**********************  Defines / Enumerated types
*********************/

#define DEAD_POINTER ((void*)(0xDEADBEEF))

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

static void task_save_decode (Task * task);

static void task_save_describe (const StatusItem*, char*, int);

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

static void
task_save_describe (const StatusItem* item, char * buf, int buflen)
{
	const MessageIdentifier * mid;

	g_return_if_fail (item!=NULL);
	g_return_if_fail (TASK(item)->identifiers!=NULL);
	g_return_if_fail (TASK(item)->identifiers->len >= 1u);

       	mid = MESSAGE_IDENTIFIER(g_ptr_array_index (TASK(item)->identifiers, 0));
        g_snprintf (buf, buflen, _("Saving \"%s\""), message_identifier_get_readable_name(mid));
}

/************
*************  PUBLIC ROUTINES
************/

/*****
******
*****/

char*
expand_download_dir (const char * dir, const char * groupname)
{
	char * retval = g_strdup (dir);

	if (is_nonempty_string(groupname))
	{
		if (strstr (dir, "%g") != NULL)
		{
			replace_gstr (&retval, pan_substitute (retval, "%g", groupname));
		}

		if (strstr (dir, "%G") != NULL)
		{
			char tmp[PATH_MAX], *pch;
			g_snprintf (tmp, sizeof(tmp), "%s.", groupname);
			for (pch=tmp; *pch; ++pch)
				if (*pch == '.')
					*pch = G_DIR_SEPARATOR;
			replace_gstr (&retval, pan_substitute (retval, "%G", tmp));
		}
	}

	return retval;
}

/*****
******
*****/

static void
task_save_destructor (PanObject* object)
{
	TaskSave * save = TASK_SAVE(object);
	debug_enter ("task_save_destructor");

	/* destruct the TaskSave parts */
        debug1 (DEBUG_PAN_OBJECT, "task_save destructor: %p", object);
	replace_gstr (&save->path_attachments, DEAD_POINTER);
	replace_gstr (&save->filename_attachments, DEAD_POINTER);
	replace_gstr (&save->path_bodies, DEAD_POINTER);
	replace_gstr (&save->filename_bodies, DEAD_POINTER);
	save->group_quark = ~0;
        save->save_attachments = ~0;
        save->save_bodies = ~0;

	/* destruct the superclass */
	task_bodies_destructor (object);
	debug_exit ("task_save_destructor");
}

PanObject*
task_save_new (Server              * server,
               MessageIdentifier  ** mids,
               int                   mid_qty)
{
	TaskSave * save = g_new0 (TaskSave, 1);

	/* initialize the parent class bits */
	task_bodies_constructor (TASK(save),
	                         TASK_TYPE_SAVE,
	                         task_save_destructor,
	                         task_save_describe,
	                         server,
	                         FALSE,
	                         mids,
	                         mid_qty,
	                         task_save_decode);

	save->group_quark = mids[0]->sources.sources[0].group_quark;

	status_item_emit_status_va (STATUS_ITEM(save),
	                            _("\"%s\""),
	                            message_identifier_get_readable_name (mids[0]));

	return PAN_OBJECT(save);
}


void
task_save_weed_duplicates (GPtrArray * parts)
{
	int i;

	for (i=0; i<parts->len; ++i)
	{
		AttachmentPart * prev = i==0 ? NULL : (AttachmentPart*) g_ptr_array_index (parts, i-1);
		AttachmentPart * part = (AttachmentPart*) g_ptr_array_index (parts, i);

		if (part->part_num == 0)
		{
			g_ptr_array_remove (parts, part);
		}
		else if (prev && prev->part_num == part->part_num)
		{
			gpointer removeme = NULL;
			/* Select the better of the two assuming that more lines
			 * is better.  Otherwise go for the most recently posted,
			 * as it's possibly a repost to fix a broken post. */

			if (prev->line_qty > part->line_qty)
				removeme = part;
			else if (prev->line_qty < part->line_qty)
				removeme = prev;
			else if (prev->date > part->date)
				removeme = part;
			else
				removeme = prev;

			if (removeme) {
				g_ptr_array_remove (parts, removeme);
				--i;
			}
		}
	}
}

PanObject*
task_save_new_from_article  (const Article  * article)
{
	int i;
	GSList * l;
	GPtrArray * weed;
	MessageIdentifier ** mids;
	PanObject * retval;
	debug_enter ("task_save_new_from_article");

	/* sanity clause */
	g_return_val_if_fail (article_is_valid(article), NULL);
	g_return_val_if_fail (article->attachments!=NULL, NULL);

	/* weed out dupliate parts */
	weed = g_ptr_array_new ();
	for (l=article->attachments; l!=NULL; l=l->next)
		g_ptr_array_add (weed, l->data);
	task_save_weed_duplicates (weed);

	/* build the weeded mids */
	mids = g_newa (MessageIdentifier*, weed->len);
	for (i=0; i<weed->len; ++i) {
		const AttachmentPart * part = (const AttachmentPart*) g_ptr_array_index (weed, i);
		mids[i] = message_identifier_new_from_part (part, article->subject.str);
	}

	/* create the task from weeded message identifiers */
	retval = task_save_new (article->group->server, mids, weed->len);
	TASK_SAVE(retval)->group_quark = article->group->name_quark;

	/* cleanup */
	for (i=0; i<weed->len; ++i)
		g_object_unref (mids[i]);
	g_ptr_array_free (weed, TRUE);
	debug_exit ("task_save_new_from_article");
	return retval;
}

extern char*
expand_download_dir (const char * dir, const char * groupname);

void
task_save_set_attachments (TaskSave      * task,
                           const char    * path,
                           const char    * filename)
{
	MessageIdentifier * mid;
	debug_enter ("task_save_set_attachments");

	/* sanity clause */
	g_return_if_fail (task!=NULL);

	task->save_attachments = TRUE;
	replace_gstr (&task->path_attachments, g_strdup(path));
	replace_gstr (&task->filename_attachments, g_strdup(filename));

	mid = MESSAGE_IDENTIFIER (g_ptr_array_index (TASK(task)->identifiers, 0));

	/* if no path specified, fall back on Pan group and global defaults */
	if (!task->path_attachments) {
		Group * group = server_get_group (TASK(task)->server, task->group_quark);
		if (group != NULL)
			task->path_attachments = g_strdup (group->download_dir);
	}
	if (!task->path_attachments)
		task->path_attachments = g_strdup (download_dir);

	/* expand */
	replace_gstr (&task->path_attachments,
	              expand_download_dir (task->path_attachments, g_quark_to_string(task->group_quark)));

	debug_exit ("task_save_set_attachments");
}
 
void
task_save_set_bodies (TaskSave      * task,
                      const char    * path,
                      const char    * filename)
{
	MessageIdentifier * mid;
	debug_enter ("task_save_set_bodies");

	g_return_if_fail (task!=NULL);

	task->save_bodies = TRUE;
	replace_gstr (&task->path_bodies, g_strdup(path));
	replace_gstr (&task->filename_bodies, g_strdup(filename));

	mid = MESSAGE_IDENTIFIER (g_ptr_array_index (TASK(task)->identifiers, 0));

	/* if no path specified, fall back on Pan group and global defaults */
	if (!task->path_bodies) {
		Group * group = server_get_group (TASK(task)->server, task->group_quark);
		if (group != NULL)
			task->path_bodies = g_strdup (group->download_dir);
	}
	if (!task->path_bodies)
		task->path_bodies = g_strdup (download_dir);

	/* expand */
	replace_gstr (&task->path_bodies,
	              expand_download_dir (task->path_bodies, g_quark_to_string(task->group_quark)));

	debug_exit ("task_save_set_bodies");
}

/*****
******
*****/

static char*
get_unique_fname (const char * path, const char * fname)
{
	/* create the unchanged filename */
	if (!is_nonempty_string (fname))
		fname = _("UNKNOWN");
	
	return pan_file_get_unique_fname(path,fname);

}

/**
***
**/

static void
task_save_decode (Task * task)
{
	TaskSave * task_save;
	TaskStateEnum retval = TASK_OK;

       	task_save = TASK_SAVE(task);
	if (task_save->save_attachments)
	{
		gboolean decode_ok;
		decode_data dd;
		status_item_emit_status (STATUS_ITEM(task), _("Saving Attachments"));

		dd.server = task->server;
		dd.group_quark = task_save->group_quark;
		dd.acache_key = ACACHE_DEFAULT_KEY;
		dd.mids = (MessageIdentifier**) task->identifiers->pdata;
		dd.mid_qty = task->identifiers->len;
		dd.subject = message_identifier_get_readable_name (dd.mids[0]);
		dd.item = STATUS_ITEM(task);
		dd.path = task_save->path_attachments;
		dd.filename = task_save->filename_attachments;
		decode_ok = decode_article (&dd);

		/* if we failed to decode something that we think
		 * we should've been able to decode, then fail. */
		if (!decode_ok)
			retval = TASK_FAIL;
	}

	if (task_save->save_bodies)
	{
		int i;
		gboolean bodies_ok = TRUE;
		status_item_emit_status (STATUS_ITEM(task), _("Saving Articles"));

		for (i=0; i<task->identifiers->len; ++i)
		{
			gboolean body_ok = TRUE;
			MessageIdentifier * mid;
			GMimeMessage * message;
			const PString * id;
			char * fname;
			char * path;

			/* get the message */
			mid = (MessageIdentifier*) g_ptr_array_index (task->identifiers, i);
			id = &mid->message_id;
			message = acache_get_message (ACACHE_DEFAULT_KEY, &id, 1);

			/* get the filename */
			fname = g_strdup (task_save->filename_bodies);
			if (!is_nonempty_string(fname))
				replace_gstr (&fname, g_strdup_printf ("%s.txt", message_identifier_get_readable_name(mid)));
			replace_gstr (&fname, get_unique_fname (task_save->path_bodies, fname));

			/* get the directory */
			path = g_path_get_dirname (fname);
			if (!pan_file_ensure_path_exists (path)) {
				log_add_va (LOG_ERROR, _("Save Article can't access path \"%s\""), path);
				body_ok = FALSE;
			}

			/* open the file for writing */
			if (body_ok) {
				FILE * fp;
				errno = 0;
				fp = pan_fopen (fname, "w+");
				if (fp == NULL) {
					log_add_va (LOG_ERROR, _("Can't create file \"%s\" %s"), fname, g_strerror(errno));
					body_ok = FALSE;
				} else {
					GMimeStream * stream = g_mime_stream_file_new (fp);
					g_mime_message_write_to_stream (message, stream);
					g_object_unref (stream);
					log_add_va (LOG_INFO, _("Saved article body to \"%s\""), fname);
				}
			}

			/* cleanup */
			g_object_unref (message);
			g_free (path);
			g_free (fname);

			if (!body_ok)
				bodies_ok = FALSE;
		}

		if (!bodies_ok)
			retval = TASK_FAIL;
	}

	task_state_set_health (&task->state, retval);

	if (retval == TASK_OK)
		task_state_set_work_completed (&task->state);
	else
		task_state_set_work_need_work (&task->state, task_save_decode);
}

