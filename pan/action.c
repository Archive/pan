/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkdialog.h>

#include <pan/base/pan-glib-extensions.h>
#include <pan/action.h>
#include <pan/articlelist.h>
#include <pan/article-actions.h>
#include <pan/grouplist.h>
#include <pan/globals.h>
#include <pan/group-action.h>
#include <pan/message-window.h>
#include <pan/prefs.h>
#include <pan/queue.h>
#include <pan/score-add-ui.h>
#include <pan/score-view-ui.h>
#include <pan/text.h>
#include <pan/util.h>
#include <pan/filters/score.h>

/**
***
**/

static void
popup_view_score_dialog (Article * article, gpointer user_data)
{
	GtkWidget * w = score_view_dialog (Pan.window, article);
	gtk_widget_show_all (w);
}

static void
popup_add_score_dialog (Article * article, gpointer user_data)
{
	const ScoreAddMode dialog_mode = (ScoreAddMode) GPOINTER_TO_INT (user_data);
	GtkWidget * w = score_add_dialog (Pan.window, article, dialog_mode);
	gtk_widget_show_all (w);
}

/**
***
**/

static gboolean
edit_scorefile_done (gpointer unused)
{
	pan_action_do (ACTION_SCORE_RESCORE);
	return FALSE;
}
static void
edit_scorefile (void)
{
	launch_external_editor (score_editor_command,
	                        score_get_main_scorefile_filename(), 0,
	                        edit_scorefile_done, NULL);
}

/**
***
**/

void
pan_action_do (PanAction action)
{
	switch (action)
	{
		case ACTION_COMPOSE_NEW:
			message_post_window ();
			break;

		case ACTION_COMPOSE_FOLLOWUP:
			message_followup_window ();
			break;

		case ACTION_GET_NEW_HEADERS_FOR_SUBSCRIBED:
			group_action_subscribed_download_new ();
			break;

		case ACTION_GET_NEW_HEADERS_FOR_SELECTED:
			group_action_selected_download_new ();
			break;

		case ACTION_GET_BODIES_FOR_SELECTED:
			group_action_selected_download_new_and_bodies ();
			break;

		case ACTION_READ_MORE:
			text_read_more ();
			break;

		case ACTION_READ_LESS:
			text_read_less ();
			break;

		case ACTION_EDIT_SCOREFILE:
			edit_scorefile ();
			break;

		case ACTION_READ_NEXT_SCORE_ARTICLE:
			header_pane_read_next_score ();
			break;

		case ACTION_READ_NEXT_UNREAD_ARTICLE:
			header_pane_read_next_unread ();
			break;

		case ACTION_READ_NEXT_UNREAD_THREAD:
			header_pane_read_next_unread_thread ();
			break;

		case ACTION_READ_NEXT_NEW_ARTICLE:
			header_pane_read_next_new ();
			break;

		case ACTION_READ_NEXT_NEW_THREAD:
			header_pane_read_next_new_thread ();
			break;

		case ACTION_READ_NEXT_UNREAD_GROUP:
			grouplist_activate_next_unread_group ();
			break;

		case ACTION_ARTICLE_DELETE:
			article_action_delete_selected_articles ();
			break;

		case ACTION_SAVE:
			article_action_selected_save_attachments ();
			break;

		case ACTION_SAVE_AS:
			article_action_selected_save_as ();
			break;

		case ACTION_MANUAL_DECODE:
			article_action_selected_manual_decode ();
			break;

		case ACTION_NET_ONLINE:
			break;

		case ACTION_NET_CANCEL_LAST_TASK:
			queue_remove_last_task ();
			break;

		case ACTION_SCORE_VIEW:
			header_pane_first_selected (popup_view_score_dialog, NULL);
			break;

		case ACTION_ADD_TO_SCOREFILE:
			header_pane_first_selected (popup_add_score_dialog, GINT_TO_POINTER(SCORE_ADD));
			break;

		case ACTION_SCORE_PLONK:
			header_pane_first_selected (popup_add_score_dialog, GINT_TO_POINTER(SCORE_ADD_PLONK));
			break;

		case ACTION_SCORE_RESCORE:
			score_invalidate (TRUE);
			break;

		case ACTION_SCORE_WATCH_THREAD:
			article_action_watch_selected_threads ();
			break;

		case ACTION_SCORE_IGNORE_THREAD:
			article_action_ignore_selected_threads ();
			break;

		default:
			pan_warn_if_reached ();
			break;
	}
}
