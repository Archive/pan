/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PAN_FONT_BUTTON_H
#define PAN_FONT_BUTTON_H

#include <gtk/gtkwidget.h>

/* if user has gtk 2.6 or higher, use GtkFontButton */
#if GTK_CHECK_VERSION(2,6,0)
#include <gtk/gtkfontbutton.h>
#define    pan_font_button_new()              gtk_font_button_new()
#define    pan_font_button_set_font_name(w,n) gtk_font_button_set_font_name(GTK_FONT_BUTTON(w), n)
#define    pan_font_button_get_font_name(w)   gtk_font_button_get_font_name(GTK_FONT_BUTTON(w))
#else
GtkWidget*   pan_font_button_new (void);
void         pan_font_button_set_font_name (GtkWidget * w, const char*);
const char*  pan_font_button_get_font_name (GtkWidget * w);
#endif /* GTK_CHECK_VERSION */

#endif /* PAN_FONT_BUTTON */
