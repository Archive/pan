/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/argset.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/log.h>

#include <pan/message-check-ui.h>
#include <pan/globals.h>
#include <pan/util.h>

#include <pan/xpm/pan-pixbufs.h>

typedef struct
{
	GtkWidget * window;

	Server * server;
	GMimeMessage * message;

	GoodnessLevel goodness;
	GPtrArray * errors;

	MessageCheckedFunc checked_func;
	gpointer checked_func_user_data;
}
CheckedCallbackStruct;

static void
warning_response_cb (GtkDialog * dialog, int response, gpointer user_data)
{
	CheckedCallbackStruct * cbs = (CheckedCallbackStruct*) user_data;

	/* if user hit `yes, go ahead' then change the goodness from WARN to OKAY */
	if (response==GTK_RESPONSE_YES)
		cbs->goodness = OKAY;

	/* let the user know that it's changed */
	(cbs->checked_func)(cbs->server, cbs->message, cbs->goodness, cbs->checked_func_user_data);

	/* cleanup */
	gtk_widget_destroy (GTK_WIDGET(dialog));
	g_object_unref (cbs->message);
	pan_g_ptr_array_foreach (cbs->errors, (GFunc)g_free, NULL);
	g_ptr_array_free (cbs->errors, TRUE);
	g_free (cbs);
}

static void
post_warning_cb (CheckedCallbackStruct * cbs)
{
	guint i;
	GString * str;
	GtkWidget * w;
	GtkWidget * button;
	GtkMessageType message_type;

	/* build the error message */
	str = g_string_new (NULL);
	for (i=0; i!=cbs->errors->len; ++i) {
		char * e = (char*)g_ptr_array_index(cbs->errors,i);
		g_string_append (str, e);
		g_string_append_c (str, '\n');
	}
	if (str->len) /* remove the last linefeed */
		g_string_truncate (str, str->len-1);

	if (cbs->goodness == WARN)
	{
		/* make sure the user wants to proceed. */
		const gchar *ques = cbs->errors->len > 1
			? _("Post anyway, despite these problems?")
			: _("Post anyway, despite this problem?");
		g_string_append (str, "\n");
		g_string_append (str, ques);
		message_type = GTK_MESSAGE_QUESTION;
	}
	else
	{
		/* I'm not asking you, I'm _telling_ you. ;) */
		message_type = GTK_MESSAGE_ERROR;
	}

	/* create a warning dialog... */
	w = gtk_message_dialog_new (GTK_WINDOW(cbs->window),
	                            GTK_DIALOG_DESTROY_WITH_PARENT,
	                            message_type,
	                            GTK_BUTTONS_NONE,
	                            "%s", str->str);
	g_signal_connect (w, "response", G_CALLBACK(warning_response_cb), cbs);
	gtk_widget_show_all (w);

	button = gtk_dialog_add_button (GTK_DIALOG(w), GTK_STOCK_CANCEL, GTK_RESPONSE_NO);
	gtk_dialog_set_default_response (GTK_DIALOG(w), GTK_RESPONSE_NO);
	gtk_widget_grab_focus (button);

	if (message_type == GTK_MESSAGE_QUESTION)
	{
		GtkWidget *label, *image, *button, *hbox, *align;

		button = gtk_button_new ();
		label = gtk_label_new_with_mnemonic (_("_Post"));
		gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));
		image = pan_gtk_image_new_from_inline_text (icon_compose_post, GTK_ICON_SIZE_BUTTON);
		hbox = gtk_hbox_new (FALSE, GUI_PAD);
		align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
		gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
		gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		gtk_container_add (GTK_CONTAINER (button), align);
		gtk_container_add (GTK_CONTAINER (align), hbox);
		gtk_widget_show_all (button);
		gtk_dialog_add_action_widget (GTK_DIALOG(w), button, GTK_RESPONSE_YES);
	}

	/* cleanup */
	g_string_free (str, TRUE);
}

void
message_check_and_prompt (GtkWidget           * window,
                          Server              * server,
                          GMimeMessage        * message,
                          MessageCheckedFunc    checked_func,
                          gpointer              checked_func_user_data)
{
	GoodnessLevel goodness;
	GPtrArray * errors;

	/* sanity clause */
	g_return_if_fail (GTK_IS_WINDOW(window));
	g_return_if_fail (server_is_valid (server));
	g_return_if_fail (GMIME_IS_MESSAGE(message));

	/* check the message */
	goodness = OKAY;
       	errors = g_ptr_array_new ();
	message_check (message, server, errors, &goodness);

	if (goodness == OKAY)
	{
		(*checked_func)(server, message, goodness, checked_func_user_data);
		g_ptr_array_free (errors, TRUE);
	}
	else
	{
		CheckedCallbackStruct * cbs;
		cbs = g_new0 (CheckedCallbackStruct, 1);
		cbs->window = window;
		cbs->server = server;
		cbs->message = message;
		cbs->goodness = goodness;
		cbs->errors = errors;
		cbs->checked_func = checked_func;
		cbs->checked_func_user_data = checked_func_user_data;
		g_object_ref (message);
		post_warning_cb (cbs);
	}
}
