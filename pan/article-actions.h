/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __ARTICLE_ACTIONS_H__
#define __ARTICLE_ACTIONS_H__

#include <gmime/gmime-message.h>
#include <pan/base/article.h>
#include <pan/base/group.h>

void      article_cancel                             (Server          * server,
                                                      GMimeMessage    * message);

void      article_supersede                          (GMimeMessage    * message);

gboolean  article_copy_articles_to_folder            (Group           * folder,
                                                      const Article  ** articles,
                                                      gint              qty);

void      article_action_selected_save_attachments   (void);

void      article_action_selected_save_as            (void);

void      article_action_selected_manual_decode      (void);

void      article_action_delete_articles             (Article        ** articles,
                                                      guint             article_qty,
                                                      gpointer          user_data);

void      article_action_delete_selected_articles    (void);

void      article_action_edit_selected               (void);

void      article_action_watch_thread                (const Article   * article);

void      article_action_watch_selected_threads      (void);

void      article_action_ignore_thread               (const Article   * article);

void      article_action_ignore_selected_threads     (void);

void      article_action_mark_selected_read          (void);

void      article_action_mark_selected_unread        (void);

void      article_action_download_selected           (void);

void      article_action_cancel_selected             (void);

void      article_action_supersede_selected          (void);


#endif
