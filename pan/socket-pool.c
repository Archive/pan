/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2004  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <stdio.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/log.h>

#include <pan/nntp.h>
#include <pan/queue.h>
#include <pan/sockets.h>
#include <pan/socket-pool.h>
#include <pan/task.h>

/*********************
**********************  Structs
*********************/

struct _SocketPool
{
	gboolean connection_pending;
	gboolean online;
	int socket_qty;
	Server * server;
	GSList * checked_in;
	PanCallback * available_cb;
};

/*********************
**********************  Variables
*********************/

static GSList * pools;
static guint tag_upkeep = 0u;

static const guint KEEPALIVE_INTERVAL_SECS = 10u;
static const guint HUP_IDLE_TIME = 20u;

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/***
****  Channel Data
***/

typedef struct
{
	SocketPool * pool;
	gboolean is_checked_in;
	time_t last_active_time;
}
ChannelData;

static GHashTable *
channel_data_get_hash (void)
{
	static GHashTable * channel_data = NULL;

	if (channel_data == NULL)
		channel_data = g_hash_table_new_full (g_direct_hash,
		                                      g_direct_equal,
		                                      NULL,
		                                      g_free);

	return channel_data;
}

static ChannelData*
channel_data_get (GIOChannel * channel)
{
	GHashTable * hash = channel_data_get_hash ();
	ChannelData * cd = (ChannelData*) g_hash_table_lookup (hash, channel);
	if (cd == NULL) {
		cd = g_new0 (ChannelData, 1);
		cd->pool = NULL;
		cd->is_checked_in = TRUE;
		cd->last_active_time = (time_t)0;
		g_hash_table_insert (hash, channel, cd);
	}
	return cd;
}

static void
channel_data_free (GIOChannel * channel)
{
	g_hash_table_remove (channel_data_get_hash(), channel);
}


/***
****
****  Life Cycle
****
***/

static void
channel_upkeep_done (GIOChannel *channel, TaskStateEnum val, gpointer unused)
{
	socket_pool_checkin (channel, val==TASK_OK);
}
static void
channel_upkeep (gpointer channel_gpointer, gpointer unused)
{
	GIOChannel * channel = (GIOChannel*) channel_gpointer;
	ChannelData * cd = channel_data_get (channel);
	const int idle_secs = (int)difftime(time(NULL),cd->last_active_time);

	/* send keepalives for awhile, then hang up. */
	if (idle_secs > HUP_IDLE_TIME)
		socket_pool_checkin (channel, FALSE);
	else 
		nntp_noop (channel, channel_upkeep_done, NULL);
}
static void
pool_upkeep (gpointer pool_gpointer, gpointer unused)
{
	SocketPool * pool = (SocketPool*) pool_gpointer;
	GSList * channels;

	/* keepalive each channel */
	channels = pool->checked_in;
	pool->checked_in = NULL;
	g_slist_foreach (channels, channel_upkeep, NULL);
	g_slist_free (channels);
}
static gboolean
pools_upkeep_timer (gpointer unused)
{
	g_slist_foreach (pools, pool_upkeep, NULL);
	return TRUE; /* upkeep every KEEPALIVE_INTERVAL_SECS */
}

SocketPool*
socket_pool_new (Server * server)
{
	SocketPool * pool;

	/* sanity clause */
	g_return_val_if_fail (server_is_valid (server), NULL);
      
       /* build the pool */	
	pool = g_new0 (SocketPool, 1);
	pool->server = server;
	pool->connection_pending = FALSE;
	pool->socket_qty = 0;
	pool->online = TRUE;
	pool->checked_in = NULL;
	pool->available_cb = pan_callback_new ();

	/* static class upkeep */
	pools = g_slist_prepend (pools, pool);
	if (!tag_upkeep) {
		const int msec = KEEPALIVE_INTERVAL_SECS * 1000;
		tag_upkeep = g_timeout_add (msec, pools_upkeep_timer, NULL);
	}

	return pool;
}

void
socket_pool_free (SocketPool * pool)
{
	/* sanity clause */
	g_return_if_fail (pool != NULL);

	pools = g_slist_remove (pools, pool);

	pool->server = NULL;
	pool->connection_pending = FALSE;
	pool->socket_qty = 0;
	g_slist_free (pool->checked_in);
	pool->checked_in = NULL;
	pan_callback_free (&pool->available_cb);
	g_free (pool);
}

/***
****
***/

static void
fire_connection_available_change (SocketPool * pool)
{
	pan_callback_call (pool->available_cb, pool, pool->server);
}

static void
add_channel_to_pool (SocketPool   * pool,
                     GIOChannel   * channel)
{
	ChannelData * cd = channel_data_get (channel);
	cd->pool = pool;
	cd->is_checked_in = TRUE;
	pool->checked_in = g_slist_append (pool->checked_in, channel);
}

static void
handshake_done_func (GIOChannel       * channel,
                     TaskStateEnum      state,
                     gpointer           user_data)
{
	SocketPool * pool = (SocketPool*) user_data;

	if (state != TASK_OK) {
		channel_data_free (channel);
		g_io_channel_shutdown (channel, FALSE, NULL);
		g_io_channel_unref (channel);
		channel = NULL;
	}

	if (state == TASK_FAIL) { /* the news server doesn't love us anymore! */
		queue_set_online (FALSE);
	}

	/* if success ... */
	if (channel != NULL)
	{
		/* update the pool */
		debug2 (DEBUG_QUEUE, "success with handshake %s, channel %p",
		                     pool->server->name,
		                     channel);

		pool->connection_pending = FALSE;
		++pool->socket_qty;
		channel_data_get(channel)->last_active_time = time (NULL);
		add_channel_to_pool (pool, channel);

		/* let clients know */
		fire_connection_available_change (pool);
	}
}

void
socket_pool_request_connection (SocketPool * pool)
{
	/* If we're online,
	 * and we don't have any sockets handy,
	 * and there aren't any pending connections,
	 * and we haven't maxed out the number of connections,
	 * then try to make a new connection. */
	if ((pool->online) &&
	    (pool->checked_in == NULL) &&
	    (pool->connection_pending == FALSE) &&
	    (pool->server->max_connections - pool->socket_qty > 0))
	{
		GIOChannel * channel;
		Server * server = pool->server;

		/* create the socket */
		debug1 (DEBUG_QUEUE, "connecting to server %s", server->name);
		channel = pan_get_giochannel_socket (&server->address, server->port);
		if (channel != NULL)
		{
			pool->connection_pending = TRUE;

			nntp_handshake (channel,
			                server->username.str,
			                server->password.str,
			                handshake_done_func,
			                pool);
		}
	}
}

PanCallback*
socket_pool_get_available_callback (SocketPool     * pool)
{
	return pool->available_cb;
}

GIOChannel*
socket_pool_checkout (SocketPool * pool)
{
	GIOChannel * channel = NULL;

	/* pop the first channel from the checked_in list */
	if (pool->online && pool->checked_in!=NULL)
	{
		GSList * pop = pool->checked_in;
		pool->checked_in = g_slist_remove_link (pool->checked_in, pop);
		channel = (GIOChannel*) pop->data;
		g_slist_free_1 (pop);
	}

	/* if we got a channel, mark it as `checked out' */
	if (channel != NULL)
	{
		ChannelData * cd = channel_data_get (channel);
		cd->is_checked_in = FALSE;
		debug2 (DEBUG_QUEUE, "checkout pool %p channel %p", pool, channel);
	}

	return channel;
}

static void
goodbye_done (GIOChannel      * channel,
              TaskStateEnum     ignored,
              gpointer          user_data)
{
	SocketPool * pool = (SocketPool*) user_data;	

	/* note that we don't check the outcome of the command --
	   if `QUIT' has failed there's nothing we can do anyway. */
	channel_data_free (channel);
	g_io_channel_shutdown (channel, FALSE, NULL);
	g_io_channel_unref (channel);
	--pool->socket_qty;
	fire_connection_available_change (pool);
}


void
socket_pool_checkin (GIOChannel    * channel,
                     gboolean        channel_ok)
{
	ChannelData * cd;
	SocketPool * pool;
	debug2 (DEBUG_QUEUE, "checkin pool %p channel %p", pool, channel);

	g_return_if_fail (channel!=NULL);

	cd = channel_data_get (channel);
	pool = cd->pool;

	if (!cd->is_checked_in)
		cd->last_active_time = time (NULL);

	if (channel_ok
		&& pool->socket_qty <= pool->server->max_connections
		&& pool->online)
	{
		debug0 (DEBUG_QUEUE, "adding back to list");
		add_channel_to_pool (pool, channel);
	} else {
		debug0 (DEBUG_QUEUE, "error or offline or too many connections -- closing");
		nntp_goodbye (channel, goodbye_done, pool);
	}
}

int
socket_pool_get_connection_qty (const SocketPool  * pool)
{
	g_return_val_if_fail (pool!=NULL, 0);

	return pool->socket_qty;
}

void
socket_pool_set_online (SocketPool * pool, gboolean online)
{
	g_return_if_fail (pool!=NULL);

	if (online != pool->online)
	{
		pool->online = online;

		fire_connection_available_change (pool);
	}
}
