/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2003  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __PAN_ACTION_H__
#define __PAN_ACTION_H__

typedef enum
{
	ACTION_COMPOSE_NEW,
	ACTION_COMPOSE_FOLLOWUP,
	ACTION_GET_NEW_HEADERS_FOR_SUBSCRIBED,
	ACTION_GET_NEW_HEADERS_FOR_SELECTED,
	ACTION_GET_BODIES_FOR_SELECTED,
	ACTION_READ_MORE,
	ACTION_READ_LESS,
	ACTION_READ_NEXT_SCORE_ARTICLE,
	ACTION_READ_NEXT_UNREAD_ARTICLE,
	ACTION_READ_NEXT_UNREAD_THREAD,
	ACTION_READ_NEXT_UNREAD_GROUP,
	ACTION_READ_NEXT_NEW_ARTICLE,
	ACTION_READ_NEXT_NEW_THREAD,
	ACTION_ARTICLE_DELETE,
	ACTION_SAVE,
	ACTION_SAVE_AS,
	ACTION_MANUAL_DECODE,
	ACTION_SHOW_ONLY_NEW,
	ACTION_SHOW_ONLY_MINE,
	ACTION_SHOW_ONLY_CACHED,
	ACTION_SHOW_ONLY_ATTACHMENTS,
	ACTION_SHOW_ONLY_WATCHED,
	ACTION_NET_ONLINE,
	ACTION_NET_CANCEL_LAST_TASK,
	ACTION_EDIT_SCOREFILE,
	ACTION_ADD_TO_SCOREFILE,
	ACTION_SCORE_PLONK,
	ACTION_SCORE_VIEW,
	ACTION_SCORE_WATCH_THREAD,
	ACTION_SCORE_IGNORE_THREAD,
	ACTION_SCORE_RESCORE,
	ACTION_QTY
}
PanAction;

extern void pan_action_do (PanAction);

#endif
