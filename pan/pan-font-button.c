/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * This is a quick-and-dirty substitute for the Gnome widget for
 * non-Gnome environments.  This should be compiled out on Gnome systems.
 */

/* if user has gtk 2.6 or higher, use GtkColorButton */
#include <gtk/gtk.h>
#if GTK_CHECK_VERSION(2,6,0)
#else

#include <pan/base/pan-i18n.h>
#include <pan/pan-font-button.h>

static void
font_button_response_cb (GtkDialog * dialog, int response, gpointer button)
{
	if (response == GTK_RESPONSE_APPLY || response == GTK_RESPONSE_OK) {
		GtkWidget * fontsel  = GTK_FONT_SELECTION_DIALOG(dialog)->fontsel;
		char * font_name = gtk_font_selection_get_font_name (GTK_FONT_SELECTION(fontsel));
		pan_font_button_set_font_name (GTK_WIDGET(button), font_name);
		g_free (font_name);
	}

	if (response == GTK_RESPONSE_CANCEL || response == GTK_RESPONSE_OK)
		gtk_widget_destroy (GTK_WIDGET(dialog));
}

static void
button_clicked_cb (GtkWidget * button, gpointer unused)
{
	GtkWidget * w;
	GtkWidget * top;
	const char * font_name;

	/* create the dialog */
       	w = gtk_font_selection_dialog_new (_("Select Font"));
	top = gtk_widget_get_toplevel (button);
	if (GTK_WIDGET_TOPLEVEL(top)) {
		gtk_window_set_transient_for (GTK_WINDOW(w), GTK_WINDOW(top));
		gtk_window_set_destroy_with_parent (GTK_WINDOW (w), TRUE);
	}

	/* set its state & listen for changes */
	g_signal_connect (w, "response", G_CALLBACK(font_button_response_cb), button);
       	font_name = (const char*) g_object_get_data (G_OBJECT(button), "font_name");
	gtk_font_selection_dialog_set_font_name (GTK_FONT_SELECTION_DIALOG(w), font_name);
	gtk_widget_show (w);
}

GtkWidget*
pan_font_button_new (void)
{
	GtkWidget * w;
	GtkWidget * h;
	GtkWidget * family_lb;
	GtkWidget * size_lb;
	char * font_name;

	w = gtk_button_new ();
	g_signal_connect (w, "clicked", G_CALLBACK(button_clicked_cb), NULL);
	h = gtk_hbox_new (FALSE, 0);
	family_lb = gtk_label_new (_("Font Family"));
	size_lb = gtk_label_new ("14");
	gtk_box_pack_start (GTK_BOX(h), family_lb, FALSE, FALSE, 6);
	gtk_box_pack_start (GTK_BOX(h), gtk_vseparator_new(), FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(h), size_lb, FALSE, FALSE, 6);
	gtk_container_add (GTK_CONTAINER(w), h);

	font_name = pango_font_description_to_string (w->style->font_desc);
	g_object_set_data (G_OBJECT(w), "family_lb", family_lb);
	g_object_set_data (G_OBJECT(w), "size_lb", size_lb);
	pan_font_button_set_font_name (w, font_name);
	g_free (font_name);

	return w;
}

void
pan_font_button_set_font_name (GtkWidget * w, const char * font_name)
{
	int size;
	const char * family;
	char buf[64];
	GtkStyle * style;
	PangoFontDescription * desc;
	GtkWidget * family_lb = GTK_WIDGET (g_object_get_data (G_OBJECT(w), "family_lb"));
	GtkWidget * size_lb = GTK_WIDGET (g_object_get_data (G_OBJECT(w), "size_lb"));

	/* remember this name */
	g_object_set_data_full (G_OBJECT(w), "font_name", g_strdup(font_name), g_free);

	/* the show */
	desc = pango_font_description_from_string (font_name);
	family = pango_font_description_get_family (desc);
	gtk_widget_ensure_style (family_lb);
	style = gtk_style_copy (family_lb->style);
	if (style->font_desc != NULL)
		pango_font_description_free (style->font_desc);
	style->font_desc = desc;
	gtk_widget_set_style (family_lb, style);
	gtk_style_unref (style);
	size = pango_font_description_get_size(desc) / PANGO_SCALE;
	gtk_label_set_text (GTK_LABEL(family_lb), family);
	g_snprintf (buf, sizeof(buf), "%d", size);
	gtk_label_set_text (GTK_LABEL(size_lb), buf);
}

const char*
pan_font_button_get_font_name (GtkWidget * button)
{
	return (const char*) g_object_get_data (G_OBJECT(button), "font_name");
}

#endif /* GTK_CHECK_VERSION */
