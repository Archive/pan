/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include <pan/base/article.h>
#include <pan/base/article-thread.h>
#include <pan/base/debug.h>
#include <pan/base/file-headers.h>
#include <pan/base/log.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/nntp.h>
#include <pan/queue.h>
#include <pan/task-bodies.h>
#include <pan/task-headers.h>
#include <pan/rules/rule-manager.h>

/*********************
**********************  Defines / Enumerated types
*********************/

#define SAMPLE_SIZE_DEFAULT 150

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

static void task_headers_run_download (Task * task, GIOChannel * channel);

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

#define DEAD_POINTER ((void*)(0xDEADBEEF))

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PUBLIC ROUTINES
************/

/*****
******  TASK LIFE CYCLE
*****/

static void
task_headers_describe (const StatusItem* item, char *buf, int buflen)
{
	const char * name = item==NULL ? "" : group_get_name(TASK_HEADERS(item)->group);
	const int type = item==NULL ? 0 : TASK_HEADERS(item)->download_type;

	if (type == HEADERS_ALL)
		g_snprintf (buf, buflen, _("Getting all headers for \"%s\""), name);
	else if (type == HEADERS_NEW)
		g_snprintf (buf, buflen, _("Getting new headers for \"%s\""), name);
	else
		g_snprintf (buf, buflen, _("Sampling headers for \"%s\""), name);
}

static void
task_headers_destructor (PanObject* o)
{
	TaskHeaders * task_h = (TaskHeaders*) o;

	task_h->group = DEAD_POINTER;
	task_h->download_type = ~0;
	task_h->download_bodies = ~0;
	task_h->body_index = ~0;
	task_h->sample_size = ~0;

	task_destructor (o);
}

static PanObject*
real_task_headers_new (Group                * group,
		       HeaderDownloadType     dl_type,
		       gboolean               download_bodies,
                       int                    sample_size)
{
	TaskHeaders * item;
	debug_enter ("real_task_headers_new");

	g_return_val_if_fail (group_is_valid(group), NULL);
	g_return_val_if_fail (!group_is_folder(group), NULL);
	g_return_val_if_fail (dl_type==HEADERS_ALL || dl_type==HEADERS_NEW || dl_type==HEADERS_SAMPLE, NULL);

       	item = g_new0 (TaskHeaders, 1);
	debug1 (DEBUG_PAN_OBJECT, "task_headers_new: %p", item);

	task_constructor (TASK(item),
	                  TASK_TYPE_HEADERS,
	                  task_headers_destructor,
	                  task_headers_describe,
	                  group->server,
	                  TRUE); /* high priority */
	task_state_set_work_need_socket (&TASK(item)->state, group->server, task_headers_run_download);

	item->group = group;
	item->download_type = dl_type;
	item->download_bodies = download_bodies;
	item->body_index = 0;
	item->sample_size = sample_size;

	debug_exit ("real_task_headers_new");
	return PAN_OBJECT(item);
}

PanObject*
task_headers_new (Group               * group,
                  HeaderDownloadType    dl_type)
{
	return real_task_headers_new (group,
	                              dl_type,
	                              FALSE,
	                              SAMPLE_SIZE_DEFAULT);
}

PanObject*
task_headers_new_with_bodies   (Group               * group,
				HeaderDownloadType    dl_type)
{
	return real_task_headers_new (group,
	                              dl_type,
	                              TRUE,
	                              SAMPLE_SIZE_DEFAULT);
}

PanObject*
task_headers_new_sample (Group     * group,
			 guint       sample_size,
                         gboolean    download_bodies)
{
	return real_task_headers_new (group,
	                              HEADERS_SAMPLE,
	                              download_bodies,
	                              sample_size);
}

/*****
******
*****/

static void
mark_all_articles_old (Group * group, Article ** articles, guint article_qty, gpointer user_data)
{
	if (article_qty > 0u)
		articles_set_new (articles, article_qty, FALSE);
}

static void
mark_read_articles_old (Group * group, Article ** articles, guint article_qty, gpointer user_data)
{
	guint i;
	GPtrArray * read;

	/* get a list of read articles */
	read = g_ptr_array_sized_new (article_qty);
	for (i=0; i!=article_qty; ++i)
		if (article_is_read (articles[i]))
			g_ptr_array_add (read, articles[i]);

	/* mark those articles as old */
	if (read->len > 0)
		articles_set_new ((Article**)read->pdata, read->len, FALSE);

	/* cleanup */
	g_ptr_array_free (read, TRUE);
}

/***
****
***/

typedef struct
{
	Task * task;
	TaskHeaders * task_h;
	StatusItem * status;

	Group * group;

	const char * charset;

	gulong high_number_so_far;
	gulong fetch_hi;
	gulong fetch_lo;
	gulong group_lo;
	gulong group_hi;
	gulong group_qty;

	HeaderDownloadType download_type;
	gulong sample_size;

	gulong parts_added;
	gulong articles_added;

	GHashTable * all_articles_norm;
	GHashTable * all_articles_mid;
	GPtrArray * new_articles;
	GHashTable * changed_articles;
}
TaskHeadersWorkData;

static const TaskHeadersWorkData DEAD_WORK_DATA = {
	DEAD_POINTER, DEAD_POINTER, DEAD_POINTER,
	DEAD_POINTER,
	DEAD_POINTER,
	~0, ~0, ~0, ~0, ~0, ~0,
	~0, ~0,
	~0, ~0,
	DEAD_POINTER, DEAD_POINTER,
	DEAD_POINTER, DEAD_POINTER
};

static void
xover_line_func (GIOChannel      * channel,
                 const char      * line,
                 int               line_len,
                 gpointer          user_data)
{
	TaskHeadersWorkData * wdata = (TaskHeadersWorkData*) user_data;

	gboolean create_article = FALSE;
	int part = 0;
        int parts = 0;
	const char * pch;
	const char * s;
	const char * march = line;
	const PString group_name = wdata->group->name;
	char norm_buf[4096];
	int norm_len = 0;
	PString norm = PSTRING_INIT;
	time_t date_val;

	gulong number, byte_qty, line_qty;
	PString subject = PSTRING_INIT;
	PString author = PSTRING_INIT;
	PString date = PSTRING_INIT;
	PString message_id = PSTRING_INIT;
	PString references = PSTRING_INIT;
	PString xref = PSTRING_INIT;
	PString tok = PSTRING_INIT;

	/***
	****  Tokenize the line
	***/

	number = get_next_token_ulong (march, '\t', &march);
	get_next_token_pstring (march, '\t', &march, &tok); subject = pstring_strstrip_shallow (&tok);
	get_next_token_pstring (march, '\t', &march, &tok); author = pstring_strstrip_shallow (&tok);
	get_next_token_pstring (march, '\t', &march, &tok); date = pstring_strstrip_shallow (&tok);
	get_next_token_pstring (march, '\t', &march, &tok); message_id = pstring_strstrip_shallow (&tok);
	get_next_token_pstring (march, '\t', &march, &tok); references = pstring_strstrip_shallow (&tok);
	byte_qty = get_next_token_ulong (march, '\t', &march);
	line_qty = get_next_token_ulong (march, '\t', &march);
	get_next_token_pstring (march, '\t', &march, &tok); xref = pstring_strstrip_shallow (&tok);
	if (xref.len>6 && !strncmp(xref.str,"Xref: ", 6)) {
		xref = pstring_substr_shallow (&xref, xref.str+6, NULL); /* skip "Xref: " */
		xref = pstring_strstrip_shallow (&xref); /* remove trailing linefeed */
	}

	/* does it look valid? */
	if (!number
		|| !pstring_is_set(&subject) /* missing subject */
		|| !pstring_is_set(&author) /* missing author */
		|| !pstring_is_set(&date) /* missing date */
		|| !pstring_is_set(&message_id) /* missing mid */
		|| message_id.str[0]!='<' /* corrupt mid */
		|| !(!pstring_is_set(&references) || references.str[0]=='<')) /* corrupt references */
	{
		GString * gstr = g_string_new (_("Corrupt header skipped: "));
		g_string_append_len (gstr, line, line_len);
		log_add (LOG_INFO, gstr->str);
		g_string_free (gstr, TRUE);
		return;
	}

	/* is it a unique message id? */
	if (g_hash_table_lookup(wdata->all_articles_mid,&message_id) != NULL)
		return;

	wdata->high_number_so_far = MAX (wdata->high_number_so_far, number);


	/***
	****  Figure out parts
	***/

	/* Look for the (n/N) or [n/N] construct in subject lines,
	 * starting at the end of the string and working backwards */
	part = 0;
	parts = 0;
	s = subject.str;
	pch = s + subject.len - 1;
	while (pch != s)
	{
		/* find the ']' of [n/N] */
		--pch;
		if ((pch[1]!=')' && pch[1]!=']') || !isdigit((guchar)*pch))
			continue;

		/* find the '/' of [n/N] */
		while (s!=pch && isdigit((guchar)*pch))
			--pch;
		if (s==pch || (*pch!='/' && *pch!='|'))
			continue;

		/* N -> parts */
		parts = atoi (pch+1);
		--pch;

		/* find the '[' of [n/N] */
		while (s!=pch && isdigit((guchar)*pch))
			--pch;
		if (s==pch || (*pch!='(' && *pch!='[')) {
			parts = 0;
			continue;
		}

		/* n -> part */
		part = atoi (pch+1);
		break;
	}

	/* if not a multipart yet, AND if it's a big message, AND
	   it's either in one of the pictures/fan/sex groups or it
	   has commonly-used image names in the subject, guess it's
	   a single-part binary */
	if (!parts
		&& line_qty>400
		&& (((g_strstr_len (group_name.str, group_name.len, "binaries")
				|| g_strstr_len (group_name.str, group_name.len, "fan")
				|| g_strstr_len (group_name.str, group_name.len, "mag")
				|| g_strstr_len (group_name.str, group_name.len, "sex")))
			|| ((pan_strstr(s,".jpg") || pan_strstr(s,".JPG")
				|| pan_strstr(s,".jpeg") || pan_strstr(s,".JPEG")
				|| pan_strstr(s,".gif")  || pan_strstr(s,".GIF")
				|| pan_strstr(s,".tiff") || pan_strstr(s,".TIFF")))))
		part = parts = 1;

	/* but if it's starting the subject with "Re:" and doesn't
	   have many lines, it's probably a followup to a part, rather
	   than an actual part. */
	if (subject.len>3 && !strncmp (subject.str, "Re:", 3) && line_qty<100)
		part = parts = 0;

	/* Subjects containing (0/N) aren't part of an N-part binary;
	   they're text description articles that accompany the binary. */
	if (part == 0)
		parts = 0;

	/***
	****  Handle multiparts
	***/

	create_article = TRUE;

	norm_len = normalize_subject (norm_buf, &subject, STRIP_MULTIPART_NUMERATOR);
	norm = pstring_shallow (norm_buf, norm_len);

	date_val = g_mime_utils_header_decode_date (date.str, NULL);

	if (parts > 1)
	{
		Article * a = (Article*) g_hash_table_lookup (wdata->all_articles_norm, &norm);

		if (a != NULL)
		{
			/* add a part to an existing multipart collection */
			AttachmentPart * ap = article_alloc_part (a);
			ap->part_num = part;
			ap->line_qty = line_qty;
			ap->byte_qty = byte_qty;
			ap->date = date_val;
			pstring_copy (&ap->message_id, &message_id);
			message_sources_add_from_xref (&ap->sources, wdata->group->server->name_quark, xref.str, xref.len);
			article_add_part (a, ap);

			/* since new parts have come in, give this article a new lease on life */
			a->date = date_val;

			++wdata->parts_added;
			create_article = FALSE;
			g_hash_table_insert (wdata->changed_articles, a, a);
		}

	}

	if (create_article)
	{
		Article * a;
		++wdata->articles_added;

		/* create the new article */
		a = article_new (wdata->group);
		a->is_new = TRUE;
		a->number = number;
		tok = pstring_shallow (pan_header_to_utf8 (subject.str, subject.len, wdata->charset), -1);
		article_set_subject (a, &tok);
		pstring_clear (&tok);
		article_set_author (a, &author);
		a->date = date_val;
		article_set_message_id (a, &message_id);
		a->byte_qty = byte_qty;
		a->line_qty = line_qty;
		a->parts = parts;
		if (pstring_is_set (&references)) article_set_references (a, &references);
		message_sources_add_from_xref (&a->xref, wdata->group->server->name_quark, xref.str, xref.len);

		if (parts > 0)
		{
			/* add a part to an existing multipart collection */
			AttachmentPart * ap = article_alloc_part (a);
			ap->part_num = part;
			ap->line_qty = line_qty;
			ap->byte_qty = byte_qty;
			ap->date = g_mime_utils_header_decode_date (date.str, NULL);
			pstring_copy (&ap->message_id, &message_id);
			message_sources_add_from_xref (&ap->sources, wdata->group->server->name_quark, xref.str, xref.len);
			article_add_part (a, ap);
			create_article = FALSE;

			++wdata->parts_added;
		}

		/* Add the article to the article list */
		g_ptr_array_add (wdata->new_articles, a);
		g_hash_table_insert (wdata->changed_articles, a, a);
		g_hash_table_insert (wdata->all_articles_norm, pstring_dup(&norm), a);
		g_hash_table_insert (wdata->all_articles_mid, &a->message_id, a);
	}

	/* update the count & progress ui */
	{
		const gulong tot = wdata->parts_added + wdata->articles_added;
		status_item_emit_next_step (wdata->status);
		if (!(tot % 100ul)) {
			if (!tot)
				status_item_emit_status (wdata->status, _("Fetching articles"));
			else if (!wdata->parts_added)
				status_item_emit_status_va (wdata->status, _("%lu Articles"), wdata->articles_added);
			else
				status_item_emit_status_va (wdata->status, _("%lu Parts, %lu Articles"), wdata->parts_added, wdata->articles_added);
		}
	}
}

static void
wdata_free (TaskHeadersWorkData * wdata)
{
	g_return_if_fail (wdata != NULL);

	g_ptr_array_free (wdata->new_articles, TRUE);
	g_hash_table_destroy (wdata->all_articles_mid);
	g_hash_table_destroy (wdata->all_articles_norm);
	g_hash_table_destroy (wdata->changed_articles);

	*wdata = DEAD_WORK_DATA;
	g_free (wdata);
}

static void
xover_done_func (GIOChannel       * channel,
                 TaskStateEnum      state,
                 gpointer           user_data)
{
	TaskHeadersWorkData * wdata = (TaskHeadersWorkData*) user_data;
	TaskHeaders * task_h = wdata->task_h;
	GPtrArray * new_articles = wdata->new_articles;
	GPtrArray * changed;
	StatusItem * status = wdata->status;

	if (state == TASK_OK)
		status_item_emit_progress (wdata->status, 100);

	/* get a list of the changed articles */
	changed = g_ptr_array_new ();
	pan_hash_to_ptr_array (wdata->changed_articles, changed);

	/* let the user know what we got */
	log_add_va (LOG_INFO, _("Added %lu parts, %lu articles in \"%s\""),
		wdata->parts_added,
		wdata->articles_added,
		group_get_name(task_h->group));

	/* update the group's bookkeeping pointers */
	if (changed->len) {
		group_mark_new_article_number (task_h->group, wdata->high_number_so_far);
		group_set_article_range (task_h->group, wdata->group_lo, wdata->high_number_so_far);
	}

	/* update the articles' "new" state:
	   if we haven't read the group since the last time we fetched headers, mark nothing as old.
	   otherwise if there were new articles, mark all the previous ones as old.
	   otherwise mark all the previous read ones as old. */
	if (task_h->group->loaded_since_last_fetch)
	{
		task_h->group->loaded_since_last_fetch = FALSE;

		if (changed->len)
			group_article_forall (task_h->group, mark_all_articles_old, NULL);
		else
			group_article_forall (task_h->group, mark_read_articles_old, NULL);
	}

	/* give the group the new articles */
	if (new_articles->len)
		group_add_articles (task_h->group, new_articles, status);

	/* reflag everything with new parts as new, so old multiparts show up new again */
	if (changed->len)
		articles_set_new ((Article**)changed->pdata, changed->len, TRUE);

	/* apply the rules to the changed articles */
	if (changed->len)
		rule_manager_process_incoming_articles (changed);

	/* do we need to download these? */
	if (new_articles->len && task_h->download_bodies) {
		const Article ** a = (const Article **) new_articles->pdata;
		queue_add (TASK(task_bodies_new_from_articles (a,  new_articles->len)));
	}

	/* finish with the group */
	if (changed->len)
		file_headers_save (wdata->group, status);
	group_unref_articles (wdata->group, status);

	/* refresh task state.
	   this destroys the task if !channel_ok,
	   so don't reference 'task' or 'status' past this point */
	task_state_set_health (&wdata->task->state, state);
	if (state == TASK_OK)
		task_state_set_work_completed (&wdata->task->state);
	else
		task_state_set_work_need_socket (&wdata->task->state,
		                                 wdata->task->server,
		                                 task_headers_run_download);
	task_checkin_socket (wdata->task, channel, state==TASK_OK);

	/* cleanup */
	g_ptr_array_free (changed, TRUE);
	wdata_free (wdata);
}

static void
group_cmd_data_func (GIOChannel * io,
                     gulong qty,
                     gulong lo,
                     gulong hi,
                     GQuark group_quark,
                     gpointer user_data)
{
	TaskHeadersWorkData * wdata = ( TaskHeadersWorkData *) user_data;

	wdata->group_lo = lo;
	wdata->group_hi = hi;
	wdata->group_qty = qty;

	switch (wdata->download_type)
	{
		case HEADERS_NEW: {
			const gulong p_last = wdata->group->article_high;
			wdata->fetch_lo = MAX (lo, p_last+1);
			wdata->fetch_hi = hi;
			break;	
		}

		case HEADERS_SAMPLE: {
			wdata->fetch_lo = hi - MIN (qty, wdata->sample_size);
			wdata->fetch_hi = hi;
			break;
		}

		case HEADERS_ALL:
			wdata->fetch_lo = lo;
			wdata->fetch_hi = hi;
			break;

		default:
			pan_warn_if_reached ();
			wdata->fetch_lo = 0ul;
			wdata->fetch_hi = 0ul;
			break;
	}
}

static void
build_mid_lookup (Group * group, Article ** articles, guint article_qty, gpointer hash_gpointer)
{
	guint i;
	GHashTable * hash = (GHashTable*) hash_gpointer;
	for (i=0; i!=article_qty; ++i) {
		Article * article = articles[i];
		g_hash_table_insert (hash, &article->message_id, article);
	}
}

static void
build_multipart_lookup (Group * group, Article ** articles, guint article_qty, gpointer hash_gpointer)
{
	guint i;
	GHashTable * hash = (GHashTable*) hash_gpointer;
	char norm_buf[4096];
	for (i=0; i!=article_qty; ++i) {
		Article * article = articles[i];
		int norm_len = normalize_subject (norm_buf, &article->subject, STRIP_MULTIPART_NUMERATOR);
		PString norm = pstring_shallow (norm_buf, norm_len);
		g_hash_table_insert (hash, pstring_dup(&norm), article);
	}
}

static void
group_cmd_done_func (GIOChannel       * channel,
                     TaskStateEnum      state,
                     gpointer           user_data)
{
	TaskHeadersWorkData * wdata = ( TaskHeadersWorkData *) user_data;

	if (state != TASK_OK)
	{
		/* set state */
		task_state_set_health (&wdata->task->state, state);
		task_state_set_work_need_socket (&wdata->task->state, wdata->task->server, task_headers_run_download);
		task_checkin_socket (wdata->task, channel, FALSE);

		wdata_free (wdata);
	}
	else if (!wdata->group_qty || wdata->fetch_lo > wdata->fetch_hi)
	{
		/* no articles, or no new articles... */
		task_state_set_health (&wdata->task->state, TASK_OK);
		task_state_set_work_completed (&wdata->task->state);
		task_checkin_socket (wdata->task, channel, TRUE);

		wdata_free (wdata);
	}
	else /* we have a valid range, so now we XOVER */
	{
		int steps;

		/* load the existing headers */
		group_ref_articles (wdata->group, wdata->status);
		group_expire_articles_not_in_range (wdata->group, wdata->group_lo, wdata->group_hi); 
		group_article_forall (wdata->group, build_multipart_lookup, wdata->all_articles_norm);
		group_article_forall (wdata->group, build_mid_lookup, wdata->all_articles_mid);

		/* init the status item for the header download */
		steps = MAX (0, wdata->fetch_hi - wdata->fetch_lo);
		status_item_emit_init_steps (wdata->status, steps);

		/* get the article headers... */
		pan_g_ptr_array_reserve (wdata->new_articles, wdata->fetch_hi - wdata->fetch_lo);

		nntp_xover (channel,
		            &wdata->task->abort_flag,
		            xover_line_func, wdata,
		            xover_done_func, wdata,
		            wdata->fetch_lo, wdata->fetch_hi);
	}
}

static void
task_headers_run_download (Task * task, GIOChannel * channel)
{

	TaskHeaders * task_h = TASK_HEADERS(task);
	TaskHeadersWorkData * wdata;
	debug_enter ("task_headers_run");

	/* set current state */
	task_state_set_work_working (&task->state);

	/* prep this mammoth state struct so that we can pass it around
	   during the GROUP and XOVER commands that follow */
	wdata = g_new (TaskHeadersWorkData, 1);
	wdata->task = task;
	wdata->task_h = TASK_HEADERS(task);
	wdata->status = STATUS_ITEM(task);
	wdata->group = task_h->group;
	wdata->charset = group_get_default_charset (wdata->group);
	wdata->high_number_so_far = 0ul;
	wdata->fetch_hi = 0ul;
	wdata->fetch_lo = 0ul;
	wdata->group_lo = 0ul;
	wdata->group_hi = 0ul;
	wdata->group_qty = 0ul;
	wdata->download_type = task_h->download_type;
	wdata->sample_size = task_h->sample_size;
	wdata->new_articles = g_ptr_array_new ();
	wdata->parts_added = 0;
	wdata->articles_added = 0;
	wdata->changed_articles = g_hash_table_new (g_direct_hash, g_direct_equal);
	wdata->all_articles_mid = g_hash_table_new (pstring_hash, pstring_equal);
	wdata->all_articles_norm = g_hash_table_new_full (pstring_hash,
	                                                  pstring_equal,
	                                                  (GDestroyNotify)pstring_free,
	                                                  NULL);


	/* before we can XOVER, we need to set the GROUP so that we can
	   get up-to-date article numbers to build the XOVER command */
	nntp_group (channel,
	            task_h->group->name_quark,
	            group_cmd_data_func, wdata,
	            group_cmd_done_func, wdata);
}
