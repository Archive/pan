/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __TEXT_H__
#define __TEXT_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtktextbuffer.h>

#include <gmime/gmime-message.h>

#include <pan/base/pan-callback.h>
#include <pan/base/message-identifier.h>
#include <pan/base/text-massager.h>

/**
***  CURRENT ARTICLE
**/

/**
 * called whenever the current article changes.
 * @param call_object the old article
 * @param call_arg the new article
 */
extern PanCallback * current_article_changed;

/**
 * Returns the current displayed article, or NULL if no article.
 * If the message is non-NULL, you must g_object_unref() it when you're done with it.
 */
extern GMimeMessage * get_current_message (void);

extern gboolean text_pane_has_message (void);

/**
***  Wrapping
**/

void          text_set_wrap                                  (gboolean);
gboolean      text_get_wrap                                  (void);
PanCallback*  text_get_fill_body_changed_callback            (void);

/**
***  Show all Headers
**/

void          text_set_show_all_headers                      (gboolean);
gboolean      text_get_show_all_headers                      (void);
PanCallback*  text_get_show_all_headers_changed_callback     (void);

/**
***  Mute Quoted Text
**/

void          text_set_mute_quoted                           (gboolean);
gboolean      text_get_mute_quoted                           (void);
PanCallback*  text_get_mute_quoted_changed_callback          (void);


TextMassager*   text_pane_get_text_massager                  (void);

/**
***  Rot 13
**/

void          text_rot13_selected_text_nolock                (void);

/**
***  Colors
**/

extern GdkColor text_url_color;
extern GdkColor text_quoted_color[3];
extern GdkColor signature_color;


/**
***
**/
void         text_set_text_buffer_tags        (GtkTextBuffer * buffer);
GtkWidget*   text_create                     (void);
void         text_read_more                  (void);
void         text_read_less                  (void);
void         text_select_all                 (void);
void         text_deselect_all               (void);
void         text_refresh                    (void);
void         text_clear_nolock               (void);
void         text_set_raw                    (const char   * text);

void         text_set_from_identifiers       (Server              * server,
                                              const char          * acache_key,
                                              const char          * default_charset,
                                              MessageIdentifier  ** mids,
                                              int                   mid_qty);


void         text_set_font                   (void);


gchar*       text_get_message_to_reply_to    (void);

void         update_body_pane                (const TextMassager  * text_massager,
                                              GtkTextBuffer       * buffer,
	                                      const gchar         * body,
					      gboolean              mute_quotes);

#endif /* __TEXT_H__ */
