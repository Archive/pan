/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __TASK_BODIES_H_
#define __TASK_BODIES_H_

#include <pan/base/group.h>
#include <pan/task.h>

#define TASK_BODIES(obj) ((TaskBodies*)(obj))

typedef struct
{
	/* parent class */
	Task           task;

	/* private */
	int            mids_running_qty;
	GQueue       * mids;
	TaskWorkFunc   done_work_func;
}
TaskBodies;

/**
***  PROTECTED
**/


void     task_bodies_destructor   (PanObject                 * obj);

void     task_bodies_constructor  (Task                      * task,
                                   gint8                       task_type,
                                   PanObjectDestructor         dtor,
                                   StatusItemDescribeFunc      describe,
                                   Server                    * server,
                                   gboolean                    high_priority,
                                   MessageIdentifier        ** mids,
                                   int                         mid_qty,
                                   TaskWorkFunc                done_work_func);

/**
***  PUBLIC
**/

PanObject*   task_bodies_new                (Server              * server,
                                             MessageIdentifier  ** mids,
                                             int                   mid_qty);

PanObject*   task_bodies_new_from_articles  (const Article      ** articles,
                                             int                   article_qty);

#endif /* __TASK_BODY__H__ */
