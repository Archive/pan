/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PAN_COLOR_BUTTON_H
#define PAN_COLOR_BUTTON_H

#include <gdk/gdkcolor.h>
#include <gtk/gtkwidget.h>

/* if user has gtk 2.6 or higher, use GtkColorButton */
#if GTK_CHECK_VERSION(2,6,0)
#include <gtk/gtkcolorbutton.h>
#define    pan_color_button_new() gtk_color_button_new()
#define    pan_color_button_set_color(w,c) gtk_color_button_set_color(GTK_COLOR_BUTTON(w), c)
#define    pan_color_button_get_color(w,c) gtk_color_button_get_color(GTK_COLOR_BUTTON(w), c)
#else
GtkWidget* pan_color_button_new (void);
void       pan_color_button_set_color (GtkWidget * w, const GdkColor*);
void       pan_color_button_get_color (GtkWidget * w, GdkColor*);
#endif /* GTK_CHECK_VERSION */

#endif /* PAN_COLOR_BUTTON_H */
