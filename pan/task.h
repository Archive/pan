/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __TASK__H__
#define __TASK__H__

#include <glib.h>

#include <pan/base/article.h>
#include <pan/base/message-identifier.h>
#include <pan/base/pan-callback.h>
#include <pan/base/server.h>
#include <pan/base/status-item.h>
#include <pan/base/article.h>

#include <pan/task-state.h>

/**
***  TASK CLASS DEFINITION
**/

enum
{
	TASK_TYPE_OTHER,
	TASK_TYPE_BODY,
	TASK_TYPE_BODIES,
	TASK_TYPE_HEADERS,
	TASK_TYPE_SAVE,
	TASK_TYPE_POST
};

#define TASK(a) ((Task*)a)
typedef struct _Task Task;
typedef int (*TaskRunFunc)(Task*);
struct _Task
{
	/* Parent Class */
	StatusItem parent;

	/* Public Fields */
	PanCallback * task_ran_callback;

	/* Accessed by Queue */
	int thread_qty;
	gboolean abort_flag;

	/* Private Fields */  
	guint high_priority      : 1;
	gint8 type;
	Server * server;
	GPtrArray * identifiers;
	GHashTable * checkin_funcs;
	TaskState state;
};

/**
***  PROTECTED
**/

void task_destructor      (PanObject                 * object);

void task_constructor     (Task                      * task,
                           gint8                       type,
                           PanObjectDestructor         dtor,
                           StatusItemDescribeFunc      describe,
                           Server                    * server,
                           gboolean                    high_priority);

void task_checkin_socket  (Task                      * task,
                           GIOChannel                * channel,
                           gboolean                    channel_ok);

/**
***  PUBLIC
**/

typedef void (TaskForallMidsFunc)(Task                 * task,
                                  MessageIdentifier   ** identifiers,
                                  int                    identifier_qty,
                                  gpointer               user_data);

void         task_forall_identifiers   (Task               * task,
                                        TaskForallMidsFunc   forall_func,
                                        gpointer             forall_func_user_data);

void         task_add_identifiers     (Task                 * task,
                                       MessageIdentifier   ** identifiers,
                                       int                    identifier_qty);

void         task_add_identifiers_from_articles     (Task                 * task,
                                                     const Article       ** articles,
                                                     int                    article_qty);



typedef void (*TaskSocketCheckinFunc)(Task          * task,
                                      GIOChannel    * channel,
                                      gboolean        channel_ok,
                                      gpointer        user_data);

void         task_give_socket (Task                  * task,
                               GIOChannel            * io,
                               TaskSocketCheckinFunc   checkin_func,
                               gpointer                checkin_func_user_data);

void         task_work_gui (Task * task);

void         task_work_child (Task * task);

gulong       task_get_xfer_rate_KiBps (const Task * task);



#endif
