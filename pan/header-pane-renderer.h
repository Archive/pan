/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2003  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __HEADER_PANE_RENDERER_H__
#define __HEADER_PANE_RENDERER_H__

#include <gdk/gdk.h>
#include <gtk/gtkstyle.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkctree.h>

enum
{
	SCORE_COLOR_WATCHED,
	SCORE_COLOR_HIGH,
	SCORE_COLOR_MEDIUM,
	SCORE_COLOR_LOW,
	SCORE_COLOR_IGNORED,
	SCORE_COLOR_QTY
};

extern GdkColor score_color[SCORE_COLOR_QTY][2];
extern GdkColor read_color;
extern GdkColor unread_color;

enum
{
	COLUMN_ACTION_STATE,
	COLUMN_ARTICLE_STATE,
	COLUMN_SCORE,
	COLUMN_SUBJECT,
	COLUMN_LINES,
	COLUMN_AUTHOR,
	COLUMN_DATE,
	COLUMN_TYPE_QTY
};

extern int articlelist_columns[];
extern int articlelist_column_qty;

typedef struct _HeaderPaneRenderer  HeaderPaneRenderer;

HeaderPaneRenderer*
header_pane_renderer_new           (GtkWidget           * main_window,
                                    GtkCTree            * ctree);

void
header_pane_renderer_reset_style   (HeaderPaneRenderer  * renderer);

void
header_pane_renderer_refresh_node  (HeaderPaneRenderer  * renderer,
                                    GtkCTreeNode        * node,
                                    const Article       * article);

gboolean
header_pane_renderer_populate_node (GtkCTree            * ctree,
                                    guint                 depth,  
                                    GNode               * node,
                                    GtkCTreeNode        * cnode,
                                    gpointer              renderer_gpointer);

#endif /* __HEADER_PANE_RENDERER__H__ */
