/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef TASK_SAVE__H
#define TASK_SAVE__H

#include <pan/base/group.h>
#include <pan/base/article.h>
#include <pan/task-bodies.h>


/**
***  TASK SAVE DECLARATION
**/

#define TASK_SAVE(obj) ((TaskSave*)obj)

typedef struct
{
	/* parent */

	TaskBodies         item;

	/* private */

	GQuark         group_quark;
	gboolean       save_attachments;
	char         * path_attachments;
	char         * filename_attachments;

	gboolean       save_bodies;
	char         * path_bodies;
	char         * filename_bodies;
}
TaskSave;


/**
***   PUBLIC
**/

PanObject*  task_save_new                 (Server              * server,
                                           MessageIdentifier  ** mids,
                                           int                   mid_qty);

PanObject*  task_save_new_from_article    (const Article       * article);

void        task_save_set_attachments     (TaskSave            * task,
                                           const char          * path,
                                           const char          * filename);

void        task_save_set_bodies          (TaskSave            * task,
                                           const char          * path,
                                           const char          * filename);

void        task_save_weed_duplicates     (GPtrArray           * articles);


char*       expand_download_dir           (const char          * dir,
                                           const char          * groupname);

#endif
