/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __TASK_POST_H_
#define __TASK_POST_H_

#include <pan/task.h>

#define TASK_POST(obj) ((TaskPost*)(obj))

typedef struct
{
	/* parent class */
	Task           task;

	/* private, passed in ctor */
	GMimeMessage * message;
}
TaskPost;

/**
***  PUBLIC
**/

PanObject*   task_post_new     (Server        * server,
                                GMimeMessage  * message);


/**
***  UTIL
**/

void queue_message_for_posting (Server          * server,
                                GMimeMessage    * message,
                                gboolean          check_message_first);

void flush_sendlater_messages  (void);

char* build_nntp_message       (GMimeMessage    * message);

#endif /* __TASK_POST__H__ */
