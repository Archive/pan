#ifndef __SCORE_ADD_UI_H__
#define __SCORE_ADD_UI_H__

#include <gtk/gtkwidget.h>
#include <pan/base/article.h>

typedef enum
{
	SCORE_ADD,
	SCORE_ADD_PLONK,
	SCORE_ADD_WATCH_SUBTHREAD,
	SCORE_ADD_IGNORE_SUBTHREAD
}
ScoreAddMode;

GtkWidget* score_add_dialog (GtkWidget*, Article*, ScoreAddMode);

#endif
