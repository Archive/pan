/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2003  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __SOCKET_POOL_H__
#define __SOCKET_POOL_H__

#include <glib/gtypes.h>
#include <pan/base/pan-callback.h>
#include <pan/base/server.h>

#define PAN_SOCKET_POOL(a) ((SocketPool *)a)

typedef struct _SocketPool SocketPool;

/**
***  PUBLIC - Life Cycle
**/

SocketPool*   socket_pool_new                    (Server            * server);

void          socket_pool_free                   (SocketPool        * pool);

/**
***  CONNECTIONS
**/

PanCallback*  socket_pool_get_available_callback (SocketPool        * pool);

void          socket_pool_request_connection     (SocketPool        * pool);

GIOChannel*   socket_pool_checkout               (SocketPool        * pool);

void          socket_pool_checkin                (GIOChannel        * gio,
                                                  gboolean            gio_ok);

int           socket_pool_get_connection_qty     (const SocketPool  * pool);

/**
***  ONLINE
***
***  If online is set to false, requests will be ignored,
***  checkout will always return NULL, and all checked-in connections
***  will be closed when refresh() is called.
**/

void          socket_pool_set_online             (SocketPool        * pool,
                                                  gboolean            online);


#endif /* __SOCKET_POOL_H__ */
