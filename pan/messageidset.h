/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __MESSAGEIDSET_H__
#define __MESSAGEIDSET_H__

#include <glib/gtypes.h>
#include <pan/base/article.h>

/**
 * This class maintains a threadsafe set of unique message-ids.
 */
typedef struct MsgIdSet MessageIdSet;

typedef void (*MsgIdSetForeachFunc)(const char * message_id, gpointer user_data);

/**
***  Life Cycle
**/

MessageIdSet* messageidset_new                         (void);

void          messageidset_free                        (MessageIdSet        * mset);

/**
***  Accessors
**/

GPtrArray*    messageidset_get_ids                     (const MessageIdSet  * msets);

/**
***  Mutators
**/

void          messageidset_add_message_ids             (MessageIdSet        * mset,
                                                        const PString      ** message_ids,
                                                        int                   message_id_qty);

void          messageidset_add_articles                (MessageIdSet        * mset,
                                                        const Article      ** articles,
                                                        int                   article_qty);

void          messageidset_add_articles_and_ancestors  (MessageIdSet        * mset,
                                                        const Article      ** articles,
                                                        int                   article_qty);

void          messageidset_add_articles_and_threads    (MessageIdSet        * mset,
                                                        const Article      ** articles,
                                                        int                   article_qty);

void          messageidset_clear                       (MessageIdSet        * mset);


#endif /* __MESSAGEIDSET_H__ */
