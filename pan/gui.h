/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GUI_H__
#define __GUI_H__

#include <gtk/gtkwidget.h>

extern GtkWidget * groups_vbox;
extern GtkWidget * articlelist_ctree;
extern GtkWidget * text_box;

enum {
	GUI_PANED = 1,
	GUI_NOTEBOOK
};

enum {
	GROUPS_PANE = 0,
	HEADERS_PANE,
	BODY_PANE
};

void gui_construct (const char *geometry);

void gui_layout_clear_workarea (void);
void gui_layout_refresh        (void);
void gui_page_set              (int page);

GtkWidget* gui_add_network_button_to_toolbar  (GtkWidget * toolbar);

extern GdkColormap* cmap;

#endif /* __GUI_H__ */
