/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2003  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <string.h>

#include <gmime/gmime-message.h>

#include <pan/base/article.h>
#include <pan/base/debug.h>
#include <pan/base/log.h>
#include <pan/base/server.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/nntp.h>
#include <pan/task-cancel.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PRIVATE ROUTINES
************/

/***
****  Running
***/

static void
task_cancel_run (Task * task, GIOChannel * channel);

static void
task_cancel_ran (GIOChannel     * channel,
                 TaskStateEnum    val,
                 gpointer         task_gpointer)
{
	StatusItem * status = STATUS_ITEM(task_gpointer);
	Task * task = TASK(task_gpointer);
	GMimeMessage * message = TASK_CANCEL(task)->message;

	/* did it work? */
	if (val == TASK_OK)
	{
		const char * cpch = g_mime_message_get_message_id (message);
		PString id = pstring_shallow (cpch, -1);
		const char * subject = g_mime_message_get_subject (message);
		const char * newsgroups = g_mime_message_get_header (message, HEADER_NEWSGROUPS);
		char ** groups = g_strsplit (newsgroups, ",", 0);
		guint i;

		for (i=0; groups[i]; ++i) {
			const GQuark group_quark = g_quark_from_string (groups[i]);
			Group * group = server_get_group (task->server, group_quark);
			group_ref_articles (group, status); {
				Article * article = ARTICLE(group_get_article_by_message_id (group, &id));
				if (article)
					group_remove_articles (group, &article, 1);
			}
			group_unref_articles (group, status);
		}

		log_add_va (LOG_INFO|LOG_URGENT, _("Article %s (%s) canceled"), subject, id.str);

		/* cleanup */
		g_strfreev (groups);
	}

	/* refresh the task state */
	task_state_set_health (&task->state, val);
        if (val == TASK_OK)
                task_state_set_work_completed (&task->state);
        else
		task_state_set_work_need_socket (&task->state, task->server, task_cancel_run);
        task_checkin_socket (task, channel, val==TASK_OK);
}

static void
task_cancel_run (Task * task, GIOChannel * channel)
{
	GMimeMessage * message;
	const char * cpch;
	PString id;

	status_item_emit_status_va (STATUS_ITEM(task), _("Canceling article"));
	
	message = TASK_CANCEL(task)->message;
	cpch = g_mime_message_get_message_id (message);
	id = pstring_shallow (cpch, -1);
        nntp_cancel (channel, &id, task_cancel_ran, task);
}


static void
task_cancel_describe (const StatusItem * status, char * buf, int buflen)
{
	g_strlcpy (buf, _("Canceling article"), buflen);
}


static void
task_cancel_destructor (PanObject * obj)
{
	debug_enter ("task_cancel_destructor");

	/* sanity clause */
	g_return_if_fail (obj!=NULL);

	/* task-cancel dtor */
	g_object_unref (TASK_CANCEL(obj)->message);

	/* destroy parent class */
	task_destructor (obj);

	debug_exit ("task_cancel_destructor");
}

/************
*************  PUBLIC ROUTINES
************/

PanObject*
task_cancel_new (Server * server, GMimeMessage * message)
{
	TaskCancel * cancel = NULL;
	debug_enter ("task_cancel_new");

	/* sanity clause */
	g_return_val_if_fail (server_is_valid(server), NULL);
	g_return_val_if_fail (GMIME_IS_MESSAGE(message), NULL);

	/* create the object */
       	cancel = g_new0 (TaskCancel, 1);
        debug1 (DEBUG_PAN_OBJECT, "task_cancel_new: %p", cancel);

	/* initialize the parent class */
	task_constructor (TASK(cancel),
	                  TASK_TYPE_POST,
	                  task_cancel_destructor,
	                  task_cancel_describe, server,
	                  TRUE);
	task_state_set_work_need_socket (&TASK(cancel)->state, server, task_cancel_run);

	/* initialize the task-cancel */
	cancel->message = message;
	g_object_ref (message);

	debug_exit ("task_cancel_new");
	return PAN_OBJECT(cancel);
}
