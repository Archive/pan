/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __PAN_CHARSET_PICKET_H__
#define __PAN_CHARSET_PICKET_H__

#include <gtk/gtk.h>

GtkWidget  * pan_charset_picker_new (const char * charset);

char       * pan_charset_picker_free (GtkWidget * w);

gboolean     pan_charset_picker_set_charset (GtkWidget * w, const char * charset);

const char * pan_charset_picker_get_charset (GtkWidget * w);

gboolean     pan_charset_picker_has (const char * charset);

#endif

