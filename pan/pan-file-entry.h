/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * This is a quick-and-dirty substitute for the Gnome widget for
 * non-Gnome environments.  This should be compiled out on Gnome systems.
 */


#ifndef PAN_FILE_ENTRY_H
#define PAN_FILE_ENTRY_H

#include <gtk/gtkwidget.h>

GtkWidget*    pan_file_entry_new (const char * title);

GtkWidget*    pan_file_entry_gtk_entry (GtkWidget * w);

void          pan_file_entry_set (GtkWidget * w, const char * file);

const char*   pan_file_entry_get (GtkWidget * w);

#endif
