/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GROUPLIST_H__
#define __GROUPLIST_H__

#include <glib.h>

#include <pan/base/group.h>
#include <pan/base/pan-callback.h>

extern PanCallback * grouplist_group_selection_changed;

void       grouplist_update_font                            (void);
void       grouplist_refresh_nolock                         (void);

void       grouplist_select_next_unread_group               (void);
void       grouplist_select_next_group                      (void);
void       grouplist_activate_next_unread_group             (void);
void       grouplist_activate_next_group                    (void);
void       grouplist_select_all	                            (void);
void       grouplist_add_subscribed_to_selection_nolock     (void);
void       grouplist_deselect_all	                    (void);
GPtrArray* grouplist_get_selected_groups                    (void);
Group*     grouplist_get_selected_group                     (void);

void       grouplist_get_all	                            (void);

gpointer   grouplist_create                                 (void);
void       grouplist_shutdown_module                        (void);

#endif /* __GROUPLIST_H__ */
