/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2003  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/server.h>

#include <pan/task-state.h>

/*********************
**********************  Structs
*********************/

/*********************
**********************  Variables
*********************/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

void
task_state_set_work_need_socket (TaskState         * state,
                                 struct _Server    * server,
                                 TaskSocketFunc      socket_func)
{
	g_return_if_fail (state != NULL);
	g_return_if_fail (server_is_valid (server));

	state->server = server;
	state->socket_func = socket_func;
	state->work = TASK_NEED_SOCKET;
}

void
task_state_set_work_need_work (TaskState      * state,
                               TaskWorkFunc     work_func)
{
	g_return_if_fail (state != NULL);

	state->server = NULL;
	state->work_func = work_func;
	state->work = TASK_NEED_WORK;
}

void
task_state_set_work_working (TaskState * state)
{
	g_return_if_fail (state != NULL);

	state->server = NULL;
	state->work_func = NULL;
	state->socket_func = NULL;
	state->work = TASK_WORKING;
}

void
task_state_set_work_completed (TaskState * state)
{
	g_return_if_fail (state != NULL);

	state->server = NULL;
	state->work_func = NULL;
	state->socket_func = NULL;
	state->work = TASK_COMPLETED;
}

void
task_state_set_health (TaskState     * state,
                       TaskStateEnum   health)
{
	g_return_if_fail (state != NULL);
	g_return_if_fail (health==TASK_OK || health==TASK_FAIL || health==TASK_FAIL_NETWORK);

	state->health = health;
}
