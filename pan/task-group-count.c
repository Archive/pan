/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/nntp.h>
#include <pan/task-group-count.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

static void task_group_count_run (Task*, GIOChannel*);

static void task_group_count_describe (const StatusItem*, char* buf, int len);

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

#define DEAD_POINTER ((void*)(0xDEADBEEF))

/*********************
**********************  BEGINNING OF SOURCE
*********************/

static void
task_group_count_destructor (PanObject * o )
{
	TaskGroupCount * t = TASK_GROUP_COUNT(o);
	g_queue_free (t->groups);
	t->groups = DEAD_POINTER;

	/* destroy parent class */
	task_destructor (o);
}

/************
*************  PUBLIC ROUTINES
************/

PanObject*
task_group_count_new   (Group      ** groups,
                        guint         qty)
{
	guint i;
	TaskGroupCount * t = NULL;

	/* sanity checks */
	g_return_val_if_fail (groups!=NULL, NULL);
	g_return_val_if_fail (qty!=0u, NULL);

	/* create the object */
       	t = g_new0 (TaskGroupCount, 1);
        debug1 (DEBUG_PAN_OBJECT, "task_group_count: %p", t);

	/* initialize the parent class */
	task_constructor (TASK(t),
	                  TASK_TYPE_OTHER,
	                  task_group_count_destructor,
	                  task_group_count_describe,
	                  groups[0]->server,
	                  TRUE);

	/* initialize the task-bodies */
	t->groups = g_queue_new ();
	for (i=0; i<qty; ++i)
		g_queue_push_tail (t->groups, groups[i]);

	task_state_set_work_need_socket (&TASK(t)->state,
	                                 groups[0]->server,
	                                 task_group_count_run);

	return PAN_OBJECT(t);
}

/*****
******
*****/

static void
task_group_count_describe (const StatusItem * status, char * buf, int buflen)
{
	g_snprintf (buf, buflen,
	            _("Updating article counts for %u groups from server \"%s\""),
	            TASK_GROUP_COUNT(status)->groups->length,
	            TASK(status)->server->name.str);
}

/*****
******
*****/

static void
task_group_count_data (GIOChannel    * io,
                       gulong          estimated_qty,
                       gulong          low,
                       gulong          high,
                       GQuark          group_quark,
                       gpointer        user_data)
{
	Group * group = g_queue_peek_head (TASK_GROUP_COUNT(user_data)->groups);
	group_set_article_qty (group, estimated_qty);
}

static void
task_group_count_ran (GIOChannel    * channel,
                      TaskStateEnum   val,
                      gpointer        user_data)
{
	Task * task = TASK(user_data);
	TaskGroupCount * count = TASK_GROUP_COUNT(user_data);

	if (val == TASK_OK)
	{
		g_queue_pop_head (count->groups);

		if (!g_queue_is_empty (count->groups))
			task_group_count_run (task, channel);
		else {
			task_state_set_work_completed (&task->state);
			task_checkin_socket (task, channel, TRUE);
		}
	}
	else
	{
		task_state_set_health (&task->state, val);
		task_state_set_work_need_socket (&task->state, task->server, task_group_count_run);
		task_checkin_socket (task, channel, TRUE);
	}
}
static void
task_group_count_run (Task * task, GIOChannel * channel)
{
	TaskGroupCount * count = TASK_GROUP_COUNT(task);
	Group * group = g_queue_peek_head (count->groups);

	task_state_set_work_working (&task->state);

	nntp_group (channel,
	            group->name_quark,
	            task_group_count_data, task,
	            task_group_count_ran, task);
}
