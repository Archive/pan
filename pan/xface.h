#ifndef __PAN_XFACE_H__
#define __PAN_XFACE_H__

#include <gdk/gdktypes.h>

extern GdkPixbuf*   pan_gdk_pixbuf_create_from_x_face (const char *);

#endif
