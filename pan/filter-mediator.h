/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef FILTER_CURRENT_MEDIATOR__H
#define FILTER_CURRENT_MEDIATOR__H

#include <glib.h>
#include <gtk/gtkwidget.h>
#include <pan/base/pan-callback.h>

typedef struct
{
	GtkWidget * _match_only_new_ckm;
	GtkWidget * _match_only_unread_ckm;
	GtkWidget * _match_only_read_ckm;
	GtkWidget * _match_only_new_tb;

	GtkWidget * _match_only_complete_ckm;
	GtkWidget * _match_only_text_ckm;
	GtkWidget * _match_only_complete_tb;

	GtkWidget * _match_only_cached_ckm;
	GtkWidget * _match_only_cached_tb;

	GtkWidget * _match_only_mine_ckm;
	GtkWidget * _match_only_mine_tb;

	GtkWidget * _match_only_watched_rmi;
	GtkWidget * _match_only_high_rmi;
	GtkWidget * _match_only_medium_rmi;
	GtkWidget * _match_only_zero_rmi;
	GtkWidget * _match_only_low_rmi;
	GtkWidget * _match_only_ignored_rmi;
	GtkWidget * _match_only_watched_tb;

	GtkWidget * _show_articles_rmi;
	GtkWidget * _show_subthreads_rmi;
	GtkWidget * _show_threads_rmi;
}
FilterCurrentMediatorCtor;

void          filter_mediator_init                  (FilterCurrentMediatorCtor*);

void          filter_mediator_get_bits              (gulong      * setme_bits,
                                                     FilterShow  * setme_show);

void          filter_mediator_set_bits              (gulong        bits,
                                                     FilterShow    setme_show);

Filter*       filter_mediator_get_filter            (gulong        bits,
                                                     gboolean      negate);

PanCallback*  filter_mediator_get_change_callback   (void);

#endif
