/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __ARTICLE_LIST_H__
#define __ARTICLE_LIST_H__

#include <pan/base/article.h>
#include <pan/base/group.h>
#include <pan/base/pan-callback.h>

/**
***  Acting upon Selected Articles
***
*** "active" articles is the set of articles that an operation should
*** usually be performed on.  It's all the selected articles, plus if
*** any of the selected articles have collapsed subthreads, then also
*** the articles in those collapsed subthread.
*** 
*** For example, that way a `delete articles'  or `flag articles'
*** function would know to also operate on the collapsed subthreads
*** even though they're not selected.
**/

extern void header_pane_first_selected   (ArticleForeach, gpointer user_data);
extern void header_pane_foreach_selected (ArticleForeach, gpointer user_data);
extern void header_pane_foreach_active   (ArticleForeach, gpointer user_data);
extern void header_pane_forall_selected  (ArticleForall,  gpointer user_data, gboolean invoke_forall_even_if_no_selection);
extern void header_pane_forall_active    (ArticleForall,  gpointer user_data);
extern gboolean header_pane_has_selection (void);

/**
***  Navigating the Articles
**/

typedef gboolean (*ArticleTest)(const Article * article, gpointer test_func_user_data);
extern void header_pane_select_next_if (ArticleTest test_func, gpointer test_func_user_data);
extern void header_pane_select_prev_if (ArticleTest test_func, gpointer test_func_user_data);

extern void header_pane_read_next                 (void);
extern void header_pane_read_next_new             (void);
extern void header_pane_read_next_unread          (void);
extern void header_pane_read_next_score           (void);
extern void header_pane_read_next_thread          (void);
extern void header_pane_read_next_unread_thread   (void);
extern void header_pane_read_next_new_thread      (void);
extern void header_pane_read_prev                 (void);
extern void header_pane_read_prev_read            (void);
extern void header_pane_read_prev_thread          (void);
extern void header_pane_read_parent               (void);

/****
*****
*****
****/


extern Group*     articlelist_get_group                  (void);
extern gboolean   articlelist_has_prev_read              (void);
extern guint      articlelist_get_selected_count_nolock  (void);
extern gpointer   create_articlelist_ctree               (void);

extern void articlelist_set_threaded                     (gboolean threaded_on);

extern void articlelist_set_group                        (Group*);
extern void articlelist_read_selected                    (void);
extern void articlelist_activate_selected                (void);

extern void articlelist_update_columns                   (void);
extern void articlelist_reset_style_nolock               (void);

/**
***  Selection
**/

extern void articlelist_select_all_nolock                (void);
extern void articlelist_deselect_all_nolock              (void);
extern void articlelist_add_replies_to_selection_nolock  (void);
extern void articlelist_add_thread_to_selection_nolock   (void);
extern void articlelist_add_set_to_selection_nolock      (void);

extern void articlelist_selected_unflag_for_dl_nolock    (void);
extern void articlelist_selected_flag_for_dl_nolock      (void);
extern void articlelist_selected_copy_to_folder_nolock   (void);

extern void articlelist_expand_selected_threads          (void);
extern void articlelist_collapse_selected_threads        (void);

/****
*****  CALLBACKS
****/

/**
 * called whenever the article tree's active group changes.
 *
 * @param call_object the article ctree
 * @param call_arg the new active group
 */
extern PanCallback* articlelist_get_group_changed_callback (void);

/**
 * called whenever the article tree's selection state changes.
 *
 * @param call_object the article ctree
 * @param call_arg NULL
 */
extern PanCallback* articlelist_get_selection_changed_callback (void);


/***
****
***/


#endif /* __ARTICLE_LIST_H__ */
