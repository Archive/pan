/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <string.h>
#include <locale.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/pan-charset-picker.h>

#define PAN_DEFAULT_CHARSET	"ISO-8859-1"

static struct CharsetStruct {
    const char *charset, *name;
} charsets[] = {
	{"ISO-8859-4",   N_("Baltic")},
	{"ISO-8859-13",  N_("Baltic")},
	{"windows-1257", N_("Baltic")},
	{"ISO-8859-2",   N_("Central European")},
	{"windows-1250", N_("Central European")},
	{"gb2312",       N_("Chinese Simplified")},
	{"big5",         N_("Chinese Traditional")},
	{"ISO-8859-5",   N_("Cyrillic")},
	{"windows-1251", N_("Cyrillic")}, 
	{"KOI8-R",       N_("Cyrillic")},
	{"KOI8-U",       N_("Cyrillic, Ukrainian")},
	{"ISO-8859-7",   N_("Greek")},
	{"ISO-2022-jp",  N_("Japanese")},
	{"euc-kr",       N_("Korean")},
	{"ISO-8859-9",   N_("Turkish")},
	{"ISO-8859-1",   N_("Western")},
	{"ISO-8859-15",  N_("Western, New")},
	{"windows-1252", N_("Western")},
	{"UTF-8",        N_("Unicode, UTF-8")}
};



/**
 ** PUBLIC FUNCTIONS
 **/


GtkWidget *
pan_charset_picker_new (const char *charset)
{
	GtkWidget  * menu;
	GtkWidget  * w;
	gint         i;
	gint         charset_idx = -1;
	gint         locale_idx = -1;
	gint         default_idx = -1;
	const char * locale_charset = get_charset_from_locale ();

	menu = gtk_menu_new ();
	for (i=0; i<G_N_ELEMENTS(charsets); i++)
	{
		GtkWidget * menu_child;
		char        buf[512];

		g_snprintf (buf, sizeof buf, "%s (%s)", charsets[i].name, 
			charsets[i].charset);
		menu_child = gtk_menu_item_new_with_label (buf);
		g_object_set_data (G_OBJECT(menu_child), "charset", charsets+i);
		gtk_menu_shell_append (GTK_MENU_SHELL(menu), menu_child);

		/* match the passed charset
		 * for fallback, we'll need the locale's charset and, if
		 * that doesn't match, Pan's default charset */
		if (!g_strcasecmp (charsets[i].charset, charset))
			charset_idx = i;
		else
		if (!g_strcasecmp (charsets[i].charset, locale_charset))
			locale_idx = i;
		else
		if (!g_strcasecmp (charsets[i].charset, PAN_DEFAULT_CHARSET))
			default_idx = i;
	}

	w = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU(w), menu);
	if (charset_idx != -1)
		gtk_option_menu_set_history (GTK_OPTION_MENU(w), charset_idx);
	else
	if (locale_idx != -1)
		gtk_option_menu_set_history (GTK_OPTION_MENU(w), locale_idx);
	else
	if (default_idx != -1)
		gtk_option_menu_set_history (GTK_OPTION_MENU(w), default_idx);

	return w;
}

char *
pan_charset_picker_free (GtkWidget * w)
{
	return NULL;
}

gboolean
pan_charset_picker_set_charset (GtkWidget * w, const char * charset)
{
	gboolean retval = FALSE;
	gint     i;

	g_return_val_if_fail (w!=NULL, FALSE);

	for (i=0; i<G_N_ELEMENTS(charsets); i++)
		if (strcmp (charsets[i].charset, charset) == 0)
			break;

	if (i < G_N_ELEMENTS(charsets)) {
		gtk_option_menu_set_history (GTK_OPTION_MENU(w), i);
		retval = TRUE;
	}

	return retval;
}

const char *
pan_charset_picker_get_charset (GtkWidget * w)
{
	GtkWidget                  * menu;
	GtkWidget                  * menu_item;
	const struct CharsetStruct * charset;

	g_return_val_if_fail (w!=NULL, PAN_DEFAULT_CHARSET);

	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU(w));
	menu_item = gtk_menu_get_active (GTK_MENU(menu));
	charset = (const struct CharsetStruct *) g_object_get_data (G_OBJECT(menu_item), "charset");

	return charset ? charset->charset : PAN_DEFAULT_CHARSET;
}

gboolean
pan_charset_picker_has (const char * charset)
{
	gint i;
	gint n = G_N_ELEMENTS(charsets);

	if (charset == NULL)
		return FALSE;

	for (i=0; i<G_N_ELEMENTS(charsets); i++)
		if (!g_strcasecmp (charsets[i].charset, charset))
			break;

	return i!=n;
}

