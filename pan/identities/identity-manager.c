/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>

#include <pan/base/base-prefs.h>
#include <pan/base/debug.h>
#include <pan/base/pan-config.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/base/pan-callback.h>

#include <pan/identities/identity.h>
#include <pan/identities/identity-manager.h>
#include <pan/identities/identity-xml.h>

/***
****
***/

static GPtrArray * _identities     = NULL;
static gchar     * _default_news_id = NULL;
static gchar     * _default_mail_id = NULL;
/*static gboolean    dirty           = FALSE;*/

/***
****
***/

static gchar *
get_identity_filename (void)
{
	return g_build_filename (get_data_dir(), "profiles.xml", NULL);
}

/*
 * This is a migration function: first time around, no identities will
 * exist yet. So, we'll construct one from the preferences currently
 * set in ~/.gnome/Pan and add it to the _identities database.
 *
 * Since all settings will be removed from prefs.c, we'll directly
 * access the preferences through pan_config_get_string()
 */

static Identity * 
construct_identity_from_prefs (void)
{
	Identity * id = NULL;
	gchar    * real;
	gchar    * addr;

	debug_enter ("construct_identity_from_prefs");

	real = pan_config_get_string("/Pan/User/Full_Name", NULL);
	addr = pan_config_get_string("/Pan/User/Email", NULL);

	if (is_nonempty_string(real) && is_nonempty_string(addr))
	{
		gchar * pch;

		id = identity_new();

		pch = g_strdup_printf ("%s <%s>", real, addr);
		identity_set_name (id, pch);
		g_free (pch);

		identity_set_author_real (id, real);
		identity_set_author_addr (id, addr);
	
		pch = pan_config_get_string("/Pan/User/Reply_To", NULL);
		if (is_nonempty_string (pch))
			identity_set_reply_to (id, pch);
		g_free (pch);

		pch = pan_config_get_string("/Pan/User/Organization", NULL);
		if (is_nonempty_string (pch))
			identity_set_organization (id, pch);
		g_free (pch);

		pch = pan_config_get_string("/Pan/User/Signature_File", NULL);
		if (is_nonempty_string (pch))
			identity_set_signature (id, pch);
		g_free (pch);

		pch = pan_config_get_string ("/Pan/User/attribution_line", NULL);
		if (is_nonempty_string (pch))
			identity_set_attribution (id, pch);
		g_free (pch);
	}
	
	g_free (real);
	g_free (addr);

	return id;
}

void
identity_manager_save (void)
{
	if (_identities)
	{
		gchar * fname= get_identity_filename ();

		/* TODO: support for dirty */

		identity_xml_write (fname, (const Identity **) _identities->pdata,
			_identities->len, _default_news_id, _default_mail_id);

		g_free (fname);
	}
}

static const char*
get_default_identity_name (const char * old_default_identity_name)
{
	const char * name = NULL;

	if (name==NULL)
	{
		const Identity * id = identity_manager_get_identity_nocopy (old_default_identity_name);
		if (id != NULL)
			name = id->name;
	}

	if (name==NULL && _identities->len)
	{
		const Identity * id = (const Identity*) g_ptr_array_index (_identities, 0);
		if (id != NULL)
			name = id->name;
	}

	return name;
}

static void
identity_manager_module_init (void)
{
	char * fname;
	char * xml_news_id = NULL;
	char * xml_mail_id = NULL;
	const char * news_id = NULL;
	const char * mail_id = NULL;
	gboolean need_save = FALSE;

	debug_enter ("identity_manager_module_init");

	_identities = g_ptr_array_new ();

	/* read the file */
	fname = get_identity_filename ();
	identity_xml_read (fname, _identities, &xml_news_id, &xml_mail_id);
	replace_gstr (&fname, NULL);

	/* try to ensure that there's at least one identity */
	if (_identities->len == 0) {
		Identity * id = construct_identity_from_prefs(); 
		if (id) {
			g_ptr_array_add (_identities, id);
			need_save = TRUE;
		}
	}

	/* make sure we have valid defaults for the identities */
	news_id = get_default_identity_name (xml_news_id);
	mail_id = get_default_identity_name (xml_mail_id);
	if (!need_save)
		need_save = pan_strcmp (news_id, xml_news_id) || pan_strcmp (mail_id, xml_mail_id);
	replace_gstr (&_default_news_id, g_strdup(news_id));
	replace_gstr (&_default_mail_id, g_strdup(mail_id));

	/* if the identities or defaults have changed, save the file */
	if (need_save)
		identity_manager_save ();

	/* cleanup */
	g_free (xml_news_id);
	g_free (xml_mail_id);
	debug_exit ("identity_manager_module_init");
}

/***
****
***/

guint
identity_manager_count (void)
{
	debug_enter ("identity_manager_count");

	if (_identities == NULL)
		identity_manager_module_init ();

	debug_exit ("identity_manager_count");

	return _identities ? _identities->len : 0;
}

Identity *
identity_manager_get_identity_nocopy (const gchar * name)
{
	Identity * id = NULL;
	guint      i;
	debug_enter ("identity_manager_get_identity");

        if (_identities == NULL)
                identity_manager_module_init ();

        for (i=0; i<_identities->len; i++)
        {
                id = g_ptr_array_index (_identities, i);

                if (!pan_strcmp(id->name, name))
			break;
        }

	debug_exit ("identity_manager_get_identity");
	return id;
}

Identity *
identity_manager_get_identity (const gchar * name)
{
	Identity *id = NULL;
	debug_enter ("identity_manager_get_identity");

	id = identity_manager_get_identity_nocopy (name);
	if (id != NULL)
		id = identity_dup (id);

	debug_exit ("identity_manager_get_identity");
	return id;
}

Identity *
identity_manager_get_identity_by_author (const gchar * author_real,
                                         const gchar * author_addr)
{
	Identity * id = NULL;
	guint      i;
	debug_enter ("identity_manager_get_identity_by_author");

	if (_identities == NULL)
		identity_manager_module_init ();

	for (i=0; i<_identities->len; i++)
	{
		Identity *test = g_ptr_array_index (_identities, i);

		if (!pan_strcmp(author_real, test->author_real) &&
		    !pan_strcmp(author_addr, test->author_addr))
		{
			id = identity_dup (test);
			break;
		}
	}

	debug_exit ("identity_manager_get_identity_by_author");
	return id;
}

Identity *
identity_manager_get_default (int type)
{
	Identity    * id;
	const gchar * id_name;

	debug_enter ("identity_manager_get_default");

	if (_identities == NULL)
		identity_manager_module_init ();

	id_name = type == ID_MAIL_DEFAULT ? _default_mail_id : _default_news_id;

	id = identity_manager_get_identity (id_name);

	debug_exit ("identity_manager_get_default");
	return id;
}

static void
identity_manager_set_default_no_save (const gchar * id_name, int type)
{
	Identity  * id;
	gchar    ** default_id_name;

	debug_enter ("identity_manager_set_default_no_save");

	default_id_name = type == ID_MAIL_DEFAULT ? &_default_mail_id : 
                                                    &_default_news_id;

	if (is_nonempty_string (id_name))
	{
		id = identity_manager_get_identity (id_name);
		g_return_if_fail (id!=NULL);

		replace_gstr (default_id_name, g_strdup(id_name));

		pan_object_unref (PAN_OBJECT(id));
	}
	else
	{
		replace_gstr (default_id_name, NULL);
	}

	debug_exit ("identity_manager_set_default_no_save");
}

void
identity_manager_set_default (const gchar * id_name, int type)
{
	debug_enter ("identity_manager_set_default");

	identity_manager_set_default_no_save (id_name, type);
	identity_manager_save ();

	debug_exit ("identity_manager_set_default");
}

void
identity_manager_get_identities (GPtrArray * fillme)
{
	gint i;
	debug_enter ("identity_manager_get_identities");

	if (_identities == NULL)
		identity_manager_module_init ();

	for (i=0; i<_identities->len; ++i) {
		Identity * id = IDENTITY(g_ptr_array_index(_identities,i));
		g_ptr_array_add (fillme, identity_dup (id));
	}

	debug_exit ("identity_manager_get_identities");
}

void
identity_manager_set_identities (GPtrArray   * ids)
{
	guint i;
	GPtrArray * tmp;
	GPtrArray * old;

	/* make copies of the new */
	tmp = g_ptr_array_new ();
	for (i=0; i<ids->len; ++i) {
		Identity * id = IDENTITY(g_ptr_array_index(ids,i));
		Identity * dup = identity_dup (id);
		g_ptr_array_add (tmp, dup);
	}

	/* empty out the old */
	old = _identities;
	_identities = tmp;
	if (old != NULL) {
		pan_g_ptr_array_foreach (old, (GFunc)pan_object_unref, NULL);
		g_ptr_array_free (old, TRUE);
	}

	/* mark the defaults */
	identity_manager_set_default_no_save (get_default_identity_name (_default_news_id), ID_NEWS_DEFAULT);
	identity_manager_set_default_no_save (get_default_identity_name (_default_mail_id), ID_MAIL_DEFAULT);

	identity_manager_save ();

	/* fire a notice to listeners */
	pan_callback_call (identity_manager_get_identities_changed_callback(),
		_identities, NULL);
}

PanCallback*
identity_manager_get_identities_changed_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb == NULL) cb = pan_callback_new ();
	return cb;
}

void
identity_manager_add_identity (const Identity * id)
{
	debug_enter ("identity_manager_add_identity");

	if (_identities == NULL)
		identity_manager_module_init ();

	g_ptr_array_add (_identities, identity_dup (id));
	identity_manager_save ();

	debug_exit ("identity_manager_add_identity");
	
}

void
identity_manager_shutdown_module (void)
{
	debug_enter ("identity_manager_shutdown_module");

	identity_manager_save ();

	pan_g_ptr_array_foreach (_identities, (GFunc)pan_object_unref, NULL);
        g_ptr_array_free (_identities, TRUE);
	g_free (_default_news_id);
	g_free (_default_mail_id);

	debug_exit ("identity_manager_shutdown_module");
}

