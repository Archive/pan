/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __IDENTITY_XML_H__
#define __IDENTITY_XML_H__

#include <glib.h>
#include <pan/identities/identity.h>

void    identity_xml_read      (const char      * filename,
                                GPtrArray       * appendme_identities, 
				char           ** default_news_id,
				char           ** default_mail_id);

void    identity_xml_write     (const char      * filename,
                                const Identity ** identities,
                                int               identities_qty,
				const char      * default_news_id,
				const char      * default_mail_id);

char*   identity_xml_to_string (const Identity ** identities,
                                int               identities_qty,
                                const char      * default_news_id,
                                const char      * default_mail_id);


#endif
