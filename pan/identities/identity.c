/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/identities/identity.h>

/***
****
****/


void
identity_constructor (Identity * id)
{
	g_return_if_fail (id!=NULL);

	pan_object_constructor (PAN_OBJECT(id), identity_destructor);
	debug1 (DEBUG_PAN_OBJECT, "identity_ctor: %p", id);

	id->name = NULL;
	id->author_real = NULL;
	id->author_addr = NULL;
	id->signature = NULL;
	id->organization = NULL;
	id->reply_to = NULL;
	id->attribution = g_strdup (_("On %d, %n wrote:")); /* default attribution */
	id->msg_id_fqdn = NULL;
	id->posting_charset = NULL;
	id->custom_headers = g_ptr_array_new ();
}

void
identity_destructor (PanObject * obj)
{
	Identity * id = IDENTITY(obj);

	debug1 (DEBUG_PAN_OBJECT, "identity_dtor: %p", id);

	g_free (id->name);
	g_free (id->author_real);
	g_free (id->author_addr);
	g_free (id->signature);
	g_free (id->organization);
	g_free (id->reply_to);
	g_free (id->attribution);
	g_free (id->posting_charset);
	g_free (id->msg_id_fqdn);

	if (id->custom_headers != NULL)
	{
		gint i;

		for (i=0; i<id->custom_headers->len; i++)
		{
			Header * header = g_ptr_array_index (id->custom_headers, i);

			if (header != NULL)
			{
				g_free (header->name);
				g_free (header->value);
				g_free (header);
			}
		}
		g_ptr_array_free (id->custom_headers, TRUE);
	}

	pan_object_destructor (obj);
}

/***
****
****/

Identity *
identity_new (void)
{

	Identity * id= g_new0 (Identity, 1);
	identity_constructor (id);

	return id;
}

/*****
******
*****/

Identity *
identity_dup (const Identity * id)
{
	Identity * new_id;

	g_return_val_if_fail (id!=NULL, NULL);

	new_id = identity_new ();

	replace_gstr (&new_id->name, g_strdup (id->name));
	replace_gstr (&new_id->author_addr, g_strdup (id->author_addr));
	replace_gstr (&new_id->author_real, g_strdup (id->author_real));
	replace_gstr (&new_id->signature, g_strdup (id->signature));
	replace_gstr (&new_id->organization, g_strdup (id->organization));
	replace_gstr (&new_id->reply_to, g_strdup (id->reply_to));
	replace_gstr (&new_id->attribution, g_strdup (id->attribution));
	replace_gstr (&new_id->msg_id_fqdn, g_strdup (id->msg_id_fqdn));
	replace_gstr (&new_id->posting_charset, g_strdup (id->posting_charset));

	if (id->custom_headers!=NULL)
	{
		gint i;

		for (i=0; i<id->custom_headers->len; i++)
		{
			Header * src = g_ptr_array_index(id->custom_headers, i);
			Header * dst;

			if (src != NULL)
			{
 				dst = g_new0 (Header, 1);

				dst->name = g_strdup (src->name);
				dst->value = g_strdup (src->value);

				g_ptr_array_add (new_id->custom_headers, dst);
			}
		}
	}

	return new_id;
}

void
identity_set_name (Identity * id, const char * name)
{
	g_return_if_fail (id!=NULL);
	g_return_if_fail (is_nonempty_string(name));

	replace_gstr (&id->name, g_strdup (name));
}

void
identity_set_author_real (Identity * id, const char * real)
{
	char * pch;

	g_return_if_fail (id!=NULL);
	g_return_if_fail (is_nonempty_string(real));

	if (pan_strstr (real, "\"") != NULL) 
		pch = pan_substitute (real, "\"", "'");
	else
		pch = g_strdup (real);

	replace_gstr (&id->author_real, pch);
}

void
identity_set_author_addr (Identity * id, const char * addr)
{
	g_return_if_fail (id!=NULL);
	g_return_if_fail (is_nonempty_string(addr));

	replace_gstr (&id->author_addr, g_strdup (addr));
}

void
identity_set_reply_to (Identity * id, const char * addr)
{
	g_return_if_fail (id!=NULL);

	replace_gstr (&id->reply_to, g_strdup (addr));
}

void
identity_set_organization (Identity * id, const char * org)
{
	g_return_if_fail (id!=NULL);

	replace_gstr (&id->organization, g_strdup (org));
}

void
identity_set_signature (Identity * id, const char * sig)
{
	g_return_if_fail (id!=NULL);

	replace_gstr (&id->signature, g_strdup (sig));
}

void
identity_set_attribution (Identity * id, const char * attrib)
{
	g_return_if_fail (id!=NULL);

	replace_gstr (&id->attribution, g_strdup (attrib));
}

void
identity_set_msg_id_fqdn (Identity * id, const char * msg_id_fqdn)
{
	g_return_if_fail (id!=NULL);

	replace_gstr (&id->msg_id_fqdn, g_strdup (msg_id_fqdn));
}

void
identity_set_posting_charset (Identity * id, const char * posting_charset)
{
	g_return_if_fail (id!=NULL);

	replace_gstr (&id->posting_charset, g_strdup (posting_charset));
}

gboolean
identity_is_valid (const Identity * id)
{
	gint i;
	debug_enter ("identity_is_valid");

	if (id==NULL) return FALSE;
	if (!is_nonempty_string(id->name)) return FALSE;
	if (!is_nonempty_string(id->author_real)) return FALSE;
	if (!is_nonempty_string(id->author_addr)) return FALSE;

	for (i=0; i<id->custom_headers->len; ++i)
	{
		const struct _Header * h = g_ptr_array_index (id->custom_headers,i);

		if (!is_nonempty_string(h->name)) return FALSE;
		if (!is_nonempty_string(h->value)) return FALSE;
	}

	debug_exit ("identity_is_valid");
	return TRUE;
}
