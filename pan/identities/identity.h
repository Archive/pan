/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __IDENTITY_H__
#define __IDENTITY_H__

#include <glib.h>

#include <pan/base/pan-object.h>

#define IDENTITY(x) ((Identity*)(x))
#define HEADER(x)   ((Header*)(x))

typedef struct _Header Header;

struct _Header
{
	char * name;
	char * value;
};

typedef struct _Identity Identity;

struct _Identity
{
	/* Parent */
	PanObject parent;

	/* Fields */
	char     * name;

	char     * author_real;
	char     * author_addr;
	char     * signature;
	char     * organization;
	char     * reply_to;
	char     * attribution;
	char     * msg_id_fqdn;
	char     * posting_charset;
	GPtrArray * custom_headers;
};

/**
*** Public 
**/

Identity * identity_new (void);

Identity * identity_dup (const Identity * id);

void       identity_set_name            (Identity * id, const char * name);
void       identity_set_author_real     (Identity * id, const char * real);
void       identity_set_author_addr     (Identity * id, const char * addr);
void       identity_set_reply_to        (Identity * id, const char * addr);
void       identity_set_organization    (Identity * id, const char * org);
void       identity_set_signature       (Identity * id, const char * sig);
void       identity_set_attribution     (Identity * id, const char * attrib);
void       identity_set_msg_id_fqdn     (Identity * id, const char * msg_id_fqdn);
void       identity_set_posting_charset (Identity * id, const char * posting_charset);

gboolean   identity_is_valid         (const Identity *);

/**
***  Protected
**/ 

void       identity_constructor (Identity            * identity);
void       identity_destructor  (PanObject           * identity);

#endif

