/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __IDENTITY_MANAGER_H__
#define __IDENTITY_MANAGER_H__

#include <pan/base/pan-callback.h>
#include <pan/identities/identity.h>

enum {
	ID_NEWS_DEFAULT,
	ID_MAIL_DEFAULT
};

void       identity_manager_shutdown_module (void);

void       identity_manager_save (void);

guint      identity_manager_count (void);


Identity * identity_manager_get_identity (const gchar * name);

Identity * identity_manager_get_identity_by_author (const gchar * author_real,
                                                    const gchar * author_addr);

/*
 * This is ugly, I know. Looking at this now, having the 
 * identity_manager make copies each identity was not the 
 * best approach. Should be replaced with a refcount mechanism
 */
Identity * identity_manager_get_identity_nocopy (const gchar * name);

Identity * identity_manager_get_default (int type);

void       identity_manager_set_default (const gchar * id_name, int type);

void       identity_manager_get_identities (GPtrArray * ids);

void       identity_manager_set_identities (GPtrArray   * ids);

void       identity_manager_add_identity (const Identity * id);

PanCallback * identity_manager_get_identities_changed_callback (void);

#endif

