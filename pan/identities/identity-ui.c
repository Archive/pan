/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/base-prefs.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/identities/identity.h>
#include <pan/identities/identity-ui.h>
#include <pan/identities/identity-edit-ui.h>
#include <pan/identities/identity-manager.h>

#include <pan/util.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

typedef struct
{
	GtkWidget * identity_clist;
	GtkWidget * dialog;
	GPtrArray * identities;

	GtkWidget * copy_button;
	GtkWidget * remove_button;
	GtkWidget * edit_button;
}
IdentityListDialog;

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PRIVATE
************/

static gint
get_selected_index (IdentityListDialog * d)
{
	gint sel = -1;

	/* find the selected identity */
	GtkCList * clist = GTK_CLIST(d->identity_clist);
	if (clist->selection != NULL)
		sel = GPOINTER_TO_INT(clist->selection->data);

	return sel;
}                                                                                                                               

static void
button_refresh (IdentityListDialog * d)
{
	const gint sel = get_selected_index (d);
	const gboolean have_sel = sel != -1;

	gtk_widget_set_sensitive (d->copy_button, have_sel);
	gtk_widget_set_sensitive (d->edit_button, have_sel);
	gtk_widget_set_sensitive (d->remove_button, have_sel);
}

static void
clist_refresh (IdentityListDialog * d)
{
	gint i;
	const gint sel = get_selected_index (d);
	GtkCList * clist = GTK_CLIST(d->identity_clist);

	gtk_clist_freeze (clist);
	gtk_clist_clear (clist);
	for (i=0; i<d->identities->len; ++i)
	{
		gint row;
		Identity * id = IDENTITY(g_ptr_array_index(d->identities,i));
	       	row = gtk_clist_insert (clist, -1, &id->name);
		gtk_clist_set_row_data (clist, row, id);
	}
	gtk_clist_select_row (clist, sel, 0);
	gtk_clist_thaw (clist);
}

/*****
******
*****/

static void
identity_dialog_response_cb (GtkDialog * dialog, int response, gpointer user_data)
{
	IdentityListDialog * d = (IdentityListDialog*) user_data;

	/* save changed identities */
	identity_manager_set_identities (d->identities);

	/* cleanup identities */
	if (d->identities != NULL) {
		pan_g_ptr_array_foreach (d->identities, (GFunc)pan_object_unref, NULL);
		g_ptr_array_free (d->identities, TRUE);
		d->identities = NULL;
	}

	/* cleanup dialog */
	gtk_widget_destroy (GTK_WIDGET(dialog));
	g_free (d);
}


/**
*** Remove
**/

static void
remove_button_clicked_cb (GtkButton * button, gpointer data)
{
	IdentityListDialog * d = (IdentityListDialog*) data;
	gint sel = get_selected_index (d);
	if (sel != -1)
	{
		if (d->identities->len <= 1)
		{
			pan_error_dialog_parented (d->dialog,
				_("You need at least one profile"));
		}
		else
		{
			Identity * id;

			id = IDENTITY(g_ptr_array_index(d->identities, sel));

			g_ptr_array_remove_index (d->identities, sel);
			pan_object_unref (PAN_OBJECT(id));

			clist_refresh (d);
			button_refresh (d);

		}
	}
}

/**
*** Add
**/

static void
add_dialog_response_cb (GtkDialog * dialog, gint response, gpointer data)
{
	if (response == GTK_RESPONSE_OK)
	{
		guint                i;
		GtkWidget *          w = GTK_WIDGET(dialog);
		IdentityListDialog * d = (IdentityListDialog*) data;
		Identity           * id;
		GError             * err = NULL;

		id = identity_edit_dialog_get_identity (w, &err);

		/* make sure we have an identity */
		if (err!=NULL) {
			pan_error_dialog_parented (dialog, err->message);
			g_error_free (err);
		}
		if (err!=NULL || id==NULL)
			return;

		/* see if we've already got this identity */
		for (i=0; i!=d->identities->len; ++i)
		{
			Identity * id_old;

			id_old = IDENTITY(g_ptr_array_index(d->identities,i));
			if (!pan_strcmp(id->name, id_old->name))
					break;
		}

		/* either insert or update */
		if (i == d->identities->len)
		{
			g_ptr_array_add (d->identities, id);
		}
		else
		{
			Identity * id_old;

			id_old = IDENTITY(g_ptr_array_index(d->identities,i));
			g_ptr_array_index (d->identities,i) = id;
			pan_object_unref (PAN_OBJECT(id_old));
		}

		/* rebuild the clist */
		clist_refresh (d);
	}

	gtk_widget_destroy (GTK_WIDGET(dialog));
}

static void
add_button_clicked_cb (GtkButton * button, gpointer data)
{
	IdentityListDialog * f = (IdentityListDialog*) data;
	GtkWidget * dialog = identity_edit_dialog_new (GTK_WINDOW(f->dialog), NULL);
	gtk_signal_connect (GTK_OBJECT(dialog), "response", GTK_SIGNAL_FUNC(add_dialog_response_cb), f);
	gtk_widget_show_all (dialog);
}

/**
*** Copy 
**/

static void
copy_button_clicked_cb (GtkButton *button, gpointer data)
{
	IdentityListDialog * d = (IdentityListDialog*) data;
	gint sel = get_selected_index (d);
	if (sel != -1)
	{
		const Identity * id_old;
		Identity       * id_new;
		GtkWidget      * dialog;

		id_old = IDENTITY(g_ptr_array_index(d->identities,sel));
		id_new = identity_dup(id_old);
		replace_gstr (&id_new->name, g_strdup(_("New Profile")));
		dialog = identity_edit_dialog_new (GTK_WINDOW(d->dialog), id_new);
		pan_object_unref (PAN_OBJECT (id_new));
		g_signal_connect (GTK_OBJECT(dialog), "response", GTK_SIGNAL_FUNC(add_dialog_response_cb), d);
		gtk_widget_show_all (dialog);

	}
}

/**
***  Edit
**/

static void
edit_dialog_response_cb (GtkDialog * dialog, gint response, gpointer data)
{
	gboolean do_close = TRUE;

	if (response == GTK_RESPONSE_OK)
	{
		IdentityListDialog * d = (IdentityListDialog*) data;
		gint sel = get_selected_index (d);
		if (sel != -1)
		{ 
			GError * err = NULL;
			GtkWidget * w = GTK_WIDGET(dialog);
			Identity * id_old = IDENTITY(g_ptr_array_index(d->identities,sel));
			Identity * id_new = identity_edit_dialog_get_identity (w, &err);

			/* make sure we have an identity */
			if (err != NULL)
			{
				pan_error_dialog_parented (dialog, err->message);
				g_error_free (err);
				do_close = FALSE;
			}
			else if (id_new != NULL)
			{
				g_ptr_array_index(d->identities,sel) = id_new;

				pan_object_unref (PAN_OBJECT(id_old));

				clist_refresh (d);
			}
		}
	}

	if (do_close)
		gtk_widget_destroy (GTK_WIDGET(dialog));
}
static void
edit_button_clicked_cb (GtkButton * button, gpointer data)
{
	IdentityListDialog * d = (IdentityListDialog*) data;
	gint sel = get_selected_index (d);
	if (sel != -1)
	{
		const Identity * id_old = IDENTITY(g_ptr_array_index(d->identities,sel));
		GtkWidget * dialog;

		dialog = identity_edit_dialog_new (GTK_WINDOW(d->dialog), id_old);
		gtk_signal_connect (GTK_OBJECT(dialog), "response", GTK_SIGNAL_FUNC(edit_dialog_response_cb), d);
		gtk_widget_show_all (dialog);
	}
}
static gboolean
identity_clist_button_press_cb (GtkWidget * w, GdkEventButton * b, gpointer data)
{
	if (b->button==1 && b->type==GDK_2BUTTON_PRESS)
		edit_button_clicked_cb (NULL, data);
	return FALSE;
}

static void     
list_selection_changed_cb (GtkCList          * clist,
                           gint                row,
                           gint                column,
                           GdkEventButton    * event,
                           gpointer            user_data)
{
	button_refresh ((IdentityListDialog*)user_data);
}


/************
*************  PROTECTED
************/

/************
*************  PUBLIC
************/

GtkWidget*
identity_dialog_new (GtkWindow * parent)
{
	GtkWidget * w;
	GtkWidget * hbox;
	GtkWidget * bbox;
	GtkTooltips * tips = gtk_tooltips_new ();
	IdentityListDialog * d = g_new0 (IdentityListDialog, 1);
	char * titles [1];

	/* load identities */
	d->identities = g_ptr_array_new ();
	identity_manager_get_identities (d->identities);

	/* dialog */
	w = d->dialog = gtk_dialog_new_with_buttons (_("Pan: Profile"), parent,
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_STOCK_CLOSE, GTK_RESPONSE_NONE,
			NULL);
	gtk_window_set_policy (GTK_WINDOW(w), TRUE, TRUE, TRUE);
	g_signal_connect (GTK_OBJECT(w), "response", G_CALLBACK(identity_dialog_response_cb), d);

	/* workarea */
	hbox = gtk_hbox_new (FALSE, GUI_PAD);
	gtk_container_set_border_width (GTK_CONTAINER(hbox), 12);
	gtk_box_pack_start (GTK_BOX(GTK_DIALOG(w)->vbox), hbox, TRUE, TRUE, 0);

	/* clist */
	titles[0] = _("Profile");
	w = d->identity_clist= gtk_clist_new_with_titles (1, titles);
	gtk_clist_set_selection_mode (GTK_CLIST(w), GTK_SELECTION_BROWSE);
	gtk_signal_connect (GTK_OBJECT(w), "button_press_event",
	                    GTK_SIGNAL_FUNC(identity_clist_button_press_cb), d);
	gtk_signal_connect (GTK_OBJECT(w), "select_row",
	                    GTK_SIGNAL_FUNC(list_selection_changed_cb), d);
	gtk_signal_connect (GTK_OBJECT(w), "unselect_row",
	                    GTK_SIGNAL_FUNC(list_selection_changed_cb), d);


        w = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(w), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER(w), d->identity_clist);
	gtk_box_pack_start (GTK_BOX (hbox), w, TRUE, TRUE, 0);
	gtk_widget_set_usize (w, 300, 300);

	/* button box */
	bbox = gtk_vbox_new (FALSE, GUI_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (hbox), bbox, FALSE, FALSE, 0);

	/* add button */
	w = gtk_button_new_from_stock (GTK_STOCK_ADD);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_tooltips_set_tip (tips, w, _("Add a new profile"), NULL);
	gtk_signal_connect (GTK_OBJECT(w), "clicked", GTK_SIGNAL_FUNC(add_button_clicked_cb), d);

	/* copy from button */
	w = gtk_button_new_from_stock (GTK_STOCK_COPY);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_tooltips_set_tip (tips, w, _("Use the selected profile to create a new profile"), NULL);
	gtk_signal_connect (GTK_OBJECT(w), "clicked", GTK_SIGNAL_FUNC(copy_button_clicked_cb), d);
	d->copy_button = w;

	/* edit button */
	w = gtk_button_new_from_stock (GTK_STOCK_OPEN);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_tooltips_set_tip (tips, w, _("Edit the selected profile"), NULL);
	gtk_signal_connect (GTK_OBJECT(w), "clicked", GTK_SIGNAL_FUNC(edit_button_clicked_cb), d);
	d->edit_button = w;

	/* remove button */
	w = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_tooltips_set_tip (tips, w, _("Remove the selected profile"), NULL);
	gtk_signal_connect (GTK_OBJECT(w), "clicked", GTK_SIGNAL_FUNC(remove_button_clicked_cb), d);
	d->remove_button = w;

	clist_refresh (d);
	button_refresh (d);
	return d->dialog;
}
