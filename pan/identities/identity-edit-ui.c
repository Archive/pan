/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <gmime/internet-address.h>

#include <pan/base/debug.h>
#include <pan/base/gnksa.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/identities/identity.h>
#include <pan/identities/identity-edit-ui.h>

#include <pan/pan-file-entry.h>
#include <pan/pan-charset-picker.h>

#include <pan/util.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

typedef struct
{
	GtkWidget * dialog;

	GtkWidget * name_entry;

	GtkWidget * author_real_entry;
	GtkWidget * author_addr_entry;

	GtkWidget * reply_to_entry;
	GtkWidget * organization_entry;
	GtkWidget * signature_entry;
	GtkWidget * attribution_entry;
	GtkWidget * msg_id_fqdn_entry;
	GtkWidget * posting_charset_entry;

	GtkWidget * custom_headers_text;

	gchar     * identity_name;
}
IdentityEditDialog;

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

extern GtkTooltips * ttips;

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PRIVATE
***********/

/*****
******
******  IDENTITY INFO
******
*****/

static GtkWidget*
create_identity_tab (IdentityEditDialog *d)
{
	GtkWidget * t;
	GtkWidget * w;
	int row;

	row = 0;
	t = pan_hig_workarea_create ();

	pan_hig_workarea_add_section_title (t, &row, _("Profile Information"));
	
		pan_hig_workarea_add_section_spacer (t, row, 1);

		w = d->name_entry = gtk_entry_new ();
		pan_hig_workarea_add_row (t, &row, _("_Profile Name:"), w, NULL);

	pan_hig_workarea_add_section_divider (t, &row);
	pan_hig_workarea_add_section_title (t, &row, _("Required Information"));

		pan_hig_workarea_add_section_spacer (t, row, 2);

		w = d->author_real_entry = gtk_entry_new ();
		pan_hig_workarea_add_row (t, &row, _("_Full Name:"), w, NULL);

		w = d->author_addr_entry = gtk_entry_new ();
		pan_hig_workarea_add_row (t, &row, _("_Email Address:"), w, NULL);

	pan_hig_workarea_add_section_divider (t, &row);
	pan_hig_workarea_add_section_title (t, &row, _("Optional Information"));

		pan_hig_workarea_add_section_spacer (t, row, 6);

		w = d->reply_to_entry = gtk_entry_new ();
		gtk_tooltips_set_tip (GTK_TOOLTIPS(ttips), w, _("Used when email replies to your posts should go somewhere other than the email address above.  Because Reply-To: is relatively difficult for spammers to harvest, it's polite give your email address here if you gave an invalid email address above."), "");
		pan_hig_workarea_add_row (t, &row, _("_Reply-To Header:"), w, NULL);

		w = d->organization_entry = gtk_entry_new ();
		pan_hig_workarea_add_row (t, &row, _("Or_ganization Header:"), w, NULL);

		w = d->msg_id_fqdn_entry = gtk_entry_new ();
		gtk_tooltips_set_tip (GTK_TOOLTIPS(ttips), w, _("When posting to Usenet, your article's Message-ID contains a domain name.  You can set a custom domain name here, or leave it blank to let Pan use the domain name from your email address."), "");
		pan_hig_workarea_add_row (t, &row, _("Message-ID _Domain Name:"), w, NULL);

		w = d->attribution_entry = gtk_entry_new ();
		gtk_tooltips_set_tip (GTK_TOOLTIPS(ttips), w, _("%i for Message-ID\n%a for Author and Address\n%n for Author name\n%d for Date"), "");
		pan_hig_workarea_add_row (t, &row, _("_Attribution:"), w, NULL);

		w = d->signature_entry = pan_file_entry_new (_("Select Signature File"));
		pan_hig_workarea_add_row (t, &row, _("_Signature File:"), w, NULL);

		w = d->posting_charset_entry = pan_charset_picker_new ("");
		gtk_tooltips_set_tip (GTK_TOOLTIPS(ttips), w, _("The default charset set for articles posted with this profile"), "");
		pan_hig_workarea_add_row (t, &row, _("C_haracter Set:"), w, NULL);

	return t;
}

static void
set_entry_from_struct_field (GtkWidget      * entry,
                             gconstpointer    structure,
                             int              struct_offset,
                             const char     * default_text)
{
	const char * cpch = NULL;
	
	g_assert (GTK_IS_ENTRY (entry));

	if (structure != NULL) 
		cpch = G_STRUCT_MEMBER (char*, structure, struct_offset);
	if (!is_nonempty_string (cpch))
		cpch = default_text;
	if (cpch == NULL)
		cpch = "";
	pan_gtk_entry_set_text (entry, cpch);
}

static void
populate_identity_tab (IdentityEditDialog * d,
                       const Identity     * id)
{
	set_entry_from_struct_field (d->name_entry, id, G_STRUCT_OFFSET(Identity,name), "");
	set_entry_from_struct_field (d->author_real_entry, id, G_STRUCT_OFFSET(Identity,author_real), "");
	set_entry_from_struct_field (d->author_addr_entry, id, G_STRUCT_OFFSET(Identity,author_addr), "");
	set_entry_from_struct_field (d->reply_to_entry, id, G_STRUCT_OFFSET(Identity,reply_to), "");
	set_entry_from_struct_field (d->msg_id_fqdn_entry, id, G_STRUCT_OFFSET(Identity,msg_id_fqdn), "");
	set_entry_from_struct_field (d->organization_entry, id, G_STRUCT_OFFSET(Identity,organization), "");
	set_entry_from_struct_field (d->attribution_entry, id, G_STRUCT_OFFSET(Identity,attribution), _("On %d, %n wrote:"));
	set_entry_from_struct_field (pan_file_entry_gtk_entry(d->signature_entry), id, G_STRUCT_OFFSET(Identity,signature), "");

	pan_charset_picker_set_charset (d->posting_charset_entry, id!=NULL && is_nonempty_string (id->posting_charset)
	                                                          ?  id->posting_charset
	                                                          : get_charset_from_locale ());
}

static void
replace_string_from_editable (char ** old_string, GtkWidget * editable, gboolean do_trim)
{
	char * pch;

	g_assert (old_string != NULL);
	g_assert (GTK_IS_EDITABLE(editable));

	pch = gtk_editable_get_chars (GTK_EDITABLE(editable), 0, -1);
	if (do_trim)
		g_strstrip (pch);
	replace_gstr (old_string, pch);
}

static void
identity_from_identity_tab (IdentityEditDialog * d, 
                            Identity           * id)
{
	const char * cpch;

	g_return_if_fail (d!=NULL);
	g_return_if_fail (id!=NULL);

	replace_string_from_editable (&id->name,            d->name_entry,            TRUE);
	replace_string_from_editable (&id->author_real,     d->author_real_entry,     TRUE);
	replace_string_from_editable (&id->author_addr,     d->author_addr_entry,     TRUE);
	replace_string_from_editable (&id->reply_to,        d->reply_to_entry,        TRUE);
	replace_string_from_editable (&id->msg_id_fqdn,     d->msg_id_fqdn_entry,     TRUE);
	replace_string_from_editable (&id->organization,    d->organization_entry,    TRUE);
	replace_string_from_editable (&id->attribution,     d->attribution_entry,     TRUE);
	replace_string_from_editable (&id->signature,       pan_file_entry_gtk_entry(d->signature_entry),       TRUE);

	cpch = pan_charset_picker_get_charset (d->posting_charset_entry);
	replace_gstr (&id->posting_charset, g_strdup (cpch));
	g_strstrip (id->posting_charset);
}

/*****
******
******  CUSTOM HEADERS
******
*****/

static GtkWidget*
create_headers_tab (IdentityEditDialog *d)
{
	GtkWidget * h;
	GtkWidget * v;
	GtkWidget * w;
	GtkWidget * l;
	GtkWidget * scroll;
	char buf[512];

	v = gtk_vbox_new (FALSE, GUI_PAD);
	gtk_container_set_border_width (GTK_CONTAINER(v), 12);

		g_snprintf (buf, sizeof(buf), "<b>%s</b>", _("Extra Headers"));
		l = gtk_label_new (buf);
		gtk_misc_set_alignment (GTK_MISC(l), 0.0f, 0.5f);
		gtk_label_set_use_markup (GTK_LABEL(l), TRUE);
		gtk_box_pack_start (GTK_BOX(v), l, FALSE, FALSE, 0);

		h = gtk_hbox_new (FALSE, GUI_PAD);
		gtk_box_pack_start (GTK_BOX(v), h, TRUE, TRUE, 0);

			w = gtk_alignment_new (0.0f, 0.0f, 0.0f, 0.0f);
			gtk_widget_set_usize (w, 18u, 0u);
			gtk_box_pack_start (GTK_BOX(h), w, FALSE, FALSE, 0);

			w = d->custom_headers_text = gtk_text_view_new ();
			scroll = gtk_scrolled_window_new (NULL, NULL);
			gtk_container_set_border_width (GTK_CONTAINER(scroll), GUI_PAD_SMALL);
			gtk_container_add (GTK_CONTAINER(scroll), w);
			gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
							GTK_POLICY_AUTOMATIC,
							GTK_POLICY_AUTOMATIC);
			w = gtk_frame_new (NULL);
			gtk_frame_set_shadow_type (GTK_FRAME(w), GTK_SHADOW_IN);
			gtk_container_add (GTK_CONTAINER(w), scroll);

			gtk_box_pack_start (GTK_BOX(h), w, TRUE, TRUE, 0);
	
	return v;
}

static void
populate_headers_tab (IdentityEditDialog *d, const Identity *id)
{
	g_return_if_fail (d!=NULL);

	if (id!=NULL && id->custom_headers)
	{
		GtkTextBuffer * buffer;
		GtkTextIter     start;
		GtkTextIter     end;
		GString       * headers = g_string_new (NULL);
		guint           i;

		for (i=0; i!=id->custom_headers->len; ++i) {
			const Header * h = g_ptr_array_index (id->custom_headers, i);
			g_string_append_printf (headers, "%s: %s\n", h->name, h->value);
		}

		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW(d->custom_headers_text));
		gtk_text_buffer_get_bounds (buffer, &start, &end);
		gtk_text_buffer_delete (buffer, &start, &end);
		gtk_text_buffer_insert (buffer, &start, headers->str, headers->len);

		g_string_free (headers, TRUE);
	}
	
}

static void
identity_from_headers_tab (IdentityEditDialog *d, Identity *id)
{
	char  * text;
	int     i;
	char ** lines;

	g_return_if_fail (d!=NULL);
	g_return_if_fail (id!=NULL);
	g_return_if_fail (id->custom_headers!=NULL);

	/* Clear all current custom headers */
	for (i=0; i<id->custom_headers->len; i++)
	{
		Header * h = g_ptr_array_index (id->custom_headers, i);

		g_free (h->name);
		g_free (h->value);
		g_free (h);
		g_ptr_array_remove (id->custom_headers, h);
	}

	/* get custom headers from textview */
	if (1)
	{
		GtkTextBuffer * buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW(d->custom_headers_text));
		GtkTextIter start;
		GtkTextIter end;
		gtk_text_buffer_get_bounds (buffer, &start, &end);
		text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
	}

	lines = g_strsplit (text, "\n", -1);
	for (i=0; lines!=NULL && lines[i]!=NULL; i++)
	{
		gchar * delimit;

		g_strstrip (lines[i]);
		delimit = strchr (lines[i], ':');

		if (delimit != NULL)
		{
			Header * h = g_new0 (Header, 1);
			gchar  * n = g_strndup (lines[i], delimit - lines[i]);
			gchar  * v = g_strdup (delimit + 1);

			g_strstrip (n);
			g_strstrip (v);

			h->name = n;
			h->value = v;

			g_ptr_array_add (id->custom_headers, h);
		}
	}

	/* cleanup */
	g_free (text);
	g_strfreev (lines);
}

/*****
******
******  
******
*****/

static gint
dialog_destroy_cb (GtkDialog * dialog, gpointer data)
{
	IdentityEditDialog * d = (IdentityEditDialog*) data;
	g_free (d->identity_name);
	g_free (d);
	return FALSE;
}
 
GtkWidget*
identity_edit_dialog_new (GtkWindow      * window,
                          const Identity * id)
{
	GtkWidget *w, *n, *d_v;
	IdentityEditDialog * d;

	d = g_new0 (IdentityEditDialog, 1);

	/* dialog */

	w = d->dialog = gtk_dialog_new_with_buttons (_("Pan: Edit Profile"), window,
	                                             GTK_DIALOG_DESTROY_WITH_PARENT,
	                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                                             GTK_STOCK_OK, GTK_RESPONSE_OK,
	                                             NULL);
	d_v = GTK_DIALOG(w)->vbox;
	gtk_window_set_policy (GTK_WINDOW(w), TRUE, TRUE, TRUE);
	gtk_signal_connect (GTK_OBJECT(w), "destroy", GTK_SIGNAL_FUNC(dialog_destroy_cb), d);
	gtk_object_set_user_data (GTK_OBJECT(w), d);

	/* notebook */
	n = gtk_notebook_new ();
	w = create_identity_tab (d);
	gtk_container_add (GTK_CONTAINER(n), w);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK(n), w, gtk_label_new_with_mnemonic (_("_User Profile")));
	w = create_headers_tab (d);
	gtk_container_add (GTK_CONTAINER(n), w);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK(n), w, gtk_label_new_with_mnemonic (_("Cus_tom Headers")));
	gtk_container_set_border_width (GTK_CONTAINER(n), 12);
	gtk_box_pack_start (GTK_BOX(d_v), n, TRUE, TRUE, GUI_PAD_SMALL);

	/* populate */
	populate_identity_tab (d, id);
	populate_headers_tab (d, id);

	return d->dialog;
}

/*
 * FIXME: move to base? 
 */
static gboolean
identity_edit_check_address (const char *name, const char *address)
{
	InternetAddress * iaddr = internet_address_new_name (name, address);
	gboolean retval = FALSE;

	if (iaddr) {
		char *pch = internet_address_to_string (iaddr, TRUE);

		if (pch) {
			retval = GNKSA_OK == gnksa_check_from(pch, TRUE);
			g_free (pch);
		}
		
		internet_address_unref (iaddr);
	}

	return retval;
}

Identity*
identity_edit_dialog_get_identity (GtkWidget * w, GError ** err)
{
	Identity           * id = NULL;
	IdentityEditDialog * d  = (IdentityEditDialog*) gtk_object_get_user_data (GTK_OBJECT(w));

	g_assert (w!=NULL);
	g_assert (d!=NULL);
	g_return_val_if_fail (*err==NULL, NULL);
	
	id = identity_new ();
	identity_from_identity_tab (d, id);
	identity_from_headers_tab (d, id);

	/* make sure user didn't skip on any of the essential parts */
	if (!*err && !identity_is_valid (id))
		*err = g_error_new (g_quark_from_string("identity-ui"), 1, _("You must specify a username or disable authorization."));

	/* double-quotes in the real name aren't allowed: translate to single */
	if (!*err && id!=NULL && is_nonempty_string(id->author_real) && strchr(id->author_real,'"')!=NULL) {
		replace_gstr (&id->author_real, pan_substitute (id->author_real, "\"", "'"));
		pan_info_dialog (_("Your real name contained double quotes.\nConverting into single quotes."));
	}

	/* is the source email address ok? */
	if (!*err) {
		if (!identity_edit_check_address(id->author_real, id->author_addr))
			*err = g_error_new (g_quark_from_string("identity-ui"), 1, _("Invalid address \"%s\" <%s>"), id->author_real, id->author_addr);
	}

	/* is the reply-to email address ok? */
	if (!*err && is_nonempty_string(id->reply_to)) {
		if (!identity_edit_check_address(NULL, id->reply_to))
			*err = g_error_new (g_quark_from_string("identity-ui"), 1, _("Invalid address <%s>"), id->reply_to);
	}

	if (*err) {
		pan_object_unref (PAN_OBJECT(id));
		id = NULL;
	}

	return id;
}
