#ifndef __SCORE_VIEW_UI_H__
#define __SCORE_VIEW_UI_H__

#include <gtk/gtkwidget.h>
#include <pan/base/article.h>

GtkWidget* score_view_dialog (GtkWidget*, Article*);

#endif
