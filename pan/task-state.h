/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2003  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __TASK_STATE__H_
#define __TASK_STATE__H_

#define TASK_STATE(a) ((TaskState *)a)

typedef enum
{
	TASK_COMPLETED            =  0, /* task has finished successfully */
	TASK_NEED_SOCKET          =  1, /* waiting on a socket */
	TASK_NEED_WORK            =  2, /* waiting to run */
	TASK_NEED_WORK_SUBTHREAD  =  3, /* waiting to run in a worker thread */
	TASK_WORKING              =  4  /* running */
}
TaskWorkEnum;

typedef enum
{
        TASK_OK                =  0,  /* ok -- no errors yet */
	TASK_FAIL              = -1,  /* failed, and should stopped until requeued by the user */
	TASK_FAIL_NETWORK      = -2   /* failed because of network trouble; keep it queued so that it'll complete itself when the network clears up */
}
TaskStateEnum;


struct _Task;
struct _Server;

typedef void (*TaskWorkFunc)(struct _Task*);
typedef void (*TaskSocketFunc)(struct _Task*, GIOChannel*);

typedef struct _TaskState TaskState;

struct _TaskState
{
	struct _Server * server;

	TaskSocketFunc socket_func;
	TaskWorkFunc work_func;

	TaskWorkEnum work;
	TaskStateEnum health;
};

/**
***  PUBLIC 
**/

void     task_state_set_work_completed      (TaskState         * task);

void     task_state_set_work_working        (TaskState         * task);

void     task_state_set_work_need_socket    (TaskState         * task,
                                             struct _Server    * server,
                                             TaskSocketFunc      socket_func);

void     task_state_set_work_need_work      (TaskState         * task,
                                             TaskWorkFunc        work_func);

void     task_state_set_health              (TaskState         * task,
                                             TaskStateEnum       state);


#endif /* __SOCKET_POOL_H__ */
