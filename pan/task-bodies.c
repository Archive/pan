/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include <pan/base/acache.h>
#include <pan/base/article.h>
#include <pan/base/debug.h>
#include <pan/base/log.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/nntp.h>
#include <pan/queue.h>
#include <pan/task-bodies.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

static void task_bodies_run (Task* item, GIOChannel * channel);

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

#define DEAD_POINTER ((void*)(0xDEADBEEF))

/*********************
**********************  BEGINNING OF SOURCE
*********************/

static void
task_bodies_describe (const StatusItem* si, char * buf, int buflen)
{
	GPtrArray * mids = TASK(si)->identifiers;

	if (mids->len != 1u)
		g_snprintf (buf, buflen, _("Downloading %u articles"), mids->len);
	else {
		MessageIdentifier * mid = MESSAGE_IDENTIFIER (g_ptr_array_index (mids, 0));
		g_strlcpy (buf, message_identifier_get_readable_name(mid), buflen);
		if (!g_utf8_validate (buf, -1, NULL))
			g_strlcpy (buf, _("Downloading 1 article"), buflen);
	}
}

/************
*************  Protected
************/

void
task_bodies_destructor (PanObject * obj)
{
	TaskBodies * task;
	gpointer p;
	debug_enter ("task_bodies_destructor");
	debug1 (DEBUG_PAN_OBJECT, "task-bodies destructor: %p", obj);

	/* sanity clause */
	g_return_if_fail (obj!=NULL);

	/* task-bodies dtor */
	{
		GPtrArray * mids = TASK(obj)->identifiers;
		const PString ** ids = message_identifiers_get_id_array ((const MessageIdentifier**)mids->pdata, mids->len);
		acache_checkin (ACACHE_DEFAULT_KEY, ids, mids->len);
		g_free (ids);
	}
	task = TASK_BODIES(obj);
	while ((p = g_queue_pop_head (task->mids)))
		g_object_unref (G_OBJECT(p));
	g_queue_free (task->mids);
        task->mids_running_qty = ~0;
        task->mids = DEAD_POINTER;
        task->done_work_func = DEAD_POINTER;


	/* destroy parent class */
	task_destructor (obj);

	debug_exit ("task_bodies_destructor");
}

void
task_bodies_constructor (Task                      * task,
                         gint8                       task_type,
                         PanObjectDestructor         dtor,
                         StatusItemDescribeFunc      describe,
                         Server                    * server,
                         gboolean                    high_priority,
                         MessageIdentifier        ** mids,
                         int                         mid_qty,
                         TaskWorkFunc                done_work_func)
{
	int i;
	int lines_total = 0;
	int lines_cached = 0;
	TaskBodies * bodies = TASK_BODIES(task);
	StatusItem * status = STATUS_ITEM(task);
	const PString ** ids = 0;
	debug_enter ("task_bodies_new");

	/* sanity clause */
	g_return_if_fail (mids!=NULL);
	g_return_if_fail (mid_qty!=0);

	/* initialize the parent class' bits */
	debug1 (DEBUG_PAN_OBJECT, "task-bodies constructor: %p", task);
	task_constructor (TASK(bodies),
	                  task_type,
	                  dtor,
	                  describe,
	                  server,
	                  high_priority);
	task_add_identifiers (TASK(bodies), mids, mid_qty);

	/* initialize the task_bodies bits */
	bodies->done_work_func = done_work_func;
	bodies->mids = g_queue_new ();
	bodies->mids_running_qty = 0;
	for (i=0; i<mid_qty; ++i) {
		MessageIdentifier * mid = mids[i];
		lines_total += mid->line_qty;
		if (acache_has_message (ACACHE_DEFAULT_KEY, &mid->message_id))
			lines_cached += mid->line_qty;
		else {
			g_queue_push_tail (bodies->mids, mid);
			g_object_ref (mid);
		}
	}

	/* check out all the articles */
	ids = message_identifiers_get_id_array ((const MessageIdentifier**)mids, mid_qty);
	acache_checkout (ACACHE_DEFAULT_KEY, ids, mid_qty);
	g_free (ids);

	/* set the work state */
	if (!g_queue_is_empty (bodies->mids))
		task_state_set_work_need_socket (&task->state, server, task_bodies_run);
	else if (done_work_func == NULL)
		task_state_set_work_completed (&task->state);
	else
		task_state_set_work_need_work (&task->state, done_work_func);

	/* init the status-item */
	status_item_emit_init_steps (status, lines_total);
	status_item_emit_set_step (status, lines_cached);
	if (mid_qty > 1)
		status_item_emit_status_va (status, _("Downloading %d articles"), mid_qty);
	else
		status_item_emit_status_va (status, _("\"%s\""), message_identifier_get_readable_name (mids[0]));

	/* return the task */
	debug_exit ("task_bodies_constructor");
}

/************
*************  PUBLIC ROUTINES
************/

PanObject*
task_bodies_new (Server               * server,
                 MessageIdentifier   ** mids,
                 int                    mid_qty)
{
	TaskBodies * bodies = g_new0 (TaskBodies, 1);

	/* initialize the parent class' bits */
	task_bodies_constructor (TASK(bodies),
	                         TASK_TYPE_BODIES,
	                         task_bodies_destructor,
	                         task_bodies_describe,
	                         server,
	                         TRUE,
	                         mids,
	                         mid_qty,
	                         NULL);

	return PAN_OBJECT(bodies);
}

PanObject*
task_bodies_new_from_articles (const Article       ** articles,
                               int                    article_qty)
{
	int i;
	MessageIdentifier ** mids;
	PanObject * o;

	/* build an array of message-identifiers */
	mids = g_newa (MessageIdentifier*, article_qty);
	for (i=0; i<article_qty; ++i)
		mids[i] = message_identifier_new_from_article (articles[i]);
	
	o = task_bodies_new (articles[0]->group->server,
	                     mids,
	                     article_qty);

	/* cleanup */
	for (i=0; i<article_qty; ++i)
		g_object_unref (mids[i]);

	return o;
}

/*****
******
*****/

typedef struct
{
	Task * task;
	TaskBodies * task_b;
	StatusItem * status;

	MessageIdentifier * mid;
	GString * buf;
	gulong number;
}
TaskBodiesWorkData;

static void
work_data_free (TaskBodiesWorkData * wdata)
{
	g_object_unref (G_OBJECT(wdata->mid));
	g_string_free (wdata->buf, TRUE);
	g_free (wdata);
}

static void
task_bodies_nntp_line_body (GIOChannel    * io,
                            const char    * txt,
                            int             len,
                            gpointer        user_data)
{
	TaskBodiesWorkData * wdata = (TaskBodiesWorkData *) user_data;
	status_item_emit_next_step (wdata->status);
	g_string_append_len (wdata->buf, txt, len);
	g_string_append_c (wdata->buf, '\n');
}

static void
task_bodies_nntp_done_body (GIOChannel       * channel,
                            TaskStateEnum      state,
                            gpointer           user_data)
{
	TaskBodiesWorkData * wdata = (TaskBodiesWorkData *) user_data;
	Task * task = wdata->task;
	TaskBodies * bodies = wdata->task_b;

	--bodies->mids_running_qty;

	if (state != TASK_OK)
	{
		g_object_ref (G_OBJECT(wdata->mid));
		g_queue_push_tail (bodies->mids, wdata->mid);
		task_state_set_health (&task->state, state);
		task_state_set_work_need_socket (&task->state,
		                                 task->server,
		                                 task_bodies_run);
	}
	else /* got the article OK; now save it */
	{
               	acache_set_message (ACACHE_DEFAULT_KEY,
		                    &wdata->mid->message_id,
		                    wdata->buf->str,
		                    wdata->buf->len);

		if (!bodies->mids->length && !bodies->mids_running_qty)
		{
			if (bodies->done_work_func == NULL)
				task_state_set_work_completed (&task->state);
			else
				task_state_set_work_need_work (&task->state,
				             bodies->done_work_func);
		}
	}

	/* cleanup */
	task_checkin_socket (wdata->task, channel, state==TASK_OK);
	work_data_free (wdata);
}

static void
task_bodies_nntp_done_group (GIOChannel      * channel,
                             TaskStateEnum     state,
                             gpointer          user_data)
{
	TaskBodiesWorkData * wdata = (TaskBodiesWorkData *) user_data;

	if (state == TASK_OK)
	{
		nntp_article (channel,
		              &wdata->task->abort_flag,
		              task_bodies_nntp_line_body, wdata,
		              task_bodies_nntp_done_body, wdata,
		              wdata->number);
	}
	else
	{
		/* didn't get the article; push it back to the fetch list */
		g_object_ref (G_OBJECT(wdata->mid));
		g_queue_push_tail (wdata->task_b->mids, wdata->mid);
		--wdata->task_b->mids_running_qty;

		/* update the task health */
		task_state_set_health (&wdata->task->state, state);
		task_state_set_work_need_socket (&wdata->task->state,
		                                 wdata->task->server,
		                                 task_bodies_run);

		/* checkin the socket */
		task_checkin_socket (wdata->task, channel, state!=TASK_FAIL_NETWORK);

		work_data_free (wdata);
	}


	debug_exit ("task_bodies_run");
}

static void
task_bodies_run (Task * task, GIOChannel * channel)
{
	MessageSource source = MESSAGE_SOURCE_INIT;
	TaskBodiesWorkData * wdata;
	debug_enter ("task_bodies_run");

	wdata = g_new (TaskBodiesWorkData, 1);
	wdata->task = task;
	wdata->task_b = TASK_BODIES(task);
	wdata->status = STATUS_ITEM(task);
	wdata->mid = g_queue_pop_head (wdata->task_b->mids);
	++wdata->task_b->mids_running_qty;
	wdata->buf = g_string_sized_new (wdata->mid->byte_qty + 4096u);

	/* set the work state */
	if (!g_queue_is_empty (wdata->task_b->mids))
		task_state_set_work_need_socket (&task->state,
		                                 task->server,
		                                 task_bodies_run);
	else
		task_state_set_work_working (&task->state);

	message_sources_get_source (&wdata->mid->sources,
	                            task->server->name_quark,
	                            &source);

	wdata->number = source.number;

	/* ensure the group is right */
	if (nntp_get_group(channel) == source.group_quark)
		task_bodies_nntp_done_group (channel, TASK_OK, wdata);
	else nntp_group (channel, source.group_quark,
	                 NULL, NULL,
	                 task_bodies_nntp_done_group, wdata);
}
