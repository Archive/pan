/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * This is a quick-and-dirty substitute for the Gnome widget for non-Gnome
 * environments.  This should be compiled * out on Gnome systems.
 */

/* if user has gtk 2.6 or higher, use GtkColorButton */
#include <gtk/gtk.h>
#if GTK_CHECK_VERSION(2,6,0)
#else

#include <stdlib.h>
#include <string.h>
#include <pan/base/pan-i18n.h>
#include <pan/pan-color-button.h>

#define COLOR_PICKER_HEIGHT 12
#define COLOR_PICKER_WIDTH 20

static void
color_button_response_cb (GtkDialog * dialog, int response, gpointer button)
{
	if (response == GTK_RESPONSE_OK) {
		GdkColor color;
		GtkWidget * colorsel = GTK_COLOR_SELECTION_DIALOG(dialog)->colorsel;
		gtk_color_selection_get_current_color (GTK_COLOR_SELECTION(colorsel), &color);
		pan_color_button_set_color (GTK_WIDGET(button), &color);
	}

	if (response == GTK_RESPONSE_OK || response == GTK_RESPONSE_CANCEL)
		gtk_widget_destroy (GTK_WIDGET(dialog));
}

static void
button_clicked_cb (GtkWidget * button, gpointer user_data)
{
	GtkWidget * top;
	GtkWidget * w;
	GtkWidget * colorsel;
	GdkColor  * color;

	/* create the dialog */
       	w = gtk_color_selection_dialog_new (_("Select Color"));
	top = gtk_widget_get_toplevel (button);
	if (GTK_WIDGET_TOPLEVEL(top)) {
		gtk_window_set_transient_for (GTK_WINDOW(w), GTK_WINDOW(top));
		gtk_window_set_destroy_with_parent (GTK_WINDOW (w), TRUE);
	}

	/* set its state and listen for changes */
       	colorsel = GTK_COLOR_SELECTION_DIALOG(w)->colorsel;
	color = g_object_get_data (G_OBJECT(button), "pixel");
	gtk_color_selection_set_current_color (GTK_COLOR_SELECTION(colorsel), color);
	g_signal_connect (w, "response", G_CALLBACK(color_button_response_cb), button);
	gtk_widget_show_all (w);
}

GtkWidget*
pan_color_button_new (void)
{
	GtkWidget * w;
	GtkWidget * image;

	w = gtk_button_new ();
	image = gtk_image_new ();
	gtk_container_add (GTK_CONTAINER(w), image);

	g_signal_connect (w, "clicked", G_CALLBACK(button_clicked_cb), NULL);
	g_object_set_data (G_OBJECT(w), "image", image);
	g_object_set_data_full (G_OBJECT(w), "pixel", g_new0(GdkColor,1), g_free);
	pan_color_button_set_color (w, &w->style->bg[GTK_STATE_NORMAL]);
	return w;
}

static guint32
get_gdk_pixbuf_pixel (guint16 r, guint16 g, guint16 b)
{
	guint32 retval = 0;
	const int r_of_255 = (int) (255.0 * (r/65535.0));
	const int g_of_255 = (int) (255.0 * (g/65535.0));
	const int b_of_255 = (int) (255.0 * (b/65535.0));
	retval |= (r_of_255 << 24);
	retval |= (g_of_255 << 16);
	retval |= (b_of_255 << 8);
	return retval;
}

void
pan_color_button_set_color (GtkWidget * w, const GdkColor * color)
{
	guint32 pixel;
	GdkPixbuf * pixbuf;
	GdkPixmap * pixmap;
	GdkBitmap * bitmap;

	g_return_if_fail (GTK_IS_WIDGET(w));
	g_return_if_fail (color!=NULL);

	/* remember the color */
	*((GdkColor*)g_object_get_data(G_OBJECT(w),"pixel")) = *color;

	/* render it */
	pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, COLOR_PICKER_WIDTH, COLOR_PICKER_HEIGHT);
	pixel = get_gdk_pixbuf_pixel (color->red, color->green, color->blue);
	gdk_pixbuf_fill (pixbuf, pixel);
	gdk_pixbuf_render_pixmap_and_mask_for_colormap (pixbuf, gtk_widget_get_colormap(w), &pixmap, &bitmap, 128);
	gtk_image_set_from_pixmap (GTK_IMAGE(g_object_get_data(G_OBJECT(w),"image")),pixmap, bitmap);

	/* cleanup */
	g_object_unref (pixbuf);
	if (pixmap != NULL) g_object_unref (pixmap);
	if (bitmap != NULL) g_object_unref (bitmap);
}

void
pan_color_button_get_color (GtkWidget * button, GdkColor * color)
{
	*color = *((GdkColor*)g_object_get_data(G_OBJECT(button),"pixel"));
}

#endif /* GTK_CHECK_VERSION */
