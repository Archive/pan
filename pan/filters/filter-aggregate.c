/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/filters/filter-aggregate.h>

const gchar * FILTER_AGGREGATE_CLASS_ID = "PanObject::Filter::FilterAggregate";


/************
*************  PRIVATE
************/

static Filter*
filter_aggregate_dup (const Filter * f_old)
{
	Filter * f_new;
	debug_enter ("filter_aggregate_dup");

	f_new = filter_aggregate_new ();
	filter_class_dup (f_old, f_new);
	filter_aggregate_class_dup (FILTER_AGGREGATE(f_old), FILTER_AGGREGATE(f_new));

	debug_exit ("filter_aggregate_dup");
	return f_new;
}

static gchar*
filter_aggregate_to_string (const Filter * filter)
{
	gchar * pch = NULL;
	const FilterAggregate * f = FILTER_AGGREGATE(filter);

	if (f->type==AGGREGATE_TYPE_AND) {
		if (filter->negate)
			pch = g_strdup (_("NONE OF:"));
		else
			pch = g_strdup (_("ALL OF:"));
	}
	else if (f->type==AGGREGATE_TYPE_OR) {
		if (filter->negate)
			pch = g_strdup (_("NONE OF:"));
		else
			pch = g_strdup (_("ANY OF:"));
	}
	else pch = g_strdup (_("Error"));

	return pch;
}


/**
 * If the aggregate has no child filters, then the articles will
 * test TRUE on AND aggregates, FALSE on OR aggregates.
 */
static void
filter_aggregate_test_articles (Filter          * filter,
                                const Article  ** articles,
                                gint              article_qty,
                                gboolean        * passfail)
{
	FilterAggregate * fa = FILTER_AGGREGATE(filter);

	if (fa->children==NULL || fa->children->len==0)
	{
		int i;
		for (i=0; i<article_qty; ++i)
			passfail[i] = TRUE;
	}
	else if (fa->children->len == 1)
	{
		Filter * child = FILTER(g_ptr_array_index (fa->children,0));
		filter_test_articles (child, articles, article_qty, passfail);
	}
	else /* more than one child */
	{
		int i;
		int remaining_qty = article_qty;
		int * original_indices = g_new (int, article_qty);
		int * original_indices_swap = g_new (int, article_qty);
		const Article ** remaining_articles = g_memdup (articles, sizeof(Article*) * article_qty);
		const Article ** remaining_articles_swap = g_new (const Article*, article_qty);
		const gboolean and = fa->type == AGGREGATE_TYPE_AND;

		/* init the article indices array */
		for (i=0; i<article_qty; ++i)
			original_indices[i] = i;

		/* walk through the filters and remove the failures */
		for (i=0; 0<remaining_qty && i<fa->children->len; ++i)
		{
			int j;
			int new_qty;
			Filter * child;
			const Article ** tmp_ppA;
			int * tmp_pI;

			/* test the articles against the next filter */
			child = FILTER(g_ptr_array_index (fa->children,i));
			filter_test_articles (child, remaining_articles, remaining_qty, passfail);

			/* remove the failing (for AND) or passing (for OR) articles */
			new_qty = 0;
			for (j=0; j<remaining_qty; ++j) {
				if ((passfail[j]!=0) == and) {
					remaining_articles_swap[new_qty] = remaining_articles[j];
					original_indices_swap[new_qty] = original_indices[j];
					++new_qty;
				}
			}

			/* cycle the pointers around so we can repeat for the next aggregate child */
			remaining_qty = new_qty;
			tmp_ppA = remaining_articles_swap;
			remaining_articles_swap = remaining_articles;
			remaining_articles = tmp_ppA;
			tmp_pI = original_indices;
			original_indices = original_indices_swap;
			original_indices_swap = tmp_pI;
		}

		/* set passfail with the articles that made it through all the child filters.
		 * for 'and', everything failed except the items in original_indices.
		 * for 'or', everything passed except the items in original_indices.  */
		for (i=0; i<article_qty; ++i)
			passfail[i] = !and;
		for (i=0; i<remaining_qty; ++i)
			passfail[ original_indices[i] ] = and;

		/* cleanup */
		g_free (remaining_articles);
		g_free (remaining_articles_swap);
		g_free (original_indices);
		g_free (original_indices_swap);
	}
}

/************
*************  PROTECTED
************/

void
filter_aggregate_class_dup (const FilterAggregate  * fa_old,
                            FilterAggregate        * fa_new)
{
	gint i;
	fa_new->type = fa_old->type;
	for (i=0; i<fa_old->children->len; ++i) {
		Filter * c = filter_dup (FILTER(g_ptr_array_index(fa_old->children,i)));
		filter_aggregate_add (fa_new, &c, 1);
		pan_object_unref (PAN_OBJECT(c));
	}
}

void
filter_aggregate_constructor (FilterAggregate       * f,
                              PanObjectDestructor     dtor,
                              FilterTestArticlesFunc  test_func,
                              FilterToStringFunc      to_string_func,
                              FilterDupFunc           dup_func,
                              const gchar           * class_name)
{
	if (dtor == NULL)
		dtor = filter_aggregate_destructor;
	if (test_func == NULL)
		test_func = filter_aggregate_test_articles;
	if (to_string_func == NULL)
		to_string_func = filter_aggregate_to_string;
	if (dup_func == NULL)
		dup_func = filter_aggregate_dup;

	filter_constructor  ((Filter*)f, dtor, test_func, to_string_func, dup_func, class_name);
	f->children = g_ptr_array_new ();
	f->type = AGGREGATE_TYPE_AND;
}

void
filter_aggregate_destructor (PanObject * o)
{
	guint i;
	FilterAggregate * f = FILTER_AGGREGATE(o);

	/* aggregate dtor */
	for (i=0; i<f->children->len; ++i)
		pan_object_unref (PAN_OBJECT(g_ptr_array_index(f->children,i)));
	g_ptr_array_free (FILTER_AGGREGATE(o)->children, TRUE);

	/* superclass dtor */
	filter_destructor (o);
}

/************
*************  PUBLIC
************/

Filter*
filter_aggregate_new (void)
{
	FilterAggregate * f = g_new0 (FilterAggregate, 1);
	filter_aggregate_constructor (f, NULL, NULL, NULL, NULL, FILTER_AGGREGATE_CLASS_ID);
	return FILTER(f);
}


void
filter_aggregate_set_type (FilterAggregate     * f,
                           FilterAggregateType   type)
{
	g_return_if_fail (f!=NULL);
	g_return_if_fail (type==AGGREGATE_TYPE_AND || type==AGGREGATE_TYPE_OR);

	f->type = type;
}


/*****
******
*****/

void
filter_aggregate_insert (FilterAggregate     * f,
                         Filter             ** children,
                         gint                  child_qty,
                         gint                  index)
{
	gint i;
	debug_enter ("filter_aggregate_insert");

	g_return_if_fail (f!=NULL);
	g_return_if_fail (children!=NULL);
	g_return_if_fail (child_qty>=1);

	if (index<0)
		index = f->children->len;

	for (i=0; i<child_qty; ++i) {
		pan_object_ref (PAN_OBJECT(children[i]));
		pan_g_ptr_array_insert (f->children, children[i], index+i);
	}

	debug_exit ("filter_aggregate_insert");
}

void
filter_aggregate_add (FilterAggregate      * f,
                      Filter              ** children,
                      gint                   child_qty)
{
	filter_aggregate_insert (f, children, child_qty, -1);
}

void
filter_aggregate_remove (FilterAggregate     * f,
                         Filter             ** children,
                         gint                  child_qty)
{
	gint i;

	g_return_if_fail (f!=NULL);
	g_return_if_fail (children!=NULL);
	g_return_if_fail (child_qty>=1);

	for (i=0; i<child_qty; ++i)
		if (g_ptr_array_remove (f->children, children[i]))
			pan_object_unref (PAN_OBJECT(children[i]));
}

/*****
******
*****/

gint
filter_aggregate_child_index (FilterAggregate     * f,
                              const Filter        * child)
{
	gint i;	
	gint retval = -1;

	g_return_val_if_fail (f!=NULL, FALSE);
	g_return_val_if_fail (child!=NULL, FALSE);

	for (i=0; retval==-1 && i<f->children->len; ++i)
		if (g_ptr_array_index(f->children,i) == child)
			retval = i;

	return retval;
}

gboolean
filter_aggregate_has_child (FilterAggregate     * f,
                            const Filter        * child)
{
	return filter_aggregate_child_index (f, child) != -1;
}

gint
filter_aggregate_child_size (const FilterAggregate * f)
{
	return f->children->len;
}


/*****
******
*****/

Filter*
filter_aggregate_get_child_at (FilterAggregate * f, gint index)
{
	g_return_val_if_fail (f!=NULL, NULL);
	g_return_val_if_fail (index>=0, NULL);
	g_return_val_if_fail (index<f->children->len, NULL);

	return FILTER(g_ptr_array_index(f->children,index));
}
	
GPtrArray*
filter_aggregate_get_children (FilterAggregate * f)
{
	gint i;
	GPtrArray * a = pan_g_ptr_array_dup (f->children);
	for (i=0; i<a->len; ++i)
		pan_object_ref (PAN_OBJECT(g_ptr_array_index(a,i)));
	return a;
}

void
filter_aggregate_clear_child_array (GPtrArray * a)
{
	gint i;
	for (i=0; i<a->len; ++i)
		pan_object_unref (PAN_OBJECT(g_ptr_array_index(a,i)));
	g_ptr_array_free (a, TRUE);
}
