/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>
#include <pan/base/pan-i18n.h>

#include <pan/filters/filter-bytes.h>

const char * FILTER_BYTES_CLASS_ID = "PanObject::Filter::FilterBytes";

/************
*************  PROTECTED
************/

static Filter*
filter_bytes_dup (const Filter * f_old)
{
	Filter * f_new = filter_bytes_new ();
	FilterBytes * fl_old = FILTER_BYTES (f_old);
	FilterBytes * fl_new = FILTER_BYTES (f_new);
	filter_class_dup (f_old, f_new);
	fl_new->minimum_bytes = fl_old->minimum_bytes;
	return f_new;
}

static char*
filter_bytes_to_string (const Filter * filter)
{
	char * pch;
	const int i = FILTER_BYTES(filter)->minimum_bytes;

	if (filter->negate)
		pch = g_strdup_printf (_("Article is less than %d bytes long"), i);
	else
		pch = g_strdup_printf (_("Article is %d or more bytes long"), i);

	return pch;
}

static void
filter_bytes_test_articles (Filter          * filter,
                            const Article  ** articles,
                            int               article_qty,
                            gboolean        * passfail)
{
	int i;
	const gulong min = FILTER_BYTES(filter)->minimum_bytes;

	for (i=0; i<article_qty; ++i)
		passfail[i] = articles[i]->byte_qty >= min;
}

/************
*************  PUBLIC
************/

Filter*
filter_bytes_new (void)
{
	FilterBytes * f = g_new0 (FilterBytes, 1);
	filter_constructor ((Filter*)f,
	                    filter_destructor,
	                    filter_bytes_test_articles,
	                    filter_bytes_to_string,
	                    filter_bytes_dup,
	                    FILTER_BYTES_CLASS_ID);
	return FILTER(f);
}
