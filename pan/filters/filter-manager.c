/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>

#include <pan/base/base-prefs.h>
#include <pan/base/pan-callback.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/filters/filter-phrase.h>
#include <pan/filters/filter-manager.h>
#include <pan/filters/filter-xml.h>

static GPtrArray * _filters = NULL;

/************
*************  PRIVATE
************/

static char*
get_filter_filename (void)
{
	return g_build_filename (get_data_dir(), "filters.xml", NULL);
}

void
filter_manager_init (void)
{
	char * fname;

	/* populate _filters from the array */
	fname = get_filter_filename ();
	_filters = g_ptr_array_new ();
	filter_xml_read (fname, _filters);
	g_free (fname);
}

/************
*************  PUBLIC
************/

void
filter_manager_get_filters (GPtrArray * fillme)
{
	guint i;

	/* sanity clause */
	g_return_if_fail (fillme!=NULL);
	g_return_if_fail (_filters!=NULL);

	for (i=0; i<_filters->len; ++i)
	{
		Filter * old = FILTER(g_ptr_array_index(_filters,i));
		Filter * dup = filter_dup (old);
		g_ptr_array_add (fillme, dup);
	}
}


Filter*
filter_manager_get_named_filter (const char * name)
{
	guint i;
	Filter * retval = NULL;

	g_return_val_if_fail (is_nonempty_string(name), NULL);

	for (i=0; retval==NULL && i<_filters->len; ++i)
	{
		FilterTop * f = FILTER_TOP(g_ptr_array_index(_filters,i));
		if (!pan_strcmp (f->name, name))
			retval = FILTER(f);
	}

	if (retval != NULL)
		retval = filter_dup (retval);

	return retval;
}

void
filter_manager_set_filters (GPtrArray * filters)
{
	guint i;
	char * fname;
	GPtrArray * old;
	GPtrArray * tmp;

	/* make copies of the new */
	tmp = g_ptr_array_new ();
	for (i=0; i<filters->len; ++i) {
		Filter * f = FILTER(g_ptr_array_index(filters,i));
		Filter * dup = filter_dup (f);
		g_ptr_array_add (tmp, dup);
	}

	/* empty out the old */
	old = _filters;
	_filters = tmp;
	if (old != NULL) {
		pan_g_ptr_array_foreach (old, (GFunc)pan_object_unref, NULL);
		g_ptr_array_free (old, TRUE);
	}

	/* save them to disk */
	fname = get_filter_filename ();
	filter_xml_write (fname, (const FilterTop**)_filters->pdata, _filters->len);
	g_free (fname);

	/* fire a notice to listeners */
	pan_callback_call (filter_manager_get_filters_changed_callback(),
	                   _filters, NULL);
}

PanCallback*
filter_manager_get_filters_changed_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb==NULL) cb = pan_callback_new ();
	return cb;
}
