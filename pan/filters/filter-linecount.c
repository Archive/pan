/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>
#include <pan/base/pan-i18n.h>

#include <pan/filters/filter-linecount.h>

const gchar * FILTER_LINE_COUNT_CLASS_ID = "PanObject::Filter::FilterLineCount";

/************
*************  PROTECTED
************/

static Filter*
filter_line_count_dup (const Filter * f_old)
{
	Filter * f_new = filter_line_count_new ();
	FilterLineCount * fl_old = FILTER_LINE_COUNT (f_old);
	FilterLineCount * fl_new = FILTER_LINE_COUNT (f_new);
	filter_class_dup (f_old, f_new);
	fl_new->minimum_line_count = fl_old->minimum_line_count;
	return f_new;
}

static gchar*
filter_line_count_to_string (const Filter * filter)
{
	gchar * pch;
	const gint i = FILTER_LINE_COUNT(filter)->minimum_line_count;

	if (filter->negate)
		pch = g_strdup_printf (_("Article is less than %d lines long"), i);
	else
		pch = g_strdup_printf (_("Article is %d or more lines long"), i);

	return pch;
}

static void
filter_line_count_test_articles (Filter          * filter,
                                 const Article  ** articles,
                                 gint              article_qty,
                                 gboolean        * passfail)
{
	gint i;
	gint min = FILTER_LINE_COUNT(filter)->minimum_line_count;

	for (i=0; i<article_qty; ++i)
		passfail[i] = articles[i]->line_qty >= min;
}

/************
*************  PUBLIC
************/

Filter*
filter_line_count_new (void)
{
	FilterLineCount * f = g_new0 (FilterLineCount, 1);
	filter_constructor ((Filter*)f,
	                    filter_destructor,
	                    filter_line_count_test_articles,
	                    filter_line_count_to_string,
	                    filter_line_count_dup,
	                    FILTER_LINE_COUNT_CLASS_ID);
	return FILTER(f);
}
