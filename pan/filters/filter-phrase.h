/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __FILTER_PHRASE_H__
#define __FILTER_PHRASE_H__

#include <sys/types.h>

#include <pan/filters/filter.h>

extern const gchar * FILTER_PHRASE_CLASS_ID;
#define FILTER_PHRASE(a) ((FilterPhrase*)filter_cast((Filter*)a,FILTER_PHRASE_CLASS_ID))

typedef struct _FilterPhrase FilterPhrase;

typedef enum
{
	PHRASE_KEY_SUBJECT              = 1,
	PHRASE_KEY_AUTHOR               = 2,
	PHRASE_KEY_MESSAGE_ID           = 3,
	PHRASE_KEY_REFERENCES           = 4,
	PHRASE_KEY_XREF                 = 5,
}
PhraseKeyType;

typedef enum
{
	PHRASE_MATCH_CONTAINS           = 1,
	PHRASE_MATCH_IS	                = 2,
	PHRASE_MATCH_STARTS_WITH        = 3,
	PHRASE_MATCH_ENDS_WITH          = 4,
	PHRASE_MATCH_REGEX              = 5 
}
PhraseMatchType;

enum
{
	REGEX_NEED_COMPILE,
	REGEX_ERR,
	REGEX_COMPILED
};

typedef struct _PcreInfo PcreInfo;

struct _FilterPhrase
{
	/* Parent */
	Filter parent;

	/* Fields */
	PhraseMatchType public_match_type;
	PhraseMatchType match_type;
	PhraseKeyType key_type;
	char * public_key;

	char * private_key;
	int private_key_len;
	char bmhs_skip[UCHAR_MAX];

	PcreInfo * pcre_info;
	int pcre_state;
	gboolean case_sensitive;
};

/***
****  PUBLIC
***/

Filter*      filter_phrase_new                (void);

void         filter_phrase_set                (FilterPhrase         * f,
                                               PhraseMatchType        match_type,
                                               PhraseKeyType          key_type,
                                               const char           * key,
                                               gboolean               case_sensitive);

gboolean     filter_phrase_does_match         (FilterPhrase         * filter_phrase,
                                               const char           * subject,
                                               int                    subject_len);

char*        filter_phrase_create_regex       (const char           * string,
                                               PhraseMatchType        match_type);

char*        filter_phrase_validate_pattern   (const char           * pattern);

#endif
