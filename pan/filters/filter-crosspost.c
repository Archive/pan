/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>
#include <pan/base/pan-i18n.h>

#include <pan/filters/filter-crosspost.h>

const gchar * FILTER_CROSSPOST_CLASS_ID = "PanObject::Filter::FilterCrosspost";

/************
*************  PROTECTED
************/

static Filter*
filter_crosspost_dup (const Filter * f_old)
{
	Filter * f_new = filter_crosspost_new ();
	FilterCrosspost * fc_old = FILTER_CROSSPOST(f_old);
	FilterCrosspost * fc_new = FILTER_CROSSPOST(f_new);
	filter_class_dup (f_old, f_new);
	fc_new->minimum_crosspost_count = fc_old->minimum_crosspost_count;
	return f_new;
}

static char*
filter_crosspost_to_string (const Filter * filter)
{
	const int i = FILTER_CROSSPOST(filter)->minimum_crosspost_count;
	char * pch;

	if (filter->negate)
		pch = g_strdup_printf (_("Article is crossposted to less than %d groups"), i);
	else
		pch = g_strdup_printf (_("Article is crossposted to at least %d groups"), i);

	return pch;
}

static void
filter_crosspost_test_articles (Filter          * filter,
                                const Article  ** articles,
                                int               article_qty,
                                gboolean        * passfail)
{
	int i;
	int min = FILTER_CROSSPOST(filter)->minimum_crosspost_count;

	for (i=0; i<article_qty; ++i)
		passfail[i] = article_get_crosspost_qty (articles[i]) >= min;
}

/************
*************  PUBLIC
************/

Filter*
filter_crosspost_new (void)
{
	FilterCrosspost * f = g_new0 (FilterCrosspost, 1);
	filter_constructor ((Filter*)f,
	                    filter_destructor,
	                    filter_crosspost_test_articles,
	                    filter_crosspost_to_string,
	                    filter_crosspost_dup,
	                    FILTER_CROSSPOST_CLASS_ID);
	return FILTER(f);
}
