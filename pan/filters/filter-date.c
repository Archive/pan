/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/pan-i18n.h>

#include <pan/filters/filter-date.h>

const char * FILTER_DATE_CLASS_ID = "PanObject::Filter::FilterDate";

/************
*************  PROTECTED
************/

static Filter*
filter_date_dup (const Filter * f_old)
{
	Filter * f_new = filter_date_new ();
	FilterDate * fd_old = FILTER_DATE (f_old);
	FilterDate * fd_new = FILTER_DATE (f_new);
	filter_class_dup (f_old, f_new);
	fd_new->minimum_days_old = fd_old->minimum_days_old;
	return f_new;
}

static char*
filter_date_to_string (const Filter * filter)
{
	const int i = FILTER_DATE(filter)->minimum_days_old;
	char * pch;

	if (filter->negate)
		pch = g_strdup_printf (_("Article was posted in the last %d days"), i);
	else
		pch = g_strdup_printf (_("Article is more than %d days old"), i);

	return pch;
}

static void
filter_date_test_articles (Filter            * filter,
                           const Article    ** articles,
                           int                 article_qty,
			   gboolean          * passfail)
{
	int i;
	const time_t now = time(0);
	const int secs_in_day = 24 * 60 * 60;
	const int minimum_days_old = FILTER_DATE(filter)->minimum_days_old;

	for (i=0; i<article_qty; ++i)
	{
		const Article * a = articles[i];
		const time_t then = a->date;
		const time_t secs_since_posting = now - then;
		const int days_since_posting = secs_since_posting / secs_in_day;
		passfail[i] = days_since_posting >= minimum_days_old;
	}
}

/************
*************  PUBLIC
************/

Filter*
filter_date_new (void)
{
	FilterDate * f = g_new0 (FilterDate, 1);
	filter_constructor ((Filter*)f,
	                    filter_destructor,
	                    filter_date_test_articles,
	                    filter_date_to_string,
	                    filter_date_dup,
	                    FILTER_DATE_CLASS_ID);
	return FILTER(f);
}
