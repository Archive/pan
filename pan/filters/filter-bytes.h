/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __FILTER_BYTES_H_
#define __FILTER_BYTES_H_

#include <pan/filters/filter.h>

extern const char * FILTER_BYTES_CLASS_ID;
#define FILTER_BYTES(a) ((FilterBytes*)filter_cast((Filter*)a,FILTER_BYTES_CLASS_ID))

typedef struct _FilterBytes   FilterBytes;

struct _FilterBytes
{
	/* parent class */
	Filter parent;

	/* byte count class */
	gulong minimum_bytes;
};

/**
***  PUBLIC
**/

Filter*      filter_bytes_new             (void);


#endif
