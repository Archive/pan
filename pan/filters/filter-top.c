/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>

#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/filters/filter-top.h>

const gchar * FILTER_TOP_CLASS_ID = "PanObject::Filter::FilterAggregate::Top";

/************
*************  PRIVATE
************/

static Filter*
filter_top_dup (const Filter * f_old)
{
	Filter * f_new = filter_top_new ();
	const FilterTop * ft_old = FILTER_TOP(f_old);
	FilterTop * ft_new = FILTER_TOP(f_new);
	filter_aggregate_class_dup (FILTER_AGGREGATE(f_old), FILTER_AGGREGATE(f_new));
	replace_gstr (&ft_new->name, g_strdup(ft_old->name));
	ft_new->is_visible = ft_old->is_visible;
	f_new->negate = f_old->negate;

	return f_new;
}

/************
*************  PROTECTED
************/

void
filter_top_constructor (FilterTop * f)
{
	((Filter*)f)->class_id = FILTER_TOP_CLASS_ID;
	filter_aggregate_constructor (FILTER_AGGREGATE(f),
	                              filter_top_destructor,
	                              NULL,
	                              NULL,
	                              filter_top_dup,
				      FILTER_TOP_CLASS_ID);
	f->name = g_strdup ("New Filter");
	f->is_visible = TRUE;
}

void
filter_top_destructor (PanObject * o)
{
	FilterTop * f = FILTER_TOP(o);

	/* top dtor */
	g_free (f->name);

	/* superclass dtor */
	filter_aggregate_destructor (o);
}

/************
*************  PUBLIC
************/

Filter*
filter_top_new (void)
{
	FilterTop * f = g_new0 (FilterTop, 1);
	filter_top_constructor (f);
	return FILTER(f);
}

void
filter_top_set_name (FilterTop    * f,
                     const gchar  * name)
{
	g_return_if_fail (f!=NULL);
	replace_gstr (&f->name, g_strdup(name));
}

void
filter_top_set_visible (FilterTop    * f,
                        gboolean       visible)
{
	g_return_if_fail (f!=NULL);
	f->is_visible = visible;
}
