/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/pan-i18n.h>

#include <pan/filters/filter-mine.h>

#include <pan/identities/identity-manager.h>

const gchar * FILTER_MINE_CLASS_ID = "PanObject::Filter::FilterMine";


/************
*************  PROTECTED
************/

static Filter*
filter_mine_dup (const Filter * f_old)
{
	Filter * f_new = filter_mine_new ();
	filter_class_dup (f_old, f_new);
	return f_new;
}

static gchar*
filter_mine_to_string (const Filter * filter)
{
	return filter->negate ?
		g_strdup (_("Article was not posted by me")) :
		g_strdup (_("Article was posted by me"));
}

static void
filter_mine_test_articles (Filter            * filter,
                           const Article    ** articles,
                           gint                article_qty,
			   gboolean          * passfail)
{
	gint i;

	for (i=0; i<article_qty; ++i)
	{
		const Article * a  = articles[i];
		Identity      * id = identity_manager_get_identity_by_author
					(a->author_real.str, a->author_addr.str);

		passfail[i] = id != NULL;

		if (id != NULL)
			pan_object_unref(PAN_OBJECT(id));
	}
}

/************
*************  PUBLIC
************/

Filter*
filter_mine_new (void)
{
	FilterMine * f = g_new0 (FilterMine, 1);
	filter_constructor ((Filter*)f,
	                    filter_destructor,
	                    filter_mine_test_articles,
	                    filter_mine_to_string,
	                    filter_mine_dup,
	                    FILTER_MINE_CLASS_ID);
	return FILTER(f);
}
