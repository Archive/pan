/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __FILTER_SCORE_H__
#define __FILTER_SCORE_H__

#include <pan/filters/filter.h>

#define FILTER_SCORE(a) ((FilterScore*)(a))
extern const gchar * FILTER_SCORE_CLASS_ID;

typedef struct _FilterScore FilterScore;

typedef enum
{
	SCORE_WATCHED     = (1<<0),
        SCORE_HIGH        = (1<<1),
        SCORE_MEDIUM      = (1<<2),
        SCORE_ZERO        = (1<<4),
        SCORE_LOW         = (1<<5),
        SCORE_IGNORED     = (1<<6),
	SCORE_ALL         = (SCORE_WATCHED | SCORE_HIGH | SCORE_MEDIUM | SCORE_ZERO | SCORE_LOW | SCORE_IGNORED)
}
FilterScoreMode;

struct _FilterScore
{
	/* Parent */
	Filter parent;

	/* Fields */
	FilterScoreMode mode;
};

/***
****  PUBLIC
***/

FilterScoreMode  filter_score_get_score_mode    (int                  score);

Filter*          filter_score_new               (FilterScoreMode      mode);

void             filter_score_set_mode          (FilterScore        * filter,
                                                 FilterScoreMode      mode);

#endif
