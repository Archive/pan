/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>
#include <pan/base/debug.h>
#include <pan/base/pan-i18n.h>

#include <pan/filters/filter-score.h>
#include <pan/filters/score.h>

const char * FILTER_SCORE_CLASS_ID = "PanObject::Filter::FilterScore";

/************
*************  PROTECTED
************/

FilterScoreMode
filter_score_get_score_mode (int score)
{
        if (score <= -9999)     return SCORE_IGNORED;
        else if (score < 0)     return SCORE_LOW;
        else if (score == 0)    return SCORE_ZERO;
        else if (score < 5000)  return SCORE_MEDIUM;
        else if (score < 9999)  return SCORE_HIGH;
        else                    return SCORE_WATCHED;
}

static Filter*
filter_score_dup (const Filter * f_old)
{
	Filter *f_new = filter_score_new (FILTER_SCORE (f_old)->mode);
	if (f_old->negate)
		filter_negate (f_new);

	return f_new;
}

static char*
filter_score_to_string (const Filter * filter)
{
	GString * str;
	int mode_qty = 0;
	const char * modes[8];
	FilterScoreMode mode = FILTER_SCORE(filter)->mode;

	if (filter->negate)
		mode = ~mode;

	if (mode & SCORE_WATCHED)
		modes[mode_qty++] = _("watched");
	if (mode & SCORE_HIGH)
		modes[mode_qty++] = _("high");
	if (mode & SCORE_MEDIUM)
		modes[mode_qty++] = _("medium");
	if (mode & SCORE_ZERO)
		modes[mode_qty++] = _("zero");
	if (mode & SCORE_LOW)
		modes[mode_qty++] = _("low");
	if (mode & SCORE_IGNORED)
		modes[mode_qty++] = _("ignored");

       	str = g_string_sized_new (128);
	if (mode_qty == 7)
		g_string_assign (str, _("Score filter will match anything"));
	else if (mode_qty == 0)
		g_string_assign (str, _("Score filter will match nothing"));
	else {
		int i;
		g_string_assign (str, _("Article score one of: "));
		for (i=0; i<mode_qty; ++i)
			g_string_append_printf (str, "%s, ", modes[i]);
		g_string_erase (str, str->len-2, 2);
	}

	return g_string_free (str, FALSE);
}

static void
filter_score_test_articles (Filter          * filter,
                            const Article  ** articles,
                            int               article_qty,
                            gboolean        * passfail)
{
	int i;
	const FilterScoreMode  mode = FILTER_SCORE(filter)->mode;

	g_return_if_fail (articles_are_valid (articles, article_qty));

	/* FIXME: casting away const */
	ensure_articles_scored (articles[0]->group, (Article**)articles, article_qty, NULL);

	for (i=0; i<article_qty; ++i)
		passfail[i] = (mode & filter_score_get_score_mode (articles[i]->score)) != 0;
}

/************
*************  PUBLIC
************/

Filter*
filter_score_new (FilterScoreMode mode)
{
	FilterScore * f = g_new0 (FilterScore, 1);
	filter_constructor ((Filter*)f,
	                    filter_destructor,
	                    filter_score_test_articles,
	                    filter_score_to_string,
	                    filter_score_dup,
	                    FILTER_SCORE_CLASS_ID);
	f->mode = mode;
	return FILTER(f);
}

void
filter_score_set_mode (FilterScore * filter_score, FilterScoreMode mode)
{
	filter_score->mode = mode;
}
