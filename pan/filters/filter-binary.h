/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __FILTER_BINARY_H__
#define __FILTER_BINARY_H__

#include <pan/filters/filter.h>

#define FILTER_BINARY(a) ((FilterBinary*)(a))
extern const gchar * FILTER_BINARY_CLASS_ID;

typedef struct _FilterBinary FilterBinary;

typedef enum
{
	FILTER_BINARY_NONBINARY,
	FILTER_BINARY_INCOMPLETE,
	FILTER_BINARY_COMPLETE
}
FilterBinaryState;

struct _FilterBinary
{
	/* Parent */
	Filter parent;

	/* Fields */
	FilterBinaryState state;
};

/***
****  PUBLIC
***/

Filter*      filter_binary_new           (void);

void         filter_binary_set_state     (FilterBinary        * filter,
                                          FilterBinaryState     state);

#endif
