/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <pan/filters/filter.h>

#ifndef __FILTER_AGGREGATE_H__
#define __FILTER_AGGREGATE_H__

extern const gchar * FILTER_AGGREGATE_CLASS_ID;
#define FILTER_AGGREGATE(a) ((FilterAggregate*)filter_cast((Filter*)a,FILTER_AGGREGATE_CLASS_ID))

typedef enum
{
	AGGREGATE_TYPE_AND,
	AGGREGATE_TYPE_OR
}
FilterAggregateType;

typedef struct _FilterAggregate   FilterAggregate;

struct _FilterAggregate
{
	/* parent class */
	Filter parent;

	/* filter aggregate class */
	GPtrArray * children;
	FilterAggregateType type;
};

/**
***  PROTECTED
**/

void         filter_aggregate_constructor     (FilterAggregate        * f,
                                               PanObjectDestructor      dtor,
                                               FilterTestArticlesFunc   test_func,
                                               FilterToStringFunc       to_string_func,
                                               FilterDupFunc            dup_func,
                                               const gchar            * class_name);

void         filter_aggregate_destructor      (PanObject              * o);

void         filter_aggregate_class_dup       (const FilterAggregate  * fa_original,
                                               FilterAggregate        * fa_new);


/**
***  PUBLIC
**/

Filter*      filter_aggregate_new             (void);

void         filter_aggregate_set_type        (FilterAggregate     * f,
                                               FilterAggregateType   type);

void         filter_aggregate_add             (FilterAggregate     * f,
                                               Filter             ** children,
                                               gint                  child_qty);

void         filter_aggregate_insert          (FilterAggregate     * f,
                                               Filter             ** children,
                                               gint                  child_qty,
                                               gint                  index);

void         filter_aggregate_remove          (FilterAggregate     * f,
                                               Filter             ** children,
                                               gint                  child_qty);

GPtrArray*   filter_aggregate_get_children    (FilterAggregate     * f);

void         filter_aggregate_clear_child_array (GPtrArray *);

/**
***
**/

Filter*      filter_aggregate_get_child_at    (FilterAggregate     * f,
                                               gint                  index);

gint         filter_aggregate_child_size      (const FilterAggregate * f);

gint         filter_aggregate_child_index     (FilterAggregate     * f,
                                               const Filter        * child);

gboolean     filter_aggregate_has_child       (FilterAggregate     * f,
                                               const Filter        * child);

/**
***
**/


#endif /* __FILTER_AGGREGATE_H__ */
