/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __FILTER_TOP_H__
#define __FILTER_TOP_H__

#include <pan/filters/filter-aggregate.h>

extern const gchar * FILTER_TOP_CLASS_ID;
#define IS_FILTER_TOP(a)  (filter_isa(a,FILTER_TOP_CLASS_ID))
#define FILTER_TOP(a) ((FilterTop*)filter_cast((Filter*)a,FILTER_TOP_CLASS_ID))

typedef struct _FilterTop   FilterTop;

struct _FilterTop
{
	/* parent class */
	FilterAggregate parent;

	/* filter top class */
	gchar * name;
	gboolean is_visible;
};

/**
***  PROTECTED
**/

void         filter_top_constructor     (FilterTop     * f);

void         filter_top_destructor      (PanObject     * o);

/**
***  PUBLIC
**/

Filter*      filter_top_new             (void);

void         filter_top_set_name        (FilterTop     * f,
                                         const gchar   * name);

void         filter_top_set_visible     (FilterTop     * f,
                                         gboolean        visible);

#endif /* __FILTER_TOP_H__ */
