#ifndef __SCORE_H__
#define __SCORE_H__

#include <pan/base/article.h>
#include <pan/base/pan-callback.h>
#include <pan/filters/filter-aggregate.h>

typedef struct
{
	char * filename;
	int begin_line;
	int end_line;
	char * filter_string;
	char * score_name;
	int value;
	gboolean value_assign_flag;
}
ScoreViewItem;

typedef struct
{
	gboolean on;
	gboolean negate;
	char * key;
	char * value;
}
ScoreAddItem;

const char* score_get_main_scorefile_filename    (void);

/**
 * call_obj: NULL
 * call_arg: GINT_TO_POINTER(manditory_rescore_needed)
 */
PanCallback*  score_get_scorefile_invalidated_callback (void);

void        score_invalidate           (gboolean                do_rescore_all);

void        ensure_articles_scored     (Group                 * group,
                                        Article              ** articles,
                                        int                     article_qty,
                                        GPtrArray             * changed_or_null);

GPtrArray*  score_view_article         (const Article         * article);

void        score_view_free            (GPtrArray             * score_view_items);

char*       score_create_section_str   (const Article         * article);

void        score_add                  (const char            * score_name,
                                        const char            * section_str,
                                        const int               value,
                                        const gboolean          value_assign_flag,
                                        const int               lifespan_days,
                                        FilterAggregateType     item_type,
                                        const ScoreAddItem    * items,
                                        const int               item_qty,
                                        const gboolean          do_rescore_all);

void        score_remove_score         (const char            * filename,
                                        const int               line_begin,
                                        const int               line_end,
                                        const gboolean          do_rescore_all);

#endif
