/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <glib.h>
#include <pan/base/pan-i18n.h>

#include <pan/filters/filter-new.h>

const gchar * FILTER_NEW_CLASS_ID = "PanObject::Filter::FilterNew";

/************
*************  PROTECTED
************/

static Filter*
filter_new_dup (const Filter * f_old)
{
	Filter * f_new = filter_new_article_new (FILTER_NEW(f_old)->type);
	filter_class_dup (f_old, f_new);
	return f_new;
}

static gchar*
filter_new_to_string (const Filter * filter)
{
	char * pch;
	gboolean negate = filter->negate;
	FilterNewType type = FILTER_NEW(filter)->type;

	switch (type) {
		case FILTER_NEW_NEW:
			pch = negate ? _("Article is unread or old") : _("Article is new");
			break;
		case FILTER_NEW_UNREAD:
			pch = negate ? _("Article is new or read") : _("Article is unread");
			break;
		case FILTER_NEW_READ:
			pch = negate ? _("Article is new or unread") : _("Article is read");
			break;
		default:
			pch = "ERROR";
			break;
	}

	return g_strdup (pch);
}

static void
filter_new_test_articles (Filter          * filter,
                          const Article  ** articles,
                          gint              article_qty,
                          gboolean        * passfail)
{
	gint i;
	FilterNewType type = FILTER_NEW(filter)->type;

	for (i=0; i<article_qty; ++i)
	{
		const Article * article = articles[i];

		switch (type)
		{
			case FILTER_NEW_NEW: 
				passfail[i] = article_is_new(article);
				break;
			case FILTER_NEW_UNREAD:
				passfail[i] = !article_is_new(article) && !article_is_read(article);
				break;
			case FILTER_NEW_READ: 
				passfail[i] = !article_is_new(article) && article_is_read(article);
				break;
		}
	}
}

/************
*************  PUBLIC
************/

Filter*
filter_new_article_new (FilterNewType type)
{
	FilterNew * f = g_new0 (FilterNew, 1);
	filter_constructor ((Filter*)f,
	                    filter_destructor,
	                    filter_new_test_articles,
	                    filter_new_to_string,
	                    filter_new_dup,
	                    FILTER_NEW_CLASS_ID);
	f->type = type;
	return FILTER(f);
}

void
filter_new_set_state (FilterNew * filter,
                      FilterNewType type)
{
	g_return_if_fail (filter != NULL);

	filter->type = type;
}

