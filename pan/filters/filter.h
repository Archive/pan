/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __FILTER_H__
#define __FILTER_H__

#include <pan/base/article.h>

extern const gchar * FILTER_CLASS_ID;
#define FILTER(a) ((Filter*)filter_cast((Filter*)a,FILTER_CLASS_ID))

typedef struct _Filter Filter;
typedef void (*FilterTestArticlesFunc)(Filter*, const Article**, gint qty, gboolean *passfail);
typedef gchar* (*FilterToStringFunc)(const Filter*);
typedef Filter* (*FilterDupFunc)(const Filter*);

struct _Filter
{
	/* Parent */
	PanObject parent;

	/* Fields */
	gboolean negate;
	const gchar * class_id;

	/* Virtual Functions */
	FilterTestArticlesFunc test_articles_func;
	FilterToStringFunc to_string_func;
	FilterDupFunc dup_func;
};

/**
***  Protected
**/

gpointer  filter_cast                 (Filter                * filter,
                                       const gchar           * class_name);

void      filter_class_dup            (const Filter          * filter_original,
                                       Filter                * filter_new);

void      filter_constructor          (Filter                * filter,
                                       PanObjectDestructor     dtor,
                                       FilterTestArticlesFunc  test_func,
                                       FilterToStringFunc      to_string_func,
                                       FilterDupFunc           dup_func,
                                       const gchar           * class_name);

void      filter_destructor           (PanObject             * filter);

/**
***  Public
**/

void      filter_negate               (Filter               * filter);

gboolean  filter_test_article         (Filter               * filter,
                                       const Article        * article);

void      filter_test_articles        (Filter               * filter,
                                       const Article       ** articles,
                                       gint                   qty,
                                       gboolean             * passfail);

void      filter_remove_failures      (Filter               * filter,
                                       GPtrArray            * articles);

char*     filter_to_string            (const Filter         * filter);

char*     filter_to_string_deep       (const Filter         * filter);

Filter*   filter_dup                  (const Filter         * filter);

/**
***  Poor-man's RTTI.  This will be replaced with GObject after glib 2.0
**/

gboolean  filter_isa                  (const Filter         * filter,
                                       const gchar          * class_name);

#endif
