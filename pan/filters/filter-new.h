/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __FILTER_NEW_H_
#define __FILTER_NEW_H_

#include <pan/filters/filter.h>

extern const gchar * FILTER_NEW_CLASS_ID;
#define FILTER_NEW(a) ((FilterNew*)filter_cast((Filter*)a,FILTER_NEW_CLASS_ID))

typedef struct _FilterNew   FilterNew;

typedef enum
{
	FILTER_NEW_NEW,
	FILTER_NEW_UNREAD,
	FILTER_NEW_READ
}
FilterNewType;

struct _FilterNew
{
	/* parent class */
	Filter parent;

	FilterNewType type;
};

/**
***  PUBLIC
**/

Filter*      filter_new_article_new             (FilterNewType type);

void         filter_new_set_state               (FilterNew * filter,
                                                 FilterNewType type);


#endif
