/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __FILTER_MANAGER_H__
#define __FILTER_MANAGER_H__

#include <pan/filters/filter.h>

/**
 * Initializes the filters and translation strings.
 */
void filter_manager_init (void);

/**
 * Returns the named filter, or NULL if no such filter found.
 *
 * When done with the returned filter, the caller must call
 * pan_object_unref() on it.
 */
Filter* filter_manager_get_named_filter (const gchar * name);

/**
 * Populates fillme with a set of FilterTops sorted by name.
 * These are just copies of what the manager has, so they
 * need to be free'd when you're done with them:
 * pan_g_ptr_array_foreach (fillme, (GFunc)pan_object_unref, NULL);
 */
void  filter_manager_get_filters    (GPtrArray * fillme);

/**
 * The manager copies the filters in <code>filters</code>by
 * value, not by reference, so you'll need to continue to
 * manage the objects in <code>filters</code> yourself.
 *
 * After the manager copies <code>filters</code> by reference,
 * it fires a filters-changed event.
 */
void  filter_manager_set_filters    (GPtrArray * filters);

/**
 * @call_obj: GPtrArray of FilterTop*s - DO NOT KEEP!
 * @call_arg: NULL
 */
PanCallback*   filter_manager_get_filters_changed_callback (void);

#endif
