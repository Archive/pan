/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib.h>

#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/filters/filter-binary.h>

const gchar * FILTER_BINARY_CLASS_ID = "PanObject::Filter::FilterBinary";

/************
*************  PROTECTED
************/

static Filter*
filter_binary_dup (const Filter * f_old)
{
	Filter * f_new = filter_binary_new ();
	FilterBinary * fb_old = FILTER_BINARY(f_old);
	FilterBinary * fb_new = FILTER_BINARY(f_new);

	/* duplicate superclass */
	filter_class_dup (f_old, f_new);

	/* duplicate binary */
	fb_new->state = fb_old->state;

	return f_new;
}

static gchar*
filter_binary_to_string (const Filter * filter)
{
	gchar * retval = NULL;
	const gboolean neg = filter->negate;
	FilterBinaryState state = FILTER_BINARY(filter)->state;

	if (state==FILTER_BINARY_COMPLETE) {
		if (neg)
			retval = g_strdup (_("Article does not have complete attachments"));
		else
			retval = g_strdup (_("Article has complete attachments"));
	}
	else if (state==FILTER_BINARY_INCOMPLETE) {
		if (neg)
			retval = g_strdup (_("Article does not have incomplete attachments"));
		else
			retval = g_strdup (_("Article has incomplete attachments"));
	}
	else if (state==FILTER_BINARY_NONBINARY) {
		if (neg)
			retval = g_strdup (_("Article has complete or incomplete attachments"));
		else
			retval = g_strdup (_("Article has no attachments"));
	}
	else retval = g_strdup (_("Error"));

	return retval;
}

static void
filter_binary_test_articles (Filter           * filter,
                             const Article   ** articles,
                             gint               article_qty,
			     gboolean         * passfail)
{
	gint i;
	FilterBinary * fb = FILTER_BINARY(filter);
	FilterBinaryState state = fb->state;

	for (i=0; i<article_qty; ++i)
	{
		const int multipart_state = article_get_multipart_state (articles[i]);
		gboolean b;

		switch (state)
		{
			case FILTER_BINARY_COMPLETE:
				b = multipart_state == MULTIPART_STATE_ALL;
				break;

			case FILTER_BINARY_INCOMPLETE:
				b = multipart_state == MULTIPART_STATE_SOME;
				break;

			case FILTER_BINARY_NONBINARY:
				b = multipart_state == MULTIPART_STATE_NONE;
				break;

			default:
				pan_warn_if_reached ();
				b = FALSE;
				break;
		}

		passfail[i] = b;
	}
}

static void
filter_binary_destructor (PanObject * o)
{
	filter_destructor (o);
}

static void
filter_binary_constructor (FilterBinary * f)
{
	filter_constructor ((Filter*)f,
	                    filter_binary_destructor,
	                    filter_binary_test_articles,
	                    filter_binary_to_string,
	                    filter_binary_dup,
	                    FILTER_BINARY_CLASS_ID);
	f->state = FILTER_BINARY_COMPLETE;
}

/************
*************  PUBLIC
************/

Filter*
filter_binary_new (void)
{
	FilterBinary * f = g_new0 (FilterBinary, 1);
	filter_binary_constructor (f);
	return FILTER(f);
}

void
filter_binary_set_state (FilterBinary        * filter_binary,
                         FilterBinaryState     state)
{
	g_return_if_fail (filter_binary!=NULL);
	g_return_if_fail (state==FILTER_BINARY_COMPLETE || state==FILTER_BINARY_INCOMPLETE || state==FILTER_BINARY_NONBINARY);

	filter_binary->state = state;
}
