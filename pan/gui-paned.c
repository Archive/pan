/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <ctype.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/debug.h>
#include <pan/base/pan-config.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/articlelist.h>
#include <pan/globals.h>
#include <pan/grouplist.h>
#include <pan/gui.h>
#include <pan/gui-paned.h>
#include <pan/prefs.h>
#include <pan/util.h>

/* FIXME: replace the old pan_config keys here & in gui_shutdown() then remove uglyhack_idx */
static GtkWidget*
pack_widgets (GtkWidget * w1, GtkWidget * w2, GtkOrientation orient, gint uglyhack_idx)
{
	GtkWidget * w;
	gboolean vertical = orient == GTK_ORIENTATION_VERTICAL;

	if (w1!=NULL && w2!=NULL) {
		int pos;

		if (uglyhack_idx == 0)
			pos = pan_config_get_int ("/Pan/Geometry/vpaned", 302);
		else
			pos = pan_config_get_int ("/Pan/Geometry/hpaned", 265);

		if (vertical) {
			w = gtk_vpaned_new ();
			gtk_widget_set_usize (w1, -1, 50);
			gtk_widget_set_usize (w2, -1, 50);
		} else {
			w = gtk_hpaned_new ();
			gtk_widget_set_usize (w1, 50, -1);
			gtk_widget_set_usize (w2, 50, -1);
		}
		gtk_paned_pack1 (GTK_PANED(w), w1, FALSE, TRUE);
		gtk_paned_pack2 (GTK_PANED(w), w2, TRUE, FALSE);
		gtk_paned_set_position (GTK_PANED(w), pos);
	}
	else if (w1!=NULL)
		w = w1;
	else if (w2!=NULL)
		w = w2;
	else
		w = NULL;

	return w;
}


/*---[ gui_paned_construct ]------------------------------------------
 * build the paned GUI layout
 *--------------------------------------------------------------------*/
void
gui_paned_construct (void)
{
	GtkWidget * w [3];
	GtkWidget * work;
	const gchar * pch;
	int i;
	debug_enter ("gui_paned_construct");

	gui_layout_clear_workarea ();

	/**
	***  The layout_str is a four-character string where the first
	***  character is in range [1..6] representing the six layouts
	***  Pan supports.  The remaining three characters show which
	***  widgets are placed first, second, and third, respectively.
	***  The grouplist is represented by 'g', the thread view is
	***  represented by 't', and the article is represented by 'a'.
	**/

	pch = layout_str + 1;
	for (i=0; i<=2; ++i, ++pch) {
		if (tolower(*pch)=='g')
			w[i] = show_group_pane ? groups_vbox : NULL;
		else if (tolower(*pch)=='t')
			w[i] = show_header_pane ? articlelist_ctree : NULL;
		else
			w[i] = show_body_pane ? text_box : NULL;
	}

	/* layout part 2: build the array */
	switch (*layout_str)
	{
		case '1':
		{
			GtkWidget * h = pack_widgets (w[0], w[1], GTK_ORIENTATION_HORIZONTAL, 1);
			work = pack_widgets (h, w[2], GTK_ORIENTATION_VERTICAL, 0);
			break;
		}
		case '2':
		{
			GtkWidget * h = pack_widgets (w[1], w[2], GTK_ORIENTATION_HORIZONTAL, 1);
			work = pack_widgets (w[0], h, GTK_ORIENTATION_VERTICAL, 0);
			break;
		}
		case '3':
		{
			GtkWidget * v = pack_widgets (w[0], w[1], GTK_ORIENTATION_VERTICAL, 0);
			work = pack_widgets (v, w[2], GTK_ORIENTATION_HORIZONTAL, 1);
			break;
		}
		case '5':
		{
			GtkWidget * v = pack_widgets (w[0], w[1], GTK_ORIENTATION_VERTICAL, 0);
			work = pack_widgets (v, w[2], GTK_ORIENTATION_VERTICAL, 1);
			break;
		}
		case '6':
		{
			GtkWidget * h = pack_widgets (w[0], w[1], GTK_ORIENTATION_HORIZONTAL, 0);
			work = pack_widgets (h, w[2], GTK_ORIENTATION_HORIZONTAL, 1);
			break;
		}
		case '4':
		default:
		{
			GtkWidget * v = pack_widgets (w[1], w[2], GTK_ORIENTATION_VERTICAL, 0);
			work = pack_widgets (w[0], v, GTK_ORIENTATION_HORIZONTAL, 1);
			break;
		}
	}

	if (work != NULL)
		gtk_box_pack_start (GTK_BOX(Pan.workarea), work, TRUE, TRUE, 0);

	Pan.viewmode = GUI_PANED;
	gui_restore_column_widths (Pan.group_tree, "group");
	gtk_widget_show_all (GTK_WIDGET(Pan.workarea));

	debug_exit ("gui_paned_construct");
}


/*---[ gui_paned_get_current_pane_nolock ]----------------------------
 * get the current pane, returns an integer reflecting which pane
 * the user is 'in' ... almost identical to the gtk_notebook_get_page
 * functions
 *--------------------------------------------------------------------*/
int
gui_paned_get_current_pane_nolock (void)
{
	int retval = GROUPS_PANE; /* default to group pane */

	if (GTK_WIDGET_HAS_FOCUS (Pan.article_ctree))
		retval = HEADERS_PANE;
	else if (GTK_WIDGET_HAS_FOCUS (Pan.text))
		retval = BODY_PANE;
	
	return retval;
}

void
gui_paned_page_set_nolock (int page, GtkWidget * focus_item)
{
	if (GTK_IS_WIDGET(focus_item))
		gtk_widget_grab_focus (focus_item);
}
