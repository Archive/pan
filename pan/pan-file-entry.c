/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * This is a quick-and-dirty substitute for the Gnome widget for
 * non-Gnome environments.  This should be compiled out on Gnome systems.
 */

#include <gtk/gtk.h>
#include <pan/util.h>
#include <pan/pan-file-entry.h>
#include <pan/base/pan-i18n.h>

static void
file_dialog_response_cb (GtkDialog * dialog, int response, gpointer user_data)
{
	if (response == GTK_RESPONSE_OK)
		pan_file_entry_set (GTK_WIDGET(user_data), 
		                    gtk_file_selection_get_filename (GTK_FILE_SELECTION(dialog)));

	if (response == GTK_RESPONSE_CANCEL || response == GTK_RESPONSE_OK)
		gtk_widget_destroy (GTK_WIDGET(dialog));
}

static void
entry_clicked_cb (GtkWidget * button, gpointer user_data)
{
	GtkWidget * w;
	GtkWidget * top;
	const char * title = g_object_get_data (G_OBJECT(user_data), "title");
	GString * file = g_string_new (pan_file_entry_get (GTK_WIDGET(user_data)));

	/* create the dialog */
       	w = gtk_file_selection_new (title);
	top = gtk_widget_get_toplevel (button);
	if (GTK_WIDGET_TOPLEVEL (top)) {
		gtk_window_set_transient_for (GTK_WINDOW(w), GTK_WINDOW(top));
		gtk_window_set_destroy_with_parent (GTK_WINDOW (w), TRUE);
	}

	/* make sure directory strings end with a separator for the file selection dialog */
	if (file->len && file->str[file->len-1]!=G_DIR_SEPARATOR && g_file_test (file->str, G_FILE_TEST_IS_DIR))
		g_string_append_c (file, G_DIR_SEPARATOR);

	/* initialize its state and listen for changes */
	gtk_file_selection_set_filename (GTK_FILE_SELECTION(w), file->str);
	g_signal_connect (w, "response", G_CALLBACK(file_dialog_response_cb), user_data);
	gtk_widget_show_all (w);

	/* cleanup */
	g_string_free (file, TRUE);
}

GtkWidget*
pan_file_entry_new (const char * title)
{
	GtkWidget * e;
	GtkWidget * b;
	GtkWidget * hbox;

	/* create the widgetry */
	hbox = gtk_hbox_new (FALSE, GUI_PAD);
	e = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX(hbox), e, TRUE, TRUE, 0);
	b = gtk_button_new_with_mnemonic (_("_Browse..."));
	g_signal_connect (b, "clicked", G_CALLBACK(entry_clicked_cb), hbox);
	gtk_box_pack_start (GTK_BOX(hbox), b, FALSE, FALSE, 0);

	/* add the keys */
	g_object_set_data_full (G_OBJECT(hbox), "title", g_strdup(title), g_free);
	g_object_set_data (G_OBJECT(hbox), "entry", e);
	pan_file_entry_set (hbox, pan_get_home_dir());

	gtk_widget_show (e);
	gtk_widget_show (b);
	return hbox;
}

void
pan_file_entry_set (GtkWidget * w, const char * file)
{
	GtkEntry * e = GTK_ENTRY(g_object_get_data(G_OBJECT(w), "entry"));
	pan_gtk_entry_set_text (GTK_WIDGET(e), file);
}

const char*
pan_file_entry_get (GtkWidget * w)
{
	GtkEntry * e = GTK_ENTRY(g_object_get_data(G_OBJECT(w), "entry"));
	return (const char*) gtk_entry_get_text (e);
}

GtkWidget*
pan_file_entry_gtk_entry (GtkWidget * w)
{
	return GTK_WIDGET (g_object_get_data (G_OBJECT(w), "entry"));
}
