/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/debug.h>
#include <pan/base/group.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/serverlist.h>

#include <pan/dialogs/dialogs.h>

#include <pan/articlelist.h>
#include <pan/grouplist.h>
#include <pan/group-action.h>
#include <pan/group-ui.h>
#include <pan/globals.h>
#include <pan/prefs.h>
#include <pan/queue.h>
#include <pan/task-headers.h>
#include <pan/task-group-count.h>
#include <pan/util.h>

/**
***  Utilities
**/

static GPtrArray*
get_selected_groups (void)
{
	GPtrArray * a = grouplist_get_selected_groups ();

	if (!a->len) {
		Group * g = articlelist_get_group ();
		if (g != NULL)
			g_ptr_array_add (a, g);
	}

	return a;
}

static void
start_task_headers (GPtrArray            * a,
                    HeaderDownloadType     type,
                    gboolean               bodies)
{
	guint i;
	GSList * l = NULL;
	debug_enter ("start_task_headers");
			 
	/* make download tasks for all the selected groups. */
	for (i=0;  i<a->len; ++i) {
		Group * group = GROUP(g_ptr_array_index(a,i));
		if (!group_is_folder (group))
		{
			PanObject * o = bodies
				? task_headers_new_with_bodies (group, type)
				: task_headers_new (group, type);
			l = g_slist_prepend (l, o);
		}
	}

	/* queue the tasks. */
	if (l != NULL) {
		l = g_slist_reverse (l);
		queue_insert_tasks (l, 0);
		g_slist_free (l);
	}

	debug_exit ("start_task_headers");
}

/**
***  Public Actions
**/

void
group_action_selected_download_new (void)
{
	GPtrArray * a;
	debug_enter ("group_action_selected_download_new");
			 
	a = get_selected_groups ();
	start_task_headers (a, HEADERS_NEW, FALSE);
	g_ptr_array_free (a, TRUE);
						 
	debug_exit ("group_action_selected_download_new");
}

void
group_action_selected_download_new_and_bodies (void)
{
	GPtrArray * a = get_selected_groups ();
	start_task_headers (a, HEADERS_NEW, TRUE);
	g_ptr_array_free (a, TRUE);
}

void
group_action_subscribed_download_new_and_bodies (void)
{
	GPtrArray * a = server_get_groups (serverlist_get_active_server(), SERVER_GROUPS_SUBSCRIBED);
	start_task_headers (a, HEADERS_NEW, TRUE);
	g_ptr_array_free (a, TRUE);
}

void
group_action_subscribed_download_new (void)
{
	GPtrArray * a = server_get_groups (serverlist_get_active_server(), SERVER_GROUPS_SUBSCRIBED);
	start_task_headers (a, HEADERS_NEW, FALSE);
	g_ptr_array_free (a, TRUE);
}


void
group_action_selected_download_dialog (void)
{
	GPtrArray * a;
	debug_enter ("group_action_selected_download_dialog");
				 
	/* sample all the selected groups */
	a = get_selected_groups ();
	dialog_download_headers (GTK_WINDOW(Pan.window), a);

	/* cleanup */
	g_ptr_array_free (a, TRUE);
	debug_exit ("group_action_selected_download_dialog");
}

void
group_action_selected_mark_read (void)
{
	GPtrArray * a;
	guint i;
	debug_enter ("group_action_selected_mark_read");

	a = get_selected_groups ();
	for (i=0; i<a->len; ++i)
		group_mark_all_read (GROUP(g_ptr_array_index(a,i)), TRUE);

	g_ptr_array_free (a, TRUE);
	debug_exit ("group_action_selected_mark_read");
}

void
group_action_selected_update_count_info (void)
{
	GPtrArray * a;
	debug_enter ("group_action_selected_update_count_info");

	a = get_selected_groups ();
	if (a->len)
		queue_add (TASK(task_group_count_new ((Group**)a->pdata, a->len)));

	g_ptr_array_free (a, TRUE);
	debug_exit ("group_action_selected_update_count_info");
}

void
group_action_selected_subscribe (void)
{
	GPtrArray * a;
	debug_enter ("group_action_selected_subscribe");

	a = get_selected_groups ();
	if (a->len)
		groups_set_subscribed ((Group**)a->pdata, a->len, TRUE);

	g_ptr_array_free (a, TRUE);
	debug_exit ("group_action_selected_subscribe");
}

void
group_action_selected_unsubscribe (void)
{
	GPtrArray * a;
	debug_enter ("group_action_selected_unsubscribe");

	a = get_selected_groups ();
	if (a->len)
		groups_set_subscribed ((Group**)a->pdata, a->len, FALSE);

	g_ptr_array_free (a, TRUE);
	debug_exit ("group_action_selected_unsubscribe");
}

void
group_action_selected_properties (void)
{
	guint i;
        GPtrArray * a;
	debug_enter ("group_action_selected_properties");
		 
	a = get_selected_groups ();
	for (i=0; i<a->len; ++i)
		dialog_group_props (GTK_WINDOW(Pan.window), GROUP(g_ptr_array_index(a,i)));

	g_ptr_array_free (a, TRUE);
	debug_exit ("group_action_selected_properties");
}

void
group_action_selected_empty (void)
{
	guint i;
        GPtrArray * groups;
	debug_enter ("group_action_selected_empty");

	groups = get_selected_groups ();
	for (i=0; i!=groups->len; ++i)
		group_empty (GROUP(g_ptr_array_index(groups,i)), FALSE);
	g_ptr_array_free (groups, TRUE);

	debug_exit ("group_action_selected_empty");
}

/***
****
***/

static void
delete_groups_dialog_response_cb (GtkWidget * dialog, int response, gpointer data)
{
	GPtrArray * a;
	debug_enter ("delete_groups_dialog_cb");

	a = (GPtrArray*) data;

	if (response == GTK_RESPONSE_YES)
	{
		Server * s = GROUP(g_ptr_array_index(a,0))->server;
		server_destroy_groups (s, (Group**)a->pdata, a->len);
	}

	g_ptr_array_free (a, TRUE);
	gtk_widget_destroy (GTK_WIDGET(dialog));
	debug_exit ("delete_groups_dialog_cb");
}
void
group_action_selected_destroy (void)
{
	GPtrArray * a = get_selected_groups ();
	if (a->len != 0)
	{
		GtkWidget * w;
		w = gtk_message_dialog_new (GTK_WINDOW(Pan.window),
		                            GTK_DIALOG_DESTROY_WITH_PARENT,
		                            GTK_MESSAGE_QUESTION,
		                            GTK_BUTTONS_YES_NO,
		                            _("Are you sure you want to delete these %d groups/folders and their articles?"),
		                            (int)a->len);
		g_signal_connect (GTK_OBJECT(w), "response", G_CALLBACK(delete_groups_dialog_response_cb), a);
		gtk_widget_show (w);
	}
	else g_ptr_array_free (a, TRUE);
}
