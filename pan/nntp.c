/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include <pan/base/acache.h>
#include <pan/base/article.h> /* for HEADER_NEWSGROUPS */
#include <pan/base/debug.h>
#include <pan/base/log.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/nntp.h>
#include <pan/prefs.h> /* for pan_mute */

/**
**/

enum
{
	AUTH_REQUIRED              = 480,
	AUTH_NEED_MORE             = 381,
	AUTH_ACCEPTED              = 281,
	AUTH_REJECTED              = 482,

	SERVER_READY               = 200,
	SERVER_READY_NO_POSTING    = 201,

	GOODBYE                    = 205,

	GROUP_RESPONSE             = 211,
	GROUP_NONEXISTENT          = 411,

	INFORMATION_FOLLOWS        = 215,

	XOVER_FOLLOWS              = 224,
	XOVER_NO_ARTICLES          = 420,

	ARTICLE_FOLLOWS            = 220,

	NEWGROUPS_FOLLOWS          = 231,

	ARTICLE_POSTED_OK          = 240,
	SEND_ARTICLE_NOW           = 340,
	NO_POSTING                 = 440,
	POSTING_FAILED             = 441,

	TOO_MANY_CONNECTIONS       = 400,

	NO_GROUP_SELECTED          = 412,
	NO_SUCH_ARTICLE_NUMBER     = 423,
	NO_SUCH_ARTICLE            = 430,

	ERROR_CMD_NOT_UNDERSTOOD   = 500,
	ERROR_CMD_NOT_SUPPORTED    = 501,
	NO_PERMISSION              = 502,
	ERROR_TIMEOUT              = 503
};

typedef enum
{
	WATCH_MODE_IGNORE,
	WATCH_MODE_READ,
	WATCH_MODE_READ_LATER,
	WATCH_MODE_WRITE,
}
WatchMode;

static gulong _total_xfer_bytes = 0ul;

typedef struct
{
	nntp_done_func done_func;
	gpointer       done_func_user_data;

	nntp_line_func line_func;
	gpointer       line_func_user_data;

	nntp_group_func group_func;
	gpointer        group_func_user_data;

	char * username;
	char * password;
	char * command;
	char * post;
	GQuark group_quark;

	GString * buf_in;
	GString * buf_out;

	WatchMode mode;

	guint tag_watch;

	gboolean nntp_response_text;

	const gboolean * abort_flag;

	time_t xfer_start_time;
	gulong xfer_bytes;
}
NNTPImpl;

/***
**** 
***/

static void
fire_done_func (NNTPImpl * impl,
                GIOChannel * channel,
                TaskStateEnum state)
{
	if (impl->done_func)
	{
		nntp_done_func func = impl->done_func;
		gpointer user_data = impl->done_func_user_data;

		impl->done_func = NULL;
		impl->done_func_user_data = NULL;

		(*func)(channel, state, user_data);
	}
}


static NNTPImpl*
get_impl (GIOChannel * io)
{
	static GHashTable * impl_hash = 0;
	NNTPImpl * impl;

	if (impl_hash == NULL)
		impl_hash = g_hash_table_new (g_direct_hash, g_direct_equal);
	impl = (NNTPImpl*) g_hash_table_lookup (impl_hash, io);
	if (impl == NULL) {
		impl = g_new0 (NNTPImpl, 1);
		impl->group_quark = 0;
		impl->buf_in = g_string_new (NULL);
		impl->buf_out = g_string_new (NULL);
		impl->xfer_start_time = time (NULL);
		impl->xfer_bytes = 0ul;
		g_hash_table_insert (impl_hash, io, impl);
	}
	return impl;
}

static void
increment_xfer_stats (NNTPImpl * impl, gulong bytes)
{
	impl->xfer_bytes += bytes;
	_total_xfer_bytes += bytes;
}

static void set_watch_mode (GIOChannel * channel, WatchMode mode);

static void
set_group (GIOChannel * io, GQuark group_quark)
{
	get_impl(io)->group_quark = group_quark;
}

static void
set_funcs (NNTPImpl             * impl,
           nntp_line_func         line_func,
           gpointer               line_func_user_data,
           nntp_done_func         done_func,
           gpointer               done_func_user_data,
           nntp_group_func        group_func,
           gpointer               group_func_user_data)
{
	impl->line_func            = line_func;
	impl->line_func_user_data  = line_func_user_data;
	impl->done_func            = done_func;
	impl->done_func_user_data  = done_func_user_data;
	impl->group_func           = group_func;
	impl->group_func_user_data = group_func_user_data;
}

/***
****  WRITING
***/

static gboolean
nntp_write (GIOChannel * channel, NNTPImpl * impl)
{
	GString * g = impl->buf_out;
	gboolean finished;
	GIOStatus status;
	GError * err = 0;
	gsize out = 0;

	debug4 (DEBUG_SOCKET_OUTPUT,
	        "channel %p writing [%*.*s]",
                channel, g->len-2, g->len-2, g->str);

	status = g->len
		? g_io_channel_write_chars (channel, g->str, g->len, &out, &err)
		: G_IO_STATUS_NORMAL;

	if (status == G_IO_STATUS_NORMAL)
		status = g_io_channel_flush (channel, &err);

	if (err) {
		log_add (LOG_ERROR, err->message);
		g_error_free (err);
		fire_done_func (impl, channel, TASK_FAIL_NETWORK);
		return FALSE;
	}

	if (out) {
		increment_xfer_stats (impl, out);
		g_string_erase (g, 0, out);
	}

	finished = (!g->len) && (status==G_IO_STATUS_NORMAL);

	/* on 'quit', don't wait for a response from the server.
	   we send 'quit' before closing the connection even when
	   we're note sure of the health of the connection. */
	if (finished) {
		if (impl->command && !strcmp (impl->command, "QUIT"))
		{
			fire_done_func (impl, channel, TASK_OK);
			set_watch_mode (channel, WATCH_MODE_IGNORE);
		}
		else set_watch_mode (channel, WATCH_MODE_READ);
	}

	if (finished)
		debug1 (DEBUG_SOCKET_OUTPUT,
		        "channel %p done writing", channel);
	return !finished;
}

static void
ensure_trailing_crlf (GString * g)
{
	if (g->len<2 || g->str[g->len-2]!='\r' || g->str[g->len-1]!='\n')
		g_string_append (g, "\r\n");
}

static void
nntp_send_request (GIOChannel * channel, const char * format, ...)
{
	va_list args;
	char * str;
	NNTPImpl * impl;

	va_start (args, format);
	str = g_strdup_vprintf (format, args);
	va_end (args);

	impl = get_impl (channel);
	g_string_assign (impl->buf_out, str);
	ensure_trailing_crlf (impl->buf_out);

	set_watch_mode (channel, WATCH_MODE_WRITE);

	g_free (str);
}

static void
nntp_cmd (GIOChannel * io, const gboolean * abort_flag, const char * fmt, ...)
{
	NNTPImpl * impl;
	va_list args;
	char * str;

	va_start (args, fmt);
	str = g_strdup_vprintf (fmt, args);
	va_end (args);

	impl = get_impl (io);
	impl->abort_flag = abort_flag;
	replace_gstr (&impl->command, str);

	nntp_send_request (io, "%s", str);
}

/***
****  READING
***/

static gboolean
process_line (NNTPImpl * impl, GIOChannel * io, GString * gstr)
{
	int done_state = INT_MAX;

	if (impl->nntp_response_text)
	{
		if (gstr->len==1 && gstr->str[0]=='.') // end-of-list
		{
			done_state = TASK_OK;
			impl->nntp_response_text = FALSE;
		}
		else
		{
			if (gstr->len>=2
			    && gstr->str[0]=='.'
			    && gstr->str[1]=='.') /* rfc 977: 2.4.1 */
				g_string_erase (gstr, 0, 1);

			if (impl->line_func)
				(*impl->line_func)(io,
				                   gstr->str,
				                   gstr->len,
				                   impl->line_func_user_data);
		}
	}
	else switch (atoi (gstr->str))
	{
		case SERVER_READY:
		case SERVER_READY_NO_POSTING:
			/* FIXME: we lose posting flag */
			done_state = TASK_OK;
			break;

		case ARTICLE_POSTED_OK:
		case GOODBYE:
		case XOVER_NO_ARTICLES:
			done_state = TASK_OK;
			break;

		case AUTH_REQUIRED:
			/* must send username */
			nntp_send_request (io,
				"AUTHINFO USER %s", impl->username);
			break;

		case AUTH_NEED_MORE:
			/* must send password */
			nntp_send_request (io,
				"AUTHINFO PASS %s", impl->password);
			break;

		case AUTH_ACCEPTED:
			/* logged in ok; now retry request */
			nntp_send_request (io, "%s", impl->command);
			break;

		case AUTH_REJECTED:
			done_state = TASK_FAIL;
			break;

		case GROUP_RESPONSE: {
			GQuark group_quark;
			char * group_name;
			gulong aqty, alo, ahi;
			const char * pch = gstr->str;
			skip_next_token (pch, ' ', &pch); /* GROUP_RESPONSE */
			aqty = get_next_token_ulong (pch, ' ', &pch);
			alo  = get_next_token_ulong (pch, ' ', &pch);
			ahi  = get_next_token_ulong (pch, ' ', &pch);
			group_name = g_strdup (pch);
			g_strstrip (group_name);
			group_quark = g_quark_from_string (group_name);
			set_group (io, group_quark);
			g_free (group_name);
			if (impl->group_func)
				(*impl->group_func)(io, aqty, alo, ahi, group_quark,
				                   impl->group_func_user_data); 
			done_state = TASK_OK;
			break;
		}

		case SEND_ARTICLE_NOW:
			/* ready to get article; send it now */
			nntp_send_request (io, "%s", impl->post);
			break;

		case NO_POSTING:
		case POSTING_FAILED:
			done_state = TASK_FAIL;
			break;

		case GROUP_NONEXISTENT:
			log_add_va (LOG_ERROR,
			            _("Unable to set group: %s"), gstr->str);
			done_state = TASK_FAIL;
			break;

		case XOVER_FOLLOWS:
		case ARTICLE_FOLLOWS:
		case NEWGROUPS_FOLLOWS:
		case INFORMATION_FOLLOWS:
			impl->nntp_response_text = TRUE;
			break;


		case NO_GROUP_SELECTED:
		case ERROR_CMD_NOT_UNDERSTOOD:
		case ERROR_CMD_NOT_SUPPORTED:
		case NO_PERMISSION:
		case NO_SUCH_ARTICLE_NUMBER:
		case NO_SUCH_ARTICLE:
		case ERROR_TIMEOUT:
			log_add_va (LOG_ERROR, _("Error: %s"), gstr->str);
			done_state = TASK_FAIL;
			break;

		case TOO_MANY_CONNECTIONS:
			done_state = TASK_FAIL_NETWORK;
			break;

		default:
			g_message ("I don't know how to handle this: [%s]",
			           gstr->str);
			done_state = TASK_FAIL;
			break;
	}

	if (done_state == INT_MAX)
		return TRUE; /* more to come */
	else {
		fire_done_func (impl, io, done_state);
		return FALSE;
	}
}

static gboolean
nntp_read (GIOChannel    * channel,
           NNTPImpl      * impl)
{
	GError * err = 0;
	GString * g = impl->buf_in;
	gboolean read_more_now = TRUE;

	switch (g_io_channel_read_line_string (channel, g, NULL,  &err))
	{
		case G_IO_STATUS_AGAIN:
			//odebug1 ("reading channel %p got EAGAIN...", channel);
			read_more_now = FALSE;
			set_watch_mode (channel, WATCH_MODE_READ_LATER);
			break;

		case G_IO_STATUS_ERROR:
		case G_IO_STATUS_EOF:
			if (err != NULL) {
				log_add_va (LOG_ERROR,
				            _("Error reading from socket: %s"),
				            err->message);
				g_error_free (err);
			}
			fire_done_func (impl, channel, TASK_FAIL_NETWORK);
			break;

		case G_IO_STATUS_NORMAL:
			increment_xfer_stats (impl, g->len);
			/* strip off \r\n and process the line */
			if (g->len>=2
			    && g->str[g->len-2]=='\r'
			    && g->str[g->len-1]=='\n')
				g_string_truncate (g, g->len-2);
			debug2 (DEBUG_SOCKET_INPUT,
			        "channel %p read [%s]", channel, g->str);
			read_more_now = process_line (impl, channel, g);
			break;
	}

	if (!read_more_now)
		debug1 (DEBUG_SOCKET_INPUT,
		        "channel %p nntp_read done reading", channel);
	return read_more_now;
}

/***
****
***/

static gboolean
nntp_gio_func (GIOChannel * channel, GIOCondition cond, gpointer impl_gpointer)
{
	gboolean gimmie_more;
	NNTPImpl * impl =  (NNTPImpl*) impl_gpointer;

	if (impl->abort_flag && *impl->abort_flag)
	{
		set_watch_mode (channel, WATCH_MODE_IGNORE);
		fire_done_func (impl, channel, TASK_FAIL);
		gimmie_more = FALSE;
	}
	else if (!(cond & (G_IO_IN | G_IO_OUT)))
	{
		set_watch_mode (channel, WATCH_MODE_IGNORE);
		fire_done_func (impl, channel, TASK_FAIL_NETWORK);
		gimmie_more = FALSE;
	}
	else if (cond & G_IO_IN)
	{
		gimmie_more = nntp_read (channel, impl);
	}
	else /* G_IO_OUT */
	{
		gimmie_more = nntp_write (channel, impl);
	}

	return gimmie_more;
}

static gboolean
nntp_read_later (gpointer channel_gpointer)
{
	GIOChannel * channel = (GIOChannel*) channel_gpointer;
	set_watch_mode (channel, WATCH_MODE_READ);
	return FALSE;
}

#define SECONDS_TO_SLEEP_ON_EAGAIN 0.75
#define SLEEP_MSEC ((guint)(SECONDS_TO_SLEEP_ON_EAGAIN * 1000.0))

/**
 * set_watch_mode is a utility for adding GIOCondition watches and handling
 * the nntp_read_later loop.  This state model enforces having a single
 * watch per channel at a time.  Limiting ourselves to one watch per
 * channel is desirable for Windows stability.
 */
static void
set_watch_mode (GIOChannel * channel, WatchMode mode)
{
	guint cond;
	NNTPImpl * impl = get_impl (channel);

	if (impl->tag_watch)
	{
		g_source_remove (impl->tag_watch);
		impl->tag_watch = 0;
	}

	switch (mode)
	{
		case WATCH_MODE_IGNORE:
			debug1 (DEBUG_SOCKET,
			        "channel %p watch mode **IGNORE**", channel);
			/* don't add any watches */
			break;

		case WATCH_MODE_READ:
			debug1 (DEBUG_SOCKET,
			        "channel %p setting mode read", channel); 
			cond = G_IO_IN | G_IO_ERR | G_IO_HUP | G_IO_NVAL;
			impl->tag_watch = g_io_add_watch (
				channel, cond, nntp_gio_func, impl);
			break;

		case WATCH_MODE_READ_LATER:
			debug1 (DEBUG_SOCKET,
			        "channel %p setting mode read LATER", channel);
			impl->tag_watch = g_timeout_add (
				SLEEP_MSEC, nntp_read_later, channel);
			break;

		case WATCH_MODE_WRITE:
			debug1 (DEBUG_SOCKET,
			        "channel %p setting mode write", channel); 
			cond = G_IO_OUT | G_IO_ERR | G_IO_HUP | G_IO_NVAL;
			impl->tag_watch = g_io_add_watch (
				channel, cond, nntp_gio_func, impl);
			break;
	}
}

/***
****
***/

void
nntp_handshake (GIOChannel      * channel,
                const char      * username,
                const char      * password,
                nntp_done_func    done_func,
                gpointer          user_data)
{
	NNTPImpl * impl = get_impl (channel);
	impl->username = g_strdup (username);
	impl->password = g_strdup (password);
	set_funcs (impl, NULL, NULL, done_func, user_data, NULL, NULL);

	/* try to read the handshake line */
	set_watch_mode (channel, WATCH_MODE_READ);
}

void
nntp_xover (GIOChannel      * io,
            const gboolean  * abort_flag,
            nntp_line_func    line_func,
            gpointer          line_func_user_data,
            nntp_done_func    done_func,
            gpointer          done_func_user_data,
            gulong            low,
            gulong            high)
{
	g_return_if_fail (io != NULL);
	g_return_if_fail (nntp_get_group(io) != 0);

	set_funcs (get_impl(io), line_func, line_func_user_data,
	                         done_func, done_func_user_data,
	                         NULL, NULL);

	nntp_cmd (io, abort_flag, "XOVER %lu-%lu", low, high);
}

void
nntp_list_newsgroups (GIOChannel      * io,
                      const gboolean  * abort_flag,
                      nntp_line_func    line_func,
                      gpointer          line_func_user_data,
                      nntp_done_func    done_func,
                      gpointer          done_func_user_data)
{
	g_return_if_fail (io != NULL);

	set_funcs (get_impl(io), line_func, line_func_user_data,
	                         done_func, done_func_user_data,
	                         NULL, NULL);

	nntp_cmd (io, abort_flag, "LIST NEWSGROUPS");
}

void
nntp_list (GIOChannel      * io,
           const gboolean  * abort_flag,
           nntp_line_func    line_func,
           gpointer          line_func_user_data,
           nntp_done_func    done_func,
           gpointer          done_func_user_data)
{
	g_return_if_fail (io != NULL);

	set_funcs (get_impl(io), line_func, line_func_user_data,
	                         done_func, done_func_user_data,
	                         NULL, NULL);

	nntp_cmd (io, abort_flag, "LIST");
}


void
nntp_article (GIOChannel      * io,
              const gboolean  * abort_flag,
              nntp_line_func    line_func,
              gpointer          line_func_user_data,
              nntp_done_func    done_func,
              gpointer          done_func_user_data,
              gulong            number)
{
	g_return_if_fail (io != NULL);
	g_return_if_fail (nntp_get_group(io) != 0);

	set_funcs (get_impl(io), line_func, line_func_user_data,
	                         done_func, done_func_user_data,
	                         NULL, NULL);

	nntp_cmd (io, abort_flag, "ARTICLE %lu", number);
}

GQuark
nntp_get_group (GIOChannel  * io)
{
	return get_impl(io)->group_quark;
}

void
nntp_group (GIOChannel         * io,
            GQuark               group_quark,
            nntp_group_func      group_func,
            gpointer             group_func_user_data,
            nntp_done_func       done_func,
            gpointer             done_func_user_data)
{
	g_return_if_fail (io != NULL);
	g_return_if_fail (group_quark != 0);
	g_return_if_fail (g_quark_to_string(group_quark) != NULL);

	set_funcs (get_impl(io), NULL, NULL,
	                         done_func, done_func_user_data,
	                         group_func, group_func_user_data);

	nntp_cmd (io, NULL, "GROUP %s", g_quark_to_string(group_quark));
}

void
nntp_goodbye (GIOChannel         * io,
              nntp_done_func       done_func,
              gpointer             done_func_user_data)
{
	g_return_if_fail (io != NULL);

	set_funcs (get_impl(io), NULL, NULL,
	                         done_func, done_func_user_data,
	                         NULL, NULL);

	nntp_cmd (io, NULL, "QUIT");
}

void
nntp_noop (GIOChannel         * io,
           nntp_done_func       done_func,
           gpointer             done_func_user_data)
{
	g_return_if_fail (io != NULL);

	set_funcs (get_impl(io), NULL, NULL,
	                         done_func, done_func_user_data,
	                         NULL, NULL);

	nntp_cmd (io, NULL, "MODE READER");
}

void
nntp_post (GIOChannel        * io,
           const char        * msg,
           nntp_done_func      done_func,
           gpointer            done_func_user_data)
{
	/* if we're in mute mode, don't post */
	if (pan_mute)
	{
		fprintf (stderr, "Mute: Your Message won't be posted.");
		fprintf (stderr, "\nYour message:\n%s<end of message>\n", msg);
		fflush (NULL);
	}
	else
	{
		GString * gstr;
		NNTPImpl * impl = get_impl (io);

		/* set 'post' to the message plus the end-of-post dot */
		gstr = g_string_new (msg);
		ensure_trailing_crlf (gstr);
		g_string_append (gstr, ".\r\n");
		replace_gstr (&impl->post, g_string_free(gstr,FALSE));
	
		set_funcs (impl, NULL, NULL,
		                 done_func, done_func_user_data,
		                 NULL, NULL);
		nntp_cmd (io, NULL, "POST");
	}
}

void
nntp_cancel  (GIOChannel         * channel,
              const PString      * message_id,
              nntp_done_func       done_func,
              gpointer             done_func_user_data)
{
	const char * newsgroups;
	const char * author;
	GMimeMessage * message;
	debug_enter ("nntp_cancel");

	/* sanity checks */
	g_return_if_fail (channel != NULL);
	g_return_if_fail (pstring_is_set (message_id));
	g_return_if_fail (acache_has_message(ACACHE_DEFAULT_KEY, message_id));

	/* get info */
	message = acache_get_message (ACACHE_DEFAULT_KEY, &message_id, 1);
	newsgroups = g_mime_message_get_header (message, HEADER_NEWSGROUPS);
	author = g_mime_message_get_sender (message);
	if (is_nonempty_string(newsgroups) && is_nonempty_string(author))
	{
		char * buf = g_strdup_printf (
			"From: %s\r\n"
			"Newsgroups: %s\r\n"
			"Subject: cancel %s\r\n"
			"Control: cancel %s\r\n"
			"\r\n"
			"Ignore\r\n"
			"Article canceled by Pan %s\r\n",
			author,
			newsgroups,
			message_id->str,
			message_id->str,
			VERSION);

		/* post the cancel message */
		nntp_post (channel, buf, done_func, done_func_user_data);

		/* cleanup */
		g_free (buf);
	}

	/* cleanup */
	g_object_unref (message);
	debug_exit ("nntp_cancel");
}

gulong
nntp_get_xfer_rate_KiBps (GIOChannel * io)
{
	double time_span;
	NNTPImpl * impl = get_impl (io);

	g_return_val_if_fail (impl!=NULL, 0ul);

	time_span = difftime (time(NULL), impl->xfer_start_time);
	return (gulong)((impl->xfer_bytes / 1024ul) / time_span);
}

gulong
nntp_step_total_xfer_bytes (void)
{
	const gulong bytes = _total_xfer_bytes;
	_total_xfer_bytes = 0ul;
	return bytes;
}
