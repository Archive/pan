/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <glib.h>

#include <pan/task.h>
#include <pan/base/pstring.h>

extern void queue_init (gboolean online, gboolean remove_failed_tasks);

extern void queue_add (Task*);

extern void queue_shutdown (void);

extern void queue_set_online (gboolean online);

extern gboolean queue_is_online (void);

enum {
	QUEUE_MESSAGE_ID_DOWNLOAD	= (1<<0),
	QUEUE_MESSAGE_ID_SAVE		= (1<<1)
};
extern int queue_get_message_id_status (const PString * message_id);

typedef void (QueueForallTaskFunc)(Task   ** tasks,
                                   int       task_qty,
                                   gpointer  user_data);

void queue_forall_tasks (QueueForallTaskFunc    forall_func,
                         gpointer               forall_func_user_data,
                         gboolean               run_even_if_no_tasks);

/**
***  This rest of this file is for the queue manager.
**/


/**
***  CALLBACKS
***
***  These callbacks are called when the queue changes.
**/

/**
 * @call_obj: GINT_TO_POINTER(running_qty)
 * @call_arg: GINT_TO_POINTER(total_qty)
 */
extern PanCallback*  queue_get_size_changed_callback (void);

/**
 * @call_obj: GINT_TO_POINTER(increment)
 * @call_arg: NULL
 */
extern PanCallback*  queue_get_connection_size_changed_callback (void);

/**
 * @call_obj: const char ** message_ids
 * @call_arg: GINT_TO_POINTER(message_id_qty)
 */
extern PanCallback*  queue_get_message_id_status_changed (void);

/**
 * @call_obj: Group**
 * @call_arg: GINT_TO_POINTER(qty)
 */
extern PanCallback*  queue_get_online_status_changed_callback (void);

/**
 * @param call_object GSList* of Task*s which were added
 * @param call_arg the GINT_TO_POINTER() of the position in the queue where the first was added
 */
extern PanCallback*  queue_get_tasks_added_callback (void);

/**
 * @param call_object GSList* of Task*s which were removed
 * @param call_arg NULL
 */
extern PanCallback*  queue_get_tasks_removed_callback (void);

/**
 * @param call_object GSList of Task*s that were moved.
 * @param call_arg the index of the first Task in the list; the remaining tasks are at index+1, index+2, and so on.
 */
extern PanCallback*  queue_get_tasks_moved_callback (void);

/**
***  QUEUE MANIPULATION
**/

extern gboolean queue_is_empty (void);

/**
 * Add an item to the queue.  This increases the task's refcount by 1.
 * @param tasks the tasks to insert.
 * @param index into the queue, or -1 to add at the end.
 */
extern void queue_insert_tasks (const GSList * tasks, int index);

/**
 * Aborts a running task, has no effect on non-running tasks.
 * @param list of tasks to stop.
 */
extern void queue_stop_tasks (const GSList * tasks);

/**
 * Remove an item from the queue.  This decreases each task's refcount by 1.
 * @param tasks the tasks to remove.
 */
extern void queue_remove_tasks (const GSList * tasks);

extern void queue_remove_last_task (void);

/**
 * Reposition an item in the queue.
 * @param tasks the tasks to move
 * @param index the new position in the queue, or -1 for the end.
 */
extern void queue_move_tasks (const GSList * tasks, int index);

/**
 * Requeue a failed task.
 * @param tasks the tasks to requeue
 */
extern void queue_requeue_failed_tasks (const GSList * tasks);


/**
***  CONNECTION
**/

extern gboolean queue_get_remove_failed_tasks (void);

extern void queue_set_remove_failed_tasks (gboolean);

extern gboolean queue_has_dedicated_article_thread (void);

extern void queue_set_dedicated_article_thread (gboolean b);

extern void queue_wakeup (void);

/**
***  TASK STATUS
**/

typedef enum
{
	QUEUE_TASK_STATUS_NOT_QUEUED	= 0,
	QUEUE_TASK_STATUS_QUEUED	= 1,
	QUEUE_TASK_STATUS_RUNNING	= 2,
	QUEUE_TASK_STATUS_STOPPING	= 3,
	QUEUE_TASK_STATUS_STOPPED	= 4,
	QUEUE_TASK_STATUS_REMOVING	= 5
}
QueueTaskStatus;

/**
 * @return what the queue thinks the specified Task is doing,
 *         or 0 if the specified item is not in the queue.
 */
extern QueueTaskStatus queue_get_task_status (const Task*);

char* queue_get_tasks_filename (void);


#endif /* __QUEUE_H__ */
