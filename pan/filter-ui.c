/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/filters/filter.h>
#include <pan/filters/filter-manager.h>

#include <pan/rules/rule-manager.h>

#include <pan/filter-ui.h>
#include <pan/filter-edit-ui.h>
#include <pan/util.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

typedef struct
{
	GtkWidget * filter_clist;
	GtkWidget * dialog;
	GPtrArray * filters;

	GtkWidget * remove_button;
	GtkWidget * edit_button;
}
FilterListDialog;

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PRIVATE
************/

static int
get_selected_index (FilterListDialog * d)
{
	int sel;
	GtkCList * clist;

	/* find the selected filter */
	clist = GTK_CLIST(d->filter_clist);
	sel = -1;
	if (clist->selection != NULL)
		sel = GPOINTER_TO_INT(clist->selection->data);

	return sel;
}                                                                                                                               

static void
button_refresh (FilterListDialog * d)
{
	const int sel = get_selected_index (d);
	const gboolean have_sel = sel != -1;

	gtk_widget_set_sensitive (d->edit_button, have_sel);
	gtk_widget_set_sensitive (d->remove_button, have_sel);
}

static void
clist_refresh (FilterListDialog * d)
{
	int i;
	const int sel = get_selected_index (d);
	GtkCList * clist = GTK_CLIST(d->filter_clist);

	gtk_clist_freeze (clist);
	gtk_clist_clear (clist);
	for (i=0; i<d->filters->len; ++i)
	{
		int row;
		FilterTop * f = FILTER_TOP(g_ptr_array_index(d->filters,i));
	       	row = gtk_clist_insert (clist, -1, &f->name);
		gtk_clist_set_row_data (clist, row, f);
	}
	gtk_clist_select_row (clist, sel, 0);
	gtk_clist_thaw (clist);
}

/*****
******
*****/

static void
filter_dialog_response_cb (GtkDialog * dialog, int response, gpointer user_data)
{
	FilterListDialog * d = (FilterListDialog*) user_data;

	/* save changed filters */
	filter_manager_set_filters (d->filters);

	/* cleanup filters */
	if (d->filters != NULL)
	{
		pan_g_ptr_array_foreach (d->filters, (GFunc)pan_object_unref, NULL);
		g_ptr_array_free (d->filters, TRUE);
		d->filters = NULL;
	}

	/* cleanup dialog */
	g_free (d);
	gtk_widget_destroy (GTK_WIDGET(dialog));
}


/**
***  Add
**/

static void
used_filter_warning (FilterListDialog   * d,
                     const char         * name,
		     const char        ** rule_names,
		     int                  rule_qty)
{
	GString * str;
	int i;

	/* sanity clause */
	g_return_if_fail (d!=NULL);
	g_return_if_fail (is_nonempty_string(name));
	g_return_if_fail (rule_names!=NULL);
	g_return_if_fail (rule_qty>0);

	/* build the error message */
	str = g_string_new (NULL);
	for (i=0; i<rule_qty; ++i)
		g_string_append_printf (str, "%d: %s\n", i+1, rule_names[i]);
	pan_error_dialog_parented (d->dialog,
		_("This filter is still used by the following rules:\n%s\nPlease remove this dependency  first."),
		str->str);

	/* cleanup */
	g_string_free (str, TRUE);
}

static void
remove_button_clicked_cb (GtkButton * button, gpointer data)
{
	FilterListDialog * d = (FilterListDialog*) data;
	int sel = get_selected_index (d);
	if (sel != -1)
	{
		Filter    * f    = FILTER(g_ptr_array_index(d->filters, sel));
		FilterTop * ft   = FILTER_TOP(f);
		GPtrArray * used = rule_manager_find_filter(ft->name);

		if (used != NULL && used->len)
		{
			used_filter_warning (d, ft->name, (const char**)used->pdata, used->len);
		}
		else
		{
			g_ptr_array_remove_index (d->filters, sel);
			pan_object_unref (PAN_OBJECT(f));
			clist_refresh (d);
			button_refresh (d);
		}

		/* cleanup */
		pan_g_ptr_array_foreach (used, (GFunc)g_free, NULL);
		g_ptr_array_free (used, TRUE);
	}
}

static void
add_dialog_response_cb (GtkDialog * dialog, int response, gpointer data)
{
	if (response == GTK_RESPONSE_OK || response == GTK_RESPONSE_APPLY)
	{
		guint i;
		GtkWidget * w = GTK_WIDGET(dialog);
		FilterListDialog * d = (FilterListDialog*) data;
		Filter * f_new = filter_dup (FILTER(filter_edit_dialog_get_filter(w)));
		FilterTop * ft_new = FILTER_TOP(f_new);

		/* see if we've already got this filter */
		for (i=0; i!=d->filters->len; ++i) {
			FilterTop * ft = FILTER_TOP(g_ptr_array_index(d->filters,i));
				if (!pan_strcmp(ft_new->name, ft->name))
					break;
		}

		/* either insert or update */
		if (i == d->filters->len) {
			g_ptr_array_add (d->filters, f_new);
		} else {
			Filter * f_old = FILTER(g_ptr_array_index(d->filters,i));
			g_ptr_array_index (d->filters,i) = filter_dup (f_new);
			pan_object_unref (PAN_OBJECT(f_old));
		}

		/* rebuild the clist */
		clist_refresh (d);
	}

	if (response != GTK_RESPONSE_APPLY)
		gtk_widget_destroy (GTK_WIDGET(dialog));
}

static void
add_button_clicked_cb (GtkButton * button, gpointer data)
{
	FilterListDialog * f = (FilterListDialog*) data;
	GtkWidget * dialog = filter_edit_dialog_new (GTK_WINDOW(f->dialog), NULL);
	g_signal_connect (dialog, "response", G_CALLBACK(add_dialog_response_cb), f);
	gtk_widget_show_all (dialog);
}

/**
***  Edit
**/

static void
edit_dialog_response_cb (GtkDialog * dialog, int response, gpointer data)
{
	if (response == GTK_RESPONSE_OK || response == GTK_RESPONSE_APPLY)
	{
		FilterListDialog * d = (FilterListDialog*) data;
		int sel = get_selected_index (d);
		if (sel != -1)
		{
			Filter * f_old = FILTER(g_ptr_array_index(d->filters,sel));
			GtkWidget * w = GTK_WIDGET(dialog);
			Filter * f_new = filter_dup (FILTER(filter_edit_dialog_get_filter(w)));
			g_ptr_array_index(d->filters,sel) = f_new;
			pan_object_unref (PAN_OBJECT(f_old));
			clist_refresh (d);
		}
	}

	if (response != GTK_RESPONSE_APPLY)
		gtk_widget_destroy (GTK_WIDGET(dialog));
}
static void
edit_button_clicked_cb (GtkButton * button, gpointer data)
{
	FilterListDialog * d = (FilterListDialog*) data;
	int sel = get_selected_index (d);
	if (sel != -1)
	{
		const Filter * f_old = FILTER(g_ptr_array_index(d->filters,sel));
		GtkWidget * dialog = filter_edit_dialog_new (GTK_WINDOW(d->dialog), FILTER_TOP(f_old));
		g_signal_connect (GTK_OBJECT(dialog), "response", G_CALLBACK(edit_dialog_response_cb), d);
		gtk_widget_show_all (dialog);
	}
}
static gboolean
filter_clist_button_press_cb (GtkWidget * w, GdkEventButton * b, gpointer data)
{
	if (b->button==1 && b->type==GDK_2BUTTON_PRESS)
		edit_button_clicked_cb (NULL, data);
	return FALSE;
}

static void     
list_selection_changed_cb (GtkCList          * clist,
                           int                 row,
                           int                 column,
                           GdkEventButton    * event,
                           gpointer            user_data)
{
	button_refresh ((FilterListDialog*)user_data);
}


/************
*************  PROTECTED
************/

/************
*************  PUBLIC
************/

GtkWidget*
filter_dialog_new (GtkWindow * parent)
{
	GtkWidget * w;
	GtkWidget * hbox;
	GtkWidget * bbox;
	GtkTooltips * tips = gtk_tooltips_new ();
	FilterListDialog * d = g_new0 (FilterListDialog, 1);
	char * titles [1];


	/* load filters */
	d->filters = g_ptr_array_new ();
	filter_manager_get_filters (d->filters);

	/* dialog */
	w = d->dialog = gtk_dialog_new_with_buttons (_("Pan: Filters"),
	                                             parent,
	                                             GTK_DIALOG_DESTROY_WITH_PARENT,
	                                             GTK_STOCK_CLOSE, GTK_RESPONSE_OK,
	                                             NULL);
	gtk_window_set_policy (GTK_WINDOW(w), TRUE, TRUE, TRUE);
	g_signal_connect (GTK_OBJECT(w), "response", G_CALLBACK(filter_dialog_response_cb), d);

	/* workarea */
	hbox = gtk_hbox_new (FALSE, GUI_PAD);
	gtk_container_set_border_width (GTK_CONTAINER(hbox), 12);
	gtk_box_pack_start (GTK_BOX(GTK_DIALOG(w)->vbox), hbox, TRUE, TRUE, 0);

	/* clist */
	titles[0] = _("Filters");
	w = d->filter_clist= gtk_clist_new_with_titles (1, titles);
	gtk_clist_set_selection_mode (GTK_CLIST(w), GTK_SELECTION_BROWSE);
	g_signal_connect (w, "button_press_event",
	                  G_CALLBACK(filter_clist_button_press_cb), d);
	g_signal_connect (w, "select_row",
	                  G_CALLBACK(list_selection_changed_cb), d);
	g_signal_connect (w, "unselect_row",
	                  G_CALLBACK(list_selection_changed_cb), d);


        w = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(w), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER(w), d->filter_clist);
	gtk_box_pack_start (GTK_BOX (hbox), w, TRUE, TRUE, 0);
	gtk_widget_set_usize (w, 300, 300);

	/* button box */
	bbox = gtk_vbox_new (FALSE, GUI_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (hbox), bbox, FALSE, FALSE, 0);

	/* add button */
	w = gtk_button_new_from_stock (GTK_STOCK_ADD);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_tooltips_set_tip (tips, w, _("Add a new filter"), NULL);
	g_signal_connect (w, "clicked", G_CALLBACK(add_button_clicked_cb), d);

	/* edit button */
	w = gtk_button_new_from_stock (GTK_STOCK_OPEN);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_tooltips_set_tip (tips, w, _("Edit the selected filter"), NULL);
	g_signal_connect (w, "clicked", G_CALLBACK(edit_button_clicked_cb), d);
	d->edit_button = w;

	/* remove button */
	w = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_tooltips_set_tip (tips, w, _("Remove the selected filter"), NULL);
	g_signal_connect (w, "clicked", G_CALLBACK(remove_button_clicked_cb), d);
	d->remove_button = w;

	clist_refresh (d);
	button_refresh (d);
	return d->dialog;
}
