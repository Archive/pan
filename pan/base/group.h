/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GROUPS_H__
#define __GROUPS_H__

#include <glib/gtypes.h>
#include <glib/garray.h>
#include <glib/ghash.h>

#include <gmime/memchunk.h>
#include <gmime/gmime-message.h>

#include <pan/base/pstring.h>
#include <pan/base/server.h>
#include <pan/base/status-item.h>

typedef enum
{
	FILTER_SHOW_MATCHES                = 5,
	FILTER_SHOW_SUBTHREADS             = 6,
	FILTER_SHOW_THREAD                 = 7
}
FilterShow;

enum
{
	GROUP_ALL                          = (1<<0),
	GROUP_SUBSCRIBED                   = (1<<1), 
	GROUP_NEW                          = (1<<2),
	GROUP_FOLDERS                      = (1<<3)
};

enum
{
	STATE_FILTER_NEW                   = (1<<0),
	STATE_FILTER_UNREAD                = (1<<1),
	STATE_FILTER_READ                  = (1<<2),
	STATE_FILTER_COMPLETE_BINARIES     = (1<<3),
	STATE_FILTER_INCOMPLETE_BINARIES   = (1<<4),
	STATE_FILTER_NONBINARIES           = (1<<5),
	STATE_FILTER_CACHED                = (1<<6),
	STATE_FILTER_NOT_CACHED            = (1<<7),
	STATE_FILTER_MINE                  = (1<<8),
	STATE_FILTER_NOT_MINE              = (1<<9),

	STATE_FILTER_SCORE_WATCHED         = (1<<10),
	STATE_FILTER_SCORE_HIGH            = (1<<11),
	STATE_FILTER_SCORE_MEDIUM          = (1<<12),
	STATE_FILTER_SCORE_ZERO            = (1<<13),
	STATE_FILTER_SCORE_LOW             = (1<<14),
	STATE_FILTER_SCORE_IGNORED         = (1<<15),
	STATE_FILTER_SCORE_MASK            = (STATE_FILTER_SCORE_WATCHED
	                                     | STATE_FILTER_SCORE_HIGH
	                                     | STATE_FILTER_SCORE_MEDIUM
	                                     | STATE_FILTER_SCORE_ZERO
	                                     | STATE_FILTER_SCORE_LOW
	                                     | STATE_FILTER_SCORE_IGNORED),
	STATE_FILTER_SAVED                 = (1<<16)
};

#define GROUP(x) ((Group*)(x))

/**
 * For the time being, groups are 'owned' by their server.
 * When we go group-centric, this will change.
 *
 * Having Group a handle to its server lets us pass around
 * groups w/o a server argument for context, which should make
 * refitting for groupcentric easier...
 */
typedef struct _Group
{
        guint          loaded_since_last_fetch  : 1;
        guint          articles_dirty           : 1;     /* do the articles need saving? */
	char           permission;                       /* (read only) 'y'=posting, 'n'=nopost, 'm'=moderated */
	gint8          flags;                            /* (read only) new, subscribed, etc */
	gint8          old_sort_style;                   /* (read only) hint for sorting in UI */
	gint8          new_sort_style;                   /* (read only) hint for sorting in UI */
	gulong         filter_show;                      /* (read only) for filtering articles */
	guint32        filter_bits;                      /* (read only) for filtering articles */
	gint32         article_qty;                      /* (read only) # of articles in group */
	gint32         article_read_qty;                 /* (read only) # of read articles */
	gulong         article_low;                      /* (read only) low article number */
	gulong         article_high;                     /* (read only) high article number */
	gulong         article_high_old;                 /* (read only) previous high article */

	Server       * server;
	PString        name;                             /* alt.what.is.my.name */
	GQuark         name_quark;                       /* lookup for this group name */
	char         * filter_name;                      /* article toolbar, articlelist */
	char         * identity_name;                    /* last identity used for this group */
	char         * description;                      /* maybe null */
	char         * download_dir;                     /* where to save files, or NULL */
	char         * default_charset;                  /* default charset for articles that don't specify, or NULL */

	/* PRIVATE */
	int               _articles_refcount;
	MemChunk        * _article_chunk;                   /* Article objects */
	MemChunk        * _attachment_part_chunk;           /* AttachmentPart objects */
	struct _Newsrc  * _newsrc;                          /* tracks read/unread */
	struct _Newsrc  * _purged;                          /* tracks deleted articles */
	GHashTable      * _articles;                        /* articles */
	GPtrArray       * _deleted_articles;                /* deleted articles */
}
Group;


/**
***  PUBLIC
**/

Group*    group_new                      (Server        * server,
                                          const char    * name);

void      group_inc_article_read_qty     (Group         * group,
                                          int             article_read_qty);

void      group_set_article_qty          (Group         * group,
                                          int             article_qty);

void      group_set_article_read_qty     (Group         * group,
                                          int             article_read_qty);

void      group_set_article_range        (Group         * group,
                                          gulong          article_low,
                                          gulong          article_high);

void      group_get_article_range        (const Group   * group,
                                          gulong        * article_low,
                                          gulong        * article_high);

void      group_mark_new_article_number  (Group         * group,
                                          gulong          everything_above_this_is_new);

void      group_mark_all_read            (Group         * group,
                                          gboolean        read);

void      group_set_sort_style           (Group         * group,
                                          int             sort_style);

void      group_set_filter               (Group         * group,
                                          guint           filter_bits,
                                          gulong          filter_show,
                                          const char    * filter_name);

void      group_set_identity             (Group         * group,
                                          const char    * identity_name);

void      group_set_flags                (Group         * group,
                                          guint           flags);

void      group_set_download_dir         (Group         * group,
                                          const char    * download_dir);

gboolean  group_is_folder                (const Group   * group);

gboolean  group_is_group                 (const Group   * group);

gboolean  group_is_valid                 (const Group   * group);

gboolean  group_is_moderated             (const Group   * group);

gboolean  group_is_read_only             (const Group   * group);

void      group_set_is_folder            (Group         * group,
                                          gboolean        is_folder);

void      group_set_default_charset      (Group         * group,
                                          const char    * default_charset);

const char*    group_get_name            (const Group   * group);

const char*    group_get_default_charset (const Group   * group);

void      groups_set_subscribed          (Group        ** group,
                                          int             qty,
                                          gboolean        subscribed);

gboolean  group_is_subscribed            (const Group   * group);

void      group_set_new                  (Group         * group,
                                          gboolean        is_new);

gboolean  group_is_new                   (const Group   * group);

void      group_set_dirty                (Group         * group);

void      group_empty                    (Group         * group,
                                          gboolean        clear_counts);

void      group_get_collapsed_name       (const Group   * group,
                                          char          * buf,
                                          size_t          bufsize);


/**
 * FOLDERS
 *
 */

void          folder_add_message                  (Group            * folder,
                                                   GMimeMessage     * message);

void          folder_remove_message               (Group            * folder,
                                                   GMimeMessage     * message);

/**
 * ARTICLES
 *
 * the add and remove article functions all update the group's
 * statistics and the way it appears in the Pan user interface.
 * If you're adding or removing more than one groups, it's much
 * better to use the plural functions once instead of calling
 * the singular versions many times.
 */

struct _Article;
struct _AttachmentPart;

void          group_add_articles                  (Group            * group,
                                                   const GPtrArray  * articles,
                                                   StatusItem       * status_or_null);

void          group_init_articles                 (Group            * group,
                                                   GPtrArray        * articles,
                                                   StatusItem       * status_or_null);

void          group_remove_articles               (Group            * group,
                                                   struct _Article ** articles,
                                                   guint              article_qty);

gboolean      group_ref_articles_if_loaded        (Group            * group);

void          group_ref_articles                  (Group            * group,
                                                   StatusItem       * item);

void          group_unref_articles                (Group            * group,
                                                   StatusItem       * item);


void          group_expire_articles_not_in_range  (Group            * group,
                                                   gulong             low,
                                                   gulong             high);

/**
***
**/

gboolean      group_is_article_read               (const Group      * group,
                                                   gulong             article_number);

gboolean      group_mark_article_read             (Group            * group,
                                                   gulong             article_number,
                                                   gboolean           read);

char*         group_get_newsrc_export_string      (const Group      * group);

char*         group_get_newsrc_read_string        (const Group      * group);

void          group_set_newsrc_read_string        (Group            * group,
                                                   const char       * read_string);

/**
***
**/

char*         group_get_purged_string             (const Group      * group);

void          group_set_purged_string             (Group            * group,
                                                   const char       * purged_string);

void          group_mark_article_purged           (Group            * group,
                                                   gulong             article_number);

/**
***
**/

void          group_set_articles_dirty            (Group            * group);

typedef void (*GroupArticleFunc)(Group*, struct _Article**, guint, gpointer);

void          group_article_forall                (Group            * group,
                                                   GroupArticleFunc   func,
                                                   gpointer           func_user_data);

gpointer      group_get_article_by_message_id     (Group            * group,
                                                   const PString    * message_id);

const char*   group_get_acache_key                (const Group      * group);


struct _Article*  group_alloc_new_article        (Group         * group);

struct _AttachmentPart *  group_alloc_new_attachment_part  (Group         * group);


/**
***  PROTECTED
**/

void group_constructor    (Group                        * group,
			   const char                   * name);

void group_destructor     (Group                        * group);


extern GQuark PAN_SENDLATER(void);
extern GQuark PAN_SENT(void);


/**
***  Events
**/

/**
 * @call_obj: Group*
 * @call_arg: GPtrArray of Article* sorted by message_id
 */
PanCallback*  group_get_articles_added_callback (void);

/**
 * @call_obj: Group*
 * @call_arg: GPtrArray of Article* sorted by message_id
 */
PanCallback*  group_get_articles_removed_callback (void);


enum {
	GROUP_CHANGED_ARTICLE_READ_QTY          = (1<<0),
	GROUP_CHANGED_ARTICLE_QTY               = (1<<1),
	GROUP_CHANGED_SUBSCRIPTION_STATE        = (1<<2)
};

struct GroupChangeEvent {
	Group ** groups;
	int group_qty;
	guint change_type;
};

/**
 * @call_obj: GroupChangeEvent*
 * @call_arg: NULL
 */
PanCallback*  group_get_groups_changed_callback (void);



#endif /* __GROUPS_H__ */
