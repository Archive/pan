/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/log.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/status-item.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

static PanCallback * status_item_active_callback = NULL;

#define DEAD_POINTER ((void*)(0xDEADBEEF))

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PUBLIC ROUTINES
************/

/*****
******
*****/

void
status_item_describe (const StatusItem* status, char * buf, int buflen)
{
	g_return_if_fail (status != NULL);
	g_return_if_fail (buf != NULL);
	g_return_if_fail (buflen > 0);

	*buf = '\0';

	if (status->description != NULL)
		g_strlcpy (buf, status->description, buflen);
	else if (status != NULL)
		(*status->describe)(status, buf, buflen);
}

StatusItem*
status_item_new (StatusItemDescribeFunc describe)
{
	StatusItem *item = g_new (StatusItem, 1);
        debug1 (DEBUG_PAN_OBJECT, "status_item_new: %p", item);
	status_item_constructor (item, status_item_destructor, describe);
	return item;
}

StatusItem*
status_item_new_with_description (const char * description)
{
	StatusItem *item = g_new (StatusItem, 1);
        debug1 (DEBUG_PAN_OBJECT, "status_item_new: %p", item);
	status_item_constructor (item, status_item_destructor, NULL);
	item->description = g_strdup (description);
	return item;
}


void
status_item_constructor (StatusItem              *item,
                         PanObjectDestructor      destructor,
                         StatusItemDescribeFunc   describe)
{
	PanObject *obj = PAN_OBJECT(item);

	/* init the superclass bits */
	pan_object_constructor (obj, destructor);

	/* init the status-item bits */
        debug1 (DEBUG_PAN_OBJECT, "status_item_constructor: %p", item);
	item->progress = pan_callback_new ();
	item->status = pan_callback_new ();
	item->error = pan_callback_new ();
	item->done = pan_callback_new ();
	item->describe = describe;
	item->errors = NULL;
	item->progress_val = 0;
	item->steps = 0;
	item->step = 0;
	item->status_text = NULL;
	item->description = NULL;
}

void
status_item_destructor (PanObject *po)
{
	StatusItem *item = STATUS_ITEM(po);
	g_return_if_fail (item != NULL);

        debug1 (DEBUG_PAN_OBJECT, "status_item_destructor: %p", item);

	/* free & toxify everything */
	pan_callback_free (&item->progress);
	pan_callback_free (&item->status);
	pan_callback_free (&item->error);
	pan_callback_free (&item->done);
	replace_gstr (&item->description, DEAD_POINTER);
	replace_gstr (&item->status_text, DEAD_POINTER);
	g_slist_foreach (item->errors, (GFunc)g_free, NULL);
	g_slist_free (item->errors);
        item->errors = DEAD_POINTER;
        item->progress_val = ~0;
        item->steps = ~0;
        item->step = ~0;
        item->describe = DEAD_POINTER;

	pan_object_destructor (PAN_OBJECT(item));
}

/*****
******
*****/

PanCallback*
status_item_get_active_callback (void)
{
	if (status_item_active_callback == NULL)
		status_item_active_callback = pan_callback_new ();

	return status_item_active_callback;
}

void
status_item_set_active (StatusItem       * item,
                        gboolean           active)
{
	debug_enter ("status_item_set_active");

	g_return_if_fail (item != NULL);
 
	pan_callback_call (status_item_get_active_callback(),
	                   item,
	                   GINT_TO_POINTER((int)active));

	debug_exit ("status_item_set_active");
}

/*****
******
*****/

void
status_item_emit_init_steps (StatusItem  * item,
                             gint          steps)
{
	g_return_if_fail (item != NULL);
	g_return_if_fail (steps >= 0);

	item->step = 0;
	item->steps = steps;
}

void
status_item_emit_next_step (StatusItem * item)
{
	g_return_if_fail (item != NULL);

	status_item_emit_set_step (item, item->step+1);
}

void
status_item_emit_inc_step (StatusItem * item, gint inc)
{
	g_return_if_fail (item != NULL);

	status_item_emit_set_step (item, item->step+inc);
}

void
status_item_emit_set_step (StatusItem    * item,
                           gint            step)
{
	g_return_if_fail (item != NULL);
	g_return_if_fail (step >= 0);

	item->step = step;

	status_item_emit_progress (
		item,
		(int)(item->steps?((item->step*100.0)/item->steps):0));
}

void
status_item_emit_progress (StatusItem    * item,
                           gint            out_of_100)
{
	g_return_if_fail (item != NULL);

	if (0<=out_of_100
		&& out_of_100<=100
		&& item->progress_val!=out_of_100)
	{
		item->progress_val = out_of_100;

		pan_callback_call (item->progress,
                                   item,
                                   GINT_TO_POINTER(out_of_100));
	}
}


void
status_item_emit_done (StatusItem * item,
                       gint         status)
{
	g_return_if_fail (item != NULL);
	pan_callback_call (item->done, item, GINT_TO_POINTER(status));
}

void
status_item_emit_status (StatusItem    * item,
                         const gchar   * status)
{
	g_return_if_fail (item!=NULL);
	g_return_if_fail (status!=NULL);

	debug0 (DEBUG_QUEUE, status);
	replace_gstr (&item->status_text, g_strdup(status));
	pan_callback_call (item->status, item, (gpointer)status);
}

void
status_item_emit_status_va (StatusItem    * item,
                            const gchar   * format,
                            ...)
{
	gchar buf [1024];
       	va_list args;

	g_return_if_fail (item!=NULL);
	g_return_if_fail (format!=NULL);

	va_start (args, format);
	g_vsnprintf (buf, sizeof(buf), format, args);
	va_end (args);

	status_item_emit_status (item, buf);
}

void
status_item_emit_error (StatusItem   * item,
                        const gchar  * error)
{
	gchar * copy;

	g_return_if_fail (item != NULL);
	g_return_if_fail (error != NULL);

	copy = g_strdup (error);
	log_add (LOG_ERROR, copy);
	debug2 (DEBUG_QUEUE, "StatusItem %p has this error: %s", item, copy);
	item->errors = g_slist_append (item->errors, copy);
	pan_callback_call (item->error, item, copy);
}

void
status_item_emit_error_va (StatusItem   * item,
                           const gchar  * format,
                           ...)
{
       	va_list args;
	gchar buf [1024];

	g_return_if_fail (item != NULL);
	g_return_if_fail (format != NULL);

	va_start (args, format);
	g_vsnprintf (buf, sizeof(buf), format, args);
	va_end (args);

	status_item_emit_error (item, buf);
}

gint
status_item_get_progress_of_100 (const StatusItem * item)
{
	g_return_val_if_fail (item!=NULL, 0);
	return item->progress_val;
}
