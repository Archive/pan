/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <errno.h>
#include <string.h>

#include <pan/base/group.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/log.h>
#include <pan/base/newsrc.h>
#include <pan/base/newsrc-port.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/util-file.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PUBLIC ROUTINES
************/

/*****
******
*****/

void
newsrc_import (Server         * server,
               const PString  * filename_in,
               gboolean         subscribed_only)
{
	char * filename;
	char * body = NULL;
	gsize body_len = 0;
	GError * err;

	/* sanity clause */
	g_return_if_fail (server_is_valid (server));
	g_return_if_fail (pstring_is_set (filename_in));

	/* normalize the filename */
	filename = g_strndup (filename_in->str, filename_in->len);
	replace_gstr (&filename, pan_file_normalize (filename, NULL));
	filename_in = NULL;

	/* read the file into 'body'*/
	err = NULL;
	if (!g_file_get_contents (filename, &body, &body_len, &err))
	{
		log_add_va (LOG_ERROR, _("Error reading file \"%s\": %s"), filename, err->message);
		g_error_free (err);
	}
	else /* march through each line of newsrc... */
	{
		const char * pch = body;
		int groups_imported_qty = 0;
		GString * group_name = g_string_new (NULL);
		GString * range_string = g_string_new (NULL);
		GString * line = g_string_new (NULL);
		GPtrArray * sub_y = g_ptr_array_new ();
		GPtrArray * sub_n = g_ptr_array_new ();
		GPtrArray * groups_to_add = g_ptr_array_new ();

		while (get_next_token_g_str (pch, '\n', &pch, line))
		{
			gboolean subscribed = FALSE;

			if (newsrc_parse_raw_line (line->str, group_name, &subscribed, range_string))
			{
				Group * group;
				GQuark group_quark;

				/* maybe skip non-subscribed groups */
				if (subscribed_only && !subscribed)
					continue;

				/* get the group which this line is for. */
 				group_quark = g_quark_from_string (group_name->str);
				group = server_get_group (server, group_quark);

				/* if we didn't have this group, add it */
				if (group == NULL) {
					group = group_new (server, group_name->str);
					g_ptr_array_add (groups_to_add, group);
				}

				group_set_newsrc_read_string (group, range_string->str);

				g_ptr_array_add (subscribed?sub_y:sub_n, group);

				/* track how many groups imported */
				++groups_imported_qty;
			}
		}

		if (sub_y->len)
			groups_set_subscribed ((Group**)sub_y->pdata, sub_y->len, TRUE);
		if (sub_n->len)
			groups_set_subscribed ((Group**)sub_n->pdata, sub_n->len, FALSE);


		/* add any groups we didn't already have */
		if (groups_to_add->len != 0)
			server_add_groups (server,
					   (Group**)groups_to_add->pdata,
					   groups_to_add->len,
					   NULL,
					   NULL);

		/* log our progress */
		log_add_va (LOG_INFO, _("Imported %d groups (%d new) from \"%s\""),
				      groups_imported_qty,
				      (int)groups_to_add->len,
				      filename);

		g_string_free (line, TRUE);
		g_string_free (group_name, TRUE);
		g_string_free (range_string, TRUE);
		g_ptr_array_free (sub_y, TRUE);
		g_ptr_array_free (sub_n, TRUE);
		g_ptr_array_free (groups_to_add, TRUE);
	}

	/* cleanup */
	g_free (filename);
	g_free (body);
}

/*****
******
*****/

void
newsrc_export (Server          * server,
               const PString   * filename_in,
               gboolean          subscribed_only)
{
	FILE * fp;
	char * filename_tmp;
	char * filename;

	/* sanity clause */
	g_return_if_fail (server_is_valid (server));
	g_return_if_fail (pstring_is_set (filename_in));

	filename = g_strndup (filename_in->str, filename_in->len);
	replace_gstr (&filename, pan_file_normalize (filename, NULL));
	filename_in = NULL;

	/* open a temp file to write to */
	filename_tmp = pan_file_make_temp (&fp);
	if (fp == NULL)
	{
		log_add_va (LOG_ERROR, _("Could not create temporary file: %s"), g_strerror(errno));
	}
	else
	{
		int i;
		int groups_exported_qty = 0;
		int subbed_exported_qty = 0;
		gboolean ok;
		ServerGroupsType type = subscribed_only ? SERVER_GROUPS_SUBSCRIBED : SERVER_GROUPS_ALL;
		GPtrArray * a = server_get_groups (server, type);

		/* walk through the groups */
		for (i=0; i<a->len; ++i)
		{
			Group * group = GROUP(g_ptr_array_index(a, i));
			char * str = group_get_newsrc_export_string (group);

			if (is_nonempty_string (str))
			{
				fprintf (fp, "%s\n", str);

				/* keep track of how many we've exported */
				++groups_exported_qty;
				if (group_is_subscribed(group))
					++subbed_exported_qty;
			}

			g_free (str);
		}
		ok = !ferror(fp);
		fclose (fp);

		if (ok && pan_file_swap_datafile (filename_tmp, filename))
			log_add_va (LOG_INFO, _("Exported %d groups (%d subscribed) to \"%s\""),
					      groups_exported_qty,
					      subbed_exported_qty,
					      filename);

		/* cleanup */
		g_ptr_array_free (a, TRUE);
	}

	/* cleanup */
	g_free (filename);
	g_free (filename_tmp);
}
