/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __NEWSRC_H__
#define __NEWSRC_H__

#include <glib/gtypes.h>
#include <glib/gstring.h>

#define NEWSRC(x) ((Newsrc *)(x))

/**
 * The newsrc object manages the read state for a single group.
 * Other things typically used in a .newsrc entry -- group name and
 * subscription status -- are managed elsewhere in Pan and so are
 * only addressed in the import/export functions here.
 */
typedef struct _Newsrc Newsrc;

/**
***  PUBLIC
**/

/**
***  Life Cycle
**/

/**
 * Create a new newsrc object.
 * @param read_str a .newsrc-like string listing a series of read articles,
 * or NULL, or an empty string.  The groupname and subscription character
 * from .newsrc should _not_ be in this string -- only the read article ranges.
 * @param group_low the low article number in the group.
 * @param group_high the high article number in the group.
 * @return a new newsrc object.
 */
Newsrc*   newsrc_new              (const char         * read_str,
                                   gulong               group_low,
                                   gulong               group_high);

/**
 * Reset an existing newsrc object.
 * @param newsrc the newsrc object to be updated.
 * @param read_str same as read_str in newsrc_new.
 * @param group_low the low article number in the group.  Numbers below this value in read_str will be trimmed.
 * @param group_high the high article number in the group.  Numbers above this value in read_str will be trimmed.
 */
void      newsrc_init                 (Newsrc             * newsrc,
                                       const char         * read_str,
                                       gulong               group_low,
                                       gulong               group_high);

/**
 * Equivalent to newsrc_init where group_low and group_high
 * are defined from the boundaries of range_str.
 */
void      newsrc_init_from_range_str  (Newsrc             * newsrc,
                                       const char         * range_str);

/**
***  Client functions
**/

/**
 * @param newsrc the newsrc object
 * @param article_number the article to mark as read/unread
 * @param read true if the article is to be marked read, false if unread
 * @return previous state
 */
gboolean  newsrc_mark_article     (Newsrc             * newsrc,
                                   gulong               article_number,
				   gboolean             read);

/**
 * @param newsrc the newsrc object.
 * @param low the low article number in the range to be marked
 * @param high the low article number in the range to be marked
 * @param read true if [low...high] are to be marked read, false if unread
 * @return the number of articles whose read state changed
 */
gulong      newsrc_mark_range     (Newsrc             * newsrc,
                                   gulong               low,
                                   gulong               high,
                                   gboolean             read);
/**
 * @param newsrc the newsrc object
 * @param read true if all the articles are to be marked read, false if unread
 */
void      newsrc_mark_all         (Newsrc             * newsrc,
                                   gboolean             read);

/**
 * @param newsrc the newsrc object
 * @param article_number the article whose read state is to be checked
 * @return true if the article is marked as read, false otherwise
 */
gboolean  newsrc_is_article_read  (const Newsrc       * newsrc,
                                   gulong               article_number);


/**
***  Compatability with other newsreaders
**/

/**
 * Initialized a newsrc object based on a line from a .newsrc file.
 * @param newsrc the newsrc object to initialize
 * @param setme_subscribed set to true if the group is subscribed, false otherwise
 * @param import_string an entire line from a .newsrc file.  Unlike the read_str
 * in the constructor, this string should have the group name and the subscription
 * char typically found in a .newsrc file.
 */
void      newsrc_import_line      (Newsrc             * newsrc,
                                   gboolean           * setme_subscribed,
                                   const char         * import_string);

gboolean  newsrc_parse_raw_line   (const char     * raw_line,
                                   GString        * setme_group_name_or_NULL,
                                   gboolean       * setme_subscribed_or_NULL,
                                   GString        * setme_range_string_or_NULL);
                                     

/**
 * Generate a line suitable for writing to a .newsrc file.
 * @param newsrc the newsrc object whose article-read info is to be used
 * @param group_name the name to be written to the output string
 * @param subscribed whether or not the group is subscribed.
 */
char*      newsrc_export_line     (const Newsrc       * newsrc,
                                   const char         * group_name,
                                   gboolean             subscribed);

/**
***  Housekeeping
**/

/**
 * Update the high/low article numbers for this group.  This is used in
 * removing expired article numbers from the internal ranges table and
 * from the read_str/export str given when the newsrc object is being
 * saved/exported.
 *
 * @param newsrc the newsrc object to update
 * @param group_low the low article number in the group.
 * @param group_high the high article number in the group.
 */
void      newsrc_set_group_range  (Newsrc             * newsrc,
                                   gulong               group_low,
                                   gulong               group_high);

void      newsrc_get_group_range  (Newsrc             * newsrc,
                                   gulong             * group_low,
                                   gulong             * group_high);

/**
 * Generate a string of the series of article numbers which are read.
 * This differs from the export string in that it doesn't have the
 * group name or subscription information.
 *
 * @param newsrc the newsrc object to get article read numbers from
 */
char*      newsrc_get_read_str     (const Newsrc       * newsrc);


#endif /* __NEWSRC_H__ */
