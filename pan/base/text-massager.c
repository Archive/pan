/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <ctype.h>
#include <string.h>

#include <glib/gmem.h>
#include <glib/gstrfuncs.h>

#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/text-massager.h>

/****
*****
****/

struct _TextMassager
{
	int wrap_column;
	gboolean is_quote_char[UCHAR_MAX];
};

TextMassager*
text_massager_new (void)
{
	TextMassager * tm = g_new0 (TextMassager, 1);
	text_massager_set_wrap_column (tm, 74);
	text_massager_set_quote_chars (tm, ">");
	return tm;
}

void
text_massager_free (TextMassager * tm)
{
	g_free (tm);
}

/****
*****  QUOTE CHARACTERS
****/

gboolean
text_massager_is_quote_char (const TextMassager    * tm,
                             guchar                  ch)
{
	return tm->is_quote_char[ch];
}

char*
text_massager_get_quote_chars  (const TextMassager * tm)
{
	guint i;
	GString * str = g_string_new (NULL);

	for (i=0; i<UCHAR_MAX; ++i)
		if (text_massager_is_quote_char (tm, i))
			g_string_append_c (str, i);

	return g_string_free (str, FALSE);
}

void
text_massager_set_quote_chars (TextMassager          * tm,
                               const guchar          * quote_chars)
{
	int i;
	const guchar * pch;

	for (i=0; i<UCHAR_MAX; ++i)
		tm->is_quote_char[i] = FALSE;
	for (pch=quote_chars; pch!=NULL && *pch!='\0'; ++pch)
		tm->is_quote_char[*pch] = TRUE;
}

/****
*****  WRAP COLUMN
****/

int
text_massager_get_wrap_column (const TextMassager * tm)
{
	return tm->wrap_column;
}

void
text_massager_set_wrap_column (TextMassager          * tm,
                               int                     column)
{
	tm->wrap_column = column;
}

/****
*****  LINE WRAPPING
****/

typedef struct
{
	PString leader;
	PString content;
}
Paragraph;

static Paragraph*
paragraph_new (const char * leader, int leader_len,
               const char * content, int content_len)
{
	Paragraph * p = g_new0 (Paragraph, 1);
	p->content = p->leader = PSTRING_INIT;
	pstring_set (&p->leader, leader, leader_len);
	pstring_set (&p->content, content, content_len);
	return p;
}

static void
paragraph_free (Paragraph * p)
{
	pstring_clear (&p->leader);
	pstring_clear (&p->content);
	g_free (p);
}

static GPtrArray*
get_paragraphs (const TextMassager  * tm,
                const char          * body)
{
	int line_alloc_qty;
	int line_qty;
	const char * pch;
	Paragraph * lines;
	GPtrArray * paragraphs = g_ptr_array_new ();

	/* count the number of lines to alloc.
	 * +1 for the first line and +1 for our empty line at the end */
	for (pch=body, line_alloc_qty=1+1; *pch; ++pch)
		if (*pch == '\n')
			++line_alloc_qty;

	lines = g_newa (Paragraph, line_alloc_qty);

	/* build an array of lines that are split between leader & content */
	line_qty = 0;
	if (1)
	{
		PString line;

		while (get_next_token_pstring (body, '\n', &body, &line))
		{
			Paragraph * p;
			const char * pch = line.str;
			const char * end = line.str + line.len;

			while (pch<end && (text_massager_is_quote_char(tm, g_utf8_get_char(pch)) || g_unichar_isspace(g_utf8_get_char(pch))))
				pch=g_utf8_next_char(pch);

 			p = lines + line_qty++;
			p->leader = pstring_shallow (line.str, pch-line.str);
			p->content = pstring_shallow (pch, end-pch);
			p->content = pstring_strstrip_shallow (&p->content);
		}

		/* add an empty line to make the paragraph-making loop go smoothly */
		lines[line_qty].leader = pstring_shallow ("", 0);
		lines[line_qty].content = pstring_shallow ("", 0);
		++line_qty;
	}

	/* merge the lines into paragraphs */
	if (line_qty)
	{
		int i;
		int prev_content_len = 0;
		PString cur_leader = PSTRING_INIT;
		GString * cur_content = g_string_new (NULL);

		for (i=0; i<line_qty; ++i)
		{
			const Paragraph * p = lines + i;
			gboolean paragraph_end = TRUE;
			gboolean hard_break = FALSE;

			if (!cur_content->len || pstring_equal (&p->leader, &cur_leader))
				paragraph_end = FALSE;

			if (!p->content.len) {
				hard_break = prev_content_len!=0;
				paragraph_end = TRUE;
			}

			/* we usually don't want to wrap really short lines */
			if (prev_content_len!=0 && prev_content_len<(tm->wrap_column/2))
				paragraph_end = TRUE;

			if (paragraph_end) /* the new line is a new paragraph, so save the old line */
			{
				g_ptr_array_add (paragraphs, paragraph_new (cur_leader.str, cur_leader.len, cur_content->str, cur_content->len));
				cur_leader = p->leader;
				g_string_set_size (cur_content, 0);
				if (p->content.len)
					g_string_append_len (cur_content, p->content.str, p->content.len);

				if (hard_break)
					g_ptr_array_add (paragraphs, paragraph_new (cur_leader.str, cur_leader.len, "", 0));
			}
			else /* append to the content */
			{
				if (cur_content->len)
					g_string_append_c (cur_content, ' ');
				cur_leader = p->leader;
				g_string_append_len (cur_content, p->content.str, p->content.len);
			}

			prev_content_len = p->content.len;
		}

		/* cleanup step */
		g_string_free (cur_content, TRUE);
	}

	/* remember that empty line we added back up at the top?  We remove it now */
	if (paragraphs->len>0)
		paragraph_free ((Paragraph*) g_ptr_array_remove_index (paragraphs, paragraphs->len-1));

	return paragraphs;
}

static void
wrap_line_at_column (PString  * str,
                     int        column)
{
	char * linefeed_here;
	char * pch;
	char * line_start;
	char * end;
	int    pos;

	/* walk through the entire string */
	linefeed_here = NULL;
	for (pos=0, line_start=pch=str->str, end=line_start+str->len; pch<end; )
	{
		/* a linefeed could go here; remember this space */
		if (g_unichar_isspace (g_utf8_get_char (pch)) || *pch=='\n')
			linefeed_here = pch;

		/* line's too long; add a linefeed if we can */
		if (pos>=column && linefeed_here!=NULL)
		{
			*linefeed_here = '\n';
			pch = line_start = linefeed_here + 1;
			linefeed_here = NULL;
			pos = 0;
		}
		else
		{
			pch = g_utf8_next_char (pch);
			++pos;
		}
	}
}

static void
add_line (GPtrArray * setme, const PString * leader, const PString * content)
{
	char * line = g_malloc (leader->len + content->len + 1);
	memcpy (line, leader->str, leader->len);
	memcpy (line + leader->len, content->str, content->len);
	line [leader->len + content->len] = '\0';
	g_ptr_array_add (setme, line);
}

static void
fill_paragraph (const TextMassager  * tm,
                Paragraph           * p,
                GPtrArray           * setme)
{
	PString line = PSTRING_INIT;

	if (!p->content.len) /* blank line */
		add_line (setme, &p->leader, &p->content);
	else {
		const char * march = p->content.str;
		wrap_line_at_column (&p->content, tm->wrap_column - p->leader.len);
		while (get_next_token_pstring (march, '\n',  &march, &line))
			add_line (setme, &p->leader, &line);
	}
}

char*
text_massager_fill   (const TextMassager    * tm,
                      const char            * body)
{
	GPtrArray * array = g_ptr_array_new ();
	GPtrArray * paragraphs = NULL;
	char * new_body = NULL;
	char * tmp_body = NULL;
	char * sig = NULL;
	int i = 0;

	/* sanity checks */
	g_return_val_if_fail (tm!=NULL, NULL);
	g_return_val_if_fail (is_nonempty_string(body), NULL);
	g_return_val_if_fail (tm->wrap_column>0, g_strdup(body));

	/* get a temp copy of the body -- we don't wrap the signature. */
	tmp_body = pan_substitute (body, "\r", "");
	sig = pan_strstr (tmp_body, "\n-- \n");
	if (sig != NULL)
		*sig = '\0';

	/* fill the paragraphs */
       	paragraphs = get_paragraphs (tm, tmp_body);
	for (i=0; i<paragraphs->len; ++i)
		fill_paragraph (tm, (Paragraph*)g_ptr_array_index(paragraphs,i), array);

	/* make a single string of all filled lines */
	g_ptr_array_add (array, NULL);
	if (1) {
		int i;
		GString * s = g_string_new (NULL);
		for (i=0; g_ptr_array_index(array,i)!=NULL; ++i) {
			g_string_append (s, (const char*)g_ptr_array_index(array,i));
			g_string_append_c (s, '\n');
		}
		if (s->len > 0)
			g_string_truncate (s, s->len-1);
		new_body = g_string_free (s, FALSE);
	}

	/* if we had a sig, put it back in */
	if (sig != NULL) {
		*sig = '\n';
		replace_gstr (&new_body, g_strjoin("\n",new_body, sig, NULL));
	}

	/* cleanup */
	pan_g_ptr_array_foreach (paragraphs, (GFunc)paragraph_free, NULL);
	g_ptr_array_free (paragraphs, TRUE);
	pan_g_ptr_array_foreach (array, (GFunc)g_free, NULL);
	g_ptr_array_free (array, TRUE);
	g_free (tmp_body);

	return new_body;
}

/***
****
***/

char*
text_massager_mute_quoted  (const TextMassager    * tm,
                            const char            * text)
{
	PString line = PSTRING_INIT;
	const PString mute_str = pstring_shallow (_("> [quoted text muted]"), -1);
	const char * march;
	gboolean last_line_was_quote;
	GString * buf;

	g_return_val_if_fail (text!=NULL, NULL);

	march = text;
	last_line_was_quote = FALSE;
	buf = g_string_sized_new (strlen(text));
	while (get_next_token_pstring (march, '\n', &march, &line))
	{
		const gboolean is_quote = line.len && text_massager_is_quote_char(tm, (guchar)*line.str);

		if (!is_quote)
		{
			g_string_append_len (buf, line.str, line.len);
			g_string_append_c (buf, '\n');
		}
		else if (!last_line_was_quote)
		{
			g_string_append_len (buf, mute_str.str, mute_str.len);
			g_string_append_c (buf, '\n');
		}

		last_line_was_quote = is_quote;
	}
	if (buf->len > 0)
		g_string_truncate (buf, buf->len-1); /* trim last \n */

	return g_string_free (buf, FALSE);
}


char*
text_massager_rot13_inplace  (const TextMassager    * tm,
                              char                  * text)
{
	static guchar * translate = NULL;

	if (translate == NULL)
	{
		int i, len;
		const guchar * plain = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		const guchar * roted = "nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM";
		translate = g_new (guchar, UCHAR_MAX);
		for (i=0; i<UCHAR_MAX; ++i)
			translate[i] = (guchar)i;
		for (i=0, len=strlen(plain); i<len; ++i)
			translate[plain[i]] = roted[i];
	}

	for (; is_nonempty_string(text); ++text)
		*text = translate[(guchar)*text];

	return text;
}
