/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __SERVER_H__
#define __SERVER_H__

#include <time.h>
#include <glib/gtypes.h>
#include <glib/garray.h>

#include <gmime/memchunk.h>

#include <pan/base/pan-callback.h>
#include <pan/base/pan-object.h>
#include <pan/base/pstring.h>
#include <pan/base/status-item.h>

#define SERVER(a) ((Server*)a)

 
typedef enum
{
	SERVER_GROUPS_SUBSCRIBED      = (1<<0),
	SERVER_GROUPS_UNSUBSCRIBED    = (1<<1),
	SERVER_GROUPS_ALL             = (SERVER_GROUPS_SUBSCRIBED|SERVER_GROUPS_UNSUBSCRIBED)
}
ServerGroupsType;

typedef struct _Server Server;

struct _Server
{
	/* Parent */
	PanObject parent;

	/*Fields*/
	guint posting              : 1;  /* is posting allowed? */
	guint need_auth            : 1;  /* need authorization ? */
	guint use_newsrc           : 1;  /* import/export .newsrc files? */
	guint newsrc_imported      : 1;  /* have we already imported it? */
	gint8 max_connections;           /* max connections to server */
	gint port;	                 /* port number.  This is usually 119 for NNTP. */
	time_t last_newgroup_list_time;
	PString newsrc_filename;         /* import/export this */
	PString address;                 /* server net address */
	PString username;                /* auth username */
	PString password;                /* auth password */
	PString name;                    /* pretty name, i.e. Erols */
	GQuark name_quark;

	ServerGroupsType _groups_dirty;
	ServerGroupsType _groups_loaded;
	MemChunk * _group_memchunk;
	GHashTable * _groups;             /* don't access directly; use server_get_groups instead */
};

extern GQuark internal_server_quark (void);

/**
***  PROTECTED
**/

void       server_constructor            (Server*, PanObjectDestructor);

void       server_destructor             (PanObject*);

/**
***  PUBLIC - Life Cycle
**/

Server*       server_new (void);

gboolean      server_is_valid            (const Server*);

const char *  server_get_name            (const Server*);

void          server_set_name            (Server          * server,
                                          const PString   * name,
                                          GError         ** error);


/**
***  PUBLIC - Server Groups
**/

struct _Group;

struct _Group* server_alloc_new_group          (Server            * server);

struct _Group* server_get_group                (Server        * server,
                                                GQuark          name_quark);

struct _Group* server_get_group_in_type        (Server            * server,
                                                const GQuark        group_name_quark,
                                                ServerGroupsType    set);

void       server_init_groups             (Server            * server,
                                          struct _Group    ** groups,
                                          int                 group_qty,
                                          GPtrArray         * fillme_used,
                                          GPtrArray         * fillme_not_used);

void       server_add_groups             (Server            * server,
                                          struct _Group    ** groups,
                                          int                 group_qty,
                                          GPtrArray         * fillme_used,
                                          GPtrArray         * fillme_not_used);

void        server_remove_groups          (Server * server, struct _Group ** groups, gint qty);

void        server_destroy_groups         (Server * server, struct _Group ** groups, gint qty);

void        server_ensure_groups_loaded   (Server * server, ServerGroupsType type);

GPtrArray*  server_get_groups             (Server * server, ServerGroupsType type);

void        server_save_if_dirty          (Server * server, StatusItem * status);

void        server_save_grouplist_if_dirty (Server * server, StatusItem * status);

ServerGroupsType server_get_group_type    (const struct _Group * group);

void        server_set_group_type_dirty   (Server * server, ServerGroupsType type);


/**
***  Events
**/

/**
 * @call_obj: Server*
 * @call_arg: GPtrArray* of Group*
 */
PanCallback*   server_get_groups_removed_callback (void);

/**
 * @call_obj: Server*
 * @call_arg: GPtrArray* of Group*
 */
PanCallback*   server_get_groups_added_callback (void);


#endif /* __SERVER_H__ */
