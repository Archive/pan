/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <limits.h>
#include <glib.h>

#include <pan/base/acache.h>
#include <pan/base/debug.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/base-prefs.h>
#include <pan/base/util-file.h>

char * download_dir = NULL;
const char * default_incoming_name_real;
const char * default_incoming_name_addr;
int acache_max_megs = 10;
gboolean acache_flush_on_exit = FALSE;
gboolean break_thread_when_subject_changes = FALSE;

void
base_prefs_init (const char    * new_download_dir,
                 int             new_acache_max_megs,
                 gboolean        new_acache_flush_on_exit,
                 gboolean        break_thread)
{
	replace_gstr (&download_dir, g_strdup(new_download_dir));
	break_thread_when_subject_changes = break_thread;

	acache_max_megs = new_acache_max_megs;
	acache_flush_on_exit = new_acache_flush_on_exit;
	acache_init ();

	default_incoming_name_real = _("Unknown");
	default_incoming_name_addr = _("unknown@spammesenseless.com");
}

const char*
get_data_dir (void)
{
	static char * path = NULL;

	if (path == NULL)
	{
		const char * pan_home = g_getenv ("PAN_HOME");
		const char * user_home = pan_get_home_dir ();

		if (path==NULL)
			path = g_strdup (pan_home);

		if (path==NULL) {
			path = g_build_filename (user_home, ".pan", "data", NULL);
			if (!g_file_test (path, G_FILE_TEST_IS_DIR))
				replace_gstr (&path, NULL);
		}

		if (path==NULL)
			path = g_build_filename (user_home, ".pan", NULL);

		pan_file_ensure_path_exists (path);
	}

	return path;
}
