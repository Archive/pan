/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib/gmem.h>

#include <ctype.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include <pan/base/acache.h>
#include <pan/base/article.h>
#include <pan/base/article-thread.h>
#include <pan/base/base-prefs.h>
#include <pan/base/debug.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

static gboolean
has_reply_leader(const PString * p)
{
	return pstring_is_set(p)
	    && p->len>4 \
	    && (p->str[0]=='R' || p->str[0]=='r')
	    && (p->str[1]=='E' || p->str[1]=='e')
	    && p->str[2]==':'
	    && p->str[3]==' ';
};

/**
 * Skip the "Re: " part of a subject header, if any
 * @param subject
 * @return the non-"Re:" portion of the subject header
 */
static PString
strip_reply_leader (const PString * subject)
{
	PString ret;

	if (!pstring_is_set (subject))
		ret = PSTRING_INIT;
	else if (!has_reply_leader (subject))
		ret = pstring_shallow (subject->str, subject->len);
	else
		ret = pstring_substr_shallow (subject, subject->str+4, NULL);

	return ret;
}

/**
 * Normalized Article
 */
typedef struct
{
	gboolean is_reply;
	int subject_len;
	char * subject;
	Article * a;
}
Norm;

/**
 * Normalizing a subject header involves:
 *
 * 1. tearing out the numerator from multipart indicators
 *    (i.e., remove "21" from (21/42))
 *    for threading.
 * 2. convert it to lowercase so that, when sorting, we can
 *    have case-insensitive sorting without having to use
 *    a slower case-insensitive compare function.
 * 3. strip out all the leading noise that breaks sorting.
 *
 * When we're threading articles, it's much faster to
 * normalize the * subjects at the outset instead of
 * normalizing them for each comparison.
 */
size_t
normalize_subject (char            * buf,
                   const PString   * full_subject,
                   StripFlags        strip)
{
	static gboolean _inited = FALSE;
	static gboolean _keep_chars[UCHAR_MAX+1];
	const PString subject = strip_reply_leader (full_subject);
	register const guchar * in = subject.str;
	register const guchar * end = subject.str + subject.len;
	register char * out = buf;
	const gboolean strip_case = strip & STRIP_CASE;
	const gboolean strip_numerator = strip & STRIP_MULTIPART_NUMERATOR;
	const gboolean strip_multipart = strip & STRIP_MULTIPART;

	/* populate the _keep_chars array */
	if (!_inited) {
		int i;
		for (i=0; i<=UCHAR_MAX; ++i) {
			const guchar uch = (guchar)i;
			_keep_chars[i] = isalnum(uch) || isdigit(uch) || isspace(uch);
		}
		_inited = TRUE;
	}

	/* skip the leading noise */
	while (in!=end && isspace(*in))
		++in;

	while (in!=end)
	{
		/* strip numerator out of multipart information */
		if ((strip_multipart||strip_numerator) && (*in=='('||*in=='[') && isdigit(in[1]))
		{
			const guchar * start = ++in;

			if (strip_multipart || strip_numerator)
			{
				while (in!=end && isdigit(*in))
					++in;

				if (*in!='/' && *in!='|') /* oops, not multipart information */
					in = start;

				else if (strip_multipart)
				{
					for (++in; in!=end && *in!=']' && *in!=')'; ++in)
					{
						if (isalpha(*in)) { /* oops, not multipart information */
							in = ++start;
							break;
						}
					}
				}
			}
			continue;
		}

		/* strip out junk that breaks sorting */
		if (_keep_chars[*in])
			*out++ = (char) (strip_case ? tolower(*in) : *in);

		++in;
	}

	*out = '\0';
	return out - buf;
}


/**
 * This Normalizes a group of articles in just two memory blocks.
 * These blocks will need to be g_free()d when the client is done with them.
 */
static void
normalize_articles (Article    ** articles,
                    gint          qty,
                    StripFlags    strip_flags,
                    Norm       ** alloc_and_setme_norm,
                    gchar      ** alloc_and_setme_str)
{
	gint i;
	glong str_buf_idx;
	glong len;
	gchar * str_buf;
	Norm * norm_buf;

	/* sanity clause */
	g_return_if_fail (articles!=NULL);
	g_return_if_fail (qty>0);
	g_return_if_fail (articles_are_valid ((const Article **)articles, qty));
	g_return_if_fail (alloc_and_setme_norm!=NULL);
	g_return_if_fail (alloc_and_setme_str!=NULL);

	/* alloc a buf for the norms */
	*alloc_and_setme_norm = norm_buf = g_new (Norm, qty);

	/* alloc a buf for the subject */
	len = 0;
	for (i=0; i<qty; ++i)
		len += articles[i]->subject.len + 2;
	*alloc_and_setme_str = str_buf = g_new (char, len);
	
	/* normalize the articles */
	str_buf_idx = 0;
	for (i=0; i<qty; ++i) {
		Article * a = articles[i];
		norm_buf[i].a = a;
		norm_buf[i].is_reply = has_reply_leader (&a->subject);
		norm_buf[i].subject = str_buf + str_buf_idx;
		norm_buf[i].subject_len = normalize_subject (norm_buf[i].subject, &a->subject, strip_flags);
		str_buf_idx += norm_buf[i].subject_len + 1;
	}
}


static int
compare_pA_to_pA_by_subject (const void * va, const void * vb)
{
	int             value;
	const Article * a = (const Article *)va;
	const Article * b = (const Article *)vb;
	gboolean        a_is_reply;
	gboolean        b_is_reply;

	/* if one but not both is a reply, the reply goes second */
	a_is_reply = has_reply_leader (&a->subject);
	b_is_reply = has_reply_leader (&b->subject);
	if (a_is_reply != b_is_reply)
		return a_is_reply ? 1 : -1;

	/* order by subject */
	if ((value = pstring_compare (&a->subject, &b->subject)))
		return value;

	/* oldest goes first... */
	if (a->date < b->date)
		value = -1;
	else if (a->date > b->date)
		value = 1;
	else
		value = 0;
	return value;
}

static int
compare_pN_to_pN_by_subject (const void * va, const void * vb)
{
	register int value;
	register const Norm * a = (const Norm *)va;
	register const Norm * b = (const Norm *)vb;

	/* subject is the primary key, of course... */
	if ((value = *a->subject - *b->subject))
		return value;
	if ((value = pan_strcmp_len (a->subject, a->subject_len, b->subject, b->subject_len)))
		return value;

	/* if one but not both is a reply, the reply goes second */
	if (a->is_reply != b->is_reply)
		return a->is_reply ? 1 : -1;

	/* oldest goes first... */
	if (a->a->date < b->a->date)
		value = -1;
	else if (a->a->date > b->a->date)
		value = 1;
	else
		value = 0;
	return value;
}

static int
compare_ppA_to_ppA_by_score (const void* va, const void* vb)
{
        register const Article * a = *(const Article**)va;
        register const Article * b = *(const Article**)vb;
	return a->score - b->score;
}

static int
compare_ppA_to_ppA_by_linecount (const void* va, const void* vb)
{
        register const Article * a = *(const Article**)va;
        register const Article * b = *(const Article**)vb;
	return article_get_full_line_count(a) - article_get_full_line_count(b);
}

static int
compare_ppA_to_ppA_by_action (const void * va, const void * vb)
{
	register const Article * a = *(const Article **)va;
	register const Article * b = *(const Article **)vb;
	int ia, ib;

	ia = article_get_decode_state (a);
	ib = article_get_decode_state (b);
	if (ia != ib)
		return ib - ia;

	ia = acache_has_message (group_get_acache_key(a->group), &a->message_id) ? 1 : 0;
	ib = acache_has_message (group_get_acache_key(b->group), &b->message_id) ? 1 : 0;
	if (ia != ib)
		return ib - ia;

	return 0;
}

static int
compare_ppA_to_ppA_by_read (const void * va, const void * vb)
{
	register const Article * a = *(const Article **)va;
	register const Article * b = *(const Article **)vb;
	const gboolean a_is_read = article_is_read (a);
	const gboolean b_is_read = article_is_read (b);
	const gboolean a_is_new = article_is_new (a);
	const gboolean b_is_new = article_is_new (b);
	gint ia, ib;

	ia = (a_is_new?1:0) + a->new_children;
	ib = (b_is_new?1:0) + b->new_children;
	if (ia != ib)
		return ib - ia;

	ia = (a_is_read?0:1) + a->unread_children;
	ib = (b_is_read?0:1) + b->unread_children;
	if (ia != ib)
		return ib - ia;

	ia = a_is_read ? 0 : 1;
	ib = b_is_read ? 0 : 1;
	if (ia != ib)
		return ib - ia;

	ia = article_get_multipart_state (a);
	ib = article_get_multipart_state (b);
	if (ia != ib)
		return ib - ia;

	return 0;
}


static int
compare_ppA_to_ppA_by_date (const void* va, const void* vb)
{
	gint value;
	time_t date_a = (*(const Article**)va)->date;
	time_t date_b = (*(const Article**)vb)->date;

	if (date_a < date_b)
		value = -1;
	else if (date_a > date_b)
		value = 1;
	else
		value = 0;
	return value;
}

static int
compare_ppA_to_ppA_by_message_id (const void* va, const void* vb)
{
	register const Article * a = *(const Article **)va;
	register const Article * b = *(const Article **)vb;
	return pstring_compare (&a->message_id, &b->message_id);
}

typedef struct
{
	char name[128];
	int name_len;
	Article * article;
}
ArticleStruct;

static int
compare_pAS_to_pAS_by_data (const void * va, const void * vb)
{
	const ArticleStruct * a = (const ArticleStruct*)va;
	const ArticleStruct * b = (const ArticleStruct*)vb;
	return pan_strcmp_len (a->name, a->name_len, b->name, b->name_len);
}

void
sort_articles (Article      ** buf,
               size_t          article_qty,
               int             sort_type,
               gboolean        ascending)
{
	g_return_if_fail (articles_are_valid ((const Article **)buf, article_qty));

	if (!article_qty)
		return;

	switch (sort_type)
	{
		case ARTICLE_SORT_AUTHOR:
		{
			size_t i;
			ArticleStruct * as = g_new (ArticleStruct, article_qty);
			for (i=0; i<article_qty; ++i)
			{
				as[i].article = buf[i];
				as[i].name_len = article_get_short_author_str (buf[i], as[i].name, sizeof(as[i].name));
				g_strdown (as[i].name);
			}
			msort (as,
			       article_qty,
			       sizeof(ArticleStruct),
			       compare_pAS_to_pAS_by_data);
			for (i=0; i<article_qty; ++i)
				buf[i] = as[i].article;
			g_free (as);
			break;
		}
		case ARTICLE_SORT_LINES:
		{
			msort (buf, article_qty, sizeof(Article*), compare_ppA_to_ppA_by_linecount);
			break;
		}
		case ARTICLE_SORT_DATE:
		{
			msort (buf, article_qty, sizeof(Article*), compare_ppA_to_ppA_by_date);
			break;
		}
		case ARTICLE_SORT_MSG_ID:
		{
			msort (buf, article_qty, sizeof(Article*), compare_ppA_to_ppA_by_message_id);
			break;
		}
		case ARTICLE_SORT_ACTION_STATE:
		{
			msort (buf, article_qty, sizeof(Article*), compare_ppA_to_ppA_by_action);
			break;
		}
		case ARTICLE_SORT_READ_STATE:
		{
			msort (buf, article_qty, sizeof(Article*), compare_ppA_to_ppA_by_read);
			break;
		}
		case ARTICLE_SORT_SCORE:
		{
			msort (buf, article_qty, sizeof(Article*), compare_ppA_to_ppA_by_score);
			break;
		}
		case ARTICLE_SORT_SUBJECT:
		default:
		{
			gint i;
			Norm * norm_buf = NULL;
			char * str_buf = NULL;
			normalize_articles (buf, article_qty, STRIP_CASE, &norm_buf, &str_buf);
			msort (norm_buf, article_qty, sizeof(Norm), compare_pN_to_pN_by_subject);
			for (i=0; i<article_qty; ++i)
				buf[i] = ARTICLE(norm_buf[i].a);
			g_free (norm_buf);
			g_free (str_buf);
		}
	}

	/* if not ascending, reverse the order */
	if (!ascending) {
		const size_t mid = article_qty/2;
		size_t i;
		for (i=0; i!=mid; ++i) { /* swap */
			Article * tmp = buf[i];
			buf[i] = buf[article_qty-1-i];
			buf[article_qty-1-i] = tmp;
		}
	}
}


static gboolean
is_child_of (const Article * child,
             const Article * parent)
{
	g_return_val_if_fail (child!=NULL, FALSE);
	g_return_val_if_fail (parent!=NULL, FALSE);

	for (;;)
	{
		if (!child)
			return FALSE;
		if (child == parent)
			return TRUE;
		child = child->parent;
	}
}

static void
inc_part_num_if_match (const Article * a, const AttachmentPart * p, gpointer gulong_p)
{
	gulong* part_num = (gulong*) gulong_p;
	if (*part_num == p->part_num)
		++(*part_num);
}

static int
article_get_part_state (const Article * a)
{
	int retval;

	/* sanity clause */
	g_return_val_if_fail (a!=NULL, 0u);

	/* not a multipart */
	if (a->parts<1)
		retval = MULTIPART_STATE_NONE;

	/* someone's posted a followup to a multipart */
	else if (a->line_qty<250 && !g_strncasecmp(a->subject.str,"Re:", 3))
		retval = MULTIPART_STATE_NONE;

	/* someone's posted a "00/124" nfo message */
	else if (a->attachments == NULL)
		retval = MULTIPART_STATE_NONE;

	/* a multipart */
	else {
		gulong part_num = 1;
		article_foreach_attachment_part (a, inc_part_num_if_match, &part_num);
		retval = part_num == a->parts+1 ? MULTIPART_STATE_ALL : MULTIPART_STATE_SOME;
	}

	return retval;
}

/**
 * Thread the articles specified in list
 */
void
thread_articles (Article   ** articles,
                 guint        article_qty)
{
	guint i;
	GHashTable * mid_to_article;
	GHashTable * subject_to_article;

	/* sanity clause */
	g_return_if_fail (articles!=NULL);
	g_return_if_fail (articles_are_valid ((const Article**)articles, article_qty));

	if (article_qty<1 || !articles)
		return;

	/* make a hah of the articles, keyed by message-id.
	   Our first mode of threading is by References header. */
	mid_to_article = g_hash_table_new (pstring_hash, pstring_equal);
	for (i=0; i<article_qty; ++i)
		g_hash_table_insert (mid_to_article, &articles[i]->message_id, articles[i]);

	/* make a hash of the articles, keyed by subject.
	   After Referenecs, we fall back to finding a similar subject header. */
	subject_to_article = g_hash_table_new_full (pstring_hash, pstring_equal, (GDestroyNotify)pstring_free, NULL);
	for (i=0; i<article_qty; ++i) {
		Article * cur = articles[i];
		const PString subject = strip_reply_leader (&cur->subject);
		Article * old = (Article*) g_hash_table_lookup (subject_to_article, &subject);
		if (old==NULL || compare_pA_to_pA_by_subject(cur,old)<0)
			g_hash_table_insert (subject_to_article, pstring_dup(&subject), cur);
	}

	/* unthread the articles, just in case they were threaded before */
	for (i=0; i!=article_qty; ++i) {
		Article * a = articles[i];
		a->parent = NULL;
		g_slist_free (a->followups);
		a->followups = NULL;
	}

	/* thread the articles */
	for (i=0; i!=article_qty; ++i)
	{
		Article * parent = NULL;
		Article * a = articles[i];

		/* thread by reference.
		   We take the references header and work our way from the last message-id backwards
		   to the first.  For each message-id in this series, the first one we find in our
		   set of articles is flagged as the parent. */
		if (pstring_is_set(&a->references))
		{
			PString references = a->references;

			/* pull the last message-id from the references string. */
			const char * cpch = pan_strrchr_len (references.str, references.len, '<');
			PString message_id = pstring_substr_shallow (&references, cpch, NULL);

			while (parent==NULL && pstring_is_set (&message_id))
			{
				/* look for a match in our message-id hashtable */
				Article * match = (Article*) g_hash_table_lookup (mid_to_article, &message_id);

				/* if we found the ancestor & it's worthy, thread it */
				if (match!=NULL && !is_child_of(match,a))
				{
					gboolean subject_changed = FALSE;

					if (break_thread_when_subject_changes)
						subject_changed = !pstring_equal (&a->subject, &articles[i]->subject);

					if (!subject_changed)
						parent = match;
				}

				/* if we couldn't find the ancestor, march up the References string */
				references.len -= message_id.len;
				references = pstring_strstrip_shallow (&references);
				cpch = pan_strrchr_len (references.str, references.len, '<');
				message_id = pstring_substr_shallow (&references, cpch, NULL);
			}
		}

		/* thread by subject */
		if (!parent)
		{
			const PString subject = strip_reply_leader (&a->subject);
			Article * subject_root = (Article*) g_hash_table_lookup (subject_to_article, &subject);
			if (!is_child_of (subject_root, a))
				parent = subject_root;
		}

		/* this article has a parent */
		if (parent != NULL)
		{
			g_assert (!is_child_of(parent,a));

			/* link the two articles */
			a->parent = parent;
			parent->followups = g_slist_prepend (parent->followups, a);
		}
	}

	/* sort the followups */
	for (i=0; i!=article_qty; ++i)
		articles[i]->followups = g_slist_sort (articles[i]->followups, compare_pA_to_pA_by_subject);

	/* calculate multipart states */
	for (i=0; i!=article_qty; ++i) 
		articles[i]->multipart_state = article_get_part_state (articles[i]);

	/* cleanup */
	g_hash_table_destroy (mid_to_article);
	g_hash_table_destroy (subject_to_article);
}
