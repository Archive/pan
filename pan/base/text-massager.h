/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __UTIL_WRAP__H__
#define __UTIL_WRAP__H__

typedef struct _TextMassager TextMassager;

/**
***  Life Cycle
**/

TextMassager*   text_massager_new                       (void);

void            text_massager_free                      (TextMassager          * tm);

/**
***  Utilities
**/

char*           text_massager_rot13_inplace             (const TextMassager    * tm,
                                                         char                  * text);

char*           text_massager_mute_quoted               (const TextMassager    * tm,
                                                         const char            * text);

char*           text_massager_fill                      (const TextMassager    * tm,
                                                         const char            * text);

int             text_massager_get_wrap_column           (const TextMassager    * tm);

gboolean        text_massager_is_quote_char             (const TextMassager    * tm,
                                                         guchar                  ch);

char*           text_massager_get_quote_chars           (const TextMassager    * tm);

/**
***  Mutators
**/

void            text_massager_set_wrap_column           (TextMassager          * tm,
                                                         int                     column);

void            text_massager_set_quote_chars           (TextMassager          * tm,
                                                         const guchar          * quote_chars);



#endif /* __UTIL_WRAP__H__ */
