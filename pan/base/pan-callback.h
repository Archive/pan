/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * ABOUT PAN-CALLBACK
 *
 *   PanCallback is an Xt-style implementation of the Observer/Observable
 *   pattern. 
 *
 *   The main goals of PanCallback are simplicity and thread safety:
 *   PanCallback is threadsafe and is just under 100 lines of code.
 *   It will work equally well inside our GtkObjects and elsewhere.
 *
 * WHY NOT SIGNALS
 *
 *   Gtk-style signals are more typesafe and flexible.  As of this writing,
 *   though, there's no indication when (if ever) they'll be threadsafe.
 *   Gtk signals also tie us to GtkObject, which may be too heavyweight for
 *   our needs.
 *
 * CALLBACK SIGNATURES
 *
 *   One of the strengths (and sources of complexity) of Gtk's signals is the
 *   type safety of the signal function signatures.  This is a nice feature,
 *   but in the interests of Keeping It Simple, PanCallback instead follows
 *   the Xt callback model: all callbacks take three arguments, as defined in
 *   PanCallbackFunc.  This is a pointer to the object emitting the signal, an
 *   emit argument, and a user_data argument (passed in at pan_callback_add()).
 *
 *   csk, 2000/01/20
 */

#ifndef PAN_CALLBACK_H
#define PAN_CALLBACK_H

#include <glib/gtypes.h>


typedef struct PanCallbackStruct PanCallback;

PanCallback* pan_callback_new (void);
void pan_callback_free (PanCallback**);

typedef void (*PanCallbackFunc)(gpointer call_object, gpointer call_arg, gpointer user_data);
void pan_callback_add (PanCallback*, PanCallbackFunc, gpointer);
void pan_callback_remove (PanCallback*, PanCallbackFunc, gpointer);

void pan_callback_call (PanCallback*, gpointer call_object, gpointer call_arg);


#endif /* PAN_CALLBACK_H */
