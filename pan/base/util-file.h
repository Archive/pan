/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __UTIL_IO_H__
#define __UTIL__IOH__

#include <stddef.h>
#include <stdio.h>
#include <glib/gtypes.h>

/**
 * Wrapper around g_file_open_tmp() which logs errors to Pan's log mechanism.
 * @return filename
 * @param setme_fp if successful, set with an open FILE* to filename
 */
char*      pan_file_make_temp                      (FILE       ** setme_fp);

/**
 * Returns the number of bytes in the file, or 0 on error.
 * @return the number of bytes in the file, or 0 on error.
 * @param filename
 */
size_t     pan_file_get_size                       (const char  * filename);

/**
 * If the specified path doesn't exist, Pan tries to create it.
 * @param path
 * @return true if the path already existed or was created; false otherwise
 */
gboolean   pan_file_ensure_path_exists             (const char  * path);

/**
 * Check to see if the specifiled file exists.
 * @return true if the file exists, false otherwise
 * @param filename the file to check
 */
gboolean   pan_file_exists                         (const char  * filename);

/**
 * Removes extra '/' characters from the specified filename
 * @param filename
 * @return the filename pointer.
 */
char*      pan_file_normalize_inplace              (char        * filename);

/**
 * Normalizes a file:
 * <ul>
 * <li> removes extra directory seprators from the path
 * <li> relative paths are based from the users' home directory
 * <li> paths beginning with ~ are based from the users' home directory
 * </ul>
 */
char*      pan_file_normalize                      (const char   * filename,
                                                    const char   * fallback_absolute_path_or_NULL_for_users_home_dir);

/**
 * Move the specified file to the new filename.
 * @param old_filename the file to be moved
 * @param new_filename the target file location.
 * @return true if successful, false otherwise.
 */
gboolean   pan_file_rename                         (const char  * old_filename,
                                                    const char  * new_filename);

/**
 * Swap from a scratch file to a datafile in a somewhat safe way.
 * This will make a backup of the current datafile, then try to
 * rename the scratch file to the datafile, and will restore from
 * the backup if something goes wrong during the rename.
 * 
 * If your datafile is small enough to hold in memory, it is probably
 * more convenient to use pan_file_write_datafile().  For big data files,
 * create a filename.tmp file and fwrite into it piece-by-piece, then
 * call pan_file_swap_datafile(filename, filename.tmp).
 */
gboolean   pan_file_swap_datafile                  (const char  * filename_tmp,
                                                    const char  * filename);
/**
 * Writes the text to a datafile in a somewhat safe way.
 */
gboolean   pan_file_write_datafile                 (const char  * filename,
                                                    const char  * text,
                                                    gulong        text_len);

/**
*** Attempt to make a filename safe for use.
*** This is done by replacing illegal characters with '_'.
*** This function assumes the input is UTF8 since gmime uses UTF8 interface.
*** return value must be g_free'd.
**/
gchar* pan_file_sanitize_fname(const gchar *fname);

/**
*** Makes a unique filename given an optional path and a starting file name.
*** The filename is sanitized before looking for uniqueness.
**/
gchar* pan_file_get_unique_fname( const gchar *path, const gchar *fname);

#endif /* __UTIL_H__ */
