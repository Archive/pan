/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __ARGSET__H__
#define __ARGSET__H__

#include <glib/gtypes.h>
#include <glib/garray.h>

typedef struct
{
	GPtrArray * args;
}
ArgSet;

ArgSet * argset_new (void);
ArgSet * argset_new1 (gpointer);
ArgSet * argset_new2 (gpointer, gpointer);
ArgSet * argset_new3 (gpointer, gpointer, gpointer);
ArgSet * argset_new4 (gpointer, gpointer, gpointer, gpointer);
ArgSet * argset_new5 (gpointer, gpointer, gpointer, gpointer, gpointer);
ArgSet * argset_new6 (gpointer, gpointer, gpointer, gpointer, gpointer, gpointer);

gpointer argset_get (ArgSet*, gint index);
void argset_add (ArgSet*, gpointer);
void argset_free (ArgSet*);

#endif
