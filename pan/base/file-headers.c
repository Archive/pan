/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include <gmime/gmime.h>

#include <pan/base/acache.h>
#include <pan/base/article.h>
#include <pan/base/base-prefs.h>
#include <pan/base/debug.h>
#include <pan/base/file-headers.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/group.h>
#include <pan/base/log.h>
#include <pan/base/status-item.h>
#include <pan/base/util-file.h>

/***
****
****
****
***/

typedef struct
{
	Group * folder;
	GPtrArray * articles;
}
FolderForeachStruct;

static void
file_headers_load_folder_foreach (GMimeMessage * message, gpointer user_data)
{
	Article * article;
	FolderForeachStruct * ffs = (FolderForeachStruct*) user_data;

	article = article_new (ffs->folder);
	article_set_from_g_mime_message (article, message);
	article->number = ffs->articles->len + 1;
	g_ptr_array_add (ffs->articles, article);
}

static gboolean
file_headers_load_folder (Group * folder, StatusItem * status,
                          gulong * part_qty,
                          gulong * article_qty)
{
	FolderForeachStruct ffs;
	debug_enter ("file_headers_load_folder");

	/* sanity clause */
	g_return_val_if_fail (group_is_valid(folder), FALSE);
	g_return_val_if_fail (group_is_folder(folder), FALSE);

	/* prep for foreach */
	ffs.folder = folder;
	ffs.articles = g_ptr_array_new ();
	acache_path_foreach (folder->name.str, file_headers_load_folder_foreach, &ffs);

	/* if we've got articles then add them; otherwise; clean up */
	if (ffs.articles->len != 0)
		group_init_articles (folder, ffs.articles, status);
	else if (folder->_articles) {
		g_hash_table_destroy (folder->_articles);
		folder->_articles = NULL;
	}
	folder->articles_dirty = FALSE;

	*article_qty = ffs.articles->len;
	g_ptr_array_free (ffs.articles, TRUE);
	debug_exit ("file_headers_load_folder");
	return folder->_articles!=NULL && g_hash_table_size(folder->_articles)!=0;
}

/***
****
****
****
***/

static void
purge_0140_style_files (const Group * group)
{
	char fname [PATH_MAX];

	g_return_if_fail (group_is_valid (group));

	g_snprintf (fname, sizeof(fname), "%s%c%*.*s%c%*.*s.idx",
	            get_data_dir(),
	            G_DIR_SEPARATOR, group->server->name.len, group->server->name.len, group->server->name.str,
	            G_DIR_SEPARATOR, group->name.len, group->name.len, group->name.str);
	pan_unlink (fname);

	g_snprintf (fname, sizeof(fname), "%s%c%*.*s%c%*.*s.dat",
	            get_data_dir(),
	            G_DIR_SEPARATOR, group->server->name.len, group->server->name.len, group->server->name.str,
	            G_DIR_SEPARATOR, group->name.len, group->name.len, group->name.str);
	pan_unlink (fname);
}

static void
purge_014090_style_files (const Group * group)
{
	char fname [PATH_MAX];

	g_return_if_fail (group_is_valid (group));

	g_snprintf (fname, sizeof(fname), "%s%c%*.*s%c%*.*s",
	            get_data_dir(),
	            G_DIR_SEPARATOR, group->server->name.len, group->server->name.len, group->server->name.str,
	            G_DIR_SEPARATOR, group->name.len, group->name.len, group->name.str);
	pan_unlink (fname);
}

static void
get_014090_filename (char * buf, int buf_len, const Group * group)
{
	const PString * sname = &group->server->name;
	const PString * gname = &group->name;

	g_snprintf (buf, buf_len, "%s%c%*.*s%c%*.*s",
	            get_data_dir(),
	            G_DIR_SEPARATOR, sname->len, sname->len, sname->str,
	            G_DIR_SEPARATOR, gname->len, gname->len, gname->str);
}

static gboolean
headers_014090_style_exists (const Group * group)
{
        gchar fname [PATH_MAX];
        g_return_val_if_fail (group_is_valid (group), FALSE);

	get_014090_filename (fname, sizeof(fname), group);
        return pan_file_exists  (fname);
}

static gboolean
file_headers_load_group_014090 (Group * group, StatusItem * status,
                                gulong * part_qty,
                                gulong * article_qty)
{
	GIOChannel * in_gio;
	char in_fname[PATH_MAX];
	gboolean success = FALSE;
	debug_enter ("file_headers_load_group_014090");

	g_return_val_if_fail (group_is_valid (group), FALSE);

	if (status!=NULL)
		status_item_emit_status_va (status, _("Loading group \"%s\""), group_get_name(group));

	/* open the group's file */
	get_014090_filename (in_fname, sizeof(in_fname), group);
	in_gio = g_io_channel_new_file (in_fname, "r", NULL);

	if (in_gio != NULL)
	{
		int version;
		GString * line;

		g_io_channel_set_encoding (in_gio, NULL, NULL);
 		line = g_string_sized_new (1024);

		g_io_channel_read_line_string (in_gio, line, NULL, NULL);
		version = atoi (line->str);
		if (version < 12)
		{
			/* we've stayed backwards compatable for 11 versions,
			   but the changes in 12 aren't just cosmetic.
			   The hell with it. */
			log_add_va (LOG_ERROR,
				_("Skipped unsupported old data file for group \"%s\"."), group->name);
		}
		else if (version==12)
		{
			int i;
			int attachment_qty;
			int purged_article_count = 0;
			long qty;
			GPtrArray * addme;

			/* quantity of articles */
			g_io_channel_read_line_string (in_gio, line, NULL, NULL);
			qty = strtol (line->str, NULL, 10);

			/* load the articles */
		       	addme = g_ptr_array_sized_new (qty);
			for (i=0; i!=qty; ++i)
			{
				int j;
				Article * a = article_new (group);

#ifdef G_OS_WIN32
#define EOLN_LEN 2 /* \r\n */
#else
#define EOLN_LEN 1 /* \n */
#endif 

#define read_next_line_gstr(line) \
				g_io_channel_read_line_string (in_gio, line, NULL, NULL); \
				g_string_truncate (line, line->len-EOLN_LEN);

#define set_string_from_next_line(line,pstr) \
				read_next_line_gstr(line) \
				if (line->len) \
					pstring_set (pstr, line->str, line->len);

				set_string_from_next_line (line, &a->message_id)
				set_string_from_next_line (line, &a->author_addr)
				set_string_from_next_line (line, &a->author_real)
				set_string_from_next_line (line, &a->subject)
				set_string_from_next_line (line, &a->references)
				read_next_line_gstr (line); 
				message_sources_add_from_xref (&a->xref, group->server->name_quark, line->str, line->len);

				read_next_line_gstr (line); a->parts = (gint16) atoi (line->str);
				read_next_line_gstr (line); a->line_qty = (guint16) atoi (line->str);
				read_next_line_gstr (line); a->byte_qty = strtoul (line->str, NULL, 10);
				read_next_line_gstr (line); a->date = (time_t) strtoul (line->str, NULL, 10);
				read_next_line_gstr (line); a->number = strtoul (line->str, NULL, 10);
				read_next_line_gstr (line); a->is_new = atoi (line->str) != 0;
				read_next_line_gstr (line); attachment_qty = atoi (line->str);

				for (j=0; j<attachment_qty; ++j)
				{
					AttachmentPart * part = article_alloc_part (a);

					read_next_line_gstr (line); part->part_num = (gint16) atoi (line->str);
					read_next_line_gstr (line); part->line_qty = (guint16) strtoul (line->str, NULL, 10);
					read_next_line_gstr (line); part->byte_qty = (gulong) strtoul (line->str, NULL, 10);
					read_next_line_gstr (line); part->date = (gulong) strtoul (line->str, NULL, 10);
					set_string_from_next_line (line, &part->message_id);
					read_next_line_gstr (line); 
					message_sources_add_from_xref (&part->sources, group->server->name_quark, line->str, line->len);

					article_add_part (a, part);
					++*part_qty;
				}

				/* let the user know what we're doing */
				if (status != NULL) {
					status_item_emit_next_step (status);
					if (!(addme->len % 512) || (i == qty-1))
						status_item_emit_status_va (status,
							_("Loaded %d of %d articles"), i+1, qty);
				}

				/* add the article to the group if it looks sane */
				if (!article_is_valid (a))
					++purged_article_count;
				else {
					++*article_qty;
					g_ptr_array_add (addme, a);
				}
			}

			if (purged_article_count != 0)
			{
				log_add_va (LOG_ERROR,
				            _("Pan skipped %d corrupt headers from the local cache for group \"%*.*s\"."),
				            purged_article_count,
				            group->name.len, group->name.len, group->name.str);
				log_add (LOG_ERROR,
				         _("You may want to empty this group and download fresh headers."));
			}

			group_init_articles (group, addme, status);
			group->articles_dirty = purged_article_count!=0;
			success = TRUE;
			g_ptr_array_free (addme, TRUE);
		}
		else
		{
			log_add_va (LOG_ERROR|LOG_URGENT,
				_("Unsupported data version for %s headers: %d.\nAre you running an old version of Pan by accident?"), group->name, version);
		}

		/* cleanup */
		g_string_free (line, TRUE);
		g_io_channel_unref (in_gio);
	}

	/* cleanup */
	debug_exit ("file_headers_load_group_014090");
	return success;
}


void
file_headers_load (Group * group, StatusItem * status)
{
	gboolean success = FALSE;
	gulong part_qty = 0ul;
	gulong article_qty = 0ul;
	double diff;
	GTimeVal start;
	GTimeVal finish;
	debug_enter ("file_headers_load");

	/* start the stopwatch */
	g_get_current_time (&start);

	/* load the group */
	if (group_is_folder (group))
		success = file_headers_load_folder (group, status, &part_qty, &article_qty);
	else if (headers_014090_style_exists (group))
		success = file_headers_load_group_014090 (group, status, &part_qty, &article_qty);

#if 0
	/* expire the old articles, if any */
	if (success)
	{
		gulong low=0, high=0;
		group_get_article_range (group, &low, &high);
		group_expire_articles_not_in_range (group, low, high);
	}
#endif

	/* timing stats */
	g_get_current_time (&finish);
	diff = finish.tv_sec - start.tv_sec;
	diff += (finish.tv_usec - start.tv_usec)/(double)G_USEC_PER_SEC;
	if (article_qty != 0u)
		log_add_va (LOG_INFO, _("Loaded %lu parts, %lu articles in \"%s\" in %.1f seconds (%.0f art/sec)"),
		                      part_qty,
		                      article_qty,
		                      group_get_name(group),
		                      diff,
		                      article_qty/(fabs(diff)<0.001?0.001:diff));

	debug_exit ("file_headers_load");
}

typedef struct
{
	StatusItem * status;
	gboolean success;
}
FileHeadersSaveData;

static void
serialize_attachment_part (const Article * a, const AttachmentPart * part, gpointer gstr_p)
{
	GString * buf = (GString*) gstr_p;

	g_string_append_printf (buf,
		"%d\n" /* part_num */
		"%u\n" /* line_qty */
		"%lu\n" /* byte_qty */
		"%lu\n", /* date */
		(int) part->part_num,
		(unsigned int) part->line_qty,
		(unsigned long) part->byte_qty,
		(unsigned long) part->date);

	if (part->message_id.len)
		g_string_append_len (buf, part->message_id.str, part->message_id.len);
	g_string_append_c (buf, '\n');

	message_sources_to_xref (&part->sources, buf, a->group->server->name_quark, FALSE);
	g_string_append_c (buf, '\n');
}

static void
file_headers_save_group_articles (Group              * group,
                                  Article           ** articles,
                                  guint                article_qty,
                                  gpointer             user_data)
{
	GString * buf;
	FileHeadersSaveData * data = (FileHeadersSaveData*) user_data;
	guint i;
	FILE * out_fp;
	char * dir;
	char out_fname [PATH_MAX];
	char out_fname_tmp [PATH_MAX];
	gboolean ok = TRUE;
	debug_enter ("file_headers_save_group");

	if (!article_qty)
	{
		file_headers_destroy (group);
		data->success = FALSE;
		return;
	}

	/* prep the directory & filenames */
	get_014090_filename (out_fname, sizeof(out_fname), group);
	g_snprintf (out_fname_tmp, sizeof(out_fname_tmp), "%s.tmp", out_fname);
	dir = g_path_get_dirname (out_fname);
	pan_file_ensure_path_exists (dir);
	g_free (dir);

	/* open temporary output file */
	out_fp = pan_fopen (out_fname_tmp, "w+");
	if (out_fp == NULL)
	{
		log_add_va (LOG_ERROR, _("The group will not be saved -- can't create file \"%s\""), out_fname_tmp);
		data->success = FALSE;
		return;
	}

	/* write file format and number of headers */
	fprintf (out_fp, "12\n%ld\n", (long)article_qty);

	/* write the header information */
	buf = g_string_sized_new (1024);
	for (i=0; i!=article_qty; ++i)
	{
		const Article * a = articles[i];

		pan_warn_if_fail (a->number != 0);

		g_string_truncate (buf, 0);

		if (a->message_id.len)
			g_string_append_len (buf, a->message_id.str, a->message_id.len);
		g_string_append_c (buf, '\n');

		if (a->author_addr.len)
			g_string_append_len (buf, a->author_addr.str, a->author_addr.len);
		g_string_append_c (buf, '\n');

		if (a->author_real.len)
			g_string_append_len (buf, a->author_real.str, a->author_real.len);
		g_string_append_c (buf, '\n');

		if (a->subject.len)
			g_string_append_len (buf, a->subject.str, a->subject.len);
		g_string_append_c (buf, '\n');

		if (a->references.len)
			g_string_append_len (buf, a->references.str, a->references.len);
		g_string_append_c (buf, '\n');

		message_sources_to_xref (&a->xref, buf, group->server->name_quark, FALSE);
		g_string_append_c (buf, '\n');

		g_string_append_printf (buf,
			"%d\n" /* parts */
			"%u\n" /* line_qty */
			"%lu\n" /* byte qty */
			"%lu\n" /* date */
			"%lu\n" /* number */
			"%d\n", /* is_new */
			(int)a->parts,
			(unsigned int)a->line_qty,
			(unsigned long)a->byte_qty,
			(unsigned long)a->date,
			(unsigned long)a->number,
			(int)(a->is_new != 0));

		g_string_append_printf (buf,
			"%d\n", /* attachment parts */
			(int) g_slist_length (a->attachments));

		article_foreach_attachment_part (a, serialize_attachment_part, buf);

		fwrite (buf->str, 1, buf->len, out_fp);
	}

	/* did the files write out okay? */
	ok = !ferror(out_fp);

	/* close the temp files */
	fclose (out_fp);

	if (!ok)
	{
		log_add_va (LOG_ERROR, _("Unable to save headers for group \"%*.*s\" - is the disk full?"),
		                        group->name.len, group->name.len, group->name.str);
		pan_unlink (out_fname_tmp);
	}

	if (ok)
	{
		ok = pan_file_rename (out_fname_tmp, out_fname);
	}

	if (ok)
	{
		purge_0140_style_files (group);
	}

	/* cleanup */
	debug_exit ("file_headers_save_group");
	data->success = ok;
	g_string_free (buf, TRUE);
}

static gboolean
file_headers_save_group (Group * group, StatusItem * status)
{
	FileHeadersSaveData data;
	data.status = status;
	data.success = FALSE;
	group_article_forall (group, file_headers_save_group_articles, &data);
	return data.success;
}

static void
file_headers_save_articles (Group * group, Article ** articles, guint article_qty, gpointer user_data)
{
	StatusItem * status = (StatusItem*) user_data;

	if (!article_qty)
	{
		file_headers_destroy (group);
	}
	else if (!group_is_folder (group)) /* save the group */
	{
		guint i;
		GTimeVal start;
		GTimeVal finish;
		double diff;
		gulong parts;

		parts = 0ul;
		for (i=0u; i!=article_qty; ++i)
			parts += g_slist_length (articles[i]->attachments);

		/* start the stopwatch */
		g_get_current_time (&start);

		/* save the group */
		if (!file_headers_save_group (group, status))
			group->articles_dirty = TRUE;
		else {
			/* end the stopwatch */
			g_get_current_time (&finish);
			diff = finish.tv_sec - start.tv_sec;
			diff += (finish.tv_usec - start.tv_usec)/(double)G_USEC_PER_SEC;
			log_add_va (LOG_INFO, _("Saved %lu parts, %d articles in \"%s\" in %.1f seconds (%.0f art/sec)"),
				parts,
				article_qty,
				group_get_name (group),
				diff,
				article_qty/(fabs(diff)<0.001?0.001:diff));
		}
	}
}
void
file_headers_save_noref (Group * group, StatusItem * status)
{
	debug_enter ("file_headers_save_noref");

	if (group->articles_dirty)
	{
		group->articles_dirty = FALSE;
		group_article_forall (group, file_headers_save_articles, status);
	}

	debug_exit ("file_headers_save_noref");
}
void
file_headers_save (Group * group, StatusItem * status)
{
	debug_enter ("file_headers_save");
	g_return_if_fail (group!=NULL);

	group_ref_articles (group, status);
	file_headers_save_noref (group, status);
	group_unref_articles (group, status);

	debug_exit ("file_headers_save");
}

void
file_headers_destroy (const Group * group)
{
	debug_enter ("file_headers_destroy");

	g_return_if_fail (group_is_valid (group));

	if (!group_is_folder(group))
	{
		purge_0140_style_files (group);
		purge_014090_style_files (group);
	}

	debug_exit ("file_headers_destroy");
}

/***
****
***/

void
file_headers_server_name_changed  (const PString * old_name,
                                   const PString * new_name)
{
	char * old_path;

	/* sanity clause */
	g_return_if_fail (pstring_is_set (old_name));
	g_return_if_fail (pstring_is_set (new_name));

	/* rename the server's datafile directory */
	old_path = g_build_filename (get_data_dir(), old_name->str, NULL);
	if (g_file_test (old_path, G_FILE_TEST_IS_DIR))
	{
		char * new_path = g_build_filename (get_data_dir(), new_name->str, NULL);
		pan_file_rename (old_path, new_path);
		g_free (new_path);
	}

	/* cleanup */
	g_free (old_path);
}
