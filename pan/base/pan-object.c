/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <glib/gmem.h>
#include <glib/gtypes.h>
#include <glib/gthread.h>
#include <glib/gmessages.h>

#include <pan/base/debug.h>
#include <pan/base/pan-object.h>

static glong obj_count = 0;

void
pan_object_destroy (PanObject* o)
{
	debug1 (DEBUG_PAN_OBJECT, "pan_object_destroy: %p", o);
	g_return_if_fail (o!=NULL);

	if (o->ref_count)
		g_warning ("Destroying a PanObject with a refcount of %d", o->ref_count);

	(*o->destructor)(o);
	g_free (o);
}


void
pan_object_ref (PanObject* o)
{
	g_return_if_fail (o!=NULL);

	++o->ref_count;
	debug2 (DEBUG_PAN_OBJECT, "pan_object_ref: %p up to %d", o, o->ref_count);
}
void
pan_object_unref (PanObject* o)
{
	g_return_if_fail (o!=NULL);

	--o->ref_count;
	debug2 (DEBUG_PAN_OBJECT, "pan_object_unref: %p down to %d", o, o->ref_count);

	g_assert (o->ref_count>=0);
	if (o->ref_count<1)
		pan_object_destroy (o);
}

/**
***
**/

void
pan_object_constructor (PanObject* o, PanObjectDestructor destructor)
{
	g_return_if_fail (o!=NULL);

	++obj_count;
	debug2 (DEBUG_PAN_OBJECT, "pan_object_constructor: %p (%ld PanObjects alive)", o, obj_count);

	o->ref_count = (gint16)1;
	o->destructor = destructor;
}
void
pan_object_destructor (PanObject *o)
{
	--obj_count;
	debug2 (DEBUG_PAN_OBJECT, "pan_object_destructor: %p (%ld PanObjects alive)", o, obj_count);

	g_return_if_fail (o!=NULL);
}

gboolean
pan_object_is_valid (const PanObject * o)
{
	g_return_val_if_fail (o!=NULL, FALSE);
	g_return_val_if_fail (o->ref_count>0, FALSE);

	return TRUE;
}
