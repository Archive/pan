/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __MESSAGE_SOURCE_H__
#define __MESSAGE_SOURCE_H__

#include <glib.h>

/**
 * Tuple of [server,group,number] describing an article's location.
 *
 * @see MessageSources
 */
typedef struct
{
	GQuark server_quark;
	GQuark group_quark;
	unsigned long number;
}
MessageSource;

/**
 * A set of identifiers for an article.
 *
 * By using this as a cross-reference to all the occurences of an article,
 * we can mark every instance of a crosspost as read when it's read in one group,
 * or we can look across different servers to find the article if one server is
 * unreachable or has expired the article.
 *
 * There can only be one entry per [server + group] in a set, and
 * adding a second entry with the same [server + group] will result in failure.
 *
 * Since this is such a lightweight object, there is no message_sources_new() or _free().
 * Rather you can avoid an extra heap allocation by declaring a MessageSources struct
 * on the stack, or embedded inside another struct, and initializing it properly:
 * "MessageSource sources = MESSAGE_SOURCES_INIT;"
 *
 * @see MessageSource
 */
typedef struct
{
	MessageSource * sources;
	int source_qty;
}
MessageSources;

/**
***
**/

extern const MessageSource MESSAGE_SOURCE_INIT;
extern const MessageSource MESSAGE_SOURCE_TOXIC;

extern const MessageSources MESSAGE_SOURCES_INIT;
extern const MessageSources MESSAGE_SOURCES_TOXIC;


void     message_sources_clear          (MessageSources            * sources);

void     message_sources_copy            (MessageSources           * dest,
                                          const MessageSources     * source);

/**
***
**/

void     message_sources_add_from_xref  (MessageSources            * sources,
                                         GQuark                      server_quark,
                                         const char                * xref_str,
                                         int                         xref_len);

void     message_sources_add            (MessageSources            * sources,
                                         GQuark                      server,
                                         GQuark                      group,
                                         gulong                      number);

void     message_sources_remove_server  (MessageSources            * sources,
                                         GQuark                      server);

/**
***
**/

typedef void (*MessageSourcesForeachFunc)(GQuark                     server,
                                          GQuark                     group,
                                          gulong                     number,
                                          gpointer                   user_data);

void     message_sources_foreach         (const MessageSources     * sources,
                                          GQuark                     server_filter,
                                          MessageSourcesForeachFunc  func,
                                          gpointer                   user_data);
/**
***
**/

void     message_sources_to_xref         (const MessageSources     * sources,
                                          GString                  * appendme,
                                          GQuark                     server_filter,
                                          gboolean                   prepend_server_name);

gboolean message_sources_get_source      (const MessageSources     * source,
                                          GQuark                     server_key,
                                          MessageSource            * setme);

gulong   message_sources_get_number      (const MessageSources     * sources,
                                          GQuark                     server,
                                          GQuark                     group);


#endif
