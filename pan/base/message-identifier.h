/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __MESSAGE_IDENTIFIER_H__
#define __MESSAGE_IDENTIFIER_H__

#include <glib/gtypes.h>
#include <glib-object.h>
#include <pan/base/article.h>
#include <pan/base/pstring.h>
#include <pan/base/server.h>
#include <pan/base/message-source.h>

#define PAN_TYPE_MESSAGE_IDENTIFIER             (pan_message_identifier_get_type ())
#define MESSAGE_IDENTIFIER(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), PAN_TYPE_MESSAGE_IDENTIFIER, MessageIdentifier))
#define MESSAGE_IDEENTIFIER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), PAN_TYPE_MESSAGE_IDENTIFIER, MessageIdentifierClass))
#define PAN_IS_MESSAGE_IDENTIFIER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PAN_TYPE_MESSAGE_IDENTIFIER))
#define PAN_IS_MESSAGE_IDENTIFIER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), PAN_TYPE_MESSAGE_IDENTIFIER))
#define PAN_MESSAGE_IDENTIFIER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), PAN_TYPE_MESSAGE_IDENTIFIER, MessageIdentifierClass))

typedef struct _MessageIdentifier MessageIdentifier;
typedef struct _MessageIdentifierClass MessageIdentifierClass;

/**
 * A lighter-weight alternative to Article objects.
 * 
 * Because it has a smaller footprint, it's more appropriate for passing
 * around to code that may need to keep it even after the current Group
 * has been unloaded.  Tasks are a primary example.
 *
 * @see Article
 */
struct _MessageIdentifier
{
	GObject parent_object;

	unsigned long line_qty;
	unsigned long byte_qty;
	PString message_id;
	MessageSources sources;

	char * readable_name;
};

struct _MessageIdentifierClass
{
	GObjectClass parent_class;
};

GType pan_message_identifier_get_type (void);

gboolean             message_identifier_is_valid                   (const MessageIdentifier * mid);

gboolean             message_identifiers_are_valid                 (const MessageIdentifier * const * mids,
                                                                    int                               mid_qty);

MessageIdentifier*   message_identifier_new                        (const char              * message_id);

MessageIdentifier*   message_identifier_new_from_article           (const Article           * article);

MessageIdentifier*   message_identifier_new_from_part              (const AttachmentPart    * part,
                                                                    const char              * name);

void                 message_identifier_add_source_from_article    (MessageIdentifier       * mid,
                                                                    const Article           * article);

const char*          message_identifier_get_readable_name          (const MessageIdentifier * mid);

void                 message_identifier_set_readable_name          (MessageIdentifier       * mid,
                                                                    const char              * name);

/**
***
**/

void                 message_identifiers_mark_read                 (const MessageIdentifier * const * mids,
                                                                    int                               mid_qty,
                                                                    gboolean                          read,
                                                                    ServerGroupsType                  type);

void                 message_identifiers_delete                    (const MessageIdentifier * const * mids,
                                                                    int                               mid_qty,
                                                                    ServerGroupsType                  type);

const PString**      message_identifiers_get_id_array              (const MessageIdentifier * const * mids,
                                                                    int                               mid_qty);

#endif
