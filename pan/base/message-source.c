/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <string.h>
#include <pan/base/debug.h>
#include <pan/base/message-source.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pstring.h>

const MessageSource MESSAGE_SOURCE_INIT = { 0, 0, 0ul };
const MessageSource MESSAGE_SOURCE_TOXIC = { ~0, ~0, ~0ul };

const MessageSources MESSAGE_SOURCES_INIT = { NULL, 0 };
const MessageSources MESSAGE_SOURCES_TOXIC = { (void*)0xDEADBEEF, ~0 };

void
message_sources_clear (MessageSources * sources)
{
	g_return_if_fail (sources != NULL);

	g_free (sources->sources);
	*sources = MESSAGE_SOURCES_INIT;
}

void
message_sources_add (MessageSources   * sources,
                     GQuark             server_quark,
                     GQuark             group_quark,
                     gulong             number)
{
	MessageSource *source, *it, *end;

	/* sanity clause */
	g_return_if_fail (server_quark!=0); /* bad server */
	g_return_if_fail (group_quark!=0); /* bad group */
	g_return_if_fail (number!=0ul); /* bad number */
	for (it=sources->sources, end=it+sources->source_qty; it!=end; ++it)
		g_return_if_fail (it->server_quark!=server_quark || it->group_quark!=group_quark); /* duplicate */

	/* add it */
	sources->sources = g_renew (MessageSource, sources->sources, ++sources->source_qty);
	source = &sources->sources[sources->source_qty - 1];
	source->server_quark = server_quark;
	source->group_quark = group_quark;
	source->number = number;
}

void
message_sources_remove_server (MessageSources   * sources,
                               GQuark             server_quark)
{
	int remain_qty;
	MessageSource * it, * end;

	g_return_if_fail (sources!=NULL);
	g_return_if_fail (server_quark!=0);

	/* how many sources will be left? */
	remain_qty = 0;
	for (it=sources->sources, end=it+sources->source_qty; it!=end; ++it)
		if (it->server_quark!=server_quark)
			++remain_qty;

//odebug2 ("remove_server: going from %d sources to %d", sources->source_qty, remain_qty);

	/* remove those sources */
	if (!remain_qty)
		message_sources_clear (sources);
	else {
		MessageSource * buf=g_new (MessageSource, remain_qty), *buf_it=buf;
		for (it=sources->sources, end=it+sources->source_qty; it!=end; ++it)
			if (it->server_quark!=server_quark)
				*buf_it++ = *it;
		g_free (sources->sources);
		sources->sources = buf;
		sources->source_qty = remain_qty;
	}
}

void
message_sources_foreach (const MessageSources       * sources,
                         GQuark                       server_filter,
                         MessageSourcesForeachFunc    foreach_func,
                         gpointer                     foreach_user_data)
{
	int i;

	g_return_if_fail (sources != NULL);

	for (i=0; i<sources->source_qty; ++i) {
		const MessageSource * s = &sources->sources[i];
		if (server_filter==0 || server_filter==s->server_quark)
			(foreach_func)(s->server_quark, s->group_quark, s->number, foreach_user_data);
	}
}

void
message_sources_add_from_xref (MessageSources   * sources,
                               GQuark             server_quark,
                               const char       * xref_str,
                               int                xref_len)
{
	PString xref = PSTRING_INIT;

	g_return_if_fail (sources != NULL);
	g_return_if_fail (server_quark != 0);

	if (is_nonempty_string (xref_str)) {
		/* strip ends and erase leading "Xref: " if present */
		xref = pstring_shallow (xref_str, xref_len);
		xref = pstring_strstrip_shallow (&xref);
		if (xref.len>6 && !strncmp(xref.str,"Xref: ", 6)) {
			xref = pstring_substr_shallow (&xref, xref.str+6, NULL);
			xref = pstring_strstrip_shallow (&xref);
		}
	}

	if (xref.len)
	{
        	PString run;
		char * tmp = g_strndup (xref.str, xref.len);
		const char * march = tmp;

		/* walk through the xrefs, of format "group1:number group2:number" */
		while (get_next_token_pstring (march, ' ', &march, &run))
		{
			char * delimit = pan_strchr_len (run.str, run.len, ':');
			if (delimit != NULL)
			{
				GQuark group_quark;
				const gulong number = strtoul (delimit+1, NULL, 10);
				*delimit = '\0';
				group_quark = g_quark_from_string (run.str);
				message_sources_add (sources, server_quark, group_quark, number);
			}
        	}

		g_free (tmp);
	}
}

void
message_sources_copy (MessageSources        * to,
                      const MessageSources  * from)
{
	g_return_if_fail (to != NULL);
	g_return_if_fail (from != NULL);

	if (to != from)
	{
		to->source_qty = from->source_qty;
		to->sources = g_renew (MessageSource, to->sources, to->source_qty);
		memcpy (to->sources, from->sources, sizeof(MessageSource)*from->source_qty);
	}
}

void
message_sources_to_xref (const MessageSources  * sources,
                         GString               * appendme,
                         GQuark                  server_filter,
                         gboolean                prepend_server_name)
{
	int i;
	int added_qty;
	int old_len;

	g_return_if_fail (sources != NULL);
	g_return_if_fail (appendme != NULL);
	g_return_if_fail (server_filter != 0);

	old_len = appendme->len;
	for (added_qty=i=0; i<sources->source_qty; ++i)
	{
		const MessageSource * s = &sources->sources[i];
		if (server_filter == s->server_quark)
		{
			const char * group_name = g_quark_to_string (s->group_quark);
			if (added_qty)
				g_string_append_c (appendme, ' ');
			g_string_append_printf (appendme, "%s:%lu", group_name, s->number);
			++added_qty;
		}
	}

	if (prepend_server_name && added_qty)
	{
		const char * server_name = g_quark_to_string (server_filter);
		g_string_insert_c (appendme, old_len, ' ');
		g_string_insert (appendme, old_len, server_name);
	}
}

gboolean
message_sources_get_source (const MessageSources     * sources,
                            GQuark                     server_key,
                            MessageSource            * setme)
{
	gboolean found = FALSE;
	MessageSource * it, * end;

	g_return_val_if_fail (sources!=NULL, FALSE);
	g_return_val_if_fail (server_key!=0, FALSE);
	g_return_val_if_fail (setme!=0, FALSE);

	for (it=sources->sources, end=it+sources->source_qty; !found && it!=end; ++it) {
		if (it->server_quark == server_key) {
			found = TRUE;
			*setme = *it;
		}
	}

	return found;
}

gulong
message_sources_get_number   (const MessageSources  * sources,
                              GQuark                  server,
                              GQuark                  group)
{
	gulong retval = 0ul;
	MessageSource * it, * end;

	g_return_val_if_fail (sources!=NULL, 0ul);
	g_return_val_if_fail (server!=0, 0ul);
	g_return_val_if_fail (group!=0, 0ul);

	for (it=sources->sources, end=it+sources->source_qty; !retval && it!=end; ++it)
		if (it->server_quark==server && it->group_quark==group)
			retval = it->number;

	return retval;
}

