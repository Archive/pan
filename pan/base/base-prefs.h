/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __BASE_IO_H__
#define __BASE_IO_H__

#include <glib/gtypes.h>

extern char      * download_dir;
extern int         acache_max_megs;
extern const char * default_incoming_name_real;
extern const char * default_incoming_name_addr;
extern gboolean    acache_flush_on_exit;
extern gboolean    break_thread_when_subject_changes;

void base_prefs_init (const char * download_dir,
                      int          acache_max_megs,
                      gboolean     acache_flush_on_exit,
		      gboolean     break_thread_when_subject_changes);

const char* get_data_dir (void);

#endif
