/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>

#include <pan/base/debug.h>
#include <pan/base/message-identifier.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/base/serverlist.h>
#include <pan/base/group.h>
#include <pan/base/article.h>

/**
***  GObject stuff
**/

static GObjectClass * parent_class = NULL;

static void
pan_message_identifier_init (MessageIdentifier * mid, MessageIdentifierClass * klass)
{
	mid->line_qty = 0ul;
	mid->byte_qty = 0ul;
	mid->message_id = PSTRING_INIT;
	mid->readable_name = NULL;
	mid->sources = MESSAGE_SOURCES_INIT;
}

static void
pan_message_identifier_finalize (GObject * object)
{
	MessageIdentifier * mid = (MessageIdentifier*) object;

	/* free our fields */
	message_sources_clear (&mid->sources);
	pstring_clear (&mid->message_id);
	g_free (mid->readable_name);

	/* toxify our fields */
	mid->line_qty = ~0;
	mid->byte_qty = ~0;
        mid->message_id = PSTRING_TOXIC;
        mid->sources = MESSAGE_SOURCES_TOXIC;
        mid->readable_name = (void*)0xDEADBEEF;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
pan_message_identifier_class_init (MessageIdentifierClass * klass)
{
	GObjectClass * object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_ref (G_TYPE_OBJECT);

	object_class->finalize = pan_message_identifier_finalize;
}

GType
pan_message_identifier_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		static const GTypeInfo info =
		{
			sizeof (MessageIdentifierClass),
			NULL, /* base_class_init */
			NULL, /* base_class_finalize */
			(GClassInitFunc) pan_message_identifier_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (MessageIdentifier),
			16,   /* n_preallocs */
			(GInstanceInitFunc) pan_message_identifier_init
		};

		type = g_type_register_static (G_TYPE_OBJECT, "MessageIdentifier", &info, 0);
	}

	return type;
}

/**
***  MessageIdentifier Stuff
**/

MessageIdentifier*
message_identifier_new (const char * message_id)
{
	MessageIdentifier * mid;

	mid = g_object_new (PAN_TYPE_MESSAGE_IDENTIFIER, NULL, NULL);
	pstring_set (&mid->message_id, message_id, strlen(message_id));

	return mid;
}

MessageIdentifier*
message_identifier_new_from_article (const Article * article)
{
	MessageIdentifier * mid;

	g_return_val_if_fail (article_is_valid(article), NULL);

	mid = g_object_new (PAN_TYPE_MESSAGE_IDENTIFIER, NULL, NULL);
	pstring_copy (&mid->message_id, &article->message_id);
	message_identifier_set_readable_name (mid, article_get_subject(article));
	message_identifier_add_source_from_article (mid, article);

	return mid;
}

void
message_identifier_add_source_from_article  (MessageIdentifier  * mid,
                                             const Article      * article)
{
	g_return_if_fail (PAN_IS_MESSAGE_IDENTIFIER(mid));
	g_return_if_fail (article_is_valid(article));

	mid->line_qty = article->line_qty;
	mid->byte_qty = article->byte_qty;

	if (article->xref.source_qty > 0)
		message_sources_copy (&mid->sources, &article->xref);
	else
		message_sources_add (&mid->sources,
		                     article->group->server->name_quark,
		                     article->group->name_quark,
		                     article->number);
}

MessageIdentifier*
message_identifier_new_from_part (const AttachmentPart    * part,
                                  const char              * name)
{
	MessageIdentifier * mid;

	g_return_val_if_fail (part!=NULL, NULL);

	mid = g_object_new (PAN_TYPE_MESSAGE_IDENTIFIER, NULL, NULL);
	pstring_copy (&mid->message_id, &part->message_id);
	mid->line_qty = part->line_qty;
	mid->byte_qty = part->byte_qty;
	message_sources_copy (&mid->sources, &part->sources);
	message_identifier_set_readable_name (mid, name);

	return mid;
}

gboolean
message_identifier_is_valid (const MessageIdentifier * mid)
{
	g_return_val_if_fail (PAN_IS_MESSAGE_IDENTIFIER(mid), FALSE);
	g_return_val_if_fail (pstring_is_set (&mid->message_id), FALSE);

	return TRUE;
}

gboolean
message_identifiers_are_valid (const MessageIdentifier * const * mids, int qty)
{
	int i;
	g_return_val_if_fail (qty >= 1, FALSE);
	g_return_val_if_fail (mids != NULL, FALSE);
	for (i=0; i<qty; ++i)
		g_return_val_if_fail (message_identifier_is_valid(mids[i]), FALSE);
	return TRUE;
}

/***
****
***/

static void
group2art_delete_ghfunc (gpointer key, gpointer val, gpointer user_data)
{
	Group * group = GROUP (key);
	GHashTable * article_hash = (GHashTable*) val;
	GPtrArray * article_array;

       	article_array = g_ptr_array_new ();
	pan_hash_to_ptr_array (article_hash, article_array);
	group_remove_articles (group, (Article**)article_array->pdata, article_array->len);
	g_ptr_array_free (article_array, TRUE);

	g_hash_table_destroy (article_hash);
	group_unref_articles (group, NULL);
}

void
message_identifiers_delete (const MessageIdentifier * const * mids,
                            int                        mid_qty,
			    ServerGroupsType           type)
{
	int i;
	GHashTable * group2art;
	debug_enter ("message_identifiers_delete");

	/* sanity clause */
	g_return_if_fail (mids!=NULL);
	g_return_if_fail (mid_qty>0);
	g_return_if_fail (PAN_IS_MESSAGE_IDENTIFIER(mids[0]));

	group2art = g_hash_table_new (g_direct_hash, g_direct_equal);

	/* walk through all the mids */
	for (i=0; i<mid_qty; ++i)
	{
		int j;
		const MessageIdentifier * mid = mids[i];
		for (j=0; j<mid->sources.source_qty; ++j)
		{
			MessageSource * source = &mid->sources.sources[j];
			Server * server;
			Group * group;

			/* find the source's server */
			server = serverlist_get_server (source->server_quark);
			if (server == NULL)
				continue;

			/* find the source's group */
			group = server_get_group_in_type (server, source->group_quark, type);
			if (group == NULL)
				continue;

			/* if the group doesn't have the articles loaded, then nobody
			 * is listening to the article changing state, so we can poke
			 * the newsrc directly and not need to fire an event.
			 * Otherwise, the articles are loaded, so we need to
			 * use the Article objects. */
			if (!group_ref_articles_if_loaded (group))
				group_mark_article_purged (group, source->number);
			else {
				Article * a = group_get_article_by_message_id (group, &mid->message_id);
				if (a != NULL) {
					/* The Article container we use is a Hash, to weed out duplicates. */
					GHashTable * art_hash = (GHashTable*) g_hash_table_lookup (group2art, group);
					if (art_hash == NULL) {
						art_hash = g_hash_table_new (g_direct_hash, g_direct_equal);
						g_hash_table_insert (group2art, group, art_hash);
						group_ref_articles (group, NULL);
					}

					g_hash_table_insert (art_hash, a, a);
				}
				group_unref_articles (group, NULL);
			}
		}
	}

	g_hash_table_foreach (group2art, group2art_delete_ghfunc, GINT_TO_POINTER(read));
	g_hash_table_destroy (group2art);

	debug_exit ("message_identifiers_delete");
}

/***
****
***/

static void
group2art_mark_read_ghfunc (gpointer key, gpointer val, gpointer user_data)
{
	Group * group = GROUP (key);
	GHashTable * article_hash = (GHashTable*) val;
	GPtrArray * article_array;
	const gboolean read = user_data != NULL;

       	article_array = g_ptr_array_new ();
	pan_hash_to_ptr_array (article_hash, article_array);
	articles_set_read_simple ((Article**)article_array->pdata, article_array->len, read);
	g_ptr_array_free (article_array, TRUE);

	g_hash_table_destroy (article_hash);
	group_unref_articles (group, NULL);
}

void
message_identifiers_mark_read (const MessageIdentifier * const * mids,
                               int                        mid_qty,
                               gboolean                   read,
			       ServerGroupsType           type)
{
	int i;
	GHashTable * group2art;
	debug_enter ("message_identifiers_mark_read");

	/* sanity clause */
	g_return_if_fail (mids!=NULL);
	g_return_if_fail (mid_qty>0);
	g_return_if_fail (PAN_IS_MESSAGE_IDENTIFIER(mids[0]));

	group2art = g_hash_table_new (g_direct_hash, g_direct_equal);

	/* walk through all the mids */
	for (i=0; i<mid_qty; ++i)
	{
		int j;
		const MessageIdentifier * mid = mids[i];
		for (j=0; j<mid->sources.source_qty; ++j)
		{
			const MessageSource * source = &mid->sources.sources[j];
			Server * server;
			Group * group;

			/* find the source's server */
			server = serverlist_get_server (source->server_quark);
			if (server == NULL)
				continue;

			/* find the source's group */
			group = server_get_group_in_type (server, source->group_quark, type);
			if (group == NULL)
				continue;

			/* if the group doesn't have the articles loaded, then nobody
			 * is listening to the article changing state, so we can poke
			 * the newsrc directly and not need to fire an event.
			 * Otherwise, the articles are loaded, so we need to
			 * use the Article objects. */
			if (!group_ref_articles_if_loaded (group))
				group_mark_article_read (group, source->number, read);
			else {
				Article * a = group_get_article_by_message_id (group, &mid->message_id);
				if (a != NULL) {
					/* The Article container we use is a Hash, to weed out duplicates. */
					GHashTable * art_hash = (GHashTable*) g_hash_table_lookup (group2art, group);
					if (art_hash == NULL) {
						art_hash = g_hash_table_new (g_direct_hash, g_direct_equal);
						g_hash_table_insert (group2art, group, art_hash);
						group_ref_articles (group, NULL);
					}

					g_hash_table_insert (art_hash, a, a);
				}
				group_unref_articles (group, NULL);
			}
		}
	}

	g_hash_table_foreach (group2art, group2art_mark_read_ghfunc, GINT_TO_POINTER(read));
	g_hash_table_destroy (group2art);

	debug_exit ("message_identifiers_mark_read");
}

const char*
message_identifier_get_readable_name (const MessageIdentifier * mid)
{
	g_return_val_if_fail (PAN_IS_MESSAGE_IDENTIFIER(mid), "");

	return mid->readable_name;
}

void
message_identifier_set_readable_name (MessageIdentifier  * mid,
                                      const char         * name)
{
	g_return_if_fail (PAN_IS_MESSAGE_IDENTIFIER(mid));

	replace_gstr (&mid->readable_name, g_strdup(name));
}

const PString**
message_identifiers_get_id_array (const MessageIdentifier * const * mids,
                                  int                               mid_qty)
{
	int i;
	const PString ** retval = NULL;

	g_return_val_if_fail (message_identifiers_are_valid (mids, mid_qty), NULL);

	retval = g_new0 (const PString*, mid_qty);
	for (i=0; i<mid_qty; ++i)
		retval[i] = &mids[i]->message_id;

	return retval;
}
