/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2003  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <ctype.h>
#include <string.h>

#include <pan/base/debug.h>
#include <pan/base/pstring.h>
#include <pan/base/pan-glib-extensions.h>

const PString PSTRING_INIT = { NULL, 0, 0u };

const PString PSTRING_TOXIC = { (void*)0xDEADBEEF, ~0, ~0u };


/**
***  Manage the str field
**/

void
pstring_copy (PString  * dest, const PString * src)
{
	char * pch;

	/* sanity clause */
	g_return_if_fail (pstring_is_valid (dest));
	g_return_if_fail (pstring_is_valid (src));

	dest->hash = src->hash;
	dest->len = src->len;
	pch = g_new (char, src->len + 1);
	memcpy (pch, src->str, src->len);
	pch[src->len] = '\0';
	replace_gstr (&dest->str, pch);
}

PString*
pstring_dup (const PString * src)
{
	PString * retval;

	g_return_val_if_fail (pstring_is_valid(src), NULL);
	
	retval = g_new (PString, 1);
	*retval = PSTRING_INIT;
	pstring_copy (retval, src);
	return retval;
}

void
pstring_clear (PString * pstring)
{
	/* sanity clause */
	g_return_if_fail (pstring!=NULL);

	if (pstring->str != NULL)
		g_free (pstring->str);

	*pstring = PSTRING_INIT;
}

void
pstring_free (PString  * pstring)
{
	/* sanity clause */
	g_return_if_fail (pstring != NULL);

	pstring_clear (pstring);
	g_free (pstring);
}


void
pstring_set (PString * pstring, const char * str, int len)
{
	char * old_string;

	/* sanity clause */
	g_return_if_fail (pstring!=NULL);

	old_string = pstring->str;
	*pstring = PSTRING_INIT;

	if (str != NULL)
	{
		if (len < 0)
			len = strlen (str);
		pstring->len = len;
		pstring->str = g_new (char, len+1);
		memcpy (pstring->str, str, len);
		pstring->str[len] = '\0';
	}

	pstring->hash = 0u;

	if (old_string != NULL)
		g_free (old_string);
}

char*
pstring_strchr  (const PString * pstring, char needle)
{
	g_return_val_if_fail (pstring_is_set (pstring), NULL);

	return pan_strchr_len (pstring->str, pstring->len, needle);
}

char*
pstring_strstr (const PString * haystack, const PString * needle)
{
	g_return_val_if_fail (pstring_is_valid (haystack), NULL);
	g_return_val_if_fail (pstring_is_valid (needle), NULL);

	return pan_strnstr_len (haystack->str,
	                        haystack->len,
	                        needle->str,
	                        needle->len); 
}

void
pstring_strstrip (PString * pstring)
{
	g_return_if_fail (pstring != NULL);

	if (pstring->len > 0)
	{
		int len = pstring->len;
		const char * str = pstring->str;

		for (; len>0 && isspace((guchar)str[len-1]); --len)
			;
		for (; len>0 && isspace((guchar)str[0]); --len, ++str)
			;

		pstring_set (pstring, str, len);
	}
}

/**
***  Shallow Copies
**/

PString
pstring_shallow (const char * str, int len)
{
	PString pstring = PSTRING_INIT;

	if (str != NULL)
	{
		pstring.len = len >= 0 ? len : strlen (str);
		pstring.str = (char*) str;
	}

	return pstring;
}

PString
pstring_strstrip_shallow (const PString * pstring)
{
	PString strip;

	if (!pstring_is_set (pstring))
		strip = PSTRING_INIT;
	else
	{
		int len = pstring->len;
		const char * str = pstring->str;

		for (; len>0 && isspace((guchar)str[len-1]); --len)
			;
		for (; len>0 && isspace((guchar)str[0]); --len, ++str)
			;

		strip = pstring_shallow (str, len);
	}

	return strip;
}

PString
pstring_substr_shallow   (const PString   * pstring,
                          const char      * start,
                          const char      * end)
{
	g_return_val_if_fail (pstring_is_valid (pstring), PSTRING_INIT);

	if (start == NULL)
		start = pstring->str;
	if (end == NULL)
		end = pstring->str + pstring->len;

	return pstring_shallow (start, end-start);
}

gboolean
pstring_next_token_shallow  (const PString   * march_in,
                             char              delimiter,
                             PString         * march_out,
                             PString         * shallow_token)
{
	gboolean retval = FALSE;

	/* sanity checks */
	g_return_val_if_fail (pstring_is_valid (march_in), FALSE);
	g_return_val_if_fail (march_out != NULL, FALSE);
	g_return_val_if_fail (shallow_token != NULL, FALSE);

	if (march_in->len >= 1)
	{
		char * pch = pstring_strchr (march_in, delimiter);

		/* shallow_token needs to hold everything from the
		   start of the string up until the delimiter */
		*shallow_token = pstring_substr_shallow (march_in, NULL, pch);

		/* march_out needs to hold everything to the right of shallow_token */
		*march_out = (pch==NULL) || ((pch-march_in->str) == march_in->len)
			? PSTRING_INIT
			: pstring_substr_shallow (march_in, pch+1, NULL);

		retval = TRUE;
	}

	return retval;
}


/**
***  Sanity Checks
**/

gboolean
pstring_is_valid (const PString * pstring)
{
	return pstring!=NULL
		&& (pstring->len==0 || pstring->str!=NULL)
		&& pstring->len>=0;
}

gboolean
pstring_is_set (const PString * pstring)
{
	return pstring_is_valid(pstring)
		&& pstring->len>0
		&& is_nonempty_string (pstring->str);
}

/**
***  Hashtable comparison functions
**/

guint
pstring_hash (gconstpointer key)
{
	const PString * str = (const PString*) key;

	if (str->hash == 0u)
	{
		register const char * p = str->str;
		register const char * end = p + str->len;
		register guint h;

		for (h=0u; p!=end; ++p)
			h = (h << 5) - h + *p;

		((PString*)str)->hash = h;
	}

	return str->hash;
}

int
pstring_compare (const void * a, const void * b)
{
	const PString * stra = (const PString *) a;
	const PString * strb = (const PString *) b;

	return pan_strcmp_len (stra->str, stra->len, strb->str, strb->len);
}

gboolean
pstring_equal  (gconstpointer  pstring_1_gconstpointer,
                gconstpointer  pstring_2_gconstpointer)
{
	register const PString * pstring_1 = (const PString *) pstring_1_gconstpointer;
	register const PString * pstring_2 = (const PString *) pstring_2_gconstpointer;

	return (pstring_1->len == pstring_2->len) /* lengths must match */
		&& (pstring_1->hash==0u || pstring_2->hash==0u || pstring_1->hash==pstring_2->hash) /* if both hashes set, they must match */
		&& !memcmp (pstring_1->str, pstring_2->str, pstring_1->len); /* do the actual byte-by-byte comparison */
}
