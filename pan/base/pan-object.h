/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PAN_OBJECT_H
#define PAN_OBJECT_H

#define PAN_OBJECT(a) ((PanObject*)a)

#include <glib/gtypes.h>

typedef struct _PanObject PanObject;

typedef void (*PanObjectDestructor)(PanObject*);

/**
 * PanObject is essentially a GtkObject with most of the functionality
 * stripped out so that we are left with the bare minimum needed for a
 * class hierarchy in C.  We have inheritance and virtual functions;
 * however, we do not have RTTI or static class functions.
 *
 * GtkObject is superior to PanObject in almost every way, except for thread
 * safety.  We originally used GtkObject, only to find that it has too many
 * points of failure for use in a multithreaded application.  PanObject,
 * lacking most of GtkObject's functionality, also lacks its points of failure.
 * A secondary advantage of PanObject is its low overhead.
 *
 * PanObjects use PanCallbacks, rather than Gtk signals, for the observer/
 * observable pattern.  This also is for relative thread safety.
 *
 * 2000/07/14: The glib object system currently in development looks good.
 * perhaps when glib 2.0 comes out PanObject can be replaced with this.
 */
struct _PanObject
{
	/* virtual functions */
	PanObjectDestructor destructor;

	/* fields */
	gint16 ref_count;
	gpointer user_data;
};

/**
***  PROTECTED
**/

void      pan_object_constructor      (PanObject*,
                                       PanObjectDestructor);

void      pan_object_destructor       (PanObject*);


/**
***  PUBLIC
**/

gboolean  pan_object_is_valid         (const PanObject*);

void      pan_object_destroy          (PanObject*);

void      pan_object_ref              (PanObject*);

void      pan_object_unref            (PanObject*);

#endif
