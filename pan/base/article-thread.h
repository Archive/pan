/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __ARTICLE_THREAD_H__
#define __ARTICLE_THREAD_H__

#include <glib/garray.h>
#include <glib/gtypes.h>

#include <pan/base/article.h>

typedef enum
{
	ARTICLE_SORT_SUBJECT        = 1,
	ARTICLE_SORT_DATE           = 2,
	ARTICLE_SORT_AUTHOR         = 3,
	ARTICLE_SORT_LINES          = 4,
	ARTICLE_SORT_MSG_ID         = 5,
	ARTICLE_SORT_READ_STATE     = 6,
	ARTICLE_SORT_ACTION_STATE   = 7,
	ARTICLE_SORT_SCORE          = 8,
	ARTICLE_SORT_TYPES_QTY      = 8
}
ArticleSortType;

void sort_articles   (Article      ** buf,
                      size_t          article_qty,
                      int             sort_type,
                      gboolean        ascending);

void thread_articles (Article      ** articles,
                      guint           article_qty);


typedef enum
{
        STRIP_CASE                    = (1<<0),
        STRIP_MULTIPART_NUMERATOR     = (1<<1),
        STRIP_MULTIPART               = (1<<2)
}
StripFlags;

size_t normalize_subject (char            * buf,
                          const PString   * subject,
                          StripFlags        strip);


#endif /* __ARTICLE_THREAD_H__ */
