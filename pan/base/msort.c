
/*
 *  This is included in Pan to provide a stable sorting mechanism.  This is
 *  used, for example, in the articlelist, where sorting by one column, and
 *  then a second, provides an quick & easy way to get a two-key sort.
 *  This isn't possible without a stable sorter.
 *
 *  Loosely adapted from glibc's libc/stdlib/msort.c revision 1.21.
 *  op_t and OPSIZ comment & macro from memcmp.c revision 1.3
 *
 */

/* An alternative to qsort, with an identical interface.
   This file is part of the GNU C Library.
   Copyright (C) 1992,95-97,99,2000,01,02,04 Free Software Foundation, Inc.
   Written by Mike Haertel, September 1988.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pan/base/pan-glib-extensions.h>

/* Type to use for aligned memory operations.
   This should normally be the biggest type supported by a single load
   and store.  Must be an unsigned type.  */
#define	op_t	unsigned long int
#define OPSIZ	(sizeof(op_t))

static void
msort_with_tmp (
	void *b,
	size_t n,
	size_t s,
	int(*cmp)(const void*,const void*),
	char* t)
{
	char *tmp;
	char *b1, *b2;
	size_t n1, n2;

	if (n <= 1)
		return;

	n1 = n / 2;
	n2 = n - n1;
	b1 = b;
	b2 = (char *) b + (n1 * s);

	msort_with_tmp (b1, n1, s, cmp, t);
	msort_with_tmp (b2, n2, s, cmp, t);

	tmp = t;

	if (s == OPSIZ && (b1 - (char *) 0) % OPSIZ == 0)
		/* operating on aligned words.  Use direct word stores. */
		while (n1 > 0 && n2 > 0)
		{
			if ((*cmp) (b1, b2) <= 0)
			{
				--n1;
				*((op_t *) tmp) = *((op_t *) b1);
				tmp += sizeof (op_t);
				b1 += sizeof (op_t);
			}
			else
			{
				--n2;
				*((op_t *) tmp) = *((op_t *) b2);
				tmp += sizeof (op_t);
				b2 += sizeof (op_t);
			}
		}
	else
		while (n1 > 0 && n2 > 0)
		{
			if ((*cmp) (b1, b2) <= 0)
			{
				memcpy (tmp, b1, s);
				tmp += s;
				b1 += s;
				--n1;
			}
			else
			{
				memcpy (tmp, b2, s);
				tmp += s;
				b2 += s;
				--n2;
			}
		}
	if (n1 > 0)
		memcpy (tmp, b1, n1 * s);
	memcpy (b, t, (n - n2) * s);
}

void
msort (void *b, size_t n, size_t s, int(*cmp)(const void*, const void*))
{
	const size_t size = n * s;

	if (size < 1024)
	{
		void *buf = g_alloca (size);

		/* The temporary array is small, so put it on the stack.  */
		msort_with_tmp (b, n, s, cmp, buf);
	}
	else
	{
		int save = errno;
		char *tmp = malloc (size);
		if (tmp == NULL)
		{
			/* Couldn't get space, so use the slower algorithm
			that doesn't need a temporary array.  */
			qsort (b, n, s, cmp);
		}
		else
		{
			msort_with_tmp (b, n, s, cmp, tmp);
			free (tmp);
		}
		errno = save;
	}
}
