/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PAN_CONFIG_H
#define PAN_CONFIG_H

void       pan_config_clean_key (const char * key);

char*      pan_config_get_string (const char * key, const char * default_value);

void       pan_config_set_string (const char * key, const char * value);

void       pan_config_set_string_if_different (const char * key, const char * value, const char * default_value);

gboolean   pan_config_get_bool (const char * key, gboolean default_value);

void       pan_config_set_bool (const char * key, gboolean value);

void       pan_config_set_bool_if_different (const char * key, gboolean value, gboolean default_value);

int        pan_config_get_int (const char * key, int default_value);

void       pan_config_set_int (const char * key, int value);

void       pan_config_set_int_if_different (const char * key, int value, int default_value);

gulong     pan_config_get_ulong (const char * key, gulong default_value);

void       pan_config_set_ulong (const char * key, gulong value);

void       pan_config_set_ulong_if_different (const char * key, gulong value, gulong default_value);

void       pan_config_push_prefix (const char * prefix);

void       pan_config_pop_prefix (void);

gboolean   pan_config_has_section (const char * section);

void       pan_config_clean_section (const char * section);

void       pan_config_sync (void);


#endif
