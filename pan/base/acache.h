/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __ACACHE_H__
#define __ACACHE_H__

#include <stdio.h>
#include <glib/gtypes.h>
#include <gmime/gmime-message.h>
#include <pan/base/pan-callback.h>
#include <pan/base/pstring.h>



/**
***  Life-Cycle
**/

void           acache_init                (void);

void           acache_shutdown            (void);

/**
***  Expire
**/

int            acache_expire              (void);

int            acache_expire_all          (void);

void           acache_expire_messages     (const char               * key,
                                           const PString * const    * mids,
                                           int                        mid_qty);
/**
***  Path
**/

void           acache_add_folder          (const char            * key);

typedef void (*AcachePathForeachFunc)     (GMimeMessage          * message,
                                           gpointer                user_data);

void           acache_path_foreach        (const char            * key,
                                           AcachePathForeachFunc   func,
                                           gpointer                user_data);

/**
***  Messages
**/

extern const char * ACACHE_DEFAULT_KEY;

gboolean       acache_has_message         (const char              * path_key,
                                           const PString           * message_id);

GMimeMessage*  acache_get_message         (const char              * path_key,
                                           const PString * const   * mids,
	                                   int                       mid_qty);

void           acache_set_message         (const char              * path_key,
                                           const PString           * mid,
                                           const char              * text,
                                           guint                     message_len);

/**
***  Refcount
**/

void           acache_checkout            (const char              * path_key,
                                           const PString * const   * mids,
                                           int                       message_qty);

void           acache_checkin             (const char              * path_key,
                                           const PString * const   * mids,
                                           int                       message_qty);

/**
***  Callbacks
**/

/**
 * @call_obj: const char ** message_ids
 * @call_arg: int: message_id qty
 */
extern PanCallback* acache_get_bodies_added_callback (void);

/**
 * @call_obj: const char ** message_ids
 * @call_arg: int: message_id qty
 */
extern PanCallback* acache_get_bodies_removed_callback (void);


#endif /* __ACACHE_H__ */
