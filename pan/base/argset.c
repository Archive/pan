/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <glib/gmem.h>
#include <glib/gmessages.h>
#include <glib/gerror.h>
#include <glib/gthread.h>

#include <pan/base/argset.h>
#include <pan/base/log.h>
#include <pan/base/pan-i18n.h>

ArgSet*
argset_new (void) {
	ArgSet * argset = g_new0 (ArgSet, 1);
	argset->args = g_ptr_array_new ();
	return argset;
}
ArgSet* argset_new1 (gpointer p1)
{
	ArgSet * argset = argset_new ();
	g_ptr_array_add (argset->args, p1);
	return argset;
}
ArgSet*
argset_new2 (gpointer p1, gpointer p2) {
	ArgSet * argset = argset_new1 (p1);
	g_ptr_array_add (argset->args, p2);
	return argset;
}
ArgSet*
argset_new3 (gpointer p1, gpointer p2, gpointer p3) {
	ArgSet * argset = argset_new2 (p1, p2);
	g_ptr_array_add (argset->args, p3);
	return argset;
}
ArgSet*
argset_new4 (gpointer p1, gpointer p2, gpointer p3, gpointer p4) {
	ArgSet * argset = argset_new3 (p1, p2, p3);
	g_ptr_array_add (argset->args, p4);
	return argset;
}
ArgSet*
argset_new5 (gpointer p1, gpointer p2, gpointer p3, gpointer p4, gpointer p5) {
	ArgSet * argset = argset_new4 (p1, p2, p3, p4);
	g_ptr_array_add (argset->args, p5);
	return argset;
}
ArgSet*
argset_new6 (gpointer p1, gpointer p2, gpointer p3, gpointer p4, gpointer p5, gpointer p6) {
	ArgSet * argset = argset_new5 (p1, p2, p3, p4, p5);
	g_ptr_array_add (argset->args, p6);
	return argset;
}


gpointer
argset_get (ArgSet* argset, gint index) {
	g_return_val_if_fail (argset!=NULL, NULL);
	g_return_val_if_fail (argset->args!=NULL, NULL);
	g_return_val_if_fail (index>=0, NULL);
	g_return_val_if_fail (index<argset->args->len, NULL);
	return g_ptr_array_index (argset->args, index);
}

void
argset_add (ArgSet* argset, gpointer add) {
	g_return_if_fail (argset!=NULL);
	g_return_if_fail (argset->args!=NULL);
	g_ptr_array_add (argset->args, add);
}

void
argset_free (ArgSet * argset) {
	g_return_if_fail (argset!=NULL);
	g_return_if_fail (argset->args!=NULL);
	g_ptr_array_free (argset->args, TRUE);
	g_free (argset);
}
