/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __PAN_STATUS_ITEM__H__
#define __PAN_STATUS_ITEM__H__

#include <glib/gslist.h>
#include <pan/base/pan-callback.h>
#include <pan/base/pan-object.h>

/**
***  STATUS ITEM CLASS DEFINITION
**/

#define STATUS_ITEM(a) ((StatusItem *)a)

typedef struct _StatusItem StatusItem;
typedef void (*StatusItemDescribeFunc)(const StatusItem*, char * buf, int buflen);

struct _StatusItem
{
	/*Parent*/
	PanObject     parent;         /* parent object */

	/*Callbacks*/
	PanCallback * progress;       /* obj arg is int [0...100] */
	PanCallback * status;         /* obj arg is const char* (don't free) */
	PanCallback * error;          /* obj arg is const char* (don't free) */
	PanCallback * done;           /* obj arg is status int */

	/*Fields*/
	char       * description;     /* overrides the describe() function */
	char       * status_text;     /* the last status text emitted, or NULL */
	GSList      * errors;         /* the emitted error strings */
	int          progress_val;    /* value is [0...100] */
	int          steps;           /* number of steps for progress */
	int          step;            /* number of steps so far. [0..steps] */

	/* virtual functions */
	StatusItemDescribeFunc describe;
};

/**
***  PROTECTED
**/

void status_item_constructor    (StatusItem               * item,
                                 PanObjectDestructor        dtor,
                                 StatusItemDescribeFunc     describe);

void status_item_destructor     (PanObject                * object);


/**
***  PUBLIC - STATIC
**/

PanCallback*  status_item_get_active_callback  (void);

/**
***  PUBLIC
**/


StatusItem*   status_item_new                  (StatusItemDescribeFunc);

StatusItem*   status_item_new_with_description (const char       * description);

void          status_item_describe             (const StatusItem * item,
                                                char             * buf,
                                                int                buflen);

void          status_item_emit_status          (StatusItem       * item,
                                                const char       * status);

void          status_item_emit_status_va       (StatusItem       * item,
                                                const char       * fmt,
                                                ...);

void          status_item_emit_error           (StatusItem       * item,
                                                const char       * error);

void          status_item_emit_error_va        (StatusItem       * item,
                                                const char       * fmt,
                                                ...);

void          status_item_emit_progress        (StatusItem       * item,
                                                int                out_of_100);

void          status_item_emit_init_steps      (StatusItem       * item,
                                                int                steps);

void          status_item_emit_next_step       (StatusItem       * item);

void          status_item_emit_inc_step        (StatusItem       * item,
                                                int                inc);

void          status_item_emit_set_step        (StatusItem       * item,
                                                int                step);

void          status_item_emit_activity        (StatusItem       * item,
                                                int                n);

void          status_item_emit_done            (StatusItem       * item,
                                                int                status);

void          status_item_set_active           (StatusItem       * item,
                                                gboolean           active);

int           status_item_get_progress_of_100  (const StatusItem * item);


#endif /* __PAN_STATUS_ITEM__H__ */
