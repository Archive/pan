/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __ARTICLE_H__
#define __ARTICLE_H__

#include <time.h>

#include <glib/gtypes.h>
#include <glib/gslist.h>
#include <glib/garray.h>

#include <pan/base/group.h>
#include <pan/base/message-source.h>
#include <pan/base/pstring.h>
#include <pan/base/pan-callback.h>

#define ARTICLE(a) ((Article *)a)


enum {
	DECODE_STATE_NONE	= 0,
	DECODE_STATE_DECODED	= 1,
	DECODE_STATE_FAILED	= 2
};

enum {
	MULTIPART_STATE_NONE    = 0,
	MULTIPART_STATE_SOME    = 1,
	MULTIPART_STATE_ALL     = 2
};

/**
 * Minimal set of information unique to each part of a `binary' article of one or more parts.
 * We jettison all the common fields (author, subject, etc),
 * and ignore others that don't make sense for multiparts (references, score_date, passes_filter, etc),
 * because those fields found in the Article that holds the Parts by composition.
 *
 * This struct is very similar to MessageIdentifier.  MessageIdentifier was not used here because
 * every AttachmentPart is owned by an Article, and there are a LOT of parts in a binaries group,
 * so Parts are lightweight structs allocated in chunks, as opposed to MessageIdentifiers,
 * which are GObjects.
 * 
 * @see Article
 * @see MessageSources
 * @see group_alloc_new_attachment_part()
 * @see Group::_attachment_part_chunk
 */
typedef struct _AttachmentPart
{
	gulong byte_qty;
	guint16 line_qty;
	time_t date;
	gint16 part_num;
	PString message_id;
	MessageSources sources;
}
AttachmentPart;

/**
 * Heavyweight article representation.
 *
 * This holds all the information we have on an article.
 * It's necessary for cases where we need all that information, such as
 * when filtering, scoring, threading, and displaying articles in the header pane.
 * However this can eat a lot of memory, so articles are all allocated in a big
 * chunk via group_ref_articles() and group_unref_articles().
 *
 * If you only need to know parts of the article, such as where to find it,
 * MessageIdentifier may be a better fit.
 *
 * @see AttachmentPart
 * @see MessageIdentifier
 * @see group_ref_articles()
 * @see group_unref_articles()
 * @see Group::_article_chunk
 */
typedef struct _Article
{
        guint is_new          : 1;    /* true if article is 'new' */
	guint passes_filter   : 1;    /* articlelist */
	guint error_flag      : 1;    /* error downloading this article */
	guint8 multipart_state;       /* none, some, or all */
	guint8 decode_state;          /* none, decoded, or failed */
	gint16 parts;		      /* # of parts in multipart message */
	gint16 score;		      /* scoring */
	guint16 line_qty;             /* # of lines in the body */
	gulong byte_qty;              /* # of bytes in the body */
	time_t score_date;            /* when this article was last scored */
	time_t date;                  /* date posted, as parsed into time_t */
	GSList * followups;           /* Article* followups */
	GSList * attachments;         /* AttachmentPart* pieces */
	struct _Article * parent;     /* articlelist: parent in threading */
	PString message_id;           /* articles message-id */
	PString subject;              /* article's subject */
	PString references;           /* references string for this article.  May be empty. */
	PString author_addr;          /* article's author -- address*/
	PString author_real;          /* article's author -- real name */
	MessageSources xref;          /* article's xref header from XOVER */

	gint16 subthread_score;       /* ARTICLELIST ONLY */
	gpointer header_pane_extra;   /* ARTICLELIST ONLY */
	guint16 unread_children;      /* ARTICLELIST ONLY */
	guint16 new_children;         /* ARTICLELIST ONLY */

	/* At some point this may be an array for 'article-centric' */
	gulong number;               /* (PROTECTED) number for article */
	Group * group;               /* (PROTECTED) group this article is in */
}
Article;

/* Headers stored in their own fields but here for completeness */
#define HEADER_REFERENCES         "References"
#define HEADER_SUBJECT            "Subject"
#define HEADER_FROM               "From"
#define HEADER_MESSAGE_ID         "Message-Id"
#define HEADER_DATE               "Date"
#define HEADER_XREF               "Xref"
/* Headers not stored (must be retrieved from the body). */
#define HEADER_TO                 "To"
#define HEADER_ORGANIZATION       "Organization"
#define HEADER_SUPERSEDES         "Supersedes"
#define HEADER_FOLLOWUP_TO        "Followup-To"
#define HEADER_NEWSGROUPS         "Newsgroups"
#define HEADER_REPLY_TO           "Reply-To"
#define HEADER_USER_AGENT         "User-Agent"
#define HEADER_X_NEWSREADER       "X-Newsreader"
#define HEADER_X_MAILER           "X-Mailer"
#define HEADER_X_FACE             "X-Face"
/* Headers beginning with X-Pan- are for internal use. */
#define PAN_ATTRIBUTION           "X-Pan-Internal-Attribution"
#define PAN_ATTACH_FILE           "X-Pan-Internal-Attach-File"
#define PAN_SERVER                "X-Pan-Internal-Post-Server"
#define PAN_LINES_PER_PART        "X-Pan-Internal-Lines-Per-Part"
#define PAN_CHARSET               "X-Pan-Internal-CharSet"
#define PAN_NEWSGROUPS            "X-Pan-Internal-Sendlater-Newsgroups"
#define PAN_TO                    "X-Pan-Internal-Sendlater-To"
#define PAN_NO_MESSAGE_ID         "X-Pan-Internal-No-Message-Id"

/**
***  PUBLIC LIFE CYCLE
**/

Article*     article_new                      (Group          * group);

void         article_destructor               (Article        * article);

/**
***  FLAGS
**/

int          article_get_decode_state         (const Article  * article);

void         articles_set_decode_state        (Article       ** articles,
                                               int              qty,
                                               int              decode_state);

int          article_get_multipart_state      (const Article  * article);

void         articles_set_multipart_state     (Article       ** articles,
                                               int              qty,
                                               int              multipart_state);

void         article_set_error_flag           (Article        * article,
                                               gboolean         error_flag);

/**
***  Headers
**/

AttachmentPart*  article_alloc_part           (Article        * article);

void         article_add_part                 (Article        * article,
                                               AttachmentPart * part);

void         article_free_part                (Article        * article,
                                               AttachmentPart * part);

gboolean     article_subjects_are_similar     (const Article  * a1,
                                               const Article  * a2);

const char*  article_get_subject              (const Article  * article);

void         article_set_subject              (Article        * article,
                                               const PString  * subject);

const char*  article_get_message_id           (const Article  * article);

void         article_set_message_id           (Article        * article,
                                               const PString  * message_id);

void         article_set_references           (Article        * article,
                                               const PString  * references);

void         article_set_author               (Article        * article,
                                               const PString  * pstring);

char*        article_get_author_str           (const Article  * article,
                                               char           * buf,
                                               int              bufsize);

gulong       article_get_full_line_count      (const Article  * article);

gulong       article_get_full_byte_count      (const Article  * article);

char*        article_format_author_str        (const PString  * author_addr,
                                               const PString  * author_real,
                                               char           * buf,    
                                               int              bufsize);

int          article_format_short_author_str  (const PString  * author_addr,
                                               const PString  * author_real,
                                               char           * buf,    
                                               int              bufsize);

int          article_get_short_author_str     (const Article  * article,
                                               char           * buf,
                                               int              bufsize);

/**
 * Return the number of crossposts in the xref header,
 * or 1 if no xref header is present.
 */
int          article_get_crosspost_qty        (const Article  * article);

void         article_set_from_g_mime_message  (Article        * setme_article,
                                               GMimeMessage   * source);

Article*     article_get_root                 (const Article  * article);

void         article_get_roots                (Article       ** articles,
                                               guint            article_qty,
                                               GPtrArray      * setme);

gboolean     articles_are_in_same_thread      (const Article  * article_1,
                                               const Article  * article_2);

/**
 * Returns TRUE if the header is one only meant for Pan's internal bookkeeping
 * and should never be shown to the user.
 */
gboolean     article_header_is_internal       (const char     * key);

/**
 * This is a wart specific to message-window which should eventually be
 * moved out of article.h.  It returns true if the header isn't
 * internal, nor is followup_to, newsgroups, organization, or reply_to.
 */
gboolean     article_header_is_extra          (const char     * key);


/**
 * This is the iterator function for article_xref_foreach.
 */
typedef void (ArticleXRefFunc)(Server*,Group*,gulong,gpointer);

/**
 * For each cross-reference specified in Article a's Xref: header,
 * the specified ArticleXRefFunc is invoked.
 *
 * If skip_group_a is TRUE, then the group "a->group" is not included
 * in this foreach, even if it's listed in the Xref header.
 */
void         article_xref_foreach             (const Article   * a,
                                               ArticleXRefFunc   func,
                                               gpointer          user_data,
                                               ServerGroupsType  set,
                                               gboolean          skip_group_a);

typedef void (AttachmentPartForeachFunc)(const Article * a,
                                         const AttachmentPart * part,
                                         gpointer user_data);

void        article_foreach_attachment_part (const Article * a,
                                             AttachmentPartForeachFunc foreach_func,
                                             gpointer foreach_func_user_data);

/**
***  Sanity Checks
**/

gboolean     article_is_valid                 (const Article  * article);

gboolean     articles_are_valid               (const Article ** articles,
                                               int              qty);

gboolean     articles_are_valid_in_group      (const Article ** articles,
                                               int              qty);


/**
***  READ
**/

/**
 * Returns true if this article is marked as read.
 */
gboolean     article_is_read                  (const Article  * article);

/**
 * Marks the specified articles as read or unread and fires a single
 * ARTICLE_CHANGED_READ event.
 */
void         articles_set_read_simple         (Article      ** articles,
                                               int             article_qty,
                                               gboolean        read);

/**
 * Marks the specified articles as read or unread, as well as parts
 * 2..n of any multiparts passed in in "articles", as well as any
 * crossposts, and fires a single ARTICLE_CHANGED_READ event for each
 * group in which Articles are changed.
 */
void         articles_set_read                (Article      ** articles,
                                               int             article_qty,
                                               gboolean        read);


/**
 * Returns true if this article is new, where new is defined as having
 * been retrieved from the news server the last time the user tried to
 * fetch new headers.
 */
gboolean     article_is_new                   (const Article  * article);

void         articles_set_new                 (Article       ** articles,
                                               int              article_qty,
                                               gboolean         is_new);

void         articles_set_dirty               (Article       ** articles,
                                               int              article_qty);

/**
***  THREADS
**/

typedef enum
{
	GET_WHOLE_THREAD,
	GET_SUBTHREAD
}
ThreadGet;

typedef void (*ArticleForeach)(Article * article, gpointer user_data);
typedef void (*ArticleForall)(Article ** articles, guint article_qty, gpointer user_data);

void           article_forall_in_references  (Article            * article,
                                              ArticleForall        forall_func,
                                              gpointer             forall_func_user_data);

void           article_forall_in_subthread   (Article            * article,
                                              ArticleForall        forall_func,
                                              gpointer             forall_func_user_data);

void           article_forall_in_thread      (Article            * articles,
                                              ArticleForall        forall_func,
                                              gpointer             forall_func_user_data);

void           article_forall_in_threads     (Article           ** articles,
                                              guint                article_qty,
                                              ThreadGet            thread_get,
                                              ArticleForall        forall_func,
                                              gpointer             forall_func_user_data);

char*          article_get_thread_message_id (const Article*);

extern const char * default_incoming_name_real;

extern const char * default_incoming_name_addr;

/***
****  Events
***/
 
typedef enum
{
	ARTICLE_CHANGED_READ,
	ARTICLE_CHANGED_NEW,
	ARTICLE_CHANGED_DIRTY
}
ArticleChangeType;
 
typedef struct
{
	Group * group;
	Article ** articles;
	int article_qty;
	ArticleChangeType type;
}
ArticleChangeEvent;
 
/**
 * @call_obj: ArticleChangeEvent*
 * @call_arg: NULL
 **/
PanCallback *     article_get_articles_changed_callback      (void);                                                            



#endif /* __ARTICLE_H__ */
