/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2003  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __PSTRING__
#define __PSTRING__

#include <glib/gtypes.h>

/**
 * PString is a thin wrapper class for passing strings around with their
 * string lengths, so that the string walking overhead by replacing calls
 * to strlen with a reference to the len field, and replacing calls to strcmp()
 * with calls to pstring_strcmp() (which uses the faster memcmp).
 *
 * When PStrings are going to be long-term items, such as Group names
 * or Article Message-IDs, then the str field is usually allocated and managed
 * by the PString iteslf via calls to pstring_copy(), pstring_set() or
 * pstring_clear().
 * 
 * Most short-term PStrings are used as wrappers around character pointers
 * that already exist.  In this case, we can avoid the overhead of duplicating
 * the existing character pointer, and then freeing the duplicate later, by
 * using the _shallow() functions to make shallow copies (ie, no memory
 * management takes place) of the character pointers.  This is a measurable
 * performance win, but care must be taken to not pass these shallow copies
 * to the PString memory-management functions, or the already-existing character
 * pointer could be freed accidentally.
 */
typedef struct
{
	char * str;
	int len;

	/* don't access this directly; always use pstring_hash()!
	   This value is filled in the first time pstring_hash()
	   is called. */
	guint hash;
}
PString;

extern const PString PSTRING_INIT;

extern const PString PSTRING_TOXIC;

/**
***  Sanity Clause
**/

gboolean          pstring_is_valid       (const PString   * mid);

gboolean          pstring_is_set         (const PString   * mid);


/**
***  These handle memory management of the str field
**/

PString*          pstring_dup            (const PString   * src);

void              pstring_free           (PString         * pstring);

void              pstring_copy           (PString         * dest,
                                          const PString   * src);

void              pstring_set            (PString         * pstring,
                                          const char      * str,
                                          int               len);

char*             pstring_strchr         (const PString   * pstring,
                                          char              ch);

char*             pstring_strstr         (const PString   * haystack,
                                          const PString   * needle);

void              pstring_strstrip       (PString         * pstring);

void              pstring_clear          (PString         * pstring);

#define pstring_copy_a(dest,src) \
	*(dest) = PSTRING_INIT; \
	(dest)->len = (src)->len; \
	(dest)->str = g_newa (char, (dest)->len+1); \
	memcpy ((dest)->str, (src)->str, (dest)->len); \
	(dest)->str[(dest)->len] = '\0';

/**
***  Shallow copies of str
**/

PString           pstring_shallow             (const char      * str,
                                               int               len);

PString           pstring_strstrip_shallow    (const PString   * pstring);

PString           pstring_substr_shallow      (const PString   * pstring,
                                               const char      * start,
                                               const char      * end);

gboolean          pstring_next_token_shallow  (const PString   * march_in,
                                               char              delimiter,
                                               PString         * march_out,
                                               PString         * shallow_token);


/**
***  For hash tables & comparison
**/

guint             pstring_hash           (gconstpointer     key);

gboolean          pstring_equal          (gconstpointer     a,
                                          gconstpointer     b);

int               pstring_compare        (gconstpointer     a,
                                          gconstpointer     b);


#endif /* __PSTRING__ */
