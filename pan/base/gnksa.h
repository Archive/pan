/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GNKSA_H__
#define __GNKSA_H__

#include <glib/gtypes.h>
#include <time.h>

enum
{
	/* success/undefined error */
	GNKSA_OK			= 0,
	GNKSA_INTERNAL_ERROR		= 1,

	/* general syntax */
	GNKSA_LANGLE_MISSING		= 100,
	GNKSA_RANGLE_MISSING		= 101,
	GNKSA_LPAREN_MISSING		= 102,
	GNKSA_RPAREN_MISSING		= 103,
	GNKSA_ATSIGN_MISSING		= 104,

	/* FQDN checks */
	GNKSA_SINGLE_DOMAIN		= 200,
	GNKSA_INVALID_DOMAIN		= 201,
	GNKSA_ILLEGAL_DOMAIN		= 202,
	GNKSA_UNKNOWN_DOMAIN		= 203,
	GNKSA_INVALID_FQDN_CHAR		= 204,
	GNKSA_ZERO_LENGTH_LABEL		= 205,
	GNKSA_ILLEGAL_LABEL_LENGTH	= 206,
	GNKSA_ILLEGAL_LABEL_HYPHEN	= 207,
	GNKSA_ILLEGAL_LABEL_BEGNUM	= 208,
	GNKSA_BAD_DOMAIN_LITERAL	= 209,
	GNKSA_LOCAL_DOMAIN_LITERAL	= 210,
	GNKSA_RBRACKET_MISSING		= 211,

	/* localpart checks */
	GNKSA_LOCALPART_MISSING		= 300,
	GNKSA_INVALID_LOCALPART		= 301,
	GNKSA_ZERO_LENGTH_LOCAL_WORD	= 302,

	/* realname checks */
	GNKSA_ILLEGAL_UNQUOTED_CHAR	= 400,
	GNKSA_ILLEGAL_QUOTED_CHAR	= 401,
	GNKSA_ILLEGAL_ENCODED_CHAR	= 402,
	GNKSA_BAD_ENCODE_SYNTAX		= 403,
	GNKSA_ILLEGAL_PAREN_PHRASE	= 404,
	GNKSA_ILLEGAL_PAREN_CHAR	= 405,
	GNKSA_INVALID_REALNAME		= 406,
	GNKSA_ILLEGAL_PLAIN_PHRASE	= 407,

	/* address types */
	GNKSA_ADDRTYPE_ROUTE		= 0,
	GNKSA_ADDRTYPE_OLDSTYLE		= 1
};

int          gnksa_check_from                            (const char    * from,
                                                          gboolean        strict);

int          gnksa_do_check_from                         (const char    * from,
                                                          char          * addr,
                                                          guint           addr_max,
                                                          char          * name,
                                                          guint           name_max,
                                                          gboolean        strict);

int          gnksa_check_domain                          (const char   * domain,
                                                          guint          domain_len);

void         gnksa_strip_realname                        (char         * realname);

char*        gnksa_trim_references_to_len                (const char   * refs,
                                                          int            cutoff);

char*        gnksa_generate_references                   (const char   * references,
                                                          const char   * message_id);

char*        gnksa_generate_message_id_from_email_addr   (const char   * from);

char*        gnksa_generate_message_id                   (const char   * domain_name);

time_t       tzoffset_sec                                (time_t       * time);


typedef enum
{
   SIG_NONE,
   SIG_STANDARD,
   SIG_NONSTANDARD
}
SigType;

SigType   pan_is_signature_delimiter    (const char       * line,
                                         const int          line_len);

SigType   pan_find_signature_delimiter  (const char      * text,
                                         char           ** setme_delimiter);

gboolean  pan_remove_signature          (char             * body);

#endif
