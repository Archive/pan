/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __SERVER_MODULE_H__
#define __SERVER_MODULE_H__

#include <time.h>
#include <glib/garray.h>

#include <pan/base/pan-callback.h>
#include <pan/base/pan-object.h>
#include <pan/base/group.h>
#include <pan/base/server.h>

#define SERVER(a) ((Server*)a)

/**
***  Server Module
**/

void          serverlist_get_servers                    (GPtrArray * fillme);

void          serverlist_add_server                     (Server    * server,
                                                         GError   ** error);

void          serverlist_remove_server                  (Server * server);

void          serverlist_init                           (void);

void          serverlist_shutdown                       (void);

Server*       serverlist_get_active_server              (void);

void          serverlist_set_active_server              (Server * server);

Server*       serverlist_get_server                     (const GQuark server_quark);

void          serverlist_save                           (void);

Server*       serverlist_get_folders_server             (void);

Group*        serverlist_get_folder                     (const GQuark folder_quark);


/**
***  Events
**/

/**
 * @call_obj: Server*
 * @call_arg: NULL
 */
PanCallback*  serverlist_get_server_activated_callback  (void);


#endif /* __SERVER_MODULE_H__ */
