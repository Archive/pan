/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __FILE_GROUPLIST_H__
#define __FILE_GROUPLIST_H__

#include <pan/base/server.h>
#include <pan/base/status-item.h>

ServerGroupsType   file_grouplist_exists               (const Server * server);

void               file_grouplist_load                 (Server * server, ServerGroupsType, StatusItem * status);

void               file_grouplist_save                 (Server * server, ServerGroupsType, StatusItem * status);

void               file_grouplist_server_name_changed  (const PString * old_name, const PString * new_name);


#endif
