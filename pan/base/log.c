/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <gmime/memchunk.h>

#include <pan/base/log.h>
#include <pan/base/pan-glib-extensions.h>


static GStringChunk * _strings = NULL;
static MemChunk * _entries = NULL;
static GPtrArray * _log_entries = NULL;
static gboolean _inited = FALSE;

/* tailoring chunks, on a WAG, of roughly 100 log entries of 80 chars each */
#define LOG_CHUNK_SIZE 100
#define STRING_CHUNK_SIZE 8192

static void
log_rebuild_entry_memory (void)
{
	if (_log_entries != NULL)
		g_ptr_array_free (_log_entries, TRUE);
	_log_entries = g_ptr_array_sized_new (LOG_CHUNK_SIZE);

	if (_entries != NULL)
		memchunk_destroy (_entries);
	_entries = memchunk_new (sizeof(LogEntry), LOG_CHUNK_SIZE, FALSE);

	if (_strings != NULL)
		g_string_chunk_free (_strings);
	_strings = g_string_chunk_new (STRING_CHUNK_SIZE);

	_inited = TRUE;
}

static void
log_init (void)
{
	log_rebuild_entry_memory ();
}

void
log_clear (void)
{
	log_rebuild_entry_memory ();
	pan_callback_call (log_get_entry_list_changed_callback(), NULL, NULL);
}

void
log_shutdown_module (void)
{
	if (_inited)
	{
		_inited = FALSE;

		g_ptr_array_free (_log_entries, TRUE);
		memchunk_destroy (_entries);
		g_string_chunk_free (_strings);

		_log_entries = NULL;
		_entries = NULL;
		_strings = NULL;
	}
}

PanCallback *
log_get_entry_added_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb == NULL) cb = pan_callback_new ();
	return cb;
}

PanCallback *
log_get_entry_list_changed_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb == NULL) cb = pan_callback_new ();
	return cb;
}

GPtrArray *
log_get_entries (void)
{
	return _log_entries;
}

static void
log_add_entry (LogEntry * entry)
{
	g_ptr_array_add (log_get_entries(), entry);
}

static LogEntry*
log_entry_new (LogSeverity severity, const char * msg)
{
	LogEntry * entry;

	if (!_inited)
		log_init ();

	entry = (LogEntry*) memchunk_alloc (_entries);
	entry->message = g_strchomp (g_string_chunk_insert (_strings, msg));
	entry->date = time(NULL);
	entry->severity = severity;
	return entry;
}

void
log_add (LogSeverity severity, const char * message)
{
	LogEntry * entry;
	log_add_entry (entry = log_entry_new (severity, message));

	pan_callback_call (log_get_entry_added_callback(), entry, NULL);
}

void
log_add_va (LogSeverity severity, const char * format, ...)
{
	va_list args;
	char * message;
	LogEntry * entry;
	g_return_if_fail (format != NULL);

	va_start (args, format);
	message = g_strdup_vprintf (format, args);
	va_end (args);
	log_add_entry (entry = log_entry_new (severity, message));
	g_free (message);

	pan_callback_call (log_get_entry_added_callback(), entry, NULL);
}
