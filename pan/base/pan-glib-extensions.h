/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __PAN_GLIB_EXTENSIONS_H__
#define __PAN_GLIB_EXTENSIONS_H__

#include <stddef.h>
#include <glib/galloca.h>
#include <glib/ghash.h>
#include <glib/garray.h>
#include <glib/gstring.h>
#include <glib/gmessages.h>
#include <gmime/gmime-part.h>
#include <pan/base/pstring.h>
#include <time.h>

/**
 * @param pch the string to duplicate
 * lvalue is the duplicated string
 */
#define pan_strdup_alloca(pch) memcpy((char*)g_alloca(strlen(pch)+1),pch,strlen(pch)+1) 

/**
 * @param pch the char* to hold the duplicated string
 * @param str the string to duplicate
 * @param len the length of the string to duplicate
 */
#define pan_strndup_alloca(pch,str,len) ((pch)=(char*)g_alloca((len)+1),memcpy((pch),str,len),(pch)[len]='\0',(pch))

/**
***  Strings
**/

#define is_nonempty_string(A) ((A!=NULL) && (*(A)!='\0'))

int pan_strcmp (const char* a, const char  * b);

void replace_gstr (char ** target_free_old, char * assign_from_me);

char* pan_substitute (const char  * original,
                      const char  * search,
                      const char  * replace);

/**
***  Glib
**/

int   pan_strcmp_len  (const char * str_a,
                       int          str_a_len,
                       const char * str_b,
                       int          str_b_len);

char* pan_strchr_len (const char * haystack,
                      gssize       haystack_len,
                      char         needle);

char* pan_strrchr_len (const char * haystack,
                       gssize       haystack_len,
                       char         needle);

int pan_strncpy_len (char        * target,
                     const int     target_size,
                     const char  * source_str,
                     const int     source_len);

char* pan_strstr (const char * s1,
                  const char * s2);

void pan_g_ptr_array_append (GPtrArray   * target,
                             gpointer    * source_ptr,
			     guint         source_qty);

void pan_g_ptr_array_assign  (GPtrArray  * target,
                              gpointer   * source_ptr,
                              guint        source_qty);

GPtrArray * pan_g_ptr_array_dup     (const GPtrArray  * source);


void pan_hash_to_ptr_array  (GHashTable * hash,
                             GPtrArray  * fillme);

void pan_g_ptr_array_reserve (GPtrArray  * target,
                              int          n);

void pan_g_ptr_array_insert (GPtrArray   * target,
                             gpointer      ptr,
                             int           index);

#if GLIB_CHECK_VERSION(2,4,0)
#define pan_g_ptr_array_foreach g_ptr_array_foreach
#else
void pan_g_ptr_array_foreach (GPtrArray  * target,
                              GFunc        func,
                              gpointer     user_data);
#endif


void pan_g_string_replace (GString    * str,
                           const char * search,
                           const char * replace);

void pan_g_string_strstrip (GString    * str);

#if GLIB_CHECK_VERSION(2,6,0)
#include <glib/gstdio.h>
#define pan_unlink g_unlink
#define pan_fopen g_fopen
#else
#define pan_unlink unlink
#define pan_fopen fopen
#endif

/**
***  Tokens
**/


void skip_next_token (const char   * pch,
                      char           delimiter,
                      const char  ** setme_next_token);

int get_next_token_int (const char   * pch,
                        char           delimiter,
                        const char  ** setme_next_token);

long get_next_token_long (const char   * pch,
                          char           delimiter,
                          const char  ** setme_next_token);

gulong get_next_token_ulong (const char   * pch,
                             char           delimiter,
                             const char  ** setme_next_token);

char* get_next_token_str (const char   * pch,
                          char           delimiter,
                          const char  ** setme_next_token);

gboolean get_next_token_g_str (const char   * pch,
                               char           delimiter,
                               const char  ** setme_next_token,
                               GString      * setme);

gboolean get_next_token_pstring (const char   * pch,
                                 char           delimiter,
                                 const char  ** setme_next_token,
                                 PString      * setme_pstring);

const char*  get_next_token_run (const char   * pch,
                                 char           delimiter,
                                 const char  ** setme_next_token,
                                 const char  ** setme_start,
                                 int          * setme_len);

char* pan_strnstr_len (const char * haystack,
                       int          haystack_len,
                       const char * needle,
                       int          needle_len);


char* pan_str_escape   (const char * encode_for_xml);

char* pan_str_unescape (const char * decode_xml);

const char* pan_get_home_dir (void);


/**
 * Wrapper around localtime_r for platforms that don't have it.
 */
struct tm* pan_localtime_r (const time_t * const, struct tm *);

/**
 * Wrapper around gmtime_r for platforms that don't have it.
 */
struct tm* pan_gmtime_r (const time_t * const, struct tm *);

/**
 * Works like msort, except it's an in-place sort
 */
void msort (void *b,
            size_t n,
            size_t s,
            int(*cmp)(const void *, const void*));

/**
 * Works like bsearch, except it returns the lower bound index
 * in the array.  This is useful for finding a sorted insertion
 * point or for finding the nearest match.
 *
 * @param key the item to search for
 * @param base the beginning of the sorted array to search
 * @param n number of items in the array
 * @param size the size of each item in the array
 * @param compare standard c compare func
 * @param exact_match (optional): if non-NULL, this is set to TRUE or FALSE if
 *        the lower bound is an exact match of key
 *
 * @return the index of the lower bound of the search
 * @prerequisite base must be sorted with respect to compare
 */
int    lower_bound   (const void   * key,
                      const void   * base,
                      size_t         n,
                      size_t         size,
                      int            (*compare)(const void *, const void *),
                      gboolean     * exact_match );


typedef enum
{
	RUN_SUCCESS = 0,
	RUN_FAIL = -1,
	RUN_FAIL_HOPELESS = -2
}
RunStatus;

#ifdef __GNUC__
#define PRETTY_FUNCTION __PRETTY_FUNCTION__
#else
#define PRETTY_FUNCTION ""
#endif

#define pan_warn_if_reached()           G_STMT_START{           \
     g_log (G_LOG_DOMAIN,                                       \
            G_LOG_LEVEL_WARNING,                                \
            "file %s: line %d func %s: should not be reached, please report bug to pan@rebelbase.com",     \
            __FILE__,                                           \
            __LINE__,                                           \
            PRETTY_FUNCTION);       }G_STMT_END

#define pan_warn_if_fail(expr)          G_STMT_START{                   \
     if (!(expr))                                                       \
       {                                                                \
         g_log (G_LOG_DOMAIN,                                           \
                G_LOG_LEVEL_CRITICAL,                                   \
                "file %s: line %d func %s: assertion `%s' failed, please report bug to pan@rebelbase.com.",        \
                __FILE__,                                               \
                __LINE__,                                               \
                PRETTY_FUNCTION,                                        \
                #expr);                                                 \
       };                               }G_STMT_END

/**
 ** Charsets
 **/

const char* get_charset_from_locale    (void);

const char* pan_utf8ize                (const char    * str,
                                        gssize          len,
                                        char         ** g_freeme);

char*       pan_header_to_utf8         (const char  * header,
                                        int           header_len,
                                        const char  * charset);

char*       pan_body_to_utf8           (GMimePart    * part,
                                        const char   * default_charset);

#endif /* __PAN_GLIB_EXTENSIONS_H__ */
