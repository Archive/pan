/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <stdarg.h>
#include <stdio.h>
#include <time.h>

#include <glib/gstring.h>
#include <glib/ghash.h>
#include <glib/gstrfuncs.h>
#include <glib/gmem.h>

#include <pan/base/debug.h>
#include <pan/base/pan-glib-extensions.h>

unsigned long debug_level = 0;

void
set_debug_level (unsigned long level)
{
	debug_level = level;
}

void
debug_printf (const char    * strloc,
              const gchar   * function,
              gulong          flag,
              const gchar   * fmt,
              ...)
{
	static int depth = 0;
	char timebuf[64];
	gchar * string;
	const gchar * prefix;
	time_t now_time_t;
	va_list args;
	struct tm now_tm;

	g_return_if_fail (fmt != NULL);

	/* get prefix */
	if (flag&DEBUG_QUEUE) prefix="QUEUE";
	else if (flag&DEBUG_SOCKET_INPUT) prefix="SOCKET INPUT";
	else if (flag&DEBUG_SOCKET_OUTPUT) prefix="SOCKET OUTPUT";
	else if (flag&DEBUG_DECODE) prefix="DECODE";
	else if (flag&DEBUG_ACACHE) prefix="ACACHE";
	else if (flag&DEBUG_LOCK) prefix="LOCK";
	else if (flag&DEBUG_PAN_OBJECT) prefix="PAN OBJECT";
	else if (flag&DEBUG_TRACE) prefix="TRACE";
	else {prefix="";}

	va_start (args, fmt);
	string = g_strdup_vprintf (fmt, args);
	va_end (args);

	time (&now_time_t);
	pan_localtime_r (&now_time_t, &now_tm);
	strftime (timebuf, sizeof(timebuf), "%H:%M:%S", &now_tm);

	if (flag & DEBUG_TRACE)
	{
		const gint old_depth = depth;
		const gint new_depth = old_depth + (*fmt=='+' ? 1 : -1);

		depth = new_depth;

		printf ("(%15s:%20s)(time %s)(depth %3d) %s: %s\n",
			strloc,
			function,
			timebuf,
			new_depth,
			prefix,
			string);
	}
	else
	{
		printf ("(%15s:%20s)(time %s) %s: %s\n",
			strloc,
			function,
			timebuf,
			prefix,
			string);
	}
	fflush (NULL);

	g_free (string);
}

