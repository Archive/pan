/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#include <string.h>
#include <glib.h>
#include <pan/base/pan-callback.h>

/* arbitrary number; can be changed if needed */
#define MAX_LISTENERS 8

struct PanCallbackStruct
{
	struct
	{
		PanCallbackFunc callback;
		gpointer user_data;
	}
	listeners [MAX_LISTENERS];
	int qty;
};

/**
***  Public Interface
**/

PanCallback*
pan_callback_new (void)
{
	return g_new0 (PanCallback, 1);
}

void
pan_callback_free (PanCallback** pcl)
{
	g_free (*pcl);
	*pcl = (void*)0xDEADBEEF;
}

		 
void
pan_callback_call (PanCallback* pcl, gpointer call_object, gpointer call_arg)
{
	int i;
	PanCallback tmp = *pcl;

	for (i=0; i<tmp.qty; ++i)
		(*tmp.listeners[i].callback)(call_object,
		                             call_arg,
		                             tmp.listeners[i].user_data);
}

void
pan_callback_add (PanCallback* pcl, PanCallbackFunc cb, gpointer user)
{
	if (pcl->qty >= MAX_LISTENERS)
		g_warning ("Too many listeners to pan_callback %p", pcl);
	else {
		const int qty = pcl->qty++;
		pcl->listeners[qty].callback = cb;
		pcl->listeners[qty].user_data = user;
	}
}

void
pan_callback_remove (PanCallback* pcl, PanCallbackFunc cb, gpointer user)
{
	int i;

	for (i=0; i<pcl->qty; ++i)
		if (pcl->listeners[i].callback == cb &&
		    pcl->listeners[i].user_data == user)
			break;

	if (i!=pcl->qty) {
		const int qty = pcl->qty--;
		g_memmove (pcl->listeners+i,
		           pcl->listeners+i+1,
		           sizeof(pcl->listeners[0])*qty-1-i);
	}
}
