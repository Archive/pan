/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __LOG_H__
#define __LOG_H__

#include <time.h> /* for time_t */
#include <glib/garray.h>
#include <pan/base/pan-callback.h>

void log_shutdown_module (void);

typedef enum
{
	LOG_INFO		= 1,
	LOG_ERROR		= 2,
	LOG_URGENT		= (1<<10)
}
LogSeverity;

void log_add    (LogSeverity, const char *);

void log_add_va (LogSeverity, const char *, ...);

typedef struct
{
	time_t date;
	char * message;
	LogSeverity severity;
}
LogEntry;

GPtrArray * log_get_entries (void);

void log_clear (void);

/****
*****
*****   EVENTS
*****
****/

/**
 * @call_obj: LogEntry*
 * @call_arg: NULL
 */
PanCallback* log_get_entry_added_callback (void);

/**
 * @call_obj: NULL
 * @call_arg: NULL
 */
PanCallback* log_get_entry_list_changed_callback (void);


#endif /* __LOG_H__ */
