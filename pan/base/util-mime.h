/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __UTIL_MIME_H__
#define __UTIL_MIME_H__

#include <glib/gtypes.h>
#include <gmime/gmime-filter.h>
#include <gmime/gmime-stream.h>
#include <gmime/gmime-message.h>
#include <pan/base/group.h>

GMimeMessage*  pan_g_mime_parser_construct_message (GMimeStream  ** streams,
                                                    int             stream_qty);

void           pan_g_mime_message_mark_read        (GMimeMessage  * message);

Group*         pan_g_mime_message_get_group        (GMimeMessage  * message);

const char *   pan_g_mime_message_get_charset      (GMimeMessage * message);

void
guess_part_type_from_filename (const char   * filename,
                               const char  ** setme_type,
                               const char  ** setme_subtype);
/**
***  YEnc Functions
**/

typedef struct
{
	char * filename;

	int line_len;
	int attach_size;
	int part;
	guint offset_begin;
	guint offset_end;
	guint crc;
	guint pcrc;
	size_t size;

	GMimeFilter * filter;
}
YencInfo;


#endif
