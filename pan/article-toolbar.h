/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef ARTICLE_TOOLBAR_H
#define ARTICLE_TOOLBAR_H

#include <gtk/gtkwidget.h>
#include <pan/filters/filter.h>
#include <pan/filters/filter-phrase.h>
#include <pan/articlelist.h> /* for FilterShow */

/**
***
**/

GtkWidget*  article_toolbar_new                 (void);

void        article_toolbar_set_group           (const Group        * group);

void        article_toolbar_set_group_filter    (Group              * group);

void        article_toolbar_get_filter          (Filter            ** filter_newme,
                                                 FilterShow         * filtershow_setme);

void        article_toolbar_edit_filter         (void);


/**
***  Events
**/

typedef struct
{
	Filter * filter;
	FilterShow filter_show;
}
ArticleToolbarCallbackStruct;

/**
 * @call_obj: const ArticleToolbarCallbackStruct*
 * @call_arg: NULL
 */
PanCallback*  article_toolbar_get_user_changed_filter_callback (void);


#endif
