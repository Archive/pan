/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __PREFS_H__
#define __PREFS_H__

#include <glib.h> /* pick up G_OS_* and G_DIR_SEPARATOR_S */

/* DISPLAY */
#define PREFIX_DISPLAY "/Pan/Display/"
#define KEY_COLOR_UNREAD                           PREFIX_DISPLAY  "unread_color"
#define KEY_COLOR_READ                             PREFIX_DISPLAY  "read_color"
#define KEY_COLOR_URL                              PREFIX_DISPLAY  "text_url_color" 
#define KEY_COLOR_QUOTED_1                         PREFIX_DISPLAY  "text_quoted_color_1" 
#define KEY_COLOR_QUOTED_2                         PREFIX_DISPLAY  "text_quoted_color_2" 
#define KEY_COLOR_QUOTED_3                         PREFIX_DISPLAY  "text_quoted_color_3" 
#define KEY_COLOR_SIGNATURE                        PREFIX_DISPLAY  "signature_color" 
#define KEY_COLOR_WATCHED_BG                       PREFIX_DISPLAY  "score_color_watched_background"
#define KEY_COLOR_WATCHED_FG                       PREFIX_DISPLAY  "score_color_watched_foreground"
#define KEY_COLOR_HIGH_BG                          PREFIX_DISPLAY  "score_color_high_background"
#define KEY_COLOR_HIGH_FG                          PREFIX_DISPLAY  "score_color_high_foreground"
#define KEY_COLOR_MEDIUM_BG                        PREFIX_DISPLAY  "score_color_medium_background"
#define KEY_COLOR_MEDIUM_FG                        PREFIX_DISPLAY  "score_color_medium_foreground"
#define KEY_COLOR_ZERO_BG                          PREFIX_DISPLAY  "score_color_zero_background"
#define KEY_COLOR_ZERO_FG                          PREFIX_DISPLAY  "score_color_zero_foreground"
#define KEY_COLOR_LOW_BG                           PREFIX_DISPLAY  "score_color_low_background"
#define KEY_COLOR_LOW_FG                           PREFIX_DISPLAY  "score_color_low_foreground"
#define KEY_COLOR_IGNORED_BG                       PREFIX_DISPLAY  "score_color_ignored_background"
#define KEY_COLOR_IGNORED_FG                       PREFIX_DISPLAY  "score_color_ignored_foreground"
#define KEY_QUOTE_CHARS                            PREFIX_DISPLAY  "text_quote_chars"
#define KEY_GROUP_PANE_ENABLED                     PREFIX_DISPLAY  "group_pane_enabled"
#define KEY_GROUP_PANE_COLLAPSE_GROUP_NAMES        PREFIX_DISPLAY  "group_pane_collapse_group_names"
#define KEY_GROUP_PANE_CUSTOM_FONT_ENABLED         PREFIX_DISPLAY  "group_pane_custom_font_enabled"
#define KEY_GROUP_PANE_CUSTOM_FONT                 PREFIX_DISPLAY  "group_pane_custom_font"
#define KEY_HEADER_PANE_DATE_FORMAT                PREFIX_DISPLAY  "header_pane_date_format"
#define KEY_HEADER_PANE_ENABLED                    PREFIX_DISPLAY  "header_pane_enabled"
#define KEY_HEADER_PANE_CUSTOM_FONT_ENABLED        PREFIX_DISPLAY  "header_pane_custom_font_enabled"
#define KEY_HEADER_PANE_CUSTOM_FONT                PREFIX_DISPLAY  "header_pane_custom_font"
#define KEY_HEADER_PANE_THREADING_ENABLED          PREFIX_DISPLAY  "header_pane_threading_enabled"
#define KEY_HEADER_PANE_COLUMNS                    PREFIX_DISPLAY  "header_pane_columns"
#define KEY_HEADER_PANE_THREADS_EXPANDED           PREFIX_DISPLAY  "header_pane_threads_expanded"
#define KEY_BODY_PANE_ENABLED                      PREFIX_DISPLAY  "body_pane_enabled"
#define KEY_BODY_PANE_HEADERS                      PREFIX_DISPLAY  "body_pane_headers"
#define KEY_BODY_PANE_MUTE_QUOTED_TEXT             PREFIX_DISPLAY  "body_pane_mute_quoted_text"
#define KEY_BODY_PANE_SHOW_ALL_HEADERS             PREFIX_DISPLAY  "body_pane_show_all_headers"
#define KEY_BODY_PANE_WRAP_ENABLED                 PREFIX_DISPLAY  "body_pane_wrap_enabled"
#define KEY_BODY_PANE_CUSTOM_FONT_ENABLED          PREFIX_DISPLAY  "body_pane_custom_font_enabled"
#define KEY_BODY_PANE_CUSTOM_FONT                  PREFIX_DISPLAY  "body_pane_custom_font"
#define KEY_BODY_PANE_MONOSPACE_FONT_ENABLED       PREFIX_DISPLAY  "body_pane_monospace_font_enabled"
#define KEY_BODY_PANE_MONOSPACE_FONT               PREFIX_DISPLAY  "body_pane_monospace_font"
#define KEY_BODY_PANE_SMOOTH_SCROLLING_ENABLED     PREFIX_DISPLAY  "body_pane_smooth_scrolling_enabled"
#define KEY_BODY_PANE_SMOOTH_SCROLLING_SPEED       PREFIX_DISPLAY  "body_pane_smooth_scrolling_speed"
#define KEY_COLOR_SUBJECT_COLUMN                   PREFIX_DISPLAY  "color_subject_column"
#define KEY_COLOR_SCORE_COLUMN                     PREFIX_DISPLAY  "color_score_column"
#define KEY_PANE_LAYOUT                            PREFIX_DISPLAY  "pane_layout"
#define KEY_PANE_MODE                              PREFIX_DISPLAY  "pane_mode"
#define KEY_BREAK_THREAD_WHEN_SUBJECT_CHANGES      PREFIX_DISPLAY  "break_thread_when_subject_changes"
#define KEY_WRAP_COLUMN                            PREFIX_DISPLAY  "wrap_column"

#define DEFAULT_VALUE_GROUP_PANE_ENABLED                     TRUE
#define DEFAULT_VALUE_GROUP_PANE_COLLAPSE_NAMES              FALSE
#define DEFAULT_VALUE_HEADER_PANE_ENABLED                    TRUE
#define DEFAULT_VALUE_HEADER_PANE_IS_THREADED                TRUE
#define DEFAULT_VALUE_BODY_PANE_ENABLED                      TRUE
#define DEFAULT_VALUE_BODY_PANE_WRAP_ENABLED                 FALSE
#define DEFAULT_VALUE_BODY_PANE_MONOSPACE_FONT_ENABLED       FALSE
#define DEFAULT_VALUE_BODY_PANE_SHOW_ALL_HEADERS             FALSE
#define DEFAULT_VALUE_BODY_PANE_MUTE_QUOTED                  FALSE
#define DEFAULT_VALUE_PANE_MODE                              1
#define DEFAULT_VALUE_WRAP_COLUMN                            74


/* BEHAVIOR */
#define PREFIX_BEHAVIOR "/Pan/behavior/"
#define KEY_FETCH_NEW_ON_GROUP_ENTER               PREFIX_BEHAVIOR  "fetch_new_on_group_load"
#define KEY_MARK_READ_ON_GROUP_EXIT                PREFIX_BEHAVIOR  "mark_read_on_group_exit"
#define KEY_FETCH_NEW_FROM_SUBSCRIBED_ON_STARTUP   PREFIX_BEHAVIOR  "fetch_new_from_subscribed_on_start"
#define KEY_FETCH_NEW_AND_BODIES_ON_STARTUP        PREFIX_BEHAVIOR  "fetch_new_and_bodies_from_described_on_startup"
#define KEY_SINGLE_CLICK_SELECTS_GROUPS            PREFIX_BEHAVIOR  "single_click_selects_groups"
#define KEY_SINGLE_CLICK_SELECTS_HEADERS           PREFIX_BEHAVIOR  "single_click_selects_headers"
#define KEY_REMOVE_FAILED_TASKS                    PREFIX_BEHAVIOR  "remove_failed_tasks"
#define KEY_CACHE_FLUSH_ON_EXIT                    PREFIX_BEHAVIOR  "flush_cache_on_exit"
#define KEY_CACHE_MAXIMUM_SIZE_MEGS                PREFIX_BEHAVIOR  "maximum_cache_size_megs"

#define DEFAULT_VALUE_REMOVE_FAILED_TASKS          TRUE

/* SCORING */
#define PREFIX_SCORE "/Pan/scoring/"
#define KEY_SCOREFILE                              PREFIX_SCORE "scorefile"
#define KEY_SCOREFILE_DEFAULT                      "News" G_DIR_SEPARATOR_S "Score"

/* POSTING */
#define PREFIX_POST "/Pan/posting/"
#define KEY_POST_SPELLCHECK_ENABLED                PREFIX_POST "spellcheck_enabled"
#define KEY_POST_USER_AGENT_HEADER                 PREFIX_POST "user_agent_header_enabled"
#define KEY_POST_MESSAGE_ID_HEADER                 PREFIX_POST "message_id_header_enabled"
#define DEFAULT_VALUE_POST_SPELLCHECK_ENABLED      TRUE
#define DEFAULT_VALUE_POST_USER_AGENT_HEADER       TRUE
#define DEFAULT_VALUE_POST_MESSAGE_ID_HEADER       TRUE


/* APPS */
#define PREFIX_APPS "/Pan/apps/"
#define KEY_APP_BROWSER                            PREFIX_APPS "web_browser"
#define KEY_APP_EDITOR                             PREFIX_APPS "editor"
#define KEY_APP_SCOREFILE_EDITOR                   PREFIX_APPS "scorefile_editor"
#define KEY_APP_PRINT_COMMAND                      PREFIX_APPS "print"

#define DEFAULT_VALUE_APP_PRINT_COMMAND            "lpr"


/**
*** Global Variables
**/

extern gboolean                single_click_selects_groups;
extern gboolean                single_click_selects_headers;
extern gboolean                header_pane_is_threaded;
extern gboolean                show_group_pane;
extern gboolean                show_header_pane;
extern gboolean                show_body_pane;
extern gboolean                text_window_smooth_scrolling;
extern int                     text_window_smooth_scrolling_speed;
extern gboolean                fetch_new_on_startup;
extern gboolean                fetch_new_and_bodies_on_startup;
extern gboolean                fetch_new_on_group_enter;
extern gboolean                mark_read_on_group_exit;
extern gboolean                expand_all_threads_by_default;
extern gboolean                pan_mute;
extern char                  * header_pane_date_format;

extern gboolean                header_pane_color_subject_column;
extern gboolean                header_pane_color_score_column;
extern gboolean                header_pane_custom_font_enabled;
extern char                  * header_pane_custom_font;
extern gboolean                group_pane_custom_font_enabled;
extern char                  * group_pane_custom_font;
extern gboolean                body_pane_monospace_font_enabled;
extern char                  * body_pane_monospace_font;
extern gboolean                body_pane_custom_font_enabled;
extern char                  * body_pane_custom_font;

extern char                  * external_web_browser;
extern char                  * external_editor;
extern char                  * score_editor_command;

extern char                  * mail_server_address;
extern char                  * layout_str;
extern int                     mail_server_port;
extern gboolean                collapse_group_names;
extern gboolean                do_spellcheck;

/** 
*** Function Prototypes
**/

void prefs_spawn                         (void);

void prefs_init                          (void);

void show_group_substitution_help_dialog (gpointer window);

#endif /* __PREFS_H__ */
