/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <glib.h>

#include <pan/base/article.h>
#include <pan/base/debug.h>
#include <pan/base/log.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/nntp.h>
#include <pan/task-grouplist.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

static void list_run (Task * task, GIOChannel * channel);

static void task_grouplist_describe (const StatusItem*, char * buf, int buflen);

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PUBLIC ROUTINES
************/

PanObject*
task_grouplist_new (Server *server)
{
	TaskGroupList *item;

	debug1 (DEBUG_PAN_OBJECT, "task_grouplist_new: %p", item);
	item = g_new0 (TaskGroupList, 1);
	
	task_constructor (TASK(item),
	                  TASK_TYPE_OTHER,
	                  task_destructor,
	                  task_grouplist_describe,
	                  server,
	                  TRUE);

	task_state_set_work_need_socket (&TASK(item)->state,
	                                 server,
	                                 list_run);

	return PAN_OBJECT(item);
}

/*****
******
*****/

static void
task_grouplist_describe (const StatusItem * status, char * buf, int buflen)
{
	const char * name;

	/* sanity checks */
	g_return_if_fail (status!=NULL);

	name = server_get_name (TASK(status)->server);
	g_snprintf (buf, buflen,  _("Getting groups from server \"%s\""), name);
}

/***
****
***/

static int
compare_name_ppgroup (const void * a, const void * b)
{
	return pstring_compare ((const PString*)a, &(*(const Group**)b)->name);
}

static int
compare_ppgroup_ppgroup (gconstpointer a, gconstpointer b, gpointer unused)
{
	return pstring_compare (&(*(const Group**)a)->name,
	                        &(*(const Group**)b)->name);
}

typedef struct
{
	int desc_qty;
	StatusItem * status;
	Task * task;
	Server * server;
	GPtrArray * groups;
}
NewgroupsWorkStruct;

static void
list_newsgroups_ran (GIOChannel     * channel,
                     TaskStateEnum     val,
                     gpointer          user_data)
{
	NewgroupsWorkStruct * wdata = (NewgroupsWorkStruct*) user_data;
	char buf[512];
	guint qty = wdata->groups->len;
	Group ** groups = (Group**) wdata->groups->pdata;

	/* warn the user if we didn't get newsgroup description */
	if (val != TASK_OK)
		log_add (LOG_ERROR, _("Failed to load newsgroup descriptions"));

	/* let the user know what we got */
	g_snprintf (buf, sizeof(buf), _("Got %u groups"), qty);
	log_add (LOG_INFO, buf);
	status_item_emit_status (wdata->status, buf);

	/* add the groups we don't already have */
	if (qty)
		server_add_groups (wdata->server, groups, qty, NULL, NULL);

	/* refresh task state */
	task_state_set_health (&wdata->task->state, val);
	task_state_set_work_completed (&wdata->task->state);
	task_checkin_socket (wdata->task, channel, val==TASK_OK);

	/* cleanup */
	g_ptr_array_free (wdata->groups, TRUE);	
	g_free (wdata);
}

static void
list_newsgroups_line (GIOChannel  * channel,
                      const char  * line,
                      int           line_len,
                      gpointer      user_data)
{
	const char * pch;
	PString name = PSTRING_INIT;
	PString desc = PSTRING_INIT;
	NewgroupsWorkStruct * wdata = (NewgroupsWorkStruct*) user_data;;

	/* get the name */
	pch = line;
	while (*pch && !isspace((int)*pch)) ++pch;
	name = pstring_shallow (line, pch-line);
	desc = pstring_shallow (pch, line_len - (pch-line));
	name = pstring_strstrip_shallow (&name);
	desc = pstring_strstrip_shallow (&desc);

	/* let the user know we're still on the job */
	if (++wdata->desc_qty % 43 == 0)
		status_item_emit_status_va (wdata->status,
		                            _("Got %d group descriptions"),
		                            wdata->desc_qty);

	/* try to update the group's description field */
	if (pstring_is_set(&name) && pstring_is_set(&desc))
	{
		int i;
		gboolean exact_match = FALSE;

		i = lower_bound (&name,
		                 wdata->groups->pdata,
		                 wdata->groups->len,
				 sizeof(gpointer),
				 compare_name_ppgroup,
				 &exact_match);
		if (exact_match)
		{
			Group * g = GROUP(g_ptr_array_index(wdata->groups,i));
			replace_gstr (&g->description, g_strndup(desc.str,desc.len));
		}
	}
}

static void
list_ran (GIOChannel      * channel,
          TaskStateEnum     val,
          gpointer          user_data)
{
	NewgroupsWorkStruct * wdata = (NewgroupsWorkStruct*) user_data;

	if (val != TASK_OK)
	{
		task_state_set_health (&wdata->task->state, val);
		task_state_set_work_need_socket (&wdata->task->state, wdata->task->server, list_run);
		task_checkin_socket (wdata->task, channel, FALSE);

		g_ptr_array_free (wdata->groups, TRUE);
		g_free (wdata);
	}
	else
	{
		const char * pch = _("Fetching group descriptions");
		log_add (LOG_INFO, pch);
		status_item_emit_status (wdata->status, pch);

		g_ptr_array_sort_with_data (wdata->groups,
		                            compare_ppgroup_ppgroup,
		                            NULL);

		nntp_list_newsgroups (channel,
		                      &wdata->task->abort_flag,
		                      list_newsgroups_line, wdata,
		                      list_newsgroups_ran, wdata);
	}
}

static void
list_line (GIOChannel  * channel,
           const char  * line,
           int           line_len,
           gpointer      user_data)
{
	char * name;
	char * post;
	const char * march;
	Group * group;
	NewgroupsWorkStruct * wdata = (NewgroupsWorkStruct*) user_data;
	GPtrArray * groups = wdata->groups;

	/* parse the data line */
	march = line;
	name = get_next_token_str (march, ' ', &march);
	skip_next_token (march, ' ', &march); /* skip low number */
	skip_next_token (march, ' ', &march); /* skip high number */
	post = get_next_token_str (march, ' ', &march);

	/* create a new group object */
	group = group_new (wdata->server, name);
	group->permission = *post;
	group_set_dirty (group); /* make sure it gets saved */
	g_ptr_array_add (groups, group);

	/* periodic status feedback */
	if (!(groups->len % 100))
		status_item_emit_status_va (wdata->status,
			_("Got %d groups from server \"%s\""),
			(int)groups->len,
			server_get_name (wdata->server));

	/* cleanup */
	g_free (name);
	g_free (post);
}

static void
list_run (Task * task, GIOChannel * channel)
{
	Server * server = task->server;
	NewgroupsWorkStruct * wdata;

	task_state_set_work_working (&task->state);

	status_item_emit_status_va (STATUS_ITEM(task),
		_("Getting %d new groups from server \"%s\""),
		0, server_get_name (server));

	wdata = g_new (NewgroupsWorkStruct, 1);
	wdata->desc_qty = 0;
	wdata->status = STATUS_ITEM(task);
	wdata->task = task;
	wdata->server = task->server;
	wdata->groups = g_ptr_array_new ();

	nntp_list (channel,
	           &task->abort_flag,
	           list_line, wdata,
	           list_ran, wdata);
}
