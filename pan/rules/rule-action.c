/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <ctype.h>
#include <stdlib.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/group.h>
#include <pan/base/log.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/article-actions.h>
#include <pan/queue.h>
#include <pan/flagset.h>
#include <pan/task-bodies.h>
#include <pan/task-save.h>
#include <pan/article-actions.h>

#include <pan/rules/rule-action.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PRIVATE
************/

/*****
******
*****/

/*****
******
*****/

/*****
******
*****/

/************
*************  PROTECTED
************/

void
rule_action_constructor (RuleAction * r)
{
	g_return_if_fail (r!=NULL);

	pan_object_constructor (PAN_OBJECT(r), rule_action_destructor);
	debug1 (DEBUG_PAN_OBJECT, "rule_action_ctor: %p", r);

	r->flags = 0;
	r->alert_message = NULL;
	r->append_file = NULL;
	r->forward_to = NULL;
	r->decode_path = NULL;
	r->is_read = FALSE;
	r->is_important = FALSE;
	r->is_protected = FALSE;
}

void
rule_action_destructor (PanObject * obj)
{
	RuleAction * r = RULE_ACTION(obj);

	debug1 (DEBUG_PAN_OBJECT, "rule_action_dtor: %p", r);
	g_free (r->alert_message);
	g_free (r->append_file);
	g_free (r->forward_to);
	g_free (r->decode_path);

	/* free the superclass */
	pan_object_destructor (obj);
}

/************
*************  PUBLIC
************/

RuleAction*
rule_action_new (void)
{
	RuleAction * r = g_new0 (RuleAction, 1);
	rule_action_constructor (r);
	return r;
}

/*****
******
*****/

void
rule_action_clear (RuleAction * a)
{
	g_return_if_fail (a != NULL);

	a->flags = 0;
	replace_gstr (&a->alert_message, NULL);
	replace_gstr (&a->append_file, NULL);
	replace_gstr (&a->forward_to, NULL);
	replace_gstr (&a->decode_path, NULL);
	a->is_read = FALSE;
	a->is_important = FALSE;
	a->is_protected = FALSE;
}

/*****
******
*****/

RuleAction*
rule_action_dup (const RuleAction * src)
{
	RuleAction * tgt;

	g_return_val_if_fail (src!=NULL, NULL);

	tgt = rule_action_new ();

	/* duplicate this object's bits */
	tgt->flags          = src->flags;
	tgt->alert_message  = g_strdup (src->alert_message);
	tgt->append_file    = g_strdup (src->append_file);
	tgt->forward_to     = g_strdup (src->forward_to);
	tgt->decode_path    = g_strdup (src->decode_path);
	tgt->is_read        = src->is_read;
	tgt->is_important   = src->is_important;
	tgt->is_protected   = src->is_protected;

	return tgt;
}

/*****
******
*****/

void
rule_action_apply (const RuleAction * action,
                   GPtrArray * articles,
		   gboolean * deleted,
		   const gchar * rule_name)
{
	guint i;

	/* sanity checks */
	g_return_if_fail (action!=NULL);
	g_return_if_fail (articles!=NULL);
	g_return_if_fail (deleted!=NULL);
	for (i=0; i!=articles->len; ++i) {
		Article * a = ARTICLE(g_ptr_array_index(articles,i));
		g_return_if_fail (article_is_valid (a));
		g_return_if_fail (a->group != NULL);
	}

	/* if there are no articles, there's nothing to do. */
	if (articles->len < 1)
		return;

	*deleted = FALSE;

	if (action->flags & RULE_ACTION_DISCARD)
	{
		article_action_delete_articles ((Article**)articles->pdata, articles->len, NULL);
		*deleted = TRUE;
		return;
	}
	if (action->flags & RULE_ACTION_SHOW_ALERT)
	{
		const Group * g = ARTICLE(g_ptr_array_index(articles,0))->group;
		char * msg = action->alert_message;
		char * s = g_strdup_printf (_("%s\n\nMatches: %u\nGroup: %s\nRule: %s"),
			                    msg ? msg : "",
			                    articles->len,
			                    group_get_name(g),
			                    rule_name);
		log_add_va (LOG_URGENT|LOG_INFO, s);
		g_free (s);
	}
	if (action->flags & RULE_ACTION_MARK_READ)
	{
		articles_set_read ((Article**)articles->pdata, articles->len, TRUE);
	}
	if (action->flags & RULE_ACTION_MARK_UNREAD)
	{
		articles_set_read ((Article**)articles->pdata, articles->len, FALSE);
	}
	if (action->flags & RULE_ACTION_DECODE)
	{
		guint i;
		for (i=0; i!=articles->len; ++i)
		{
			PanObject * task = task_save_new_from_article (ARTICLE(g_ptr_array_index(articles,i)));
			task_save_set_attachments (TASK_SAVE(task), action->decode_path, NULL);
			queue_add (TASK(task));
		}
	}
	if (action->flags & RULE_ACTION_TAG_FOR_RETRIEVAL)
	{
		flagset_add_articles ((const Article**)articles->pdata, articles->len);
	}
	if (action->flags & RULE_ACTION_RETREIVE_BODY)
	{
		Task * task;
		task = TASK(task_bodies_new_from_articles ((const Article**)articles->pdata, articles->len));
		if (task != NULL)
			queue_add (task);
	}
	if (action->flags & RULE_ACTION_WATCH)
	{
		guint i;
		for (i=0; i!=articles->len; ++i) {
			const Article * a = (const Article*)(g_ptr_array_index (articles, i));
			article_action_watch_thread (a);
		}
	}
	if (action->flags & RULE_ACTION_IGNORE)
	{
		guint i;
		for (i=0; i!=articles->len; ++i) {
			const Article * a = (const Article*)(g_ptr_array_index (articles, i));
			article_action_ignore_thread (a);
		}
	}
}
