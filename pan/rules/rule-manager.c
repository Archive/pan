/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <string.h>
#include <stdlib.h>

#include <glib.h>

#include <pan/base/base-prefs.h>
#include <pan/base/debug.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>

#include <pan/filters/filter-manager.h>

#include <pan/rules/rule.h>
#include <pan/rules/rule-manager.h>
#include <pan/rules/rule-xml.h>

/***
****
***/

static GPtrArray * _rules = NULL;

static gboolean dirty = FALSE;

static gchar * delete_bozo_rule_name;

static gchar * delete_spam_rule_name;

/****
*****  PRIVATE
****/
 
static gchar*
get_rule_filename (void)
{
	return g_build_filename (get_data_dir(), "rules.xml", NULL);
}

/***
****
***/

static void
rule_manager_module_init (void)
{
	gchar * fname;
	debug_enter ("rule_manager_module_init");

	delete_bozo_rule_name = _("Delete Articles by Bozos");
	delete_spam_rule_name = _("Delete Spam");

	/* load from disk */
	_rules = g_ptr_array_new ();
	fname = get_rule_filename ();
	rule_xml_read (fname, _rules);
	g_free (fname);

	debug_exit ("rule_manager_module_init");
}

static void
rule_manager_save_if_dirty (void)
{
	debug_enter ("rule_manager_save_if_dirty");

	if (dirty)
	{
		gchar * fname = get_rule_filename ();
		rule_xml_write (fname, (const Rule**)_rules->pdata, _rules->len);
		g_free (fname);
	}

	dirty = FALSE;

	debug_exit ("rule_manager_save_if_dirty");
}

/***
****
***/

void
rule_manager_process_incoming_articles (GPtrArray * articles)
{
	guint i;
	debug_enter ("rule_manager_process_incoming_articles");

	g_return_if_fail (articles!=NULL);

	if (_rules == NULL)
		rule_manager_module_init ();

	for (i=0; i!=_rules->len; ++i) {
		Rule * r = RULE(g_ptr_array_index(_rules,i));
		if (r->apply_to_incoming)
			rule_apply (r, articles);
	}

	debug_exit ("rule_manager_process_incoming_articles");
}

/***
****
***/

void
rule_manager_get_rules (GPtrArray * fillme)
{
	gint i;
	debug_enter ("rule_manager_get_rules");

	if (_rules == NULL)
		rule_manager_module_init ();

	for (i=0; i<_rules->len; ++i) {
		Rule * r = RULE(g_ptr_array_index(_rules,i));
		g_ptr_array_add (fillme, rule_dup(r));
	}

	debug_exit ("rule_manager_get_rules");
}

void
rule_manager_set_rules (GPtrArray * rules)
{
	guint i;
	GPtrArray * old;
	GPtrArray * tmp;
	debug_enter ("rule_manager_set_rules");

	/* make copies of the new */
	tmp = g_ptr_array_new ();
	for (i=0; i<rules->len; ++i) {
		Rule * r = RULE(g_ptr_array_index(rules,i));
		Rule * dup = rule_dup (r);
		g_ptr_array_add (tmp, dup);
	}

	/* empty out the old */
	old = _rules;
	_rules = tmp;
	if (old != NULL) {
		pan_g_ptr_array_foreach (old, (GFunc)pan_object_unref, NULL);
		g_ptr_array_free (old, TRUE);
	}

	/* save them to disk */
	dirty = TRUE;
	rule_manager_save_if_dirty ();

	/* fire a notice to listeners */
	pan_callback_call (rule_manager_get_rules_changed_callback(),
	                   _rules, NULL);

	debug_exit ("rule_manager_set_rules");
}

void
rule_manager_shutdown_module (void)
{
	debug_enter ("rule_manager_shutdown_module");

	if (_rules != NULL)
	{
		guint i;

		/* see if the rule manager, or the rules, have changed */
		if (!dirty) {
			guint i;
			for (i=0; i!=_rules->len && !dirty; ++i)
				if (RULE(g_ptr_array_index(_rules,i))->dirty)
					dirty = TRUE;
		}

		/* maybe save */	
		rule_manager_save_if_dirty ();

		/* clean out the rules */
		for (i=0; i!=_rules->len; ++i)
			pan_object_unref (PAN_OBJECT(g_ptr_array_index(_rules,i)));
		g_ptr_array_free (_rules, TRUE);
		_rules = NULL;
	}

	debug_exit ("rule_manager_shutdown_module");
}

/***
****
***/

GPtrArray *
rule_manager_find_filter (const gchar *filter_name)
{
	guint       i;
	GPtrArray * used = g_ptr_array_new();
	debug_enter ("rule_manager_using_filter");

	if (_rules == NULL)
		rule_manager_module_init ();

	for (i=0; i!=_rules->len; ++i)
	{
		Rule * r = RULE(g_ptr_array_index(_rules,i));
		if (pan_strcmp(r->filter_name, filter_name) == 0)
			g_ptr_array_add (used, g_strdup(r->name));
	}

	debug_exit ("rule_manager_using_filter");
	return used;
}

/***
****
***/

PanCallback*
rule_manager_get_rules_changed_callback (void)
{
	static PanCallback * cb = NULL;
	if (cb==NULL) cb = pan_callback_new ();
	return cb;
}
