/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <stdlib.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/group.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/log.h>

#include <pan/filters/filter-manager.h>

#include <pan/rules/rule.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PRIVATE
************/

/*****
******
*****/

/*****
******
*****/

/*****
******
*****/

/************
*************  PROTECTED
************/

void
rule_constructor (Rule * r)
{
	g_return_if_fail (r!=NULL);

	pan_object_constructor (PAN_OBJECT(r), rule_destructor);
	debug1 (DEBUG_PAN_OBJECT, "rule_ctor: %p", r);

	r->name = NULL;
	r->hits = 0;
	r->tries = 0;
	r->apply_to_incoming = TRUE;
	r->dirty = FALSE;

	r->group_type = RULE_GROUP_ALL;
	r->group_wildcard = NULL;
	r->group_list = NULL;

	r->filter_name = NULL;

	r->action = rule_action_new ();
}

void
rule_destructor (PanObject * obj)
{
	Rule * r = RULE(obj);

	debug1 (DEBUG_PAN_OBJECT, "rule_dtor: %p", r);

	/* free misc */
	g_free (r->name);

	/* free the group wildcard string */
	g_free (r->group_wildcard);
	r->group_wildcard = NULL;

	/* free the group list */
	if (r->group_list != NULL) {
		pan_g_ptr_array_foreach (r->group_list, (GFunc)g_free, NULL);
		g_ptr_array_free (r->group_list, TRUE);
		r->group_list = NULL;
	}

	/* free the filter */
	replace_gstr (&r->filter_name, NULL);

	/* free the action */
	pan_object_unref (PAN_OBJECT(r->action));
	r->action = NULL;

	/* free the superclass */
	pan_object_destructor (obj);
}

/************
*************  PUBLIC
************/

Rule*
rule_new (void)
{
	Rule * r = g_new0 (Rule, 1);
	rule_constructor (r);
	return r;
}

/*****
******
*****/

Rule*
rule_dup (const Rule * rule)
{
	Rule * r;

	g_return_val_if_fail (rule!=NULL, NULL);

	r = rule_new ();
	r->dirty = TRUE;
	r->apply_to_incoming = rule->apply_to_incoming;
	r->hits = rule->hits;
	r->tries = rule->tries;
	r->name = g_strdup (rule->name);
	r->filter_name = g_strdup (rule->filter_name);
	r->action = rule_action_dup (rule->action);

	/* group */
	r->group_type = rule->group_type;
	r->group_wildcard = g_strdup (rule->group_wildcard);
	if (rule->group_list != NULL) {
		gint i;
		r->group_list = g_ptr_array_new ();
		for (i=0; i<rule->group_list->len; ++i)
			g_ptr_array_add (r->group_list, g_strdup((gchar*)g_ptr_array_index(rule->group_list,i)));
	}

	return r;
}

/*****
******
*****/

gboolean
rule_test_group (const Rule    * rule,
                 const PString * groupname)
{
	gboolean retval = FALSE;

	g_return_val_if_fail (rule!=NULL, FALSE);
	g_return_val_if_fail (pstring_is_set (groupname), FALSE);

	switch (rule->group_type)
	{
		case RULE_GROUP_ALL:
			retval = TRUE;
			break;

		case RULE_GROUP_WILDCARD:
			retval = g_pattern_match_simple (rule->group_wildcard, groupname->str);
			break;

		case RULE_GROUP_LIST:
		{
			guint i;
			for (i=0; rule->group_list!=NULL && i!=rule->group_list->len; ++i) {
				const gchar * name = (const char*) g_ptr_array_index (rule->group_list,i);
				if (!g_strcasecmp(groupname->str,name)) {
					retval = TRUE;
					break;
				}
			}
			break;
		}
		default:
			pan_warn_if_reached ();
	}

	return retval;
}

static void
rule_remove_group_failures (const Rule * rule,
                            GPtrArray * articles)
{
	guint i;

	g_return_if_fail (rule!=NULL);
	g_return_if_fail (articles!=NULL);

	if (rule->group_type == RULE_GROUP_ALL) /* no failures */
		return;

	for (i=0; i!=articles->len; )
	{
		Group * group = ARTICLE(g_ptr_array_index(articles,i))->group;
		gboolean pass = rule_test_group (rule, &group->name);

		if (pass)
			++i;
		else
			g_ptr_array_remove_index_fast (articles, i);
	}
}

guint
rule_apply (Rule * rule, GPtrArray * articles)
{
	Filter * filter;
	GPtrArray * tmp;
	gboolean deleted;
	guint matched;

	/* sanity checks */
	g_return_val_if_fail (rule!=NULL, 0);
	g_return_val_if_fail (articles!=NULL, 0);

	/* create a scratch copy of the articles */
	tmp = g_ptr_array_new ();
	pan_g_ptr_array_assign (tmp, articles->pdata, articles->len);

	/* weed out non-matches */
	rule_remove_group_failures (rule, tmp);
	filter = filter_manager_get_named_filter (rule->filter_name);
	if (filter == NULL) {
		tmp->len = 0;
		log_add_va (LOG_ERROR, _("Can't find filter \"%s\" needed by rule \"%s\", so no articles match."),
			rule->filter_name ? rule->filter_name : "",
			rule->name);
	} else {
		filter_remove_failures (filter, tmp);
		pan_object_unref (PAN_OBJECT(filter));
	}

	/* log results */
	if (tmp->len != 0)
		log_add_va (LOG_INFO, _("%u articles match rule %s"), tmp->len, rule->name);

	/* update the stats */
	rule->tries += articles->len;
	rule->hits += tmp->len;
	rule->dirty = TRUE;

	/* apply the action to the remaining articles */
	deleted = FALSE;
	rule_action_apply (rule->action, tmp, &deleted, rule->name);
	if (deleted) {
		guint i;
		for (i=0; i!=tmp->len; ++i) {
			gpointer p = g_ptr_array_index(tmp,i);
			g_ptr_array_remove_fast (articles, p);
		}
	}

	/* cleanup */
	matched = tmp->len;
	g_ptr_array_free (tmp, TRUE);

	return matched;
}


/*****
******
*****/
