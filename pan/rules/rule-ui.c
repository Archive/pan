/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/argset.h>
#include <pan/base/debug.h>
#include <pan/base/base-prefs.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/server.h>
#include <pan/base/serverlist.h>
#include <pan/base/status-item.h>

#include <pan/rules/rule.h>
#include <pan/rules/rule-ui.h>
#include <pan/rules/rule-edit-ui.h>
#include <pan/rules/rule-manager.h>

#include <pan/grouplist.h>
#include <pan/articlelist.h>
#include <pan/util.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

typedef struct
{
	GtkWidget * rule_clist;
	GtkWidget * dialog;
	GPtrArray * rules;

	GtkWidget * edit_rule_button;
	GtkWidget * remove_rule_button;
	GtkWidget * apply_selected_rule_button;
	GtkWidget * apply_subscribed_rule_button;
}
RuleListDialog;

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PRIVATE
************/

static gint
get_selected_index (RuleListDialog * d)
{
	gint sel;
	GtkCList * clist;

	/* find the selected rule */
	clist = GTK_CLIST(d->rule_clist);
	sel = -1;
	if (clist->selection != NULL)
		sel = GPOINTER_TO_INT(clist->selection->data);

	return sel;
}                                                                                                                               

static void
button_refresh (RuleListDialog * d)
{
	const gint sel = get_selected_index (d);
	const gboolean have_sel = sel != -1;

	gtk_widget_set_sensitive (d->edit_rule_button, have_sel);
	gtk_widget_set_sensitive (d->remove_rule_button, have_sel);
	gtk_widget_set_sensitive (d->apply_selected_rule_button, have_sel);
	gtk_widget_set_sensitive (d->apply_subscribed_rule_button, have_sel);
}

static void
clist_refresh (RuleListDialog * d)
{
	gint i;
	const gint sel = get_selected_index (d);
	GtkCList * clist = GTK_CLIST(d->rule_clist);

	gtk_clist_freeze (clist);
	gtk_clist_clear (clist);
	for (i=0; i<d->rules->len; ++i)
	{
		gint row;
		gchar * cols[2];
		Rule * r;
		double percent;
		
		r = RULE(g_ptr_array_index(d->rules,i));
		percent = r->tries==0 ? 0 : ((double)r->hits / (double)r->tries);
		cols[0] = r->name;
		cols[1] = g_strdup_printf ("%d%%", (int)(percent*100.0));

	       	row = gtk_clist_insert (clist, -1, cols);
		gtk_clist_set_row_data (clist, row, r);

		g_free (cols[1]);
	}
	gtk_clist_select_row (clist, sel, 0);
	gtk_clist_thaw (clist);
}

/*****
******
*****/

static void
rule_dialog_response_cb (GtkDialog * dialog, gint arg1, gpointer data)
{
	RuleListDialog * d = (RuleListDialog*) data;
	debug_enter ("rule_dialog_close_cb");

	/* save changed rules */
	rule_manager_set_rules (d->rules);

	/* cleanup rules */
	if (d->rules != NULL) {
		pan_g_ptr_array_foreach (d->rules, (GFunc)pan_object_unref, NULL);
		g_ptr_array_free (d->rules, TRUE);
		d->rules = NULL;
	}

	/* cleanup dialog */
	gtk_widget_destroy (GTK_WIDGET(dialog));
	g_free (d);
	debug_exit ("rule_dialog_close_cb");
}


/**
***  Add
**/

static void
remove_button_clicked_cb (GtkButton * button, gpointer data)
{
	RuleListDialog * d = (RuleListDialog*) data;
	gint sel = get_selected_index (d);
	if (sel != -1)
	{
		Rule * f = RULE(g_ptr_array_index(d->rules, sel));
		g_ptr_array_remove_index (d->rules, sel);
		pan_object_unref (PAN_OBJECT(f));
		clist_refresh (d);
		button_refresh (d);
	}
}

static void
add_dialog_response_cb (GtkDialog * dialog, gint response, gpointer data)
{
	if (response == GTK_RESPONSE_OK || response == GTK_RESPONSE_APPLY)
	{
		guint i;
		GtkWidget * w = GTK_WIDGET(dialog);
		RuleListDialog * d = (RuleListDialog*) data;
		Rule * r_new = rule_edit_dialog_get_rule(w);
g_message ("new rule name: [%s]", r_new->name);

		/* see if we've already got this rule */
		for (i=0; i<d->rules->len; ++i) {
			Rule * r = RULE(g_ptr_array_index(d->rules,i));
			if (!pan_strcmp(r_new->name, r->name))
				break;
		}
g_message ("do we already have this? %d", i!=d->rules->len);

		/* either insert or update */
		if (i == d->rules->len) {
			g_ptr_array_add (d->rules, r_new);
		} else {
			Rule * f_old = RULE(g_ptr_array_index(d->rules,i));
			g_ptr_array_index (d->rules,i) = r_new;
			pan_object_unref (PAN_OBJECT(f_old));
		}

		/* rebuild the clist */
		clist_refresh (d);
	}

	if (response != GTK_RESPONSE_APPLY)
		gtk_widget_destroy (GTK_WIDGET(dialog));
}
static void
add_button_clicked_cb (GtkButton * button, gpointer data)
{
	RuleListDialog * f = (RuleListDialog*) data;
	GtkWidget * dialog = rule_edit_dialog_create (GTK_WINDOW(f->dialog), NULL);
	g_signal_connect (GTK_OBJECT(dialog), "response", G_CALLBACK(add_dialog_response_cb), f);
	gtk_widget_show_all (dialog);
}

/**
***  Edit
**/

static void
edit_dialog_response_cb (GtkDialog * dialog, gint response, gpointer data)
{
	if (response == GTK_RESPONSE_OK || response == GTK_RESPONSE_APPLY)
	{
		RuleListDialog * d = (RuleListDialog*) data;
		const gchar * e_name = (const gchar*) gtk_object_get_data (GTK_OBJECT(dialog), "edit_name");
		Rule * r_new = rule_edit_dialog_get_rule (GTK_WIDGET(dialog));
		gint i;

		/* find the index of the rule being edited */
		for (i=0; i<d->rules->len; ++i)
			if (!pan_strcmp(RULE(g_ptr_array_index(d->rules,i))->name, e_name))
				break;

		/* replace the edited rule with the new one */
		if (i!=d->rules->len) {
			gpointer old = g_ptr_array_index (d->rules, i);
			g_ptr_array_index (d->rules, i) = r_new;
			pan_object_unref (PAN_OBJECT(old));
			clist_refresh (d);
		}

		/* update the edit dialog */
		gtk_object_set_data_full (GTK_OBJECT(dialog), "edit_name", g_strdup(r_new->name), g_free);
	}

	if (response != GTK_RESPONSE_APPLY)
		gtk_widget_destroy (GTK_WIDGET(dialog));
}
static void
edit_button_clicked_cb (GtkButton * button, gpointer data)
{
	RuleListDialog * d = (RuleListDialog*) data;
	gint sel = get_selected_index (d);
	if (sel != -1)
	{
		const Rule * r_old = RULE(g_ptr_array_index(d->rules,sel));
		GtkWidget * dialog = rule_edit_dialog_create (GTK_WINDOW(d->dialog), r_old);
		gtk_signal_connect (GTK_OBJECT(dialog), "response", GTK_SIGNAL_FUNC(edit_dialog_response_cb), d);
		gtk_object_set_data_full (GTK_OBJECT(dialog), "edit_name", g_strdup(r_old->name), g_free);
		gtk_widget_show_all (dialog);
	}
}

static gboolean
rule_clist_button_press_cb (GtkWidget * w, GdkEventButton * b, gpointer data)
{
	if (b->button==1 && b->type==GDK_2BUTTON_PRESS)
		edit_button_clicked_cb (NULL, data);
	return FALSE;
}

static void
list_selection_changed_cb (GtkCList          * clist,
                           gint                row,
                           gint                column,
                           GdkEventButton    * event,
                           gpointer            user_data)
{
	button_refresh ((RuleListDialog*)user_data);
}

/**
***  Apply
**/

typedef struct
{
	GPtrArray * groups;
	Rule * rule;
}
ApplyNow;

static void
apply_now_describe (const StatusItem * item, char * buf, int buflen)
{
	g_strlcpy (buf, _("Applying rule to selected groups"), buflen);
}

static void
apply_rule_to_group_articles (Group        * group,
                              Article     ** articles,
                              guint          article_qty,
                              gpointer       rule_gpointer)
{
	if (article_qty > 0u)
	{
		GPtrArray * articles_gpa = g_ptr_array_sized_new (article_qty);
		pan_g_ptr_array_assign (articles_gpa, (gpointer*)articles, article_qty);
		rule_apply ((Rule*)rule_gpointer, articles_gpa);
		g_ptr_array_free (articles_gpa, TRUE);
	}
}

static gboolean
apply_now_thread (void * data)
{
	StatusItem * status;
	ApplyNow * now = (ApplyNow*) data;
	Rule * rule = now->rule;
	GPtrArray * groups = (GPtrArray *) now->groups;
	guint i;

	/* add a gui feedback tool */
	status = STATUS_ITEM(status_item_new(apply_now_describe));
	status_item_set_active (status, TRUE);
	status_item_emit_init_steps (status, groups->len);

	/* process each group in turn */
	for (i=0; i!=groups->len; ++i)
	{
		Group * g = GROUP(g_ptr_array_index(groups,i));

		status_item_emit_status_va (status, _("Loading group \"%s\""), group_get_name(g));
		group_ref_articles (g, NULL);

		status_item_emit_status_va (status, _("Applying rule \"%s\" to group \"%s\""), rule->name, group_get_name(g));
		group_article_forall (g, apply_rule_to_group_articles, rule);

		status_item_emit_status_va (status, _("Saving \"%s\""), group_get_name(g));
		group_unref_articles (g, NULL);

		status_item_emit_next_step (status);
	}

	/* cleanup ui */
	status_item_set_active (status, FALSE);
	pan_object_unref (PAN_OBJECT(status));

	/* cleanup memory */
	g_ptr_array_free (groups, TRUE);
	g_free (data);
	return FALSE;
}


static void
apply_rules_impl (RuleListDialog * d, GPtrArray * groups)
{
	Rule * rule = NULL;
	gint sel = get_selected_index (d);

	if (sel != -1)
		rule = RULE(g_ptr_array_index(d->rules, sel));

	if (rule != NULL)
	{
		ApplyNow * now;

		now = g_new (ApplyNow, 1);
		now->groups = pan_g_ptr_array_dup (groups);
		now->rule = rule;

		g_idle_add (apply_now_thread, now);
	}
}

static void
apply_to_selected_cb (GtkButton * button, gpointer data)
{
	GPtrArray * groups = grouplist_get_selected_groups ();
	RuleListDialog * d = (RuleListDialog*) data;

	apply_rules_impl (d, groups);

	g_ptr_array_free (groups, TRUE);
}

static void
apply_to_subscribed_cb (GtkButton * button, gpointer data)
{
	Server * s;
	GPtrArray * groups;
	RuleListDialog * d = (RuleListDialog*) data;

	s = serverlist_get_active_server ();
	groups = server_get_groups (s, SERVER_GROUPS_SUBSCRIBED);
	apply_rules_impl (d, groups);

	g_ptr_array_free (groups, TRUE);
}

/************
*************  PROTECTED
************/

/************
*************  PUBLIC
************/

GtkWidget*
rule_dialog_new (GtkWindow * window)
{
	GtkWidget * w;
	GtkWidget * hbox;
	GtkWidget * bbox;
	RuleListDialog * d = g_new0 (RuleListDialog, 1);
	gchar * titles [2];
	debug_enter ("rule_dialog_new");

	/* load rules */
	d->rules = g_ptr_array_new ();
	rule_manager_get_rules (d->rules);

	/* dialog */
	w = d->dialog = gtk_dialog_new_with_buttons (_("Pan: Rules"), window,
	                                             GTK_DIALOG_DESTROY_WITH_PARENT,
	                                             GTK_STOCK_CLOSE, GTK_RESPONSE_NONE,
	                                             NULL);
	gtk_window_set_policy (GTK_WINDOW(w), TRUE, TRUE, TRUE);
	gtk_signal_connect (GTK_OBJECT(w), "response", G_CALLBACK(rule_dialog_response_cb), d);

	/* workarea */
	hbox = gtk_hbox_new (FALSE, GUI_PAD);
	gtk_container_set_border_width (GTK_CONTAINER(hbox), 12);
	gtk_box_pack_start (GTK_BOX(GTK_DIALOG(w)->vbox), hbox, TRUE, TRUE, 0);

	/* clist */
	titles[0] = _("Rules");
	titles[1] = _("Hit Ratio");
	w = d->rule_clist= gtk_clist_new_with_titles (2, titles);
	gtk_clist_set_selection_mode (GTK_CLIST(w), GTK_SELECTION_BROWSE);
	gtk_clist_set_column_width (GTK_CLIST(w), 0, 300);
	gtk_clist_set_column_width (GTK_CLIST(w), 1, 100);
	gtk_widget_set_usize (w, 400, -2);
	gtk_signal_connect (GTK_OBJECT(w), "button_press_event",
	                    GTK_SIGNAL_FUNC(rule_clist_button_press_cb), d);
	gtk_signal_connect (GTK_OBJECT(w), "select_row",
	                    GTK_SIGNAL_FUNC(list_selection_changed_cb), d);
	gtk_signal_connect (GTK_OBJECT(w), "unselect_row",
	                    GTK_SIGNAL_FUNC(list_selection_changed_cb), d);
        w = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(w), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER(w), d->rule_clist);
	gtk_box_pack_start (GTK_BOX (hbox), w, TRUE, TRUE, 0);
	gtk_widget_set_usize (w, -2, 300);

	/* button box */
	bbox = gtk_vbox_new (FALSE, GUI_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (hbox), bbox, FALSE, FALSE, 0);

	/* add button */
	w = gtk_button_new_from_stock (GTK_STOCK_ADD);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT(w), "clicked", GTK_SIGNAL_FUNC(add_button_clicked_cb), d);

	/* edit button */
	w = gtk_button_new_from_stock (GTK_STOCK_OPEN);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT(w), "clicked", GTK_SIGNAL_FUNC(edit_button_clicked_cb), d);
	d->edit_rule_button = w;

	/* remove button */
	w = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	gtk_box_pack_start (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT(w), "clicked", GTK_SIGNAL_FUNC(remove_button_clicked_cb), d);
	d->remove_rule_button = w;

	/* apply to selected */
	w = gtk_button_new_with_label (_("Apply to\nSelected\nGroups"));
	gtk_box_pack_end (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT(w), "clicked", GTK_SIGNAL_FUNC(apply_to_selected_cb), d);
	d->apply_selected_rule_button = w;

	/* apply to subscribed */
	w = gtk_button_new_with_label (_("Apply to\nSubscribed\nGroups"));
	gtk_box_pack_end (GTK_BOX (bbox), w, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT(w), "clicked", GTK_SIGNAL_FUNC(apply_to_subscribed_cb), d);
	d->apply_subscribed_rule_button = w;

	clist_refresh (d);
	button_refresh (d);
	debug_exit ("rule_dialog_new");
	return d->dialog;
}
