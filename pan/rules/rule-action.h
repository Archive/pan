/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __RULE_ACTION_H__
#define __RULE_ACTION_H__

#include <glib.h>

#include <pan/base/article.h>
#include <pan/base/pan-object.h>

#define RULE_ACTION(a) ((RuleAction *)a)

typedef struct _RuleAction   RuleAction;

typedef enum
{
	RULE_ACTION_SHOW_ALERT		= 1<<1,
	RULE_ACTION_MARK_READ		= 1<<4,
	RULE_ACTION_MARK_UNREAD		= 1<<5,
	RULE_ACTION_DECODE		= 1<<10,
	RULE_ACTION_TAG_FOR_RETRIEVAL	= 1<<11,
	RULE_ACTION_RETREIVE_BODY	= 1<<12,
	RULE_ACTION_WATCH               = 1<<13,
	RULE_ACTION_DISCARD		= 1<<15,
	RULE_ACTION_IGNORE              = 1<<16,
}
RuleActionFlags;


struct _RuleAction
{
	/* parent class */
	PanObject parent;

	/* rule action */
	gulong flags;
	char * alert_message;
	char * append_file;
	char * forward_to;
	char * decode_path;
	gboolean is_read;
	gboolean is_important;
	gboolean is_protected;
};

/**
***  PROTECTED
**/

void         rule_action_constructor  (RuleAction        * r);

void         rule_action_destructor   (PanObject         * o);

/**
***  PUBLIC - Life Cycle
**/

RuleAction*  rule_action_new          (void);

RuleAction*  rule_action_dup          (const RuleAction  * r);

void         rule_action_clear        (RuleAction        * r);

/**
***  PUBLIC
**/

void         rule_action_apply        (const RuleAction  * r,
                                       GPtrArray         * articles,
                                       gboolean          * deleted,
                                       const char        * rule_name);


#endif /* __RULE_ACTION_H__ */
