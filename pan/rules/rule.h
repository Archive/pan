
/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __RULE_H__
#define __RULE_H__

#include <time.h>
#include <glib.h>

#include <pan/base/article.h>
#include <pan/base/pan-object.h>

#include <pan/rules/rule-action.h>

#define RULE(a) ((Rule *)a)

typedef struct _Rule           Rule;

struct _Rule
{
	/* parent class */
	PanObject parent;

	/* bookkeeping */
	guint dirty              : 1;
	guint apply_to_incoming  : 1;
	gint32 hits;
	gint32 tries;
	gchar * name;

	/* private fields - groups */
	gint8 group_type;
	gchar * group_wildcard;
	GPtrArray * group_list;

	/* private fields - filter */
	gchar * filter_name;

	/* private fields - actions */
	RuleAction * action;
};

/**
***  PROTECTED
**/

enum { RULE_GROUP_WILDCARD, RULE_GROUP_LIST, RULE_GROUP_ALL };

void         rule_constructor          (Rule * r);

void         rule_destructor           (PanObject * o);

/**
***  PUBLIC - Life Cycle
**/

Rule*        rule_new                  (void);

Rule*        rule_dup                  (const Rule * r);

/**
***  PUBLIC
**/

gboolean     rule_test_group           (const Rule     * rule,
                                        const PString  * groupname);

/**
 * Apply the rule to a set of articles.
 *
 * Articles deleted as a result are removed from the "articles"
 * array before this function returns.
 *
 * The articles in "articles" must have been added to a group already,
 * because if the rule is to kill the articles, they are freed through
 * group_remove_articles().
 */
guint        rule_apply                (Rule * rule,
                                        GPtrArray * articles);


#endif /* __RULE_H__ */
