/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __RULE_MANAGER_H__
#define __RULE_MANAGER_H__

#include <glib.h>
#include <pan/base/pan-callback.h>

void rule_manager_process_incoming_articles (GPtrArray * articles);

void rule_manager_shutdown_module (void);



/**
 * Populates fillme with a set of Rules sorted by name.
 * These are just copies of what the manager has, so they
 * need to be free'd when you're done with them:
 * pan_g_ptr_array_foreach (fillme, (GFunc)pan_object_unref, NULL);
 */
void rule_manager_get_rules (GPtrArray * fillme);

/**
 * The manager copies the rules in <code>rules</code>by
 * value, not by reference, so you'll need to continue to
 * manage the objects in <code>rules</code> yourself.
 *
 * After the manager copies <code>rules</code> by reference,
 * it fires a rules-changed event.
 */
void rule_manager_set_rules (GPtrArray * rules);


/**
 * Returns an array of the newly-allocated names of rules
 * that use this filter.
 *
 * After done with this, you'll need to free the strings, as in
 * pan_g_ptr_array_foreach (array, (GFunc)g_free, NULL);
 * g_ptr_array_free (array, TRUE);
 **/
GPtrArray *rule_manager_find_filter (const gchar *filter_name);


/**
 * @call_obj: GPtrArray of const Rule*s - DO NOT KEEP!
 * @call_arg: NULL
 */
PanCallback*   rule_manager_get_rules_changed_callback   (void);

#endif /* __RULE_MANAGER_H__ */

