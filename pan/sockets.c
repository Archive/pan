/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * If this fails to compile, please submit a patch, with #ifdefs for your
 * particular operating system, to pan@rebelbase.com.
 */

/******
*******
******/

#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <glib.h>

#ifdef G_OS_WIN32
  #include <ws2tcpip.h>
  #undef gai_strerror
  #define gai_strerror(i) gai_strerror_does_not_link (i)
  static const char*
  gai_strerror_does_not_link (int errval)
  {
    static char buf[32];
    g_snprintf (buf, sizeof(buf), "Winsock error %d", errval);
    return buf;
}

#else
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <netdb.h>
  #include <arpa/inet.h>
#endif

#include <pan/base/log.h>
#include <pan/base/gnksa.h>
#include <pan/base/pan-i18n.h>
#include <pan/sockets.h>

static void
ensure_module_inited (void)
{
	static gboolean inited = FALSE;

	if (!inited)
	{
#ifdef G_OS_WIN32
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2, 0), &wsaData);
#endif
		inited = TRUE;
	}
}

GIOChannel *
pan_get_giochannel_socket (const PString   * host,
                           int               port)
{
	int err;
	int sockfd;
	char hostbuf[128];
	char portbuf[32];
	struct addrinfo hints, *ans;
	GIOStatus status;
	GIOChannel * channel;
        GList * l=NULL, *list=NULL, *ipv4_list=NULL, *ipv6_list=NULL;

	ensure_module_inited ();

	/* get an addrinfo for the host */
	g_snprintf (hostbuf, sizeof(hostbuf), "%*.*s", host->len, host->len, host->str);
	g_snprintf (portbuf, sizeof(portbuf), "%d", port);
	memset (&hints, 0, sizeof(struct addrinfo));
	hints.ai_flags = 0;
	hints.ai_family = 0;
	hints.ai_socktype = SOCK_STREAM;
	err = getaddrinfo (hostbuf, portbuf, &hints, &ans);
	if (err != 0) {
		g_message ("Lookup error: %s", gai_strerror(err));
		return NULL;
	}

	/* build lists of ipv6 and ipv4 addrinfos */
	{
		struct addrinfo *answalk;
		for (answalk=ans; answalk!=NULL; answalk=answalk->ai_next)
		{
			if (answalk->ai_family == PF_INET)
				ipv4_list = g_list_append (ipv4_list, answalk);
			else if (answalk->ai_family == PF_INET6)
				ipv6_list = g_list_append (ipv6_list, answalk);
		}
	}

	/* merge the lists, putting ipv6 addrinfos before the ipv4 */
	list = NULL;
	list = g_list_concat (list, ipv6_list);
	list = g_list_concat (list, ipv4_list);
	ipv4_list = ipv6_list = NULL;

	/* walk through the merged list until we get a good socket */
	sockfd = -1;
	for (l=list; l!=NULL; l=l->next)
	{
		struct addrinfo * ainfo = (struct addrinfo*) l->data;
		sockfd = socket (ainfo->ai_family,
		                 ainfo->ai_socktype,
		                 ainfo->ai_protocol);
		if (sockfd >= 0)
			break;
	}
	g_list_free (list);
	if (sockfd < 0) {
		g_message ("couldn't create a socket.");
		freeaddrinfo (ans);
		return NULL;
	}

	/* try to connect to the host */
	if (connect (sockfd, ans->ai_addr, ans->ai_addrlen) < 0) {
		g_message ("Connect failed: %s", g_strerror(errno));
		freeaddrinfo (ans);
		return NULL;
	}

	channel = g_io_channel_unix_new (sockfd);
#ifndef G_OS_WIN32
	status = g_io_channel_set_flags (channel, G_IO_FLAG_NONBLOCK, NULL);
#endif
	if (g_io_channel_get_encoding (channel) != NULL)
		g_io_channel_set_encoding (channel, NULL, NULL);
	g_io_channel_set_buffered (channel, TRUE);
	g_io_channel_set_line_term (channel, "\n", 1);
	log_add_va (LOG_INFO, _("New connection %p for %s, port %d"), channel, hostbuf, port);

	freeaddrinfo (ans);
	return channel;
}

gchar*
pan_get_local_fqdn (void)
{
	const char * cpch;
	const char ** aliases;
	char buf[1024];
	struct hostent * host_entry = NULL;

	ensure_module_inited ();

	if ((-1 == gethostname (buf, sizeof(buf))) || (*buf == 0))
		return NULL;

	host_entry = gethostbyname (buf);

	if ((host_entry == NULL)
		|| (host_entry->h_name == NULL)
		|| (host_entry->h_name[0] == 0))
		return NULL;

	cpch = (const char*) host_entry->h_name;
	if (gnksa_check_domain (cpch, strlen(cpch)) == GNKSA_OK)
		return g_strdup (cpch);

	aliases = (const char**) host_entry->h_aliases;
	while (aliases!=NULL && *aliases!=NULL) {
		cpch = *aliases;
		if (gnksa_check_domain (cpch, strlen(cpch)==GNKSA_OK))
			return g_strdup (cpch);
		++aliases;
	}

	return NULL;
}
