/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <string.h>

#include <glib.h>

#include <pan/base/debug.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/nntp.h>
#include <pan/task.h>

/*********************
**********************  Defines / Enumerated types
*********************/

/*********************
**********************  Macros
*********************/

/*********************
**********************  Structures / Typedefs
*********************/

/*********************
**********************  Private Function Prototypes
*********************/

/*********************
**********************  Variables
*********************/

/***********
************  Extern
***********/

/***********
************  Public
***********/

/***********
************  Private
***********/

#define DEAD_POINTER ((void*)(0xDEADBEEF))

/*********************
**********************  BEGINNING OF SOURCE
*********************/

/************
*************  PUBLIC ROUTINES
************/

/*****
******
*****/

void
task_constructor (Task                    * task,
                  gint8                     task_type,
                  PanObjectDestructor       destructor,
                  StatusItemDescribeFunc    describe,
                  Server                  * server,
                  gboolean                  high_priority)
{
	g_return_if_fail (task != NULL);

	/* construct parent's bits */
	status_item_constructor (STATUS_ITEM(task), destructor, describe);

	/* construct task's bits */
	pan_warn_if_fail (server != NULL);
	task->server = server;
	task->high_priority = high_priority;
	task->type = task_type;
	task->task_ran_callback = pan_callback_new ();
	task->identifiers = g_ptr_array_new ();
	task->checkin_funcs = g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL, g_free);
	task->thread_qty = 0;
	task->abort_flag = FALSE;
	task_state_set_health (&task->state, TASK_OK);
	task_state_set_work_completed (&task->state);

        debug1 (DEBUG_PAN_OBJECT, "task constructor: %p", task);
}

void
task_destructor (PanObject* obj)
{
	Task * task;

	/* sanity clause */
	g_return_if_fail (obj != NULL);

	/* free the task bits */
        debug1 (DEBUG_PAN_OBJECT, "task destructor: %p", obj);
	task = TASK(obj);
	pan_callback_free (&task->task_ran_callback);
	if (g_hash_table_size (task->checkin_funcs))
		g_warning ("task still has sockets");
	g_hash_table_destroy (task->checkin_funcs);
	pan_g_ptr_array_foreach (task->identifiers, (GFunc)g_object_unref, NULL);
	g_ptr_array_free (task->identifiers, TRUE);

	/* toxify the task bits */
	task->task_ran_callback = DEAD_POINTER;
	task->thread_qty = ~0;
	task->abort_flag = ~0;
	task->high_priority = ~0;
	task->type = ~0;
	task->server = DEAD_POINTER;
	task->identifiers = DEAD_POINTER;
	task->checkin_funcs = DEAD_POINTER;
	task->state.server = DEAD_POINTER;
	task->state.socket_func = DEAD_POINTER;
	task->state.work_func = DEAD_POINTER;
	task->state.work = ~0;
	task->state.health = ~0;

	/* clean up parent's bits */
	status_item_destructor (obj);
}

/*****
******
*****/

void
task_add_identifiers (Task                * task,
                      MessageIdentifier  ** identifiers,
                      int                   identifier_qty)
{
	guint i;

	/* sanity clause */
	g_return_if_fail (task!=NULL);
	g_return_if_fail (identifiers!=NULL);
	g_return_if_fail (identifier_qty>0);
	for (i=0; i<identifier_qty; ++i)
		g_return_if_fail (PAN_IS_MESSAGE_IDENTIFIER(identifiers[i]));

	/* add the message-ids */	
	for (i=0; i<identifier_qty; ++i) {
		g_object_ref (identifiers[i]);
		g_ptr_array_add (task->identifiers, identifiers[i]);
	}
}

void
task_add_identifiers_from_articles (Task                 * task,
                                    const Article       ** articles,
                                    int                    article_qty)
{
	int i;
	MessageIdentifier ** mids;

	/* sanity clause */
	g_return_if_fail (articles!=NULL);
	g_return_if_fail (article_qty>0);
	g_return_if_fail (articles_are_valid (articles, article_qty));

	/* delegate to task_add_identifiers */
	mids = g_newa (MessageIdentifier*, article_qty);
	for (i=0; i<article_qty; ++i)
		mids[i] = message_identifier_new_from_article (articles[i]);
	task_add_identifiers (task, mids, article_qty);
	for (i=0; i<article_qty; ++i)
		g_object_unref (mids[i]);
}

void
task_forall_identifiers (Task                * task,
                         TaskForallMidsFunc    forall_func,
                         gpointer              forall_func_user_data)
{
	g_return_if_fail (task!=NULL);

	(*forall_func)(task,
	               (MessageIdentifier**)task->identifiers->pdata,
	               task->identifiers->len,
	               forall_func_user_data);
}

/***
****
***/

typedef struct
{
	TaskSocketCheckinFunc checkin_func;
	gpointer checkin_func_user_data;
}
CheckinEntry;

void
task_give_socket (Task                  * task,
                  GIOChannel            * channel,
                  TaskSocketCheckinFunc   checkin_func,
                  gpointer                checkin_func_user_data)
{
	g_return_if_fail (task != NULL);
	g_return_if_fail (channel != NULL);
	g_return_if_fail (checkin_func != NULL);
	g_return_if_fail (g_hash_table_lookup(task->checkin_funcs, channel) == NULL);

	if (task->state.socket_func == NULL)
	{
		(*checkin_func)(task, channel, TRUE, checkin_func_user_data);
	}
	else
	{
		CheckinEntry * entry = g_new (CheckinEntry, 1);
		entry->checkin_func = checkin_func;
		entry->checkin_func_user_data = checkin_func_user_data;
		g_hash_table_insert (task->checkin_funcs, channel, entry);

		(*task->state.socket_func)(task, channel);
	}
}

void
task_checkin_socket (Task          * task,
                     GIOChannel    * channel,
                     gboolean        channel_ok)
{
	CheckinEntry * entry;

	g_return_if_fail (task != NULL);
	g_return_if_fail (channel != NULL);

	entry = (CheckinEntry*) g_hash_table_lookup (task->checkin_funcs, channel);
	if (entry) {
		CheckinEntry e = *entry;
		g_hash_table_remove (task->checkin_funcs, channel);
		(*e.checkin_func)(task, channel, channel_ok, e.checkin_func_user_data);
	}
}

void
task_work_gui (Task * task)
{
	g_return_if_fail (task != NULL);

	if (task->state.work_func)
	{
		(*task->state.work_func)(task);
	}
}

void
task_work_child (Task * task)
{
	/* FIXME */
	task_work_gui (task);
}

static void
task_get_xfer_rate_KiBps_ghfunc (gpointer key, gpointer val, gpointer gp)
{
	gulong * pKiBps = (gulong*) gp;
	GIOChannel * channel = (GIOChannel*) key;
	*pKiBps += nntp_get_xfer_rate_KiBps (channel);
}

gulong
task_get_xfer_rate_KiBps (const Task * task)
{
	gulong KiBps = 0ul;
	g_hash_table_foreach (task->checkin_funcs,
	                      task_get_xfer_rate_KiBps_ghfunc,
	                      &KiBps);
	return KiBps;
}
