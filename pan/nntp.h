/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __NNTP_H__
#define __NNTP_H__

#include <glib/gtypes.h>
#include <glib/giochannel.h>

#include <pan/base/pstring.h>
#include <pan/task-state.h>

/**
**/

typedef void (*nntp_line_func)(GIOChannel     * io,
                               const char     * line,
                               int              line_len,
                               gpointer         user_data);

typedef void (*nntp_done_func)(GIOChannel     * io,
                               TaskStateEnum    state,
                               gpointer         user_data);

typedef void (*nntp_group_func)(GIOChannel    * io,
                                gulong          estimated_qty,
                                gulong          low,
                                gulong          high,
                                GQuark          group_quark,
                                gpointer        user_data);

/**
**/

void nntp_handshake           (GIOChannel         * io,
                               const char         * username,
                               const char         * password,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data);

void nntp_xover               (GIOChannel         * io,
                               const gboolean     * abort_flag,
                               nntp_line_func       line_func,
                               gpointer             line_func_user_data,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data,
                               gulong               low,
                               gulong               high);

void nntp_list                (GIOChannel         * io,
                               const gboolean     * abort_flag,
                               nntp_line_func       line_func,
                               gpointer             line_func_user_data,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data);

void nntp_list_newsgroups     (GIOChannel         * io,
                               const gboolean     * abort_flag,
                               nntp_line_func       line_func,
                               gpointer             line_func_user_data,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data);

void nntp_article             (GIOChannel         * io,
                               const gboolean     * abort_flag,
                               nntp_line_func       line_func,
                               gpointer             line_func_user_data,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data,
                               gulong               number);

void nntp_group               (GIOChannel         * io,
                               GQuark               group_quark,
                               nntp_group_func      group_func,
                               gpointer             group_func_user_data,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data);

void nntp_goodbye             (GIOChannel         * io,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data);

void nntp_noop                (GIOChannel         * io,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data);

void nntp_post                (GIOChannel         * io,
                               const char         * message,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data);

void nntp_cancel              (GIOChannel         * io,
                               const PString      * message_id,
                               nntp_done_func       done_func,
                               gpointer             done_func_user_data);


/**
**/

gulong nntp_get_xfer_rate_KiBps   (GIOChannel * io);

gulong nntp_step_total_xfer_bytes (void);

GQuark nntp_get_group (GIOChannel * io);



#endif /* __NNTP_H__ */
