/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __SAVE_H__
#define __SAVE_H__

#include <glib.h>

/**
 * @param list a GSList* of Article*s to decode separately.
 *             This function assumes ownership of the GSList.
 * @param download_dirlist 
 */
extern void save_attachment_as (GSList      * articles,
                                const char  * optional_download_dir);

/**
 * Identical to save_attachment_as,
 * except it doesn't preprocess the articles by weeding them.
 * FIXME: currently weeding is, actually, done.
 */
extern void manual_decode_attachment_as (GSList      * articles,
                                         const char  * optional_download_dir);

#endif /* __QUEUE_ITEM_BODY__H__ */
