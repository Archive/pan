/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <string.h>
#include <stdlib.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/acache.h>
#include <pan/base/debug.h>
#include <pan/base/group.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/serverlist.h>

#include <pan/globals.h>
#include <pan/group-ui.h>
#include <pan/util.h>

/****
*****
*****
****/

static void
new_folder_response_cb (GtkDialog * dialog, int response, gpointer user_data)
{
	if (response == GTK_RESPONSE_OK)
	{
		Group * folder = NULL;
		Server * server = serverlist_get_folders_server ();
		char * name = g_strdup (gtk_entry_get_text (GTK_ENTRY(user_data)));
		debug_enter ("new_folder_cb");

		/* make sure the user entered something */
		g_strstrip (name);
		if (is_nonempty_string (name))
		{
			/* make sure there's not already a folder with that name */
			GQuark name_quark = g_quark_from_string (name);
			folder = serverlist_get_folder (name_quark);
			if (folder != NULL) {
				pan_error_dialog (_("Folder \"%*s\" already exists."), name);
				return;
			}

			/* make sure it's not in the reserved namespace */
			if (!g_ascii_strncasecmp (name, "pan.", 4)) {
				pan_error_dialog (_("Please don't begin folders with \"pan.\"; it confuses me."));
				return;
			}

			/* create new folder */
			folder = group_new (server, name);
			group_set_is_folder (folder, TRUE);
			group_set_dirty (folder);
			server_add_groups (server, &folder, 1, NULL, NULL);
			acache_add_folder (name);
		}

		g_free (name);
	}

	gtk_widget_destroy (GTK_WIDGET(dialog));

	/* cleanup */
	debug_exit ("new_folder_cb");
}

void
group_new_folder_dialog (void)
{
	GtkWidget * w;
	GtkWidget * hbox;
	GtkWidget * entry;

	w = gtk_dialog_new_with_buttons (_("Create New Folder"),
	                                 GTK_WINDOW(Pan.window),
	                                 GTK_DIALOG_DESTROY_WITH_PARENT,
	                                 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                                 GTK_STOCK_NEW, GTK_RESPONSE_OK,
	                                 NULL);
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER(hbox), 18);
	gtk_box_pack_start (GTK_BOX(hbox), gtk_label_new(_("New Folder Name:")),  FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(hbox), entry = gtk_entry_new(), TRUE, TRUE, 0);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG(w)->vbox), hbox);
	g_signal_connect (GTK_WIDGET(w), "response", G_CALLBACK(new_folder_response_cb), entry);
	gtk_widget_show_all (w);
}
