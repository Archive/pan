/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*********************
**********************  Includes
*********************/

#include <config.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/group.h>
#include <pan/base/pan-i18n.h>

#include <pan/articlelist.h>
#include <pan/queue.h>
#include <pan/task-headers.h>
#include <pan/util.h>

#include <pan/dialogs/dialogs.h>

/*********************
**********************  Structures / Typedefs
*********************/

typedef struct
{
	GPtrArray * groups;

	GtkWidget * dialog;
	GtkWidget * all_headers_rb;
	GtkWidget * all_new_headers_rb;
	GtkWidget * latest_n_headers_rb;
	GtkWidget * n_headers_spinbutton;
	GtkWidget * download_bodies_cb;
}
DownloadHeadersDialogStruct;

/*********************
**********************  BEGINNING OF SOURCE
*********************/

static void
switch_to_group (gpointer callobj, gpointer callarg, gpointer client)
{
	Group * group = GROUP (client);

	if (group->article_qty > 0)
		articlelist_set_group (group);
}

static void
download_headers_response_cb (GtkDialog * dialog, int response, gpointer user_data)
{
	DownloadHeadersDialogStruct * ui = (DownloadHeadersDialogStruct*)user_data;

	if (response == GTK_RESPONSE_OK)
	{
		int         i;
		PanObject * task = NULL;

		for (i=0; i<ui->groups->len; i++)
		{
			Group * group = g_ptr_array_index (ui->groups, i);

			if (group_is_folder (group))
				continue;

			if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(ui->all_headers_rb)))
			{
				task = task_headers_new (group, HEADERS_ALL);
			}
			else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(ui->all_new_headers_rb)))
			{
				task = task_headers_new (group, HEADERS_NEW);
			}
			else /* N headers */
			{
				int sample_size = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(ui->n_headers_spinbutton));
				task = task_headers_new_sample (group, sample_size, FALSE);
			}

			if (task != NULL && ui->groups->len == 1)
			{
				pan_callback_add (TASK(task)->task_ran_callback, 
					switch_to_group, group);
			}

			if (task != NULL)
			{
				const gboolean download_bodies = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(ui->download_bodies_cb));
				TASK_HEADERS(task)->download_bodies = download_bodies;
				queue_add (TASK(task));
			}
		}
	}

	/* cleanup */
	g_ptr_array_free (ui->groups, TRUE);
	gtk_widget_destroy (GTK_WIDGET(dialog));
	g_free (ui);
}

static int
spin_tickled_cb (GtkWidget * w, gpointer * event, gpointer user_data)
{
	/* if a user clicked in the spinbutton window,
	   select the spinbutton radiobutton for them. */
	DownloadHeadersDialogStruct * data = (DownloadHeadersDialogStruct*) user_data;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(data->latest_n_headers_rb), TRUE);
	return FALSE;
}

/************
*************  PUBLIC ROUTINES
************/

/*****
******
*****/

void
dialog_download_headers (GtkWindow * parent, GPtrArray * g)
{
	GPtrArray * groups;
	int         i;
	char buf[512];
	DownloadHeadersDialogStruct * dialog;
	GtkAdjustment * adj;
	GtkWidget * vbox;
	GtkWidget * hbox;
	GtkWidget * w;

	/* sanity checks */
	g_return_if_fail (g!=NULL);

	/* select all non-folder groups */
	groups = g_ptr_array_new ();
	for (i=0; i<g->len; i++) {
		Group * group = GROUP (g_ptr_array_index (g, i));
		if (!group_is_folder (group))
			g_ptr_array_add (groups, group);
	}
	
	/* any non-folders selected */
	if (groups->len == 0)
		return;

	/* create dialog */
	dialog = g_new0 (DownloadHeadersDialogStruct, 1);
	dialog->groups = groups;
	if (groups->len == 1) {
		Group * g = GROUP(g_ptr_array_index(groups,0));
		g_snprintf (buf, sizeof(buf), _("Pan: Download Headers for group `%s'"), group_get_name(g));
	}
	else
		g_snprintf (buf, sizeof(buf), _("Pan: Download Headers"));

	dialog->dialog = gtk_dialog_new_with_buttons (buf, parent, GTK_DIALOG_DESTROY_WITH_PARENT,
	                                              GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                                              GTK_STOCK_EXECUTE, GTK_RESPONSE_OK,
	                                              NULL);
	gtk_dialog_set_default_response (GTK_DIALOG(dialog->dialog), GTK_RESPONSE_OK);

	/**
	*** headers
	**/

	vbox = gtk_vbox_new (FALSE, GUI_PAD_SMALL);
	gtk_container_set_border_width (GTK_CONTAINER(vbox), 18);

	/* all new headers */
	w = gtk_radio_button_new_with_mnemonic (NULL, _("Download _new headers"));
	gtk_box_pack_start (GTK_BOX(vbox), w, FALSE, FALSE, 0);
	dialog->all_new_headers_rb = w;

	/* all headers */
	w = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON(w), _("Download _all headers"));
	gtk_box_pack_start (GTK_BOX(vbox), w, FALSE, FALSE, 0);
	dialog->all_headers_rb = w;
		
	/* some new headers */
	hbox = gtk_hbox_new (FALSE, 0);
	w = gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON(w), _("Download _recent headers: "));
	gtk_box_pack_start (GTK_BOX(hbox), w, FALSE, FALSE, 0);
	dialog->latest_n_headers_rb = w;
	adj = GTK_ADJUSTMENT(gtk_adjustment_new (
		150, 0, INT_MAX, 50, 50, 1));
	w = gtk_spin_button_new (adj, 1, 0);
	gtk_box_pack_start (GTK_BOX(hbox), w, FALSE, FALSE, 0);
	dialog->n_headers_spinbutton = w;
	gtk_box_pack_start (GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER(GTK_DIALOG(dialog->dialog)->vbox), vbox);
	gtk_signal_connect (GTK_OBJECT(w), "focus_in_event", GTK_SIGNAL_FUNC(spin_tickled_cb), dialog);
	gtk_signal_connect (GTK_OBJECT(w), "button_press_event", GTK_SIGNAL_FUNC(spin_tickled_cb), dialog);

	/* spacer */
	w = gtk_alignment_new (0.0f, 0.0f, 0.0f, 0.0f);
	gtk_widget_set_usize (w, 0u, 12u);
	gtk_box_pack_start (GTK_BOX(vbox), w, FALSE, FALSE, 0);

	/* 'download bodies too?' togglebutton */
	w = gtk_check_button_new_with_mnemonic (_("_Download bodies too"));
	dialog->download_bodies_cb = w;
	gtk_box_pack_start (GTK_BOX(vbox), w, FALSE, FALSE, 0);

	g_signal_connect (G_OBJECT(dialog->dialog), "response", G_CALLBACK(download_headers_response_cb), dialog);
	gtk_widget_show_all (dialog->dialog);
}
