/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2003  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/pan-i18n.h>
#include <pan/dialogs/dialogs.h>
#include <pan/dialogs/pan-about.h>
#include <pan/xpm/pan-pixbufs.h>
#include <pan/util.h>

#if GTK_CHECK_VERSION(2,6,0)
static void
url_link_func (GtkAboutDialog * about, const char * link, gpointer data)
{
	g_message ("url [%s]", link);
	pan_url_show (link);
}
#else
static char * get_credits_code (void);
static char * get_credits_users (void);
static char * get_credits_translators (void);
#endif

/**
 * dialog_about:
 *
 * About window to display program information, version number, and
 * copyright.
 **/
void
dialog_about (GtkWidget * window)
{
	const char *copyright =
		"Copyright \xc2\xa9 2000-2005 Charles Kerr\n"
		"Copyright \xc2\xa9 1999-2000 Matt Eagleson";
	const char *authors[] = {
		"Charles Kerr <charles@rebelbase.com>",
		"Christophe Lambin <chris@rebelbase.com>",
		"Matt Eagleson <e.Mesisah@superpimp.org>",
		"Jason Leach <leach@wam.umd.edu>",
		NULL
	};
#if GTK_CHECK_VERSION(2,6,0)
	static GtkWidget * about = NULL;

	if (about)
	{
		gtk_window_set_transient_for (GTK_WINDOW (about), GTK_WINDOW (window));
		gtk_window_present (GTK_WINDOW (about));
	}
	else
	{
		GtkAboutDialog * gad;
		GdkPixbuf * logo = gdk_pixbuf_new_from_inline (-1, icon_pan, FALSE, NULL);

		about = gtk_about_dialog_new ();
		g_object_add_weak_pointer (G_OBJECT(about), (gpointer*)&about);

		gad = GTK_ABOUT_DIALOG(about);
		gtk_about_dialog_set_name (gad, "Pan");
		gtk_about_dialog_set_version (gad, VERSION);
		gtk_about_dialog_set_copyright (gad, copyright);
		gtk_about_dialog_set_authors (gad, authors);
		gtk_about_dialog_set_translator_credits (gad, _("translator-credits"));
		gtk_about_dialog_set_logo (gad, logo);
		gtk_about_dialog_set_comments (gad, _("A Usenet newsreader useful for both text and binaries newsgroups."));
		gtk_about_dialog_set_website (gad, "http://pan.rebelbase.com/");
		gtk_about_dialog_set_url_hook (url_link_func, NULL, NULL);
		gtk_window_set_transient_for (GTK_WINDOW(about), GTK_WINDOW(window));
		gtk_window_set_destroy_with_parent (GTK_WINDOW(about), TRUE);

		g_object_unref (G_OBJECT(logo));
	}
#else
	GtkWidget * about;
	char * pch;
	char * credits_bugs;
	char * credits_i18n;
	char * credits_code;

	about = pan_about_new (GTK_WINDOW(window),
	                       "Pan", VERSION,
	                       _("A newsreader for GNOME"),
	                       _("Copyright (C) 2003 Charles Kerr"),
	                       PAN_ABOUT_LICENSE_GPL,
	                       "http://pan.rebelbase.com/");

	pan_about_add_author (about, "Charles Kerr", "charles@rebelbase.com",
	                      _("Programmer"));
	pan_about_add_author (about, "Christophe Lambin", "chris@rebelbase.com",
	                      _("Programmer"));
	pan_about_add_author (about, "Matt Eagleson", "e.Messiah@superpimp.org",
	                      _("Project Originator, Programmer"));
	pan_about_add_author (about, "Jason Leach", NULL,
	                      _("Programmer, 0.6 - 0.7"));

	credits_i18n = get_credits_translators ();
	credits_code = get_credits_code ();
	credits_bugs = get_credits_users ();
	pch = g_strdup_printf (_("Pan %s Translations\n%s\n"
                                 "\n"
                                 "Pan %s Code or Patches\n%s\n"
                                 "\n"
                                 "Pan %s Suggestions and Testing\n%s\n"),
	                         VERSION, credits_i18n,
	                         VERSION, credits_code,
	                         VERSION, credits_bugs);
	pan_about_add_text (about, _("Thanks"), pch);
	g_free (credits_i18n);
	g_free (credits_code);
	g_free (credits_bugs);
	g_free (pch);
#endif

	gtk_widget_show (about);
}


#if GTK_CHECK_VERSION(2,6,0)
#else
static char *
get_credits_translators (void)
{
	const char * str =
		"\n\tcs - Miloslav Trmac"
		"\n\tde - Benjamin Greiner"
		"\n\tes - Pablo Gonzalo del Campo"
		"\n\tet - T�ivo Leedj�rv"
		"\n\tfr - Christophe Merlet"
		"\n\tja - KAMAGASAKO Masatoshi"
		"\n\tlv - Artis Trops"
		"\n\tno - Kjartan Maraas"
		"\n\tnl - Vincent van Adrighem"
		"\n\tpl - GNOME PL Team"
		"\n\tpt - Duarte Loreto"
		"\n\tpt_BR - Evandro Fernandes Giovanini"
		"\n\tsk - Marcel Telka"
		"\n\tsr - Aleksandar Urosevic (Prevod.org)"
		"\n\tsv - Christian Rose"
		"\n\ttr - Arman Aksoy (Armish)";
	return g_convert (str, strlen(str), "UTF-8", "ISO-8859-1", NULL, NULL, NULL);
}

static char *
get_credits_code (void)
{
	const char * str = 
		"\n\tK. Haley"
		"\n\tTorstein Sunde";
	return g_convert (str, strlen(str), "UTF-8", "ISO-8859-1", NULL, NULL, NULL);
}

static char *
get_credits_users (void)
{
	const char * str = 
		"\n\tPaul Broe (#305067)";
	return g_convert (str, strlen(str), "UTF-8", "ISO-8859-1", NULL, NULL, NULL);
}
#endif
