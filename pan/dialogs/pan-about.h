/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __PAN_ABOUT_H__
#define __PAN_ABOUT_H__

#include <glib.h>
#include <gtk/gtk.h>
#if GTK_CHECK_VERSION(2,6,0)
#else

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

GtkWidget*    pan_about_new          (GtkWindow     * parent,
                                      const char    * app_title,
                                      const char    * app_version,
                                      const char    * app_short_description,
                                      const char    * app_copyright,
                                      const char    * app_license,
                                      const char    * app_homepage);

void          pan_about_add_author   (GtkWidget     * about_dialog,
                                      const char    * name, 
                                      const char    * email,
                                      const char    * job_title);

void          pan_about_add_credit   (GtkWidget     * about_dialog,
                                      const char    * name, 
                                      const char    * email,
                                      const char    * job_title);

void          pan_about_add_text     (GtkWidget     * about_dialog,
                                      const char    * tab,
                                      const char    * text);

extern const char * PAN_ABOUT_LICENSE_GPL;


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GTK_CHECK_VERSION 2.6.0 */

#endif /* __GNOME_ABOUT_H__ */
