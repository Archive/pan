/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <ctype.h>
#include <unistd.h>

#include <pan/base/debug.h>
#include <pan/base/pan-config.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/serverlist.h>
#include <pan/base/util-file.h>

#include <pan/globals.h>
#include <pan/prefs.h>
#include <pan/sockets.h>
#include <pan/util.h>

#include <pan/identities/identity.h>
#include <pan/identities/identity-manager.h>

#include <pan/pan.h>
#include <pan/gui.h>
#include <pan/pan-file-entry.h>
#include <pan/dialogs/dialogs.h>
#include <pan/xpm/pan-pixbufs.h>

enum
{
	PAN_RESPONSE_GO_BACK,
	PAN_RESPONSE_GO_FORWARD,
	PAN_RESPONSE_QUIT,
	PAN_RESPONSE_FINISHED
};

typedef struct
{
	GPtrArray * pages;
	GtkWidget * notebook;

	GtkWidget * email;
	GtkWidget * full_name;
	GtkWidget * organization;
	GtkWidget * signature_entry;

	GtkWidget * smtp_server;
	GtkWidget * smtp_port;
	
	GtkWidget * server_address;
	GtkWidget * server_port;
	GtkWidget * auth_toggle;

	GtkWidget * server_username_label;
	GtkWidget * server_username;
	GtkWidget * server_password_label;
	GtkWidget * server_password;

	GtkWidget * profile_name;
}
Druid;

static GtkTooltips * ttips = NULL;

/***
****
****  HOSTNAME
****
***/


static char*
get_default_author_address (void)
{
	char * email = NULL;
	debug_enter ("get_default_author_address");
	
	if (!email) {
		email = pan_config_get_string ("/Pan/User/Email", NULL);
		if (email != NULL)
			g_strstrip (email);
		if (!is_nonempty_string (email))
			replace_gstr (&email, NULL);
	}

	if (!email) {
		email = g_strdup (g_getenv ("EMAIL"));
		if (email != NULL)
			g_strstrip (email);
		if (!is_nonempty_string (email))
			replace_gstr (&email, NULL);
	}

	if (!email) {
		char * fqdn = pan_get_local_fqdn ();
		replace_gstr (&email,
			g_strdup_printf ("%s@%s", g_get_user_name(), fqdn));
		g_strstrip (email);
		g_free (fqdn);
	}

	debug_exit ("get_default_author_address");
	return email;
}

/***
***/

static void
druid_finish (Druid * druid)
{
	Server * server = NULL;
	GString * buf = g_string_new (NULL);
	int server_port = atoi(gtk_entry_get_text (GTK_ENTRY(druid->server_port)));
	Identity * id = NULL;
	char * pch;

	int auth_toggle = GTK_TOGGLE_BUTTON (druid->auth_toggle)->active;
	const char * profile_name = gtk_entry_get_text (GTK_ENTRY(druid->profile_name));
	const char * server_address = gtk_entry_get_text (GTK_ENTRY(druid->server_address));
	const char * server_username = gtk_entry_get_text(GTK_ENTRY(druid->server_username));
	const char * server_password = gtk_entry_get_text(GTK_ENTRY(druid->server_password));
	const char * full_name = gtk_entry_get_text (GTK_ENTRY(druid->full_name));
	const char * email = gtk_entry_get_text (GTK_ENTRY(druid->email));
	const char * organization = gtk_entry_get_text (GTK_ENTRY(druid->organization));
	const char * smtp_server = gtk_entry_get_text (GTK_ENTRY(druid->smtp_server));
	const char * smtp_port = gtk_entry_get_text (GTK_ENTRY(druid->smtp_port));
	const char * signature_file = pan_file_entry_get (druid->signature_entry);

	if (profile_name && server_address) {
		server = server_new ();
                pstring_set (&server->name, profile_name, -1);
		pstring_set (&server->address, server_address, -1);
                server->port = server_port;
                server->need_auth = auth_toggle;
                pstring_set (&server->username, auth_toggle?server_username:NULL, -1);
		pstring_set (&server->password, auth_toggle?server_password:NULL, -1);
		server->max_connections = 4;
		serverlist_add_server (server, NULL);
		pan_config_set_string ("/Pan/State/Server", profile_name);
	}

	/* save user stats */
	pan_config_set_string ("/Pan/Mail/smtp_port", smtp_port);
	pan_config_set_string ("/Pan/Mail/smtp_address", smtp_server);
	mail_server_address = g_strdup (smtp_server);

	/* add a new identity */
	id = identity_new ();
	if (is_nonempty_string (full_name))
		pch = g_strdup_printf ("%s", full_name);
	else
		pch = g_strdup_printf ("Default");
	identity_set_name (id, pch);
	replace_gstr (&pch, NULL);
	identity_set_author_real (id, full_name);
	identity_set_author_addr (id, email);
	identity_set_organization (id, organization);
	if (is_nonempty_string (signature_file))
		identity_set_signature (id, signature_file);
	identity_manager_add_identity (id);
	pan_object_unref (PAN_OBJECT (id));

	/* start Pan proper */
	pan_config_sync ();
	gui_construct (NULL);
	gtk_widget_show_all (Pan.window);
	serverlist_set_active_server (server);

	/* cleanup */
	g_free (druid);
	g_string_free (buf, TRUE);

	debug_exit ("druid_finish");
}

static void
dialog_response_cb (GtkDialog * dialog, int response, gpointer data)
{
	Druid * druid;
	int page_qty;
	int current_page;
	debug_enter ("dialog_response_cb");

	druid = (Druid*) data;
	page_qty = druid->pages->len;
 	current_page = gtk_notebook_get_current_page (GTK_NOTEBOOK(druid->notebook));
	if (response == PAN_RESPONSE_QUIT)
	{
		pan_shutdown ();
		gtk_widget_destroy (GTK_WIDGET(dialog));
	}
	else if (response == PAN_RESPONSE_FINISHED)
	{
		druid_finish (druid);
		gtk_widget_destroy (GTK_WIDGET(dialog));
	}
	else
	{
		if (response == PAN_RESPONSE_GO_BACK)
		{
			if (current_page > 0)
				gtk_notebook_set_current_page (GTK_NOTEBOOK(druid->notebook), --current_page);
		}
		else if (response == PAN_RESPONSE_GO_FORWARD)
		{
			if (current_page < (page_qty-1))
				gtk_notebook_set_current_page (GTK_NOTEBOOK(druid->notebook), ++current_page);
		}

		gtk_dialog_set_response_sensitive (dialog, PAN_RESPONSE_GO_BACK, current_page>0);
		gtk_dialog_set_response_sensitive (dialog, PAN_RESPONSE_GO_FORWARD, current_page<(page_qty-1));
		gtk_dialog_set_response_sensitive (dialog, PAN_RESPONSE_FINISHED, current_page==(page_qty-1));
	}

	debug_exit ("dialog_response_cb");
}

/***
****
***/

static GtkWidget*
druid_user_page (Druid * druid)
{
	GtkWidget * t;
	GtkWidget * w;
	int row = 0;
	char * pch;
	debug_enter ("druid_user_page");

	t = pan_hig_workarea_create ();

	pan_hig_workarea_add_section_title (t, &row, _("How should you appear when posting articles?"));
	pan_hig_workarea_add_section_divider (t, &row);
	pan_hig_workarea_add_section_title (t, &row, _("Required Information"));

		pan_hig_workarea_add_section_spacer (t, row, 2);

		/* real name */
		w = druid->full_name = gtk_entry_new();
		pan_gtk_entry_set_text (w, g_get_real_name());
		pan_hig_workarea_add_row (t, &row, _("Your _Name:"), w, NULL);

		/* email */
		w = druid->email = gtk_entry_new ();
		pch = get_default_author_address ();
		pan_gtk_entry_set_text (w, pch);
		g_free (pch);
		pan_hig_workarea_add_row (t, &row, _("Email _Address:"), w, NULL);

	pan_hig_workarea_add_section_divider (t, &row);
	pan_hig_workarea_add_section_title (t, &row, _("Optional Information"));

		pan_hig_workarea_add_section_spacer (t, row, 2);

		/* organization */
		w = druid->organization = gtk_entry_new ();
		pan_hig_workarea_add_row (t, &row, _("_Organization:"), w, NULL);

		/* signature */
		w = druid->signature_entry = pan_file_entry_new (_("Select Signature File"));
		pch = g_build_filename (pan_get_home_dir(), ".signature", NULL);
		pan_file_entry_set (w, pan_file_exists(pch) ? pch :  "");
		g_free (pch);
		pan_hig_workarea_add_row (t, &row, _("_Signature File:"), w, pan_file_entry_gtk_entry(w));

	debug_exit ("druid_user_page");
	return t;
}


static void
password_set (Druid * druid, gboolean b)
{
	debug_enter ("password_set");

	gtk_widget_set_sensitive (druid->server_username, b);
	gtk_widget_set_sensitive (druid->server_password, b);
	gtk_widget_set_sensitive (druid->server_username_label, b);
	gtk_widget_set_sensitive (druid->server_password_label, b);

	debug_exit ("password_set");
}

static void
password_needed_cb (GtkToggleButton * tb, Druid * druid)
{
	password_set (druid, tb->active);
}

static char*
get_default_nntp_server (void)
{
	char * retval = NULL;
	debug_enter ("get_default_nntp_server");

	if (!retval) {
		const char * cpch = g_getenv ("NNTPSERVER");
		if (is_nonempty_string (cpch))
			retval = g_strdup (cpch);
	}

	if (!retval) {
		char * fqdn = pan_get_local_fqdn ();
		if (is_nonempty_string (fqdn)) {
			const char * dot = strchr (fqdn, '.');
			if (dot != NULL) {
				retval = g_strdup_printf ("news.%s", dot+1);
			}
		}
		g_free (fqdn);
	}

	debug_exit ("get_default_nntp_server");
	return retval;
}

static char*
get_default_profile_name (void)
{
	char * retval = NULL;
	debug_enter ("get_default_profile_name");

	if (!retval) {
		char * pch = get_default_nntp_server ();
		if (is_nonempty_string (pch)) {
			char * lastdot = strrchr (pch, '.');
			if (lastdot != NULL)
				*lastdot = '\0';
			lastdot = strrchr (pch, '.');
			if (lastdot != NULL)
				retval = g_strdup (lastdot+1);
		}
		g_free (pch);
	}

	if (!retval) {
		retval = g_strdup (_("default"));
	}

	debug_exit ("get_default_profile_name");
	return retval;
}

static GtkWidget*
druid_smtp_page (Druid *druid)
{
	GtkWidget * t;
	GtkWidget * w;
	int row = 0;
	char * pch;
	debug_enter ("druid_smtp_page");

	t = pan_hig_workarea_create ();

	pan_hig_workarea_add_section_title (t, &row, _("What mail server should Pan use?"));

		pan_hig_workarea_add_section_spacer (t, row, 2);

		/* smtp server address */
		w = druid->smtp_server = gtk_entry_new();
		pch = pan_get_local_fqdn ();
		pan_gtk_entry_set_text (w, pch);
		g_free (pch);
		pan_hig_workarea_add_row (t, &row, _("_Address:"), w, NULL);

		/* smtp server port */
		w = druid->smtp_port = gtk_entry_new();
		pan_gtk_entry_set_text (w, "25");
		pan_hig_workarea_add_row (t, &row, _("Por_t:"), w, NULL);

	debug_exit ("druid_smtp_page");
	return t;
}

static GtkWidget*
druid_nntp_page (Druid *druid)
{
	GtkWidget * t;
	GtkWidget * w;
	int row = 0;
	char * pch;
	debug_enter ("druid_nntp_page");

	t = pan_hig_workarea_create ();

	pan_hig_workarea_add_section_title (t, &row, _("What news server should Pan use?"));

		pan_hig_workarea_add_section_spacer (t, row, 3);

		/* news server address */
		w = druid->server_address = gtk_entry_new();
		gtk_tooltips_set_tip (GTK_TOOLTIPS(ttips), w, _("The news server's actual address, e.g. \"news.mynewsserver.com\""), NULL);
		pch = get_default_nntp_server ();
		pan_gtk_entry_set_text (w, is_nonempty_string(pch) ? pch : "news");
		g_free (pch);
		pan_hig_workarea_add_row (t, &row, _("_Address:"), w, NULL);

		/* news server port */
		w = druid->server_port = gtk_entry_new();
		gtk_tooltips_set_tip (GTK_TOOLTIPS(ttips), w, _("The news server's port.  This is almost always 119."), NULL);
		pan_gtk_entry_set_text (w, "119");
		pan_hig_workarea_add_row (t, &row, _("Por_t:"), w, NULL);

		/* news server mnemonic */
		w = druid->profile_name = gtk_entry_new();
                gtk_tooltips_set_tip (ttips, w, _("Short name for Pan to show, e.g. \"mynewsserver\""), NULL);
		pch = get_default_profile_name ();
		pan_gtk_entry_set_text (w, pch);
		g_free (pch);
		pan_hig_workarea_add_row (t, &row, _("Short _name for Pan to show:"), w, NULL);

	pan_hig_workarea_add_section_divider (t, &row);
	pan_hig_workarea_add_section_title (t, &row, _("Login"));

		pan_hig_workarea_add_section_spacer (t, row, 3);

		w = druid->auth_toggle = pan_hig_workarea_add_wide_checkbutton (t, &row, _("Server requires authori_zation"), FALSE);
		gtk_signal_connect (GTK_OBJECT(w), "toggled", GTK_SIGNAL_FUNC(password_needed_cb), druid);

		w = druid->server_username = gtk_entry_new ();
		druid->server_username_label = pan_hig_workarea_add_row (t, &row, _("_Username:"), w, NULL);

		w = druid->server_password = gtk_entry_new ();
		gtk_entry_set_visibility (GTK_ENTRY(w), FALSE);
		druid->server_password_label = pan_hig_workarea_add_row (t, &row, _("_Password:"), w, NULL);

	password_set (druid, FALSE);

	debug_exit ("druid_nntp_page");
	return t;
}


static GtkWidget*
create_page (const char * title)
{
	GtkWidget * w;
	GtkWidget * vbox;
	GtkWidget * hbox;
	GtkWidget * image;
	GdkPixbuf * pixbuf;
	char * title_string;
	debug_enter ("create_page");

	/* create vbox */
	vbox = gtk_vbox_new (FALSE, GUI_PAD);
	gtk_container_set_border_width  (GTK_CONTAINER(vbox), 10u);

	/* create the titlebar */
	hbox = gtk_hbox_new (FALSE, GUI_PAD_BIG);
	pixbuf = gdk_pixbuf_new_from_inline (-1, icon_pan, FALSE, NULL);
	image = gtk_image_new_from_pixbuf (pixbuf);
	g_object_unref (G_OBJECT(pixbuf));
	gtk_box_pack_start (GTK_BOX(hbox), image, FALSE, FALSE, 0);
     	title_string = g_strconcat ("<span size=\"xx-large\" weight=\"ultrabold\">", title ? title : "", "</span>", NULL);
	w = gtk_label_new (title_string);
	gtk_label_set_use_markup (GTK_LABEL(w), TRUE);
	g_free (title_string);
	gtk_box_pack_start (GTK_BOX(hbox), w, FALSE, FALSE, 0);

	/* pack the titlebar */
	gtk_box_pack_start (GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

	/* pack the separator */
	gtk_box_pack_start (GTK_BOX(vbox), gtk_hseparator_new(), FALSE, FALSE, 0);

	/* pack space */
	w = gtk_alignment_new (0.0f, 0.0f, 0.0f, 0.0f);
	gtk_widget_set_usize (w, 0u, 6u);
	gtk_box_pack_start (GTK_BOX(vbox), w, FALSE, FALSE, 0);

	debug_exit ("create_page");
	return vbox;
}

static GtkWidget*
create_page_with_text (const char * title, const char * text)
{
	GtkWidget * label;
	GtkWidget * page;
	debug_enter ("create_page_with_text");
       
	page = create_page (title);
	label = gtk_label_new (text);
	gtk_box_pack_start (GTK_BOX(page), label, TRUE, TRUE, 0);

	debug_exit ("crewate_page_with_text");
	return page;
}



/*---[ druid_spawn ]--------------------------------------------------
 * spawn the first-run wizard
 *--------------------------------------------------------------------*/
void
dialog_newuser (void)
{
	int i;
	GtkWidget * dialog;
	GtkWidget * notebook;
	GtkWidget * w;
	Druid * druid;
	debug_enter ("dialog_newuser");

	ttips = GTK_TOOLTIPS (gtk_tooltips_new ());

	/* create the dialog */
	druid = g_new (Druid, 1);
	dialog = gtk_dialog_new_with_buttons (_("Pan Setup Wizard"),
	                                      NULL, 0,
	                                      GTK_STOCK_GO_BACK, PAN_RESPONSE_GO_BACK,
	                                      GTK_STOCK_GO_FORWARD, PAN_RESPONSE_GO_FORWARD,
	                                      GTK_STOCK_SAVE, PAN_RESPONSE_FINISHED,
	                                      GTK_STOCK_QUIT, PAN_RESPONSE_QUIT,
	                                      NULL);
	g_signal_connect (dialog, "response", G_CALLBACK(dialog_response_cb), druid);
	gtk_dialog_set_default_response (GTK_DIALOG(dialog), PAN_RESPONSE_GO_FORWARD);
	gtk_dialog_set_response_sensitive (GTK_DIALOG(dialog), PAN_RESPONSE_GO_BACK, FALSE);
	gtk_dialog_set_response_sensitive (GTK_DIALOG(dialog), PAN_RESPONSE_GO_FORWARD, TRUE);
	gtk_dialog_set_response_sensitive (GTK_DIALOG(dialog), PAN_RESPONSE_FINISHED, FALSE);

	/* create the notebook */
	notebook = druid->notebook = gtk_notebook_new ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK(notebook), FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK(notebook), FALSE);
	gtk_box_pack_start (GTK_BOX(GTK_DIALOG(dialog)->vbox), notebook, TRUE, TRUE, 0);

	/**
	*** Initialize the druid pages
	**/

	druid->pages = g_ptr_array_new ();

	/* add welcome page */
	w = create_page (_("Thank you for trying Pan!"));
	g_ptr_array_add (druid->pages, w);
	if (1) {
		GdkPixbuf * pixbuf = gdk_pixbuf_new_from_inline (-1, icon_pan_druid, FALSE, NULL);
		GtkWidget * child = gtk_image_new_from_pixbuf (pixbuf);
		char * welcome = g_strdup_printf (_("Thank you for trying Pan!\n \nIf you are new to Usenet,\n%s\nis a starting point to see what Usenet is,\nwhere to find a free news server,\nand how to use Pan.\n \nSince this is your first time running Pan,\nwe'll need to gather some information.\n"), "http://pan.rebelbase.com/intro.html");

		g_object_unref (G_OBJECT(pixbuf));
		gtk_box_pack_start (GTK_BOX(w), child, FALSE, FALSE, 0);
		child = gtk_label_new (welcome);
		gtk_box_pack_start (GTK_BOX(w), child, FALSE, FALSE, 0);
		g_free (welcome);
	}

	/* add user name */
	w = create_page (_("About You"));
	gtk_box_pack_start (GTK_BOX(w), druid_user_page(druid), FALSE, FALSE, 0);
	g_ptr_array_add (druid->pages, w);

	/* news server */
	w = create_page (_("News Server"));
	gtk_box_pack_start (GTK_BOX(w), druid_nntp_page(druid), FALSE, TRUE, 0);
	g_ptr_array_add (druid->pages, w);

	/* smtp server */
	w = create_page (_("Mail Server"));
	gtk_box_pack_start (GTK_BOX(w), druid_smtp_page(druid), FALSE, TRUE, 0);
	g_ptr_array_add (druid->pages, w);

	/* finished */
	w = create_page_with_text (_("Ready!"), _("You have successfully entered all of\nthe information required to set up Pan.\n\nTo save these settings, click Save."));
	g_ptr_array_add (druid->pages, w);

	/* populate notebook */
	for (i=0; i<druid->pages->len; ++i)
		gtk_notebook_append_page (GTK_NOTEBOOK(notebook), GTK_WIDGET(g_ptr_array_index(druid->pages,i)), NULL);

	gtk_widget_show_all (dialog);

	debug_exit ("dialog_newuser");
}
