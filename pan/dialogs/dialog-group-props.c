/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Pan - A Newsreader for Gtk+
 * Copyright (C) 2002  Charles Kerr <charles@rebelbase.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <pan/base/base-prefs.h>
#include <pan/base/serverlist.h>
#include <pan/base/pan-i18n.h>
#include <pan/base/pan-glib-extensions.h>

#include <pan/dialogs/dialogs.h>

#include <pan/grouplist.h>
#include <pan/pan-charset-picker.h>
#include <pan/pan-file-entry.h>
#include <pan/util.h>


/* Private structure for group properties */
typedef struct
{
	GtkWidget * dialog;
	GtkWidget * path_entry;
	GtkWidget * charset_entry;
	Group * group;
	Server * server;
}
GroupProperties;


static void
group_properties_response_cb (GtkDialog * dialog, int response, gpointer user_data)
{
	GroupProperties * gp = (GroupProperties*) user_data;
	Group * group = gp->group;

	/* sanity clause */
	g_return_if_fail (gp != NULL);
	g_return_if_fail (group != NULL);

	/* response */
	if (response==GTK_RESPONSE_APPLY || response==GTK_RESPONSE_OK)
	{
		const char * cpch;

		/* download directory */
		cpch = pan_file_entry_get (gp->path_entry);
		if (cpch==NULL || !*cpch || !strcmp (cpch, download_dir))
			cpch = NULL;
		group_set_download_dir (group, cpch);

		/* default charset */
		cpch = pan_charset_picker_get_charset (gp->charset_entry);
		if (pan_strcmp (cpch, group_get_default_charset(group)))
			group_set_default_charset (group, cpch);
	}

	if (response!=GTK_RESPONSE_APPLY)
	{
		gtk_widget_destroy (GTK_WIDGET(dialog));
		g_free (gp);
	}
}

static void
change_made_cb (GroupProperties * gp)
{
	GtkDialog * dialog = GTK_DIALOG (gp->dialog);
	gtk_dialog_set_response_sensitive (dialog, GTK_RESPONSE_OK, TRUE);
	gtk_dialog_set_response_sensitive (dialog, GTK_RESPONSE_APPLY, TRUE);
}

/* Build and return directory-specific options page */
static GtkWidget *
group_properties_directories_page (GroupProperties * gp)
{
	int row = 0;
	GtkWidget * t;
	GtkWidget * w;

	t = pan_hig_workarea_create ();

	pan_hig_workarea_add_section_title (t, &row, _("Group Properties"));
	pan_hig_workarea_add_section_spacer (t, row, 2);

		/* save directory */
		w = gp->path_entry = pan_file_entry_new (_("Directory for saving attachments"));
		pan_file_entry_set (w, is_nonempty_string (gp->group->download_dir) ? gp->group->download_dir : download_dir);
		g_signal_connect_swapped (pan_file_entry_gtk_entry(w), "changed", G_CALLBACK(change_made_cb), gp);
		pan_hig_workarea_add_row (t, &row, _("_Directory for saving articles:"), w, pan_file_entry_gtk_entry(w));

		/* charset */
		w = gp->charset_entry = pan_charset_picker_new (group_get_default_charset(gp->group));
		g_signal_connect_swapped (GTK_OBJECT(w), "changed", G_CALLBACK(change_made_cb), gp);
		pan_hig_workarea_add_row (t, &row, _("Default charse_t:"), w, NULL);

	return t;
}


/**
 * dialog_group_props:
 * @group: The group to set the properties for.
 * 
 * Open the group properties dialog.
 **/
void
dialog_group_props (GtkWindow * parent, Group *group)
{
	char buf[1024];
	GroupProperties * gp;

	/* sanity check */
	g_return_if_fail (group != NULL);

	/* create the dialog */
	g_snprintf (buf, sizeof(buf), _("Properties for \"%s\""), group_get_name(group));
	gp = g_new0 (GroupProperties, 1);
	gp->group = group;
	gp->server = serverlist_get_active_server ();
	gp->dialog = gtk_dialog_new_with_buttons (buf, parent,
	                                          GTK_DIALOG_DESTROY_WITH_PARENT,
	                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                                          GTK_STOCK_APPLY, GTK_RESPONSE_APPLY,
	                                          GTK_STOCK_OK, GTK_RESPONSE_OK,
	                                          NULL);

	gtk_dialog_set_response_sensitive (GTK_DIALOG(gp->dialog), GTK_RESPONSE_OK, FALSE);
	gtk_dialog_set_response_sensitive (GTK_DIALOG(gp->dialog), GTK_RESPONSE_APPLY, FALSE);
	gtk_box_pack_start (GTK_BOX(GTK_DIALOG(gp->dialog)->vbox),
	                    group_properties_directories_page(gp),
	                    TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT(gp->dialog), "response",
	                  G_CALLBACK(group_properties_response_cb), gp);

	gtk_widget_show_all (GTK_WIDGET(gp->dialog));
}
