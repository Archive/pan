#include <stdlib.h>

#include <string.h>

#include <glib.h>
#include <gmime/gmime.h>

#include <pan/base/article.h>
#include <pan/base/base-prefs.h>
#include <pan/base/debug.h>
#include <pan/base/group.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/server.h>


static void
pan_init (void)
{
	char * base;
	char * download_dir;

        g_mime_init (0);

	base = getenv ("srcdir");
	base = g_strdup_printf ("%s/", base!=NULL ? base : getenv("PWD"));
	download_dir = g_strdup_printf ("%s/data/save/", base);
	base_prefs_init (download_dir, 1000, FALSE, FALSE);
	g_free (download_dir);
	g_free (base);
}

#define check(A) \
	++test; \
	if (A) \
		g_message ("PASS article test #%d, file %s, line %d", test, __FILE__, __LINE__); \
	else { \
		g_message ("FAIL article test #%d, file %s, line %d \"%s\"", test, __FILE__, __LINE__, #A); \
		return test; \
	}

int
main (void)
{
	int test = 0;
	int article_qty = 0;
	Server * s;
	Group * g;
	Article * a;
	Article * a1;
	Article * articles [512];
	const char * out;
	char buf[512];
	PString pstr;

	pan_init ();

	s = server_new ();
	pstring_set (&s->name, "Server", -1);
	g = group_new (s, "alt.test");
	group_ref_articles (g, NULL);

	/* ctor sanity */
	++test;
	a1 = article_new (g);
	check (a1 != NULL)
	check (a1->parts == 0)
	check (a1->line_qty == 0)
	check (a1->multipart_state == 0)
	check (a1->decode_state == 0)
	check (a1->unread_children == 0)
	check (a1->date == 0)
	check (a1->followups == NULL)
	check (a1->parent == NULL)
	check (pstring_is_valid (&a1->author_addr))
	check (!pstring_is_set (&a1->author_addr))
	check (pstring_is_valid (&a1->author_real))
	check (!pstring_is_set (&a1->author_real))
	check (pstring_is_valid (&a1->subject))
	check (!pstring_is_set (&a1->subject))
	check (pstring_is_valid (&a1->message_id))
	check (!pstring_is_set (&a1->message_id))
	check (pstring_is_valid (&a1->references))
	check (!pstring_is_set (&a1->references))

	/**
	***  article_set_subject
	***  article_get_subject
	**/

	/* don't crash */
	article_set_subject (NULL, NULL);

	/* don't crash */
	article_set_subject (a1, NULL);
	check (pstring_is_valid (&a1->message_id) != FALSE)
	check (pstring_is_set (&a1->message_id) == FALSE)

	/* set the subject */
	pstr = pstring_shallow ("Some Subject", -1);
	article_set_subject (a1, &pstr);
	check (a1->subject.str != NULL)
	check (a1->subject.str != pstr.str)
	check (pstring_equal (&pstr, &a1->subject));

	/**
	*** article_set_message_id
	**/

	/* don't crash */
	article_set_message_id (NULL, NULL);
	out = article_get_message_id (NULL);
	check (!pan_strcmp (out, ""))

	/* don't crash */
	article_set_message_id (a1, NULL);
	check (pstring_is_set (&a1->message_id) == FALSE)
	check (!pan_strcmp (article_get_message_id (a1), ""));

	/* don't crash */
	pstr = pstring_shallow ("<asdf@foo.com>", -1);
	article_set_message_id (NULL, &pstr);

	/* set the message-id */
	article_set_message_id (a1, &pstr);
	check (pstring_is_set (&a1->message_id))
	check (pstring_equal (&pstr, &a1->message_id));

	/* this is the last step in place for article_is_valid() to pass */
	a1->number = 1;
	pstr = pstring_shallow ("<querty@foo.com>", -1);
	article_set_message_id (a1, &pstr);
	check (pstring_equal (&pstr, &a1->message_id));

	/**
	***  article_set_references
	**/

	article_set_references (NULL, NULL);
	article_set_references (a1, NULL);
	check (pstring_is_valid (&a1->references))
	check (!pstring_is_set (&a1->references))
	pstr = pstring_shallow ("<asdf@foo.com> <1434@bar.com>", -1);
	article_set_references (a1, &pstr);
	check (pstring_is_set (&a1->references))
	check (!pan_strcmp (a1->references.str, pstr.str));

	/**
	***  article_set_author
	***  article_get_author_str
	***  article_get_short_author_str
	**/

	a = a1;
	a->number = 1;
	pstr = pstring_shallow ("joe@foo.com", -1);
	article_set_author (a, &pstr);
	article_get_author_str (a, buf, sizeof(buf));
	check (!pan_strcmp(buf, "\"joe\" <joe@foo.com>"))
	article_get_short_author_str (a, buf, sizeof(buf));
	check (!pan_strcmp(buf, "joe"))

	pstr = pstring_shallow ("joe@foo.com (Joe Bob)", -1);
	article_set_author (a, &pstr);
	article_get_author_str (a, buf, sizeof(buf));
	check (!pan_strcmp(buf, "\"Joe Bob\" <joe@foo.com>"))
	article_get_short_author_str (a, buf, sizeof(buf));
	check (!pan_strcmp(buf, "Joe Bob"))

	pstr = pstring_shallow ("Joe Bob <joe@foo.com>", -1);
	article_set_author (a, &pstr);
	article_get_author_str (a, buf, sizeof(buf));
	check (!pan_strcmp(buf, "\"Joe Bob\" <joe@foo.com>"))
	article_get_short_author_str (a, buf, sizeof(buf));
	check (!pan_strcmp(buf, "Joe Bob"))

	pstr = pstring_shallow ("Joe Bob", -1);
	article_set_author (a, &pstr);
	article_get_author_str (a, buf, sizeof(buf));
	check (!pan_strcmp(buf, "\"Joe Bob\" <unknown@spammesenseless.com>"))
	article_get_short_author_str (a, buf, sizeof(buf));
	check (!pan_strcmp(buf, "Joe Bob"))

	/**
	***  articles_set_multipart_state
	***  article_get_multipart_state
	**/

	a = articles[article_qty++] = article_new (g);
	a->number = article_qty;
	pstr = pstring_shallow ("<213.214.242.41.21@foo.com>", -1);
	article_set_message_id (a, &pstr);
	pstr = pstring_shallow ("Me and the Major", -1);
	article_set_subject (a, &pstr);

	a = articles[article_qty++] = article_new (g);
	a->number = article_qty;
	pstr = pstring_shallow ("<213.214.242.41.21@foo.com>", -1);
	article_set_message_id (a, &pstr);
	pstr = pstring_shallow ("Like Dylan in the Movies", -1);
	article_set_subject (a, &pstr);

	articles_set_multipart_state (articles, 0, MULTIPART_STATE_SOME);
	articles_set_multipart_state (NULL, 0, MULTIPART_STATE_SOME);
	articles_set_multipart_state (NULL, article_qty, MULTIPART_STATE_SOME);
	articles_set_multipart_state (NULL, article_qty, ~0);

	articles_set_multipart_state (articles, article_qty, MULTIPART_STATE_SOME);
	check (article_get_multipart_state (articles[0]) == MULTIPART_STATE_SOME)
	check (article_get_multipart_state (articles[1]) == MULTIPART_STATE_SOME)

	articles_set_multipart_state (articles, article_qty, MULTIPART_STATE_ALL);
	check (article_get_multipart_state (articles[0]) == MULTIPART_STATE_ALL)
	check (article_get_multipart_state (articles[1]) == MULTIPART_STATE_ALL)

	articles_set_multipart_state (articles, article_qty, MULTIPART_STATE_NONE);
	check (article_get_multipart_state (articles[0]) == MULTIPART_STATE_NONE)
	check (article_get_multipart_state (articles[1]) == MULTIPART_STATE_NONE)

	/**
	***  article_get_decode_state
	***  articles_set_decode_state
	**/

	articles_set_decode_state (articles, 0, DECODE_STATE_NONE);
	articles_set_decode_state (NULL, 0, DECODE_STATE_NONE);
	articles_set_decode_state (NULL, article_qty, DECODE_STATE_NONE);
	articles_set_decode_state (NULL, article_qty, ~0);

	articles_set_decode_state (articles, article_qty, DECODE_STATE_NONE);
	check (article_get_decode_state (articles[0]) == DECODE_STATE_NONE)
	check (article_get_decode_state (articles[1]) == DECODE_STATE_NONE)

	articles_set_decode_state (articles, article_qty, DECODE_STATE_FAILED);
	check (article_get_decode_state (articles[0]) == DECODE_STATE_FAILED)
	check (article_get_decode_state (articles[1]) == DECODE_STATE_FAILED)

	articles_set_decode_state (articles, article_qty, DECODE_STATE_DECODED);
	check (article_get_decode_state (articles[0]) == DECODE_STATE_DECODED)
	check (article_get_decode_state (articles[1]) == DECODE_STATE_DECODED)


	/* success */
	g_message ("All article tests passed");
	return 0;
}
