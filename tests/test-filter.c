#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <glib.h>

#include <pan/base/article.h>
#include <pan/base/base-prefs.h>

#include <pan/filters/filter.h>
#include <pan/filters/filter-aggregate.h>
#include <pan/filters/filter-binary.h>
#include <pan/filters/filter-cached.h>
#include <pan/filters/filter-crosspost.h>
#include <pan/filters/filter-mine.h>
#include <pan/filters/filter-date.h>
#include <pan/filters/filter-linecount.h>
#include <pan/filters/filter-new.h>
#include <pan/filters/filter-phrase.h>
#include <pan/filters/filter-score.h>

static void
init_pan (void)
{
	base_prefs_init (g_get_tmp_dir(), 100, FALSE, FALSE);
}

extern int server_saving_enabled;

static int
test_phrases (Server * server, Group * group)
{
	Article * article;
	PString pstr;
	static gulong number = 0;
	int test = 0;
	Filter * f = filter_phrase_new ();

	article = article_new (group);
	article->number = ++number;
	pstr = pstring_shallow ("<asdf@foobar.com>", -1);
	article_set_message_id  (article, &pstr);

#define test(subject,expected_result) \
	pstr = pstring_shallow (subject, -1); \
	article_set_subject (article, &pstr); \
	if (!!filter_test_article (f, article) != !!expected_result) return 1; \
	g_message ("PASS: filter-phrase %d", ++test);

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_CONTAINS, PHRASE_KEY_SUBJECT, "fillyjonk", FALSE);
	test ("Can we find fillyjonk when it's by itself?", TRUE)
	test ("Can we find fillyJonk when its case is wrong?", TRUE)
	test ("Can we find FiLlYJonK when its case is more wrong?", TRUE)
	test ("Can we find FILLYJONK when it's in all uppercase?", TRUE)
	test ("Can we findfillyjonkwhen it's inside a string?", TRUE)
	test ("Can we findFillyJonKwhen it's inside a string and its case is wrong?", TRUE)
	test ("WTF is a fillyjonk?", TRUE)
	test ("WTF is a illyjonk?", FALSE)
	test ("WTF is a fillyonk?", FALSE)
	test ("WTF is a asf?", FALSE)
	test ("WTF is a fillyjon?", FALSE)

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_CONTAINS, PHRASE_KEY_SUBJECT, "fillyjonk", TRUE);
	test ("Can we find fillyjonk when it's by itself?", TRUE)
	test ("Can we find fillyJonk when its case is wrong?", FALSE)
	test ("Can we find FiLlYJonK when its case is more wrong?", FALSE)
	test ("Can we find FILLYJONK when it's in all uppercase?", FALSE)
	test ("Can we findfillyjonkwhen it's inside a string?", TRUE)
	test ("Can we findFillyJonKwhen it's inside a string and its case is wrong?", FALSE)
	test ("WTF is a fillyjonk?", TRUE)
	test ("WTF is a illyjonk?", FALSE)
	test ("WTF is a fillyonk?", FALSE)
	test ("WTF is a asf?", FALSE)
	test ("WTF is a fillyjon?", FALSE)
	test ("WTF is a Fillyjonk?", FALSE)

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_STARTS_WITH, PHRASE_KEY_SUBJECT, "fillyjonk", FALSE);
	test ("fillyjonk at the front", TRUE)
	test ("Fillyjonk at the front, in Caps", TRUE)
	test ("at the end comes the fillyjonk", FALSE)
	test ("the fillyjonk comes before the mymble", FALSE)

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_REGEX, PHRASE_KEY_SUBJECT, "^fillyjonk", FALSE);
	test ("fillyjonk at the front", TRUE)
	test ("Fillyjonk at the front, in Caps", TRUE)
	test ("at the end comes the fillyjonk", FALSE)
	test ("the fillyjonk comes before the mymble", FALSE)

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_ENDS_WITH, PHRASE_KEY_SUBJECT, "fillyjonk", FALSE);
	test ("fillyjonk at the front", FALSE)
	test ("at the end comes the fillyjonk", TRUE)
	test ("at the end, in caps, comes the Fillyjonk", TRUE)
	test ("the fillyjonk comes before the mymble", FALSE)

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_REGEX, PHRASE_KEY_SUBJECT, "fillyjonk$", FALSE);
	test ("fillyjonk at the front", FALSE)
	test ("at the end comes the fillyjonk", TRUE)
	test ("at the end, in caps, comes the Fillyjonk", TRUE)
	test ("the fillyjonk comes before the mymble", FALSE)

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_IS, PHRASE_KEY_SUBJECT, "fillyjonk", FALSE);
	test ("fillyjonk at the front", FALSE)
	test ("at the end comes the fillyjonk", FALSE)
	test ("at the end, in caps, comes the Fillyjonk", FALSE)
	test ("the fillyjonk comes before the mymble", FALSE)
	test (" fillyjonk ", FALSE)
	test ("fillyjonk ", FALSE)
	test ("fillyjonk", TRUE)
	test ("illyjonk", FALSE)
	test ("fillyjonk ", FALSE)

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_REGEX, PHRASE_KEY_SUBJECT, "^fillyjonk$", FALSE);
	test ("fillyjonk at the front", FALSE)
	test ("at the end comes the fillyjonk", FALSE)
	test ("at the end, in caps, comes the Fillyjonk", FALSE)
	test ("the fillyjonk comes before the mymble", FALSE)
	test (" fillyjonk ", FALSE)
	test ("fillyjonk ", FALSE)
	test ("fillyjonk", TRUE)
	test ("illyjonk", FALSE)
	test ("fillyjonk ", FALSE)

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_REGEX, PHRASE_KEY_SUBJECT, "(filly|jonk)", FALSE);
	test ("illyonking", FALSE)
	test ("filly at the front", TRUE)
	test ("Filly at the front, in caps", TRUE)
	test ("at the end, filly", TRUE)
	test ("in the middle fillyjonk without caps", TRUE)
	test ("in the middle Fillyjonk with caps", TRUE)

	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_REGEX, PHRASE_KEY_SUBJECT, "\\bfillyjonk\\b", FALSE);
	test ("fillyyonking at the front", FALSE)
	test ("fillyjonk at the front", TRUE)
	test ("at the end, fillyjonk", TRUE)
	test ("in the middle fillyjonk without caps", TRUE)
	test ("in the middle Fillyjonk with caps", TRUE)

	pan_object_unref (PAN_OBJECT (f));

#undef filter

	return 0;
}

static int
test_aggregate (Server * server, Group * group)
{
	Article * a;
	static gulong number = 0;
	int test = 0;
	Filter * f;
	Filter * children[5];
	int child_qty = 0;
	FilterAggregate * fa;
	Article * articles[5];
	gboolean passfail[5];
	PString pstr;

	/**
	***  Build some articles so that we can test them in aggregate
	**/

	a = articles[0] = article_new (group);
	a->number = ++number;
	pstr = pstring_shallow ("<pan.2001.01.01.12.59.59@sea.org>", -1);
	article_set_message_id (a, &pstr);
	pstr = pstring_shallow ("L Ron Hubbard Was the Man on the Grassy Knoll", -1);
	article_set_subject (a, &pstr);
	pstr = pstring_shallow ("Meg White <meg@whitestripes.com>", -1);
	article_set_author (a, &pstr);

	a = articles[1] = article_new (group);
	a->number = ++number;
	pstr = pstring_shallow ("<99049833343908@whitehouse.gov>", -1);
	article_set_message_id (a, &pstr);
	pstr = pstring_shallow ("<pan.2001.01.01.12.59.59@sea.org>", -1);
	article_set_references (a, &pstr);
	pstr = pstring_shallow ("Re: L Ron Hubbard Was the Man on the Grassy Knoll", -1);
	article_set_subject (a, &pstr);
	pstr = pstring_shallow ("Paul Wolfowitz <paulw@whitehouse.gov>", -1);
	article_set_author (a, &pstr);

	a = articles[2] = article_new (group);
	a->number = ++number;
	pstr = pstring_shallow ("<gronko@memoryhole.com>", -1);
	article_set_message_id (a, &pstr);
	pstr = pstring_shallow ("FDA to Consider Ban on Slanky Grubbleflomitz", -1);
	article_set_subject (a, &pstr);
	pstr = pstring_shallow ("R. U. Sirius <rusirius@well.com>", -1);
	article_set_author (a, &pstr);

	a = articles[3] = article_new (group);
	a->number = ++number;
	pstr = pstring_shallow ("<oiuoi34uoi3u@noneofyourbusiness.com>", -1);
	article_set_message_id (a, &pstr);
	pstr = pstring_shallow ("OT: Sigur Ros cured my insomnia!", -1);
	article_set_subject (a, &pstr);
	pstr = pstring_shallow ("Craig Wolf <craigwolf@craigwolf.com>", -1);
	article_set_author (a, &pstr);

	a = articles[4] = article_new (group);
	a->number = ++number;
	pstr = pstring_shallow ("<94083qwer9048@yahmypantsoo.com>", -1);
	article_set_message_id (a, &pstr);
	pstr = pstring_shallow ("Peach Pyjamas are Slanky", -1);
	article_set_subject (a, &pstr);
	pstr = pstring_shallow ("William Neumann <neumann@nospam.com>", -1);
	article_set_author (a, &pstr);

	/**
	***  Build the filters.
	***
	***  phrase has been tested elsewhere, so I'm not going to get too tricky about that.
	***  this portion of the test is just to exercise the aggregate.
	**/

	child_qty = 0;
	fa = FILTER_AGGREGATE (filter_aggregate_new ());
	f = children[child_qty++] = filter_phrase_new ();
	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_CONTAINS, PHRASE_KEY_SUBJECT, "slanky", FALSE);
	f = children[child_qty++] = filter_phrase_new ();
	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_CONTAINS, PHRASE_KEY_SUBJECT, "FDA", FALSE);
	filter_aggregate_insert (fa, children, child_qty, 0);

#define check_passfail(passfail_index,expected_output) \
	++test; \
	if (!!passfail[passfail_index] == !!expected_output) \
		g_message ("PASS: filter-aggregate test %d", test); \
	else \
		return test; 

	filter_test_articles (FILTER(fa), (const Article **)articles, G_N_ELEMENTS(articles), passfail);
	check_passfail (0, FALSE)
	check_passfail (1, FALSE)
	check_passfail (2, TRUE)
	check_passfail (3, FALSE)
	check_passfail (4, FALSE)

	filter_aggregate_set_type (fa, AGGREGATE_TYPE_OR);
	filter_test_articles (FILTER(fa), (const Article **)articles, G_N_ELEMENTS(articles), passfail);
	check_passfail (0, FALSE)
	check_passfail (1, FALSE)
	check_passfail (2, TRUE)
	check_passfail (3, FALSE)
	check_passfail (4, TRUE)

	f = children[child_qty++] = filter_phrase_new ();
	filter_phrase_set (FILTER_PHRASE(f), PHRASE_MATCH_CONTAINS, PHRASE_KEY_AUTHOR, "Wolf", TRUE);
	filter_aggregate_insert (fa, &f, 1, -1);

	filter_test_articles (FILTER(fa), (const Article **)articles, G_N_ELEMENTS(articles), passfail);
	check_passfail (0, FALSE)
	check_passfail (1, TRUE) /* Wolf in author */
	check_passfail (2, TRUE) /* FDA in subject, slanky in subject */
	check_passfail (3, TRUE) /* Wolf in author */
	check_passfail (4, TRUE) /* slanky in subject */

	filter_aggregate_set_type (fa, AGGREGATE_TYPE_AND);
	filter_test_articles (FILTER(fa), (const Article **)articles, G_N_ELEMENTS(articles), passfail);
	check_passfail (0, FALSE)
	check_passfail (1, FALSE)
	check_passfail (2, FALSE)
	check_passfail (3, FALSE)
	check_passfail (4, FALSE)

	filter_aggregate_remove (fa, children, 2);
	children[0] = children[2];
	child_qty = 1;
	filter_test_articles (FILTER(fa), (const Article **)articles, G_N_ELEMENTS(articles), passfail);
	check_passfail (0, FALSE)
	check_passfail (1, TRUE) /* Wolf in author */
	check_passfail (2, FALSE)
	check_passfail (3, TRUE) /* Wolf in author */
	check_passfail (4, FALSE)

#undef check_passfail

	return 0;
}


int
main (void)
{
	int val;
	Server * server;
	Group * group;

	/* setup Pan base */
	init_pan ();
	server_saving_enabled = FALSE;

	/* setup server & groups */
	server = server_new ();
	pstring_set (&server->name, "dummy_server", -1);
	group = group_new (server, "dummy_group");
	group_set_is_folder (group, TRUE);

	/* run the tests */
	if ((val = test_phrases (server, group)))
		return val;
	if ((val = test_aggregate (server, group)))
		return val;

	g_message ("All filter tests passed");
	return 0;
}
