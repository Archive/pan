#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <pan/base/pan-glib-extensions.h>
#include <pan/base/util-file.h>

static gint
compare_pint_pint_by_value (const void * va, const void *vb)
{
	return *(gint*)va - *(gint*)vb;
}

static void
count_calls_gfunc (gpointer foo, gpointer bar)
{
	gint * pint = (gint*)bar;
	++(*pint);
}

static void
sum_gfunc (gpointer foo, gpointer bar)
{
	gint * pint = (gint*)bar;
	(*pint) += GPOINTER_TO_INT(foo);
}

int main (void)
{
	gint num = 0;
	g_message ("Some of these tests feed bad input on purpose, so pan-CRITICAL warnings are okay here");

	/* pan-strcmp */
	++num;
	if (1)
	{
		if (pan_strcmp(NULL,NULL) != 0) return num;
		if (pan_strcmp("abc","abc") != 0) return num;
		if (pan_strcmp(NULL,"abc") >= 0) return num;
		if (pan_strcmp("abc",NULL) <= 0) return num;
		if (pan_strcmp("abc","bcd") >= 0) return num;
		g_message ("PASS pan_strcmp");
	}

	/* replace-gstr */
	++num;
	if (1)
	{
		gchar * a;
		gchar * b;

		/* replace_gstr */
		a = g_strdup ("abc");
		b = g_strdup ("def");
		replace_gstr (&a, b);
		if (a!=b) return num;
		replace_gstr (&a, "def");
		if (pan_strcmp(a, "def")) return num;

		/* feed it trash & don't crash */
		replace_gstr (NULL, "def");
		replace_gstr (NULL, NULL);

		g_message ("PASS replace_gstr");
	}

	/* pan-substitute */
	++num;
	if (1)
	{
		gchar * a;

		/* substitution tests */
		a = pan_substitute ("hello darkness my old friend", "darkness", "world ");
		if (pan_strcmp (a, "hello world  my old friend")) return num;
		g_free (a);
		a = pan_substitute ("xoxoxxoo", "xo", "a");
		if (pan_strcmp (a, "aaxao")) return num;
		g_free (a);
		a = pan_substitute ("coocoo to you", "o", "foo");
		if (pan_strcmp (a, "cfoofoocfoofoo tfoo yfoou")) return num;
		g_free (a);

		/* feed it trash & don't crash */
		a = pan_substitute (NULL, "xo", "a");
		if (pan_strcmp (a, NULL)) return num;
		g_free (a);
		a = pan_substitute ("foo bar", NULL, "cow");
		if (pan_strcmp (a, NULL)) return num;
		g_free (a);
		a = pan_substitute ("foo bar", "foo", NULL);
		if (pan_strcmp (a, NULL)) return num;
		g_free (a);
		a = pan_substitute (NULL, NULL, NULL);
		if (pan_strcmp (a, NULL)) return num;
		g_free (a);

		g_message ("PASS pan_substitute");
	}

	/* pan_g_ptr_array_append */
	++num;
	if (1)
	{
		GPtrArray * arr = g_ptr_array_new ();
		char * a = "a";
		char * b = "b";
		char * c = "c";
		char * foo[3];
	       
		foo[0] = a;
		foo[1] = b;
		foo[2] = c;

		g_ptr_array_add (arr, a);
		g_ptr_array_add (arr, b);
		pan_g_ptr_array_append (arr, (gpointer*)foo, 3);

		if (arr->len != 5) return num;
		if (g_ptr_array_index(arr,0) != a) return num;
		if (g_ptr_array_index(arr,1) != b) return num;
		if (g_ptr_array_index(arr,2) != a) return num;
		if (g_ptr_array_index(arr,3) != b) return num;
		if (g_ptr_array_index(arr,4) != c) return num;

		/* feed it trash & don't crash */
		pan_g_ptr_array_append (NULL, NULL, 0);
		pan_g_ptr_array_append (NULL, (gpointer*)foo, 0);
		pan_g_ptr_array_append (NULL, (gpointer*)foo, 3);

		g_message ("PASS pan_g_ptr_array_append");
	}

	/* pan_g_ptr_array_assign */
	++num;
	if (1)
	{
		gchar * foo[] = { "foo", "bar", "mum" };
		GPtrArray * a = g_ptr_array_new ();
		GPtrArray * b = g_ptr_array_new ();

		g_ptr_array_add (a, foo[0]);
		g_ptr_array_add (a, foo[2]);
		g_ptr_array_add (a, foo[1]);
		pan_g_ptr_array_assign (b, a->pdata, a->len);
		if (b->len!=3) return num;
		if (g_ptr_array_index(b,0) != foo[0]) return num;
		if (g_ptr_array_index(b,1) != foo[2]) return num;
		if (g_ptr_array_index(b,2) != foo[1]) return num;

		g_ptr_array_set_size (a, 0);
		g_ptr_array_add (b, GINT_TO_POINTER(1));
		pan_g_ptr_array_assign (b, a->pdata, a->len);
		if (b->len!=0) return num;
		pan_g_ptr_array_assign (b, a->pdata, a->len);
		pan_g_ptr_array_assign (b, NULL, 100);
		if (b->len!=0) return num;

		pan_g_ptr_array_assign (b, a->pdata, a->len);
		pan_g_ptr_array_assign (b, a->pdata, 0);
		if (b->len!=0) return num;

		/* feed it trash & don't crash */
		pan_g_ptr_array_assign (NULL, a->pdata, a->len);
		pan_g_ptr_array_assign (NULL, NULL, 100);
		pan_g_ptr_array_assign (NULL, NULL, 0);

		g_message ("PASS pan_g_ptr_array_assign");
	}

	/* pan_g_ptr_array_dup */
	++num;
	if (1)
	{
		gchar * foo [] = { "foo", "bar", "mum" };
		GPtrArray * a = g_ptr_array_new ();
		GPtrArray * b;

		/* simple array-array-dup */
		g_ptr_array_add (a, foo[0]);
		g_ptr_array_add (a, foo[1]);
		g_ptr_array_add (a, foo[2]);
		b = pan_g_ptr_array_dup (a);
		if (b->len != 3) return num;
		if (g_ptr_array_index(b,0) != foo[0]) return num;
		if (g_ptr_array_index(b,1) != foo[1]) return num;
		if (g_ptr_array_index(b,2) != foo[2]) return num;
		if (a->pdata == b->pdata) return num;

		/* feed it trash & don't crash */
		b = pan_g_ptr_array_dup (NULL);
		if (b!=NULL) return num;

		g_message ("PASS pan_g_ptr_array_dup");
	}

	/* pan_hash_to_ptr_array */
	++num;
	if (1)
	{
		GPtrArray * a;
		GHashTable * hash;
		gchar * s1 = "cold cereal";
		gchar * s2 = "veggie sandwitch & chips";
		gchar * s3 = "red beans and rice";
		gpointer p1, p2, p3;

		/* populate the hash */
	       	hash = g_hash_table_new (g_str_hash, g_str_equal);
		g_hash_table_insert (hash, "breakfast", s1);
		g_hash_table_insert (hash, "lunch", s2);
		g_hash_table_insert (hash, "dinner", s3);

		/* simple hash to ptr */
		a = g_ptr_array_new ();
		g_ptr_array_add (a, "foo");
		pan_hash_to_ptr_array (hash, a);
		if (a->len != 3) return num;
		p1 = g_ptr_array_index (a, 0);
		p2 = g_ptr_array_index (a, 1);
		p3 = g_ptr_array_index (a, 2);
		if (p1==NULL || p2==NULL || p3==NULL) return num; /* make sure none are NULL */
		if (p1==p2 || p1==p3 || p2==p3) return num; /* make sure they're all unique */
		if (p1!=s1 && p1!=s2 && p1!=s3) return num; /* make sure it's one of the strings */
		if (p2!=s1 && p2!=s2 && p2!=s3) return num; /* make sure it's one of the strings */
		if (p3!=s1 && p3!=s2 && p3!=s3) return num; /* make sure it's one of the strings */

		/* feed it trash & don't crash */
		pan_hash_to_ptr_array (NULL, a);
		if (a->len != 0) return num;
		pan_hash_to_ptr_array (hash, NULL);
		if (a->len != 0) return num;

		/* cleanup */
		g_hash_table_destroy (hash);
		g_ptr_array_free (a, TRUE);
		g_message ("PASS pan_hash_to_g_ptr_array");
	}


	/* get_next_token_g_str */
	++num;
	if (1)
	{
		GString * str = g_string_new (NULL);
		const gchar * string;
		const gchar * march;
		gint status;

		/* test empty string */
		string = NULL;
		march = NULL;
		status = get_next_token_g_str (NULL, '\n',  &march, str);
		if (status != FALSE) return num;
		g_message ("PASS get_next_token_g_str 1");

		/* test empty string 2 */
		string = "";
		march = NULL;
		status = get_next_token_g_str (NULL, '\n',  &march, str);
		if (status != FALSE) return num;
		g_message ("PASS get_next_token_g_str 2");

		/* test tokens */
		march = string = "foo\n bar\nmum ";
		status = get_next_token_g_str (march, '\n', &march, str);
		if (status == FALSE) return num;
		if (pan_strcmp (str->str, "foo")) return num;
		status = get_next_token_g_str (march, '\n', &march, str);
		if (status == FALSE) return num;
		if (pan_strcmp (str->str, " bar")) return num;
		status = get_next_token_g_str (march, '\n', &march, str);
		if (status == FALSE) return num;
		if (pan_strcmp (str->str, "mum ")) return num;
		status = get_next_token_g_str (march, '\n', &march, str);
		if (status != FALSE) return num;
		g_message ("PASS get_next_token_g_str 3");

		/* cleanup */
		g_string_free (str, TRUE);
		g_message ("PASS get_next_token_g_str");
	}


	/* skip next token */
	++num;
	if (1)
	{
		const gchar * string;
		const gchar * march;

		/* token test 1 */
		march = string = "Now is the time ";
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"is the time ")) return num;
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"the time ")) return num;
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"time ")) return num;
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"")) return num;
		g_message ("PASS skip_next_token");

		/* token test 2 */
		march = string = "Now is the time";
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"is the time")) return num;
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"the time")) return num;
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"time")) return num;
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"")) return num;
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"")) return num;
		skip_next_token (march, ' ', &march); if (pan_strcmp(march,"")) return num;
		g_message ("PASS skip_next_token 2");

		/* feed it trash & don't crash */
		march = string = "Foo Bar";
		skip_next_token (NULL, ' ', &march);
		skip_next_token (string, ' ', NULL);
	}

	/* get_next_token_int */
	++num;
	if (1)
	{
		const gchar * string;
		const gchar * march;

		/* token */
		march = string = "Foo 10 2112 Bar 1 \t 2 ";
		if (get_next_token_int (march, ' ', &march) != 0) return num;
		if (get_next_token_int (march, ' ', &march) != 10) return num;
		if (get_next_token_int (march, ' ', &march) != 2112) return num;
		if (get_next_token_int (march, ' ', &march) != 0) return num;
		if (get_next_token_int (march, ' ', &march) != 1) return num;
		if (get_next_token_int (march, ' ', &march) != 0) return num;
		if (get_next_token_int (march, ' ', &march) != 2) return num;
		if (get_next_token_int (march, ' ', &march) != 0) return num;
		if (get_next_token_int (march, ' ', &march) != 0) return num;
		if (get_next_token_int (march, ' ', &march) != 0) return num;

		/* feed it trash & don't crash */
		march = string;
		get_next_token_int (NULL, ' ', &march);
		get_next_token_int (string, ' ', NULL);
		g_message ("PASS get_next_token_int");
	}

	/* get_next_token_long */
	++num;
	if (1)
	{
		const gchar * string;
		const gchar * march;

		/* tokens */
		march = string = "Foo 10 2112 Bar 1 \t 2 ";
		if (get_next_token_long (march, ' ', &march) != 0L) return num;
		if (get_next_token_long (march, ' ', &march) != 10L) return num;
		if (get_next_token_long (march, ' ', &march) != 2112L) return num;
		if (get_next_token_long (march, ' ', &march) != 0L) return num;
		if (get_next_token_long (march, ' ', &march) != 1L) return num;
		if (get_next_token_long (march, ' ', &march) != 0L) return num;
		if (get_next_token_long (march, ' ', &march) != 2L) return num;
		if (get_next_token_long (march, ' ', &march) != 0L) return num;
		if (get_next_token_long (march, ' ', &march) != 0L) return num;
		if (get_next_token_long (march, ' ', &march) != 0L) return num;

		/* feed it trash & don't crash */
		march = string;
		get_next_token_long (NULL, ' ', &march);
		get_next_token_long (string, ' ', NULL);

		g_message ("PASS get_next_token_long");
	}

	/* get_next_token_ulong */
	++num;
	if (1)
	{
		const gchar * string;
		const gchar * march;

		/* parse a string */
		march = string = "Foo 10 2112 Bar 1 \t 2 ";
		if (get_next_token_ulong (march, ' ', &march) != 0UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 10UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 2112UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 0UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 1UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 0UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 2UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 0UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 0UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 0UL) return num;
		if (get_next_token_ulong (march, ' ', &march) != 0UL) return num;

		/* feed it trash & don't crash */
		march = string;
		get_next_token_ulong (NULL, ' ', &march);
		get_next_token_ulong (string, ' ', NULL);

		g_message ("PASS get_next_token_ulong");
	}

	/* get_next_token_str */
	++num;
	if (1)
	{
		char * pch;
		const gchar * string = "Foo 10 2112 Bar 1 \t 2 ";
		const gchar * march = string;
		if (pan_strcmp(pch=get_next_token_str(march, ' ', &march),"Foo")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, ' ', &march),"10")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, ' ', &march),"2112")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, ' ', &march),"Bar")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, ' ', &march),"1")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, ' ', &march),"\t")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, ' ', &march),"2")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, ' ', &march),NULL)) return num;
		g_free (pch);
		g_message ("PASS get_next_token_str");

		string = "Foo\t\t\t10\t2112";
		march = string;
		if (pan_strcmp(pch=get_next_token_str(march, '\t', &march),"Foo")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, '\t', &march),"")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, '\t', &march),"")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, '\t', &march),"10")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, '\t', &march),"2112")) return num;
		g_free (pch);
		if (pan_strcmp(pch=get_next_token_str(march, '\t', &march),NULL)) return num;
		g_free (pch);
		g_message ("PASS get_next_token_str 2");
	}

	/* another token test */
	++num;
	if (1)
	{
		const gchar * test = "2\n37\n0\n-1\n-1\n-1\n-1\n2\n65527\n0\n0\n0\n0";
		const gchar * pch = test;
		gint version = get_next_token_int (pch, '\n', &pch);
		glong qty = get_next_token_long (pch, '\n', &pch);
		glong name_idx = get_next_token_long (pch, '\n', &pch);
		glong read_idx = get_next_token_long (pch, '\n', &pch);
		if (version != 2) return num;
		if (qty != 37) return num;
		if (name_idx != 0) return num;
		if (read_idx != -1) return num;
		g_message ("PASS mixed token test");
	}

	/* pan_g_ptr_array_reserve */
	++num;
	if (1)
	{
		guint i;
		GPtrArray * a = g_ptr_array_new ();
		gpointer base;

		/* reserve */
		g_ptr_array_add (a, NULL);
		pan_g_ptr_array_reserve (a, 10000);
		base = a->pdata;
		for (i=1; i<10000; ++i)
			g_ptr_array_add (a, GINT_TO_POINTER(i));
		if (a->pdata != base) return num;
		pan_g_ptr_array_reserve (a, 20000);
		base = a->pdata;
		for (i=10000; i<19999; ++i)
			g_ptr_array_add (a, GINT_TO_POINTER(i));
		if (a->pdata != base) return num;
		for (i=0; i<19999; ++i)
			if (g_ptr_array_index(a,i) != GINT_TO_POINTER(i))
				return num;

		/* feed it trash & don't crash */
		pan_g_ptr_array_reserve (NULL, 100);
		pan_g_ptr_array_reserve (a, -10000);

		g_ptr_array_free (a, TRUE);
		g_message ("PASS pan_g_ptr_array_reserve");
	}

	/* pan_g_ptr_array_insert */
	++num;
	if (1)
	{
		guint i;
		GPtrArray * a;
		const gint foo [] = { 0, 0, 0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9 };
		const gint foo2 [] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, 2001, 4002 };

		/* feed it trash & don't crash */
		pan_g_ptr_array_insert (NULL, NULL, 0);

		/* insert test */
		a = g_ptr_array_new ();
		for (i=0; i<10; ++i)
			g_ptr_array_add (a, GINT_TO_POINTER(i));
		for (i=0; i<10; ++i)
			pan_g_ptr_array_insert (a, NULL, i*2);
		if (a->len != 20) return num;
		for (i=0; i<a->len; ++i)
			if (g_ptr_array_index(a,i) != GINT_TO_POINTER(foo[i]))
				return num;
		g_ptr_array_free (a, TRUE);
		g_message ("PASS pan_g_ptr_array_insert 1");

		/* another insert test */
		a = g_ptr_array_new ();
		for (i=0; i<10; ++i)
			g_ptr_array_add (a, GINT_TO_POINTER(i));
		pan_g_ptr_array_insert (a, GINT_TO_POINTER(-1), -1);
		pan_g_ptr_array_insert (a, GINT_TO_POINTER(2001), 2001);
		pan_g_ptr_array_insert (a, GINT_TO_POINTER(4002), a->len);
		for (i=0; i<a->len; ++i)
			if (g_ptr_array_index(a,i) != GINT_TO_POINTER(foo2[i]))
				return num;
		g_ptr_array_free (a, TRUE);
		g_message ("PASS pan_g_ptr_array_insert 2");
	}

	/* pan_g_ptr_array_foreach */
	++num;
	if (1)
	{
		guint i;
		gint len;
		gint sum;
		GPtrArray * a = g_ptr_array_new ();

		/* count the number of elements */
		for (i=0; i<100; ++i)
			g_ptr_array_add (a, NULL);
		len = 0;
		pan_g_ptr_array_foreach (a, count_calls_gfunc, &len);
		if (len != a->len) return num;

		/* sum them */
		g_ptr_array_add (a, GINT_TO_POINTER(1));
		g_ptr_array_add (a, GINT_TO_POINTER(10));
		g_ptr_array_add (a, GINT_TO_POINTER(100));
		g_ptr_array_add (a, GINT_TO_POINTER(1000));
		sum = 0;
		pan_g_ptr_array_foreach (a, sum_gfunc, &sum);
		if (sum != 1111) return num;

		g_ptr_array_free (a, TRUE);
		g_message ("PASS pan_g_ptr_array_foreach");
	}

	/* pan_g_string_replace */
	++num;
	if (1)
	{
		GString * s = g_string_new (NULL);

		/* test 1: replacements */
		g_string_assign (s, "dogcat");
		pan_g_string_replace (s, "cat", "cow");
		if (pan_strcmp(s->str, "dogcow")) return num;
		pan_g_string_replace (s, "o", "aa");
		if (pan_strcmp(s->str, "daagcaaw")) return num;
		pan_g_string_replace (s, "d", "w");
		pan_g_string_replace (s, "g", "c");
		pan_g_string_replace (s, "w", "");
		pan_g_string_replace (s, "c", "b");
		pan_g_string_replace (s, "aa", "a");
		if (pan_strcmp(s->str, "abba")) return num;
		pan_g_string_replace (s, "ba", "bacab");
		pan_g_string_replace (s, "bb", "b");
		if (pan_strcmp(s->str, "abacab")) return num;
		pan_g_string_replace (s, "abacab", "b.c.dull");
		if (pan_strcmp(s->str, "b.c.dull")) return num;

		/* feed it trash & don't crash */
		g_string_assign (s, "I still haven't changed");
		pan_g_string_replace (s, NULL, "abc");
		if (pan_strcmp(s->str, "I still haven't changed")) return num;
		pan_g_string_replace (s, NULL, NULL);
		if (pan_strcmp(s->str, "I still haven't changed")) return num;
		pan_g_string_replace (s, "still", NULL);
		if (pan_strcmp(s->str, "I still haven't changed")) return num;
		pan_g_string_replace (NULL, "still", NULL);
		if (pan_strcmp(s->str, "I still haven't changed")) return num;

		g_string_free (s, TRUE);
		g_message ("PASS pan_g_string_replace");
	}

	/* pan_g_string_replace */
	++num;
	if (1)
	{
		GString * s = g_string_new (NULL);

		/* test 1: stripping */
		g_string_assign (s, " \t  blah blah        ");
		pan_g_string_strstrip (s);
		if (pan_strcmp(s->str, "blah blah")) return num;
		g_string_assign (s, " \t  ha ha \n\n  \t      ");
		pan_g_string_strstrip (s);
		if (pan_strcmp(s->str, "ha ha")) return num;
		g_string_assign (s, "                        \t\t\t\t\t\t  trala la\n\n  \t      ");
		pan_g_string_strstrip (s);
		if (pan_strcmp(s->str, "trala la")) return num;

		/* test 2: feed it crap */
		pan_g_string_strstrip (NULL);

		g_string_free (s, TRUE);
		g_message ("PASS pan_g_string_strstrip");
	}

	/* lower bound */
	++num;
	if (1)
	{
		GArray * a = g_array_new (FALSE, FALSE, sizeof(gint));
		const gint iterations = 5000;
		gint i;
		srand(time(NULL));
		for (i=0; i<iterations; ++i) {
			gint val = rand ();
			gint idx = lower_bound (&val, a->data, a->len, sizeof(gint), compare_pint_pint_by_value, NULL);
			g_array_insert_val (a, idx, val);
		}
		if (a->len != iterations) return num;
		for (i=0; i<iterations-1; ++i)
			if (g_array_index(a,gint,i) > g_array_index(a,gint,i+1)) return num;

		g_array_free (a, TRUE);
		g_message ("PASS lower_bound");
	}

	/* pan_file_normalize */
	++num;
	if (1)
	{
		const char * home_dir = g_get_home_dir ();
		const char * in;
		char * expected;
		char * out;

		in = "~"G_DIR_SEPARATOR_S".signature";
		expected = g_build_filename (home_dir, ".signature", NULL);
		out = pan_file_normalize (in, NULL);
		if (pan_strcmp (expected, out))
			return num;
		g_free (expected);
		g_free (out);

		in = "~"G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S".signature";
		expected = g_build_filename (home_dir, ".signature", NULL);
		out = pan_file_normalize (in, NULL);
		if (pan_strcmp (expected, out))
			return num;
		g_free (expected);
		g_free (out);

		in = ".signature";
		expected = g_build_filename (home_dir, ".signature", NULL);
		out = pan_file_normalize (in, NULL);
		if (pan_strcmp (expected, out))
			return num;
		g_free (expected);
		g_free (out);

		in = G_DIR_SEPARATOR_S"var"G_DIR_SEPARATOR_S"tmp"G_DIR_SEPARATOR_S".signature";
		expected = G_DIR_SEPARATOR_S"var"G_DIR_SEPARATOR_S"tmp"G_DIR_SEPARATOR_S".signature";
		out = pan_file_normalize (in, NULL);
		if (pan_strcmp (expected, out))
			return num;
		g_free (out);

		in = G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S
		     "var"G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S"tmp"
		     G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S
		     ".signature";
		expected = G_DIR_SEPARATOR_S"var"G_DIR_SEPARATOR_S"tmp"G_DIR_SEPARATOR_S".signature";
		out = pan_file_normalize (in, NULL);
		if (pan_strcmp (expected, out))
			return num;
		g_free (out);

		in = "  "G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S
		     "var"G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S"tmp"
		     G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S G_DIR_SEPARATOR_S
		     ".signature     ";
		expected = G_DIR_SEPARATOR_S"var"G_DIR_SEPARATOR_S"tmp"G_DIR_SEPARATOR_S".signature";
		out = pan_file_normalize (in, NULL);
		if (pan_strcmp (expected, out))
			return num;
		g_free (out);

		g_message ("PASS pan_file_normalize");
	}

	/* success */
	g_message ("All pan-glib-extensions tests passed");
	return 0;
}
