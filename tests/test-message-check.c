#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gmime/gmime.h>

#include <pan/base/article.h>
#include <pan/base/base-prefs.h>
#include <pan/base/gnksa.h>
#include <pan/base/message-check.h>
#include <pan/base/pan-glib-extensions.h>

#define PRINT_ERRORS \
	if (1) { \
		gint i; \
		g_message ("[%d] errors, goodness [%d]", errors->len, (int)goodness); \
		for (i=0; i<errors->len; ++i) \
			g_message ("[%d][%s]", i, (gchar*)g_ptr_array_index(errors,i)); \
	}

#define check(A) \
        ++test; \
        if (A) \
                g_message ("PASS article test #%d, file %s, line %d", test, __FILE__, __LINE__); \
        else { \
                g_message ("FAIL article test #%d, file %s, line %d \"%s\"", test, __FILE__, __LINE__, #A); \
                return test; \
        }
                                                                                                                                                              

extern int server_saving_enabled;

int main (void)
{
	gint test = 0;
	Server * s;
	Group * g;
	Article * a;
	gchar * pch;
	GPtrArray * errors = g_ptr_array_new ();
	GMimePart * part;
	GMimeMessage * msg;
	GoodnessLevel goodness = OKAY;

	/* init panbase */
	g_mime_init (0);
	server_saving_enabled = FALSE;
	if (1) {
		gchar * download_dir = g_strdup_printf ("%s/data/save/", getenv("PWD"));
		base_prefs_init (download_dir, 1000, FALSE, FALSE);
		g_free (download_dir);
	}

	/* create an article */
	s = server_new ();
	pstring_set (&s->name, "Server", -1);
	g = group_new (s, "alt.test");
	group_ref_articles (g, NULL);
	server_add_groups (s, &g, 1, NULL, NULL);
	a = article_new (g);

	/* populate a simple article */
	goodness = OKAY;
	msg = g_mime_message_new (FALSE);
	g_mime_message_set_sender (msg, "\"Charles Kerr\" <charles@rebelbase.com>");
	pch = gnksa_generate_message_id ("rebelbase.com");
	g_mime_message_set_message_id (msg, pch);
	g_mime_message_set_subject (msg, "MAKE MONEY FAST");
	g_mime_message_set_subject (msg, "MAKE MONEY FAST");
	g_mime_message_set_header (msg, HEADER_ORGANIZATION, "Lazars Android Works");
	g_mime_message_set_header (msg, HEADER_NEWSGROUPS, "alt.test");
	g_mime_message_set_header (msg, PAN_ATTRIBUTION, "Someone wrote");
	part = g_mime_part_new_with_type ("text", "plain");
	pch = "Hello World!";
	g_mime_part_set_content (part, pch, strlen(pch));
	g_mime_message_set_mime_part (msg, GMIME_OBJECT(part));
	/* this should pass the tests */
	message_check (msg, s, errors, &goodness);
	check (errors->len==0)
	check (goodness==OKAY)

	/* all quoted */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch = "> Hello World!\n> All quoted text.";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len == 2)
	check (goodness == REFUSE)
	check (!pan_strcmp((gchar*)g_ptr_array_index(errors,0), "WARNING: The message is entirely quoted text!"))
	check (!pan_strcmp((gchar*)g_ptr_array_index(errors,1), "ERROR: Message appears to have no new content."))

	/* mostly quoted: less than 20% of message is new content */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch = "> Hello World!\n> quoted\n> text\n> foo\n> bar\nnew text";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((gchar*)g_ptr_array_index(errors,0), "WARNING: The message is mostly quoted text."))

	/* mostly quoted border condition: 20% of message is new content (should pass) */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch = "> Hello World!\n> quoted\n> text\n> foo\nnew text";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==0)
	check (goodness==OKAY)

	/* sig check: too long */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch = "Hello!\n\n-- \nThis\nSig\nIs\nToo\nLong\n";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((gchar*)g_ptr_array_index(errors,0), "WARNING: Signature is more than 4 lines long"))

	/* sig check: too wide */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch =
"Hello!\n"
"\n"
"-- \n"
"This sig line is exactly 80 characters wide.  I'll keep typing until I reach 80.\n"
"This sig line is greater than 80 characters wide.  In fact, it's 84 characters wide.\n"
"This sig line is greater than 80 characters wide.  In fact, it measures 95 characters in width!\n"
"This sig line is less than 80 characters wide.";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((char*)g_ptr_array_index(errors,0), "WARNING: Signature is more than 80 characters wide."))


	/* sig check: sig marker, no sig */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch = "Hello!\n\n-- \n";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((gchar*)g_ptr_array_index(errors,0), "WARNING: Signature prefix with no signature."))

	/* sig check: okay sig */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch = "Hello!\n\n-- \nThis is a short, narrow sig.\nIt should pass.\n";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==0)
	check (goodness==OKAY)

	/* adrian's fake followup */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch =   ">>>>>>>>>>>> I think A\n"
		">>>>>>>>>>> No, it's not\n"
		">>>>>>>>>> But B => C\n"
		">>>>>>>>> What's that got to do with A?\n"
		">>>>>>>> I still think B => C\n"
		">>>>>>> It's not even B => C. But Still waiting for proof for A\n"
		">>>>>> You don't prove !A, either.\n"
		">>>>> There's the FAQ: X => !A and Y => !A\n"
		">>>> But there in the text it sais T' => A\n"
		">>> But T' is only a subset of T. T => !A.\n"
		">> Moron\n"
		"> Jackass.\n"
		"\n"
		"I don't know wether I am amused or annoyed. Apparently the funny side\n"
		"prevailed so far, as I'm still reading.\n"
		"\n"
		"-- vbi";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==0)
	check (goodness==OKAY)

	/* body too wide */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch = "Hello!\n"
	      "This sig line is exactly 80 characters wide.  I'll keep typing until I reach 80.\n"
	      "This sig line is greater than 80 characters wide.  In fact, it's 84 characters wide.\n"
	      "This sig line is greater than 80 characters wide.  In fact, it measures 95 characters in width!\n"
	      "This sig line is less than 80 characters wide.";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((char*)g_ptr_array_index(errors,0), "WARNING: 2 lines are more than 80 characters wide."))

	/* body empty */
	goodness = OKAY;
	g_ptr_array_set_size (errors, 0);
	pch = "\n\t\n   \n-- \nThis is the sig.";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==2)
	check (goodness==REFUSE)
	check (!pan_strcmp((gchar*)g_ptr_array_index(errors,0), "ERROR: Message is empty."))
	check (!pan_strcmp((gchar*)g_ptr_array_index(errors,1), "ERROR: Message appears to have no new content."))
	pch = "Innocent body.";
	g_mime_part_set_content (part, pch, strlen(pch));

	/* empty subject */
	goodness = OKAY;
	pan_g_ptr_array_foreach (errors, (GFunc)g_free, NULL);
	g_ptr_array_set_size (errors, 0);
	g_mime_message_set_subject (msg, "");
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==REFUSE)
	g_mime_message_set_subject (msg, "Happy Lucky Feeling");

	/* newsgroups */
	goodness = OKAY;
	pan_g_ptr_array_foreach (errors, (GFunc)g_free, NULL);
	g_ptr_array_set_size (errors, 0);
	g_mime_message_set_header (msg, HEADER_NEWSGROUPS, "alt.test,unknown.group");
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((gchar*)g_ptr_array_index(errors,0), "WARNING: Unknown group: \"unknown.group\"."))
	g_mime_message_set_header (msg, HEADER_NEWSGROUPS, "alt.test");

	/* newsgroups */
	goodness = OKAY;
	pan_g_ptr_array_foreach (errors, (GFunc)g_free, NULL);
	g_ptr_array_set_size (errors, 0);
	g_mime_message_set_header (msg, HEADER_FOLLOWUP_TO, "alt.test,unknown.group");
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((gchar*)g_ptr_array_index(errors,0), "WARNING: Unknown group: \"unknown.group\"."))
	g_mime_object_remove_header (GMIME_OBJECT(msg), HEADER_FOLLOWUP_TO);

	/* top posting */
	goodness = OKAY;
	pan_g_ptr_array_foreach (errors, (GFunc)g_free, NULL);
	g_ptr_array_set_size (errors, 0);
	g_mime_message_set_header (msg, HEADER_REFERENCES, "<asdf@foo.com>");
	pch = "How Fascinating!\n"
	      "\n"
	      "> Blah blah blah.\n";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((char*)g_ptr_array_index(errors,0), "WARNING: Reply seems to be top-posted."))
	g_mime_object_remove_header (GMIME_OBJECT(msg), HEADER_REFERENCES);

	/* top posting */
	goodness = OKAY;
	pan_g_ptr_array_foreach (errors, (GFunc)g_free, NULL);
	g_ptr_array_set_size (errors, 0);
	g_mime_message_set_header (msg, HEADER_REFERENCES, "<asdf@foo.com>");
	pch = "How Fascinating!\n"
	      "\n"
	      "> Blah blah blah.\n"
	      "\n"
	      "-- \n"
	      "Pan shouldn't mistake this signature for\n"
	      "original content in the top-posting check.\n";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((char*)g_ptr_array_index(errors,0), "WARNING: Reply seems to be top-posted."))
	g_mime_object_remove_header (GMIME_OBJECT(msg), HEADER_REFERENCES);

	/* bad signature */
	goodness = OKAY;
	pan_g_ptr_array_foreach (errors, (GFunc)g_free, NULL);
	g_ptr_array_set_size (errors, 0);
	pch = "Testing to see what happens if the signature is malformed.\n"
	      "It *should* warn us about it.\n"
	      "\n"
	      "--\n"
	      "This is my signature.\n";
	g_mime_part_set_content (part, pch, strlen(pch));
	message_check (msg, s, errors, &goodness);
	check (errors->len==1)
	check (goodness==WARN)
	check (!pan_strcmp((char*)g_ptr_array_index(errors,0), "WARNING: The signature marker should be \"-- \", not \"--\"."));
	g_mime_object_remove_header (GMIME_OBJECT(msg), HEADER_REFERENCES);

	/* success */
	g_message ("Passed all message-check tests");
	return 0;
}
