
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <glib.h>

#include <pan/base/base-prefs.h>
#include <pan/base/debug.h>
#include <pan/base/newsrc.h>
#include <pan/base/newsrc-port.h>
#include <pan/base/util-file.h>


static gboolean
files_differ (const gchar * filename1, const gchar * filename2)
{
	char *f1=NULL, *f2=NULL;
	size_t f1_len=0, f2_len=0;
	gboolean diff;

	g_file_get_contents (filename1, &f1, &f1_len, NULL);
	g_file_get_contents (filename2, &f2, &f2_len, NULL);
	diff = f1==NULL || f2==NULL || f1_len!=f2_len || memcmp(f1,f2,f1_len);

	g_message ("files [%s] and [%s] are %s", filename1, filename2, diff?"different":"equal");
	g_free (f1);
	g_free (f2);
	return diff;
}

static int
import_test (void)
{
	int test = 0;
	char * cur;
	char *base, *noisy, *less_noisy, *in, *in_sub_only, *out, *out_bak;
	PString pin;
	PString pout;
	PString pnoisy;
	PString pdoesntexist;
	Server * server;

	cur = g_get_current_dir ();
	base = getenv ("srcdir");
	if (!g_path_is_absolute (base))
		base = g_build_filename (cur, base, NULL);
	noisy        = g_build_filename (base, "data", "newsrc_noise", NULL);
	less_noisy   = g_build_filename (base, "data", "newsrc_noise_cleaned_up", NULL);
	in           = g_build_filename (base, "data", "newsrc_1", NULL);
	in_sub_only  = g_build_filename (base, "data", "newsrc_1_subscribed_only", NULL);
	out          = g_build_filename (g_get_tmp_dir(), "newsrc_2", NULL);
	out_bak      = g_build_filename (g_get_tmp_dir(), "newsrc_2.bak", NULL);
	pin          = pstring_shallow (in, -1);
	pout         = pstring_shallow (out, -1);
	pnoisy       = pstring_shallow (noisy, -1);
	pdoesntexist = pstring_shallow ("file_that_doesnt_exit", -1);

	/* create server */
	server = server_new ();
	pstring_set (&server->name, "Temp", -1);

	/* simple import/export */
	++test;
	newsrc_import (server, &pin, FALSE);
	newsrc_export (server, &pout, FALSE);
	if (files_differ (in, out)) return test;
	g_message ("PASS simple newsrc import/export");

	/* export + backup */
	++test;
	newsrc_export (server, &pout, FALSE);
	if (files_differ (in, out)) return test;
	if (files_differ (out_bak, out)) return test;
	unlink (out);
	unlink (out_bak);
	g_message ("PASS newsrc export + backup");

	/* export subscribed only */
	++test;
	if (!pan_file_exists (in_sub_only)) return test;
	newsrc_export (server, &pout, TRUE);
	if (files_differ (in_sub_only, out)) return test;
	if (pan_file_exists (out_bak)) return test;
	unlink (out);
	g_message ("PASS newsrc export subscribed only");

	/* import noisy without crashing */
	++test;
	server = server_new ();
	pstring_set (&server->name, "Temp", -1);
	newsrc_import (server, &pnoisy, FALSE);
	newsrc_export (server, &pout, FALSE);
	if (files_differ (less_noisy, out)) return test;
	g_message ("PASS survive importing corrupt newsrc file");
	unlink (out);

	/* try to import nonexistant file w/o crashing */
	++test;
	newsrc_import (server, &pdoesntexist, FALSE);
	g_message ("PASS survive importing nonexistant newsrc file");

	g_free (base);
	g_free (cur);
	g_free (noisy);
	g_free (less_noisy);
	g_free (in);
	g_free (in_sub_only);
	g_free (out);
	g_free (out_bak);
	return 0;
}


static void
init_pan (void)
{
	gchar * base;
	gchar * download_dir;

	base = getenv ("srcdir");
	base = g_strdup_printf ("%s/", base!=NULL ? base : getenv("PWD"));
	download_dir = g_strdup ("/tmp/");
	base_prefs_init (download_dir, 100, FALSE, FALSE);

	g_free (download_dir);
	g_free (base);
}

extern int server_saving_enabled;

#define check(A) \
        ++test; \
        if (A) \
                g_message ("PASS: newsrc test %d", test); \
        else { \
                g_message ("FAIL: newsrc test %d, file %s, line %d", test, __FILE__, __LINE__); \
                return test;  \
        }

#define check_read_str(A) \
	pch = newsrc_get_read_str (n); \
	check (!strcmp (pch, A)) \
	g_free (pch);


int
main (void)
{
	int i;
	int test = 0;
	char * pch;
	gboolean b;
	Newsrc * n;

	init_pan ();
	server_saving_enabled = FALSE;

	/* init newsrc */
       	n = newsrc_new (NULL, 0, 1000);
	newsrc_init (n, "1-20,100,102,200-300", 0, 1000);

	/* simple test */
	check (newsrc_is_article_read (n,1))

	/* testing boundaries */
	check (newsrc_is_article_read (n,19))
	check (newsrc_is_article_read (n,20))
	check (!newsrc_is_article_read (n,21))

	/* more boundary testing */
	check ( newsrc_is_article_read (n,100))
	check (!newsrc_is_article_read (n,101))
	check ( newsrc_is_article_read (n,102))

	/* make sure marking read doesn't corrupt boundaries */
	b = newsrc_mark_article (n, 101, TRUE);
	check (b==FALSE)
	check (!newsrc_is_article_read(n, 99))
	check ( newsrc_is_article_read(n,100))
	check ( newsrc_is_article_read(n,101))
	check ( newsrc_is_article_read(n,102))
	check (!newsrc_is_article_read(n,103))

	/* make sure marking unread doesn't corrupt boundaries */
	b = newsrc_mark_article (n, 101, FALSE);
	check (b!=FALSE)
	check (!newsrc_is_article_read(n, 99))
	check ( newsrc_is_article_read(n,100))
	check (!newsrc_is_article_read(n,101))
	check ( newsrc_is_article_read(n,102))
	check (!newsrc_is_article_read(n,103))

	/* mark all read */
	newsrc_mark_all (n, TRUE);
	check (newsrc_is_article_read(n, 99))
	check (newsrc_is_article_read(n,100))
	check (newsrc_is_article_read(n,101))
	check (newsrc_is_article_read(n,102))
	check (newsrc_is_article_read(n,103))

	/* mark all unread */
	newsrc_mark_all (n, FALSE);
	check (!newsrc_is_article_read(n, 99))
	check (!newsrc_is_article_read(n,100))
	check (!newsrc_is_article_read(n,101))
	check (!newsrc_is_article_read(n,102))
	check (!newsrc_is_article_read(n,103))

	/* mark it read again & test the read string */
	newsrc_mark_all (n, TRUE);
	check (newsrc_is_article_read(n, 99))
	check (newsrc_is_article_read(n,100))
	check (newsrc_is_article_read(n,101))
	check (newsrc_is_article_read(n,102))
	check (newsrc_is_article_read(n,103))

	/* newsrc_get_read_str */
	newsrc_init (n, "1-20,100,102,200-300", 0, 1000);
	check_read_str ("1-20,100,102,200-300")
	b = newsrc_mark_article (n, 15, FALSE);
	check (b!=FALSE)
	check_read_str ("1-14,16-20,100,102,200-300")
	b = newsrc_mark_article (n, 101, TRUE);
	check (b==FALSE)
	check_read_str ("1-14,16-20,100-102,200-300")
	b = newsrc_mark_article (n, 103, TRUE);
	check (b==FALSE)
	check_read_str ("1-14,16-20,100-103,200-300")
	b = newsrc_mark_article (n, 105, TRUE);
	check (b==FALSE)
	check_read_str ("1-14,16-20,100-103,105,200-300")

	/* newsrc_mark_range */
	newsrc_init (n, "1-1000", 0, 1000);
	i = newsrc_mark_range (n, 100, 199, FALSE);
	check (i==100);
	check_read_str ("1-99,200-1000")
	i = newsrc_mark_range (n, 100, 199, TRUE);
	check (i==100)
	check_read_str ("1-1000")

	newsrc_init (n, "1-500", 0, 1000);
	i = newsrc_mark_range (n, 400, 800, TRUE);
	check (i==300)
	check_read_str ("1-800")

	newsrc_init (n, "250-500", 0, 1000);
	i = newsrc_mark_range (n, 100, 400, TRUE);
	check (i==150)
	check_read_str ("100-500")

	newsrc_init (n, "250-500", 0, 1000);
	i = newsrc_mark_range (n, 100, 600, TRUE);
	check (i==250)
	check_read_str ("100-600")

	newsrc_init (n, "250-500", 0, 1000);
	i = newsrc_mark_range (n, 100, 600, TRUE);
	check (i==250)
	check_read_str ("100-600")

	newsrc_init (n, "100-500", 0, 1000);
	i = newsrc_mark_range (n, 50, 600, FALSE);
	check (i==401)
	check_read_str ("")

	newsrc_init (n, "100-500", 0, 1000);
	i = newsrc_mark_range (n, 50, 200, FALSE);
	check (i==101)
	check_read_str ("201-500")

	newsrc_init (n, "100-500", 0, 1000);
	i = newsrc_mark_range (n, 400, 600, FALSE);
	check (i==101)
	check_read_str ("100-399")

	newsrc_init (n, "1-50,100-150,200-250,300-350", 0, 1000);
	i = newsrc_mark_range (n, 75, 275, FALSE);
	check (i==102)
	check_read_str ("1-50,300-350")

	newsrc_init (n, "1-50,100-150,200-250,300-350", 0, 1000);
	i = newsrc_mark_range (n, 75, 300, FALSE);
	check (i==103)
	check_read_str ("1-50,301-350")

	newsrc_init (n, "250-500", 0, 1000);
	i = newsrc_mark_range (n, 600, 700, FALSE);
	check (i==0)
	check_read_str ("250-500")
	i = newsrc_mark_range (n, 50, 200, FALSE);
	check (i==0)
	check_read_str ("250-500")

	newsrc_init (n, "1-498,500-599,601-1000", 0, 1000);
	i = newsrc_mark_range (n, 500, 599, FALSE);
	check (i==100)
	check_read_str ("1-498,601-1000")

	newsrc_init (n, "1-498,500-599,601-1000", 0, 1000);
	i = newsrc_mark_range (n, 498, 601, FALSE);
	check (i==102)
	check_read_str ("1-497,602-1000")

	newsrc_init (n, "1-498,500-599,601-1000", 0, 1000);
	i = newsrc_mark_range (n, 499, 601, FALSE);
	check (i==101)
	check_read_str ("1-498,602-1000")

	/* Janus Sandsgaard reported this one against 0.9.7pre5 */
	newsrc_init (n, "17577-17578,17581-17582,17589-17590", 0, 999999);
	i = newsrc_mark_range (n, 17578, 17578, TRUE);
	check (i==0)
	check_read_str ("17577-17578,17581-17582,17589-17590")

	/* Jiri Kubicek reported this one against 0.11.2.90 */
	newsrc_init (n, "", 1, 10);
	newsrc_mark_range (n, 1, 4, TRUE);
	newsrc_mark_range (n, 7, 9, TRUE);
	newsrc_mark_range (n, 3, 4, FALSE);
	check_read_str ("1-2,7-9")

	/* found this during a code review for 0.12.90 */
	newsrc_init (n, "1-75, 100-120, 150-200", 1, 200);
	i = newsrc_mark_range (n, 50, 175, TRUE);
	check (i==53)
	check_read_str ("1-200")

	/* newsrc_set_group_range
	 * note that after a mark all, the lower bound is 0.
	 * this follows newsrc conventions. */
	newsrc_init (n, "1-20,100,102,200-300", 0, 1000);
	newsrc_set_group_range (n, 15, 220);
	check_read_str ("15-20,100,102,200-220")
	newsrc_set_group_range (n, 50, 219);
	check_read_str ("100,102,200-219")
	newsrc_set_group_range (n, 10, 1000);
	check_read_str ("100,102,200-219")
	newsrc_mark_all (n, TRUE);
	check_read_str ("0-1000")

	/* http://bugzilla.gnome.org/show_bug.cgi?id=77878 */
	newsrc_init (n, "", 1, 10);
	newsrc_mark_range (n, 1, 4, TRUE);
	newsrc_mark_range (n, 7, 9, TRUE);
	newsrc_mark_range (n, 3, 4, FALSE);
	check_read_str ("1-2,7-9")

	/* import/export test */
	if ((test = import_test ())) return test;

	/* success */
	g_message ("PASSED all newsrc tests");
	pan_object_unref (PAN_OBJECT(n));
	return 0;
}
