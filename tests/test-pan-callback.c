#include <glib.h>
#include <pan/base/pan-callback.h>

static int cb_calls = 0;
static PanCallback * cb = NULL;

static void
callback_1 (gpointer a, gpointer b, gpointer c)
{
	g_assert (a == (gpointer)0xF00F);
	g_assert (c == GINT_TO_POINTER(1));
	++cb_calls;
}

static void
callback_3 (gpointer a, gpointer b, gpointer c)
{
	++cb_calls;
}

static void
callback_2 (gpointer a, gpointer b, gpointer c)
{
	g_assert (a == (gpointer)0xF00F);
	g_assert (c == GINT_TO_POINTER(2));
	++cb_calls;
	pan_callback_remove (cb, callback_3, GINT_TO_POINTER(3));
	pan_callback_remove (cb, callback_3, GINT_TO_POINTER(4));
}

int
main (void)
{
	cb = pan_callback_new ();
	pan_callback_add (cb, callback_1, GINT_TO_POINTER(1));
	pan_callback_add (cb, callback_2, GINT_TO_POINTER(2));
	pan_callback_add (cb, callback_3, GINT_TO_POINTER(3));
	pan_callback_add (cb, callback_3, GINT_TO_POINTER(4));
	pan_callback_add (cb, callback_3, GINT_TO_POINTER(5));
	pan_callback_call (cb, (gpointer)0xF00F, NULL);
	g_assert (cb_calls == 5);

	cb_calls = 0;
	pan_callback_remove (cb, callback_2, GINT_TO_POINTER(2));
	pan_callback_call (cb, (gpointer)0xF00F, NULL);
	g_assert (cb_calls == 2);

	g_message ("PASS: pan_callback");
	return 0;
}
