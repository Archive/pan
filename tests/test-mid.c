#include <stdlib.h>

#include <string.h>

#include <glib.h>
#include <gmime/gmime.h>

#include <pan/base/article.h>
#include <pan/base/base-prefs.h>
#include <pan/base/debug.h>
#include <pan/base/group.h>
#include <pan/base/pan-glib-extensions.h>
#include <pan/base/message-identifier.h>
#include <pan/base/server.h>
#include <pan/base/serverlist.h>


static void
pan_init (void)
{
	char * base;
	char * download_dir;

        g_mime_init (0);

	base = getenv ("srcdir");
	base = g_strdup_printf ("%s/", base!=NULL ? base : getenv("PWD"));
	download_dir = g_strdup_printf ("%s/data/save/", base);
	base_prefs_init (download_dir, 1000, FALSE, FALSE);
	g_free (download_dir);
	g_free (base);
}


#define check(A) \
	++test; \
	if (A) \
                g_message ("PASS mid test #%d, file %s, line %d", test, __FILE__, __LINE__); \
        else { \
                g_message ("FAIL mid test #%d, file %s, line %d \"%s\"", test, __FILE__, __LINE__, #A); \
                return test; \
        }

int
main (void)
{
	int test = 0;
	Server * s;
	Group * g;
	Group * groups[3];
	Article * a1;
	MessageIdentifier * mid;
	MessageSource * source;
	const char * cpch;
	const char * cpch_in;
	GPtrArray * tmp_array;
	PString pstr;

	pan_init ();

	/* setup some scaffolding: news server */
	s = server_new ();
	pstr = pstring_shallow ("news.rebelbase.com", -1);
	server_set_name (s, &pstr, NULL);
	serverlist_add_server (s, NULL);

	/* set up some scaffolding: groups */
	groups[0] = group_new (s, "alt.fan.navel.lint");
	groups[1] = group_new (s, "rec.arts.twaddle");
	groups[2] = group_new (s, "sci.phrenology");
	server_init_groups (s, groups, G_N_ELEMENTS(groups), NULL, NULL);

	g = groups[2];
	group_ref_articles (g, NULL);
	a1 = article_new (g);
	a1->number = 999;
	pstr = pstring_shallow ("Aimlessly walking around the house with a mallet", -1);
	article_set_subject (a1, &pstr);
	pstr = pstring_shallow ("<foobar.foo.bar>", -1);
	article_set_message_id (a1, &pstr);
	pstr = pstring_shallow ("news.rebelbase.com alt.fan.navel.lint:1234 rec.arts.twaddle:5678 sci.phrenology:999", -1);
	message_sources_add_from_xref (&a1->xref, a1->group->server->name_quark, pstr.str, pstr.len);
	tmp_array = g_ptr_array_new ();
	g_ptr_array_add (tmp_array, a1);
	group_init_articles (g, tmp_array, NULL);
	g_ptr_array_free (tmp_array, TRUE);

	/**
	***  Test: message_identifier_new ()
	**/

	++test;
	mid = message_identifier_new (a1->message_id.str);
	check (mid != NULL)
	check (pstring_is_set (&mid->message_id))
        check (pstring_equal (&a1->message_id, &mid->message_id))
	check (mid->readable_name == NULL)
	check (mid->line_qty == 0ul)
	check (mid->byte_qty == 0ul)
	check (mid->sources.source_qty == 0u)
	g_object_unref (G_OBJECT(mid));

	/**
	***  Test: message_identifier_new_from_article ()
	**/

	++test;
	mid = message_identifier_new_from_article (a1);
	check (message_identifier_is_valid(mid))
	check (pstring_is_set (&mid->message_id))
        check (pstring_equal (&a1->message_id, &mid->message_id))
	check (mid->readable_name != a1->subject.str)
	check (!pan_strcmp (mid->readable_name, a1->subject.str))
	check (mid->line_qty == 0ul)
	check (mid->byte_qty == 0ul)
	check (mid->sources.source_qty == 3u)
	/* check the sources... */
	source = &mid->sources.sources[0];
	check (source != NULL)
	check (source->server_quark == s->name_quark)
	check (source->group_quark == g_quark_from_string("alt.fan.navel.lint"))
	check (source->number == 1234ul)
	source = &mid->sources.sources[1];
	check (source != NULL)
	check (source->server_quark == s->name_quark)
	check (source->group_quark == g_quark_from_string("rec.arts.twaddle"))
	check (source->number == 5678ul)
	source = &mid->sources.sources[2];
	check (source != NULL)
	check (source->server_quark == s->name_quark)
	check (source->group_quark == g_quark_from_string("sci.phrenology"))
	check (source->number == a1->number)

	/**
	***  Test: message_identifier_set_readable_name
	**/

	cpch_in = "And you shall know us by the trail of the dead";
	message_identifier_set_readable_name (mid, cpch_in);
	cpch = message_identifier_get_readable_name (mid);
	check (cpch == mid->readable_name)
	check (cpch != cpch_in)
	check (!pan_strcmp (cpch, cpch_in))

	/**
	***  Test: message_identifiers_mark_read
	**/

	check (!article_is_read (a1))
	message_identifiers_mark_read ((const MessageIdentifier**)&mid, 1, TRUE, SERVER_GROUPS_ALL);
	check (article_is_read (a1))
	message_identifiers_mark_read ((const MessageIdentifier**)&mid, 1, FALSE, SERVER_GROUPS_ALL);
	check (!article_is_read (a1))
	g_object_unref (G_OBJECT(mid));

	/**
	***  Test: corrupt/duplicate xref entries
	**/

	g_message ("there should be a warning here about message_identifier_get_source_from_group()==NULL");
	pstr = pstring_shallow ("news.rebelbase.com alt.fan.navel.lint:1234 rec.arts.twaddle:5678 sci.phrenology:999 rec.arts.twaddle:5678", -1);
	message_sources_add_from_xref (&a1->xref, a1->group->server->name_quark, pstr.str, pstr.len);
	mid = message_identifier_new_from_article (a1);
	check (mid != NULL)
	check (mid->sources.source_qty == 3u)
	g_object_unref (G_OBJECT(mid));

	/**
	***  Success
	**/

	g_message ("All message-identifier tests passed");
	return 0;
}
