#include <stdlib.h>

#include <string.h>

#include <glib.h>
#include <gmime/gmime.h>
#include <pan/base/base-prefs.h>
#include <pan/base/pstring.h>


static void
pan_init (void)
{
	char * base;
	char * download_dir;

        g_mime_init (0);

	base = getenv ("srcdir");
	base = g_strdup_printf ("%s/", base!=NULL ? base : getenv("PWD"));
	download_dir = g_strdup_printf ("%s/data/save/", base);
	base_prefs_init (download_dir, 1000, FALSE, FALSE);
	g_free (download_dir);
	g_free (base);
}


#define check(A) \
	++test; \
	if (A) \
                g_message ("PASS pstring test #%d, file %s, line %d", test, __FILE__, __LINE__); \
        else { \
                g_message ("FAIL pstring test #%d, file %s, line %d \"%s\"", test, __FILE__, __LINE__, #A); \
                return test; \
        }

int
main (void)
{
	int test = 0;
	const char * cpch;
	PString a;
	PString b;
	PString march;
	PString token;

	pan_init ();

	/* shallow ctor */
	cpch = "Hello World!";
	a = pstring_shallow (cpch, -1);
	check (a.str == cpch)
	check (a.len == strlen(cpch))

	/* strstr */

	a = pstring_shallow ("Grand Canyon Sunset.jpg (1/2)", -1);
	b = pstring_shallow ("Grand", -1);
	cpch = pstring_strstr (&a, &b);
	check (cpch == a.str)

	a = pstring_shallow ("Grand Canyon Sunset.jpg (1/2)", -1);
	b = pstring_shallow ("Canyon", -1);
	cpch = pstring_strstr (&a, &b);
	check (cpch == a.str+6)

	a = pstring_shallow ("Grand Canyon Sunset.jpg (1/2)", -1);
	b = pstring_shallow ("anyon Sun", -1);
	cpch = pstring_strstr (&a, &b);
	check (cpch == a.str+7)

	a = pstring_shallow ("Grand Canyon Sunset.jpg (1/2)", -1);
	b = pstring_shallow ("anyon sun", -1);
	cpch = pstring_strstr (&a, &b);
	check (cpch == NULL)

	/* token */

	a = pstring_shallow ("Grand Canyon Sunset.jpg (1/2)", -1);
	march = a;
	check (pstring_next_token_shallow (&march, ' ', &march, &token))
	b = pstring_shallow ("Grand", -1);
	check (pstring_equal (&token, &b))
	b = pstring_shallow ("Canyon Sunset.jpg (1/2)", -1);
	check (pstring_equal (&march, &b))
	check (pstring_next_token_shallow (&march, ' ', &march, &token))
	b = pstring_shallow ("Canyon", -1);
	check (pstring_equal (&token, &b))
	b = pstring_shallow ("Sunset.jpg (1/2)", -1);
	check (pstring_equal (&march, &b))
	check (pstring_next_token_shallow (&march, ' ', &march, &token))
	b = pstring_shallow ("Sunset.jpg", -1);
	check (pstring_equal (&token, &b))
	b = pstring_shallow ("(1/2)", -1);
	check (pstring_equal (&march, &b))
	check (pstring_next_token_shallow (&march, ' ', &march, &token))
	b = pstring_shallow ("(1/2)", -1);
	check (pstring_equal (&token, &b))
	check (!march.len)
	check (!pstring_next_token_shallow (&march, ' ', &march, &token))

	/**
	***  Success
	**/

	g_message ("All pstring tests passed");
	return 0;
}
