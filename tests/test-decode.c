#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <glib.h>

#include <gmime/gmime.h>
#include <gmime/md5-utils.h>

#include <pan/base/acache.h>
#include <pan/base/article.h>
#include <pan/base/base-prefs.h>
#include <pan/base/decode.h>
#include <pan/base/group.h>
#include <pan/base/server.h>
#include <pan/base/status-item.h>
#include <pan/base/util-file.h>

static void
check_md5 (const gchar * filename, const guchar * expected_sum)
{
	gint i;
	gchar * full_filename;
	gchar * tmp;
	guchar sum [16];
	guchar sum_hex [33];

	/* init */
	memset (sum, '\0', 16);
	memset (sum_hex, '\0', 33);

	/* get the md5sum */
	tmp = pan_file_sanitize_fname(filename);
	full_filename = g_strdup_printf ("%s/%s", download_dir, tmp);
	g_free(tmp);
	pan_file_normalize_inplace (full_filename);
	g_message ("md5 check for `%s'", full_filename);
	md5_get_digest_from_file (full_filename, sum);
	for (i=0; i<16; ++i)
		sprintf (sum_hex+(i*2), "%02x", sum[i]);
	sum_hex[32] = '\0';
	g_message ("expected checksum [%s]", expected_sum);
	g_message (" decoded checksum [%s]", sum_hex);

	/* compare */
	if (memcmp(expected_sum, sum_hex, 32)) {
		g_message ("File `%s' failed md5sum test", filename);
		exit (1);
	}

	unlink (full_filename);
	g_free (full_filename);
	return;
}

static void
init_pan (void)
{
	char * base;
	char * data_dir;

	base = getenv ("srcdir");
	base = g_strdup_printf ("%s", base!=NULL ? base : getenv("PWD"));
	data_dir = g_strdup_printf ("PAN_HOME=%s/data/", base);
	putenv (data_dir);
	base_prefs_init (g_get_tmp_dir(), 100, FALSE, FALSE);
	g_message ("data_dir = `%s'", get_data_dir());

	g_free (data_dir);
	g_free (base);
}

static Server * server = NULL;
static Group * group = NULL;
static StatusItem * status = NULL;

static void
decode_from_message_ids (const char ** message_ids, int parts)
{
	decode_data dd;
	MessageIdentifier ** mids;
	guint i;

       	mids = g_alloca (sizeof(MessageIdentifier*) * parts);
	for (i=0; i<parts; ++i)
		mids[i] = message_identifier_new (message_ids[i]);
	
	dd.server = server;
	dd.acache_key = ACACHE_DEFAULT_KEY;
	dd.subject = message_ids[0];
	dd.mids = mids;
	dd.mid_qty = parts;
	dd.path = download_dir;
	dd.filename = NULL;
	dd.item = status;
	decode_article (&dd);

	for (i=0; i<parts; ++i)
		g_object_unref (mids[i]);
}

extern int server_saving_enabled;

int
main (void)
{
	const char * message_ids [64];
	int parts = 0;
	GTimeVal start;
	GTimeVal finish;
	double diff;

	g_mime_init (0);
	init_pan ();
	server_saving_enabled = FALSE;

	server = server_new ();
	pstring_set (&server->name, "Server", -1);
	group = group_new (server, "alt.test");
	group_ref_articles (group, NULL);

	server_add_groups (server, &group, 1, NULL, NULL);
	status = status_item_new_with_description ("decoding regression tests");

	g_usleep (G_USEC_PER_SEC);
	g_get_current_time (&start);

	/**
	*** "Cut Here"
	*** I hate those posts.
	**/

	parts = 0;
	message_ids[parts++] = "<noisy_1_2>";
	message_ids[parts++] = "<noisy_2_2>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("chessboard.gif", "9fa22e8183029f23bff2a85da3c2a034");
	g_message ("PASS: noisy message with `cut here' and end-of-message sig noise");

	/**
	***  Two UU Attachments
	**/

	parts = 0;
	message_ids[parts++] = "<two_uu_attachments>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("foo_01.txt", "b673788512ff1e2d2db02a522c712964");
	check_md5 ("foo_02.txt", "9c22e248d30256891e2f0d61b67bc287");
	g_message ("PASS: two uu attachments in message");

	/**
	***  Multipart yenc
	**/

	parts = 0;
	message_ids[parts++] = "<yenc_1_2>";
	message_ids[parts++] = "<yenc_2_2>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("joystick.jpg", "3331360a7976e76a9fb57eb4be23e4a3");
	g_message ("PASS: multi part yenc");

	/**
	***  Mime Attachment
	**/

	parts = 0;
	message_ids[parts++] = "<two_mime_attachments>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("Down-We-Go-1.JPG", "b4bfe57389edb1051f0eb48accbea989");
	check_md5 ("The-Only-Answer-1.JPG", "857f7f9e1fce68414cdf6a110a4ad2d2");
	g_message ("PASS: two mime attachments in message");

	/**
	***  UUEncoded, � in Filename
	**/

#if 0
	parts = 0;
	message_ids[parts++] = "<micro>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("micro�.txt", "39c14a46362de0f4764faef78e1a77b7");
	g_message ("PASS: filename with �");
#endif

	/**
	***  Mime Attachment with x-uuencode encoding
	**/

	parts = 0;
	message_ids[parts++] = "<xuuencode>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("firstspacetrip1024.jpg", "d3ac2f5a4b4b344565547af23f592fda");
	g_message ("PASS: mime message with Content-Transfer-Encoding: x-uuencode");

	/**
	***  Yenc Attachment
	**/

	parts = 0;
	message_ids[parts++] = "<yenc>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("Jungle ha itsumo Hale nochi Guu - OST.txt", "e3e458125877636a58fca66f47cd9bac");
	g_message ("PASS: yenc stuff bug");

	/**
	***  Filename collision - should rename the second one
	**/

	parts = 0;
	message_ids[parts++] = "<collision>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("collision",   "da32d55e3e524200b59d064384449db2");
	check_md5 ("collision_copy_2", "9b6f6037c580d3cc8bdf3da972ea39d9");
	check_md5 ("collision_copy_3", "25246baff3f46d51e18d6bbb5398560e");
	g_message ("PASS: filenamename collision avoided");

	/**
	***  UUEncoded, in Apostrophe Filename
	**/

	parts = 0;
	message_ids[parts++] = "<apostrophe>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("ap'str'ph'.txt", "ddcfb039ae385314aa5d8155196bb550");
	g_message ("PASS: filenames with apostrophes");

	/**
	***  This one's so noisy there's even crap INSIDE the encoded text
	**/

	parts = 0;
	message_ids[parts++] = "<penguin_1_2>";
	message_ids[parts++] = "<penguin_2_2>";
	decode_from_message_ids (message_ids, parts);
	check_md5 ("sit3-shine.7.gif", "b2934b889f259ed4e44d4c934e995c00");
	g_message ("PASS: bluespace");

	/**
	***  Done
	**/

	g_get_current_time (&finish);
	diff = finish.tv_sec - start.tv_sec;
	diff += (finish.tv_usec - start.tv_usec)/(double)G_USEC_PER_SEC;
	g_message ("All decode tests passed in %.3f seconds", diff);
	return 0;
}
