/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Jeffrey Stedfast <fejj@helixcode.com>
 *
 *  Copyright 2000 Helix Code, Inc. (www.helixcode.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __GMIME_H__
#define __GMIME_H__

#include <glib/gtypes.h>
#include "gmime-charset.h"
#include "gmime-iconv.h"
#include "gmime-iconv-utils.h"
#include "gmime-param.h"
#include "gmime-content-type.h"
#include "gmime-disposition.h"
#include "gmime-data-wrapper.h"
#include "gmime-object.h"
#include "gmime-part.h"
#include "gmime-multipart.h"
#include "gmime-message.h"
#include "gmime-message-part.h"
#include "gmime-message-partial.h"
#include "internet-address.h"
#include "gmime-parser.h"
#include "gmime-utils.h"
#include "gmime-stream.h"
#include "gmime-stream-buffer.h"
#include "gmime-stream-cat.h"
#include "gmime-stream-file.h"
#include "gmime-stream-filter.h"
#include "gmime-stream-mem.h"
#include "gmime-filter.h"
#include "gmime-filter-basic.h"
#include "gmime-filter-charset.h"
#include "gmime-filter-html.h"
#include "gmime-filter-yenc.h"

/* GMIME version */
static const guint gmime_major_version = 1;
static const guint gmime_minor_version = 90;
static const guint gmime_micro_version = 0;
static const guint gmime_interface_age = 0;
static const guint gmime_binary_age    = 0;

#define GMIME_CHECK_VERSION(major,minor,micro)    \
    (gmime_major_version > (major) || \
     (gmime_major_version == (major) && gmime_minor_version > (minor)) || \
     (gmime_major_version == (major) && gmime_minor_version == (minor) && \
      gmime_micro_version >= (micro)))


/**
 * GMIME_INIT_FLAG_UTF8:
 *
 * Initialization flag to enable UTF-8 interfaces throughout GMime.
 *
 * Note: this flag is really a no-op and remains only for backward
 * compatablity. Interfaces will be UTF-8 whether this flag is used or
 * not.
 **/
#define GMIME_INIT_FLAG_UTF8  (1 << 0)

void g_mime_init (guint32 flags);

#endif /* __GMIME_H__ */
