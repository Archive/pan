/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Jeffrey Stedfast <fejj@ximian.com>
 *
 *  Copyright 2002 Ximian, Inc. (www.ximian.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */


#ifndef __GMIME_TYPE_UTILS_H__
#define __GMIME_TYPE_UTILS_H__

#define	GMIME_CHECK_CAST        G_TYPE_CHECK_INSTANCE_CAST
#define	GMIME_CHECK_CLASS_CAST  G_TYPE_CHECK_CLASS_CAST
#define GMIME_CHECK_GET_CLASS   G_TYPE_INSTANCE_GET_CLASS
#define	GMIME_CHECK_TYPE        G_TYPE_CHECK_INSTANCE_TYPE
#define	GMIME_CHECK_CLASS_TYPE  G_TYPE_CHECK_CLASS_TYPE

#endif /* __GMIME_TYPE_UTILS_H__ */
