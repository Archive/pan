/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Jeffrey Stedfast <fejj@ximian.com>
 *
 *  Copyright 2001 Ximian, Inc. (www.ximian.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include "gmime-stream-filter.h"

#define READ_PAD (64)		/* bytes padded before buffer */
#define READ_SIZE (4096)

#define _PRIVATE(o) (((GMimeStreamFilter *)(o))->priv)

struct _filter {
	struct _filter *next;
	GMimeFilter *filter;
	int id;
};

struct _GMimeStreamFilterPrivate {
	struct _filter *filters;
	int filterid;		/* next filter id */
	
	char *realbuffer;	/* buffer - READ_PAD */
	char *buffer;		/* READ_SIZE bytes */
	
	char *filtered;		/* the filtered data */
	size_t filteredlen;
	
	int last_was_read;	/* was the last op read or write? */
};

static void g_mime_stream_filter_class_init (GMimeStreamFilterClass *klass);
static void g_mime_stream_filter_init (GMimeStreamFilter *stream, GMimeStreamFilterClass *klass);
static void g_mime_stream_filter_finalize (GObject *object);

static gssize stream_read (GMimeStream *stream, char *buf, size_t len);
static gssize stream_write (GMimeStream *stream, char *buf, size_t len);
static int stream_flush (GMimeStream *stream);
static int stream_close (GMimeStream *stream);
static gboolean stream_eos (GMimeStream *stream);
static int stream_reset (GMimeStream *stream);
static off_t stream_seek (GMimeStream *stream, off_t offset, GMimeSeekWhence whence);
static off_t stream_tell (GMimeStream *stream);
static gssize stream_length (GMimeStream *stream);
static GMimeStream *stream_substream (GMimeStream *stream, off_t start, off_t end);


static GMimeStreamClass *parent_class = NULL;


GType
g_mime_stream_filter_get_type (void)
{
	static GType type = 0;
	
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GMimeStreamFilterClass),
			NULL, /* base_class_init */
			NULL, /* base_class_finalize */
			(GClassInitFunc) g_mime_stream_filter_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (GMimeStreamFilter),
			16,   /* n_preallocs */
			(GInstanceInitFunc) g_mime_stream_filter_init,
		};
		
		type = g_type_register_static (GMIME_TYPE_STREAM, "GMimeStreamFilter", &info, 0);
	}
	
	return type;
}


static void
g_mime_stream_filter_class_init (GMimeStreamFilterClass *klass)
{
	GMimeStreamClass *stream_class = GMIME_STREAM_CLASS (klass);
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	
	parent_class = g_type_class_ref (GMIME_TYPE_STREAM);
	
	object_class->finalize = g_mime_stream_filter_finalize;
	
	stream_class->read = stream_read;
	stream_class->write = stream_write;
	stream_class->flush = stream_flush;
	stream_class->close = stream_close;
	stream_class->eos = stream_eos;
	stream_class->reset = stream_reset;
	stream_class->seek = stream_seek;
	stream_class->tell = stream_tell;
	stream_class->length = stream_length;
	stream_class->substream = stream_substream;
}

static void
g_mime_stream_filter_init (GMimeStreamFilter *stream, GMimeStreamFilterClass *klass)
{
	stream->source = NULL;
	stream->priv = g_new (struct _GMimeStreamFilterPrivate, 1);
	stream->priv->filters = NULL;
	stream->priv->filterid = 0;
	stream->priv->realbuffer = g_malloc (READ_SIZE + READ_PAD);
	stream->priv->buffer = stream->priv->realbuffer + READ_PAD;
	stream->priv->last_was_read = TRUE;
	stream->priv->filteredlen = 0;
}

static void
g_mime_stream_filter_finalize (GObject *object)
{
	GMimeStreamFilter *filter = (GMimeStreamFilter *) object;
	struct _GMimeStreamFilterPrivate *p = filter->priv;
	struct _filter *fn, *f;
	
	f = p->filters;
	while (f) {
		fn = f->next;
		g_mime_filter_destroy (f->filter);
		g_free (f);
		f = fn;
	}
	
	g_free (p->realbuffer);
	g_free (p);
	
	if (filter->source)
		g_mime_stream_unref (filter->source);
	
	G_OBJECT_CLASS (parent_class)->finalize (object);
}


static gssize
stream_read (GMimeStream *stream, char *buf, size_t len)
{
	GMimeStreamFilter *filter = (GMimeStreamFilter *) stream;
	struct _GMimeStreamFilterPrivate *p = filter->priv;
	struct _filter *f;
	gssize size;
	
	p->last_was_read = TRUE;
	
	if (p->filteredlen <= 0) {
		size_t presize = READ_SIZE;
		
		size = g_mime_stream_read (filter->source, p->buffer, READ_SIZE);
		if (size <= 0) {
			/* this is somewhat untested */
			if (g_mime_stream_eos (filter->source)) {
				f = p->filters;
				p->filtered = p->buffer;
				p->filteredlen = 0;
				while (f) {
					g_mime_filter_complete (f->filter, p->filtered, p->filteredlen,
								presize, &p->filtered, &p->filteredlen,
								&presize);
					f = f->next;
				}
				size = p->filteredlen;
			}
			if (size <= 0)
				return size;
		} else {
			f = p->filters;
			p->filtered = p->buffer;
			p->filteredlen = size;
			
			while (f) {
				g_mime_filter_filter (f->filter, p->filtered, p->filteredlen, presize,
						      &p->filtered, &p->filteredlen, &presize);
				
				f = f->next;
			}
		}
	}
	
	size = MIN (len, p->filteredlen);
	memcpy (buf, p->filtered, size);
	p->filteredlen -= size;
	p->filtered += size;
	
	return size;
}

static gssize
stream_write (GMimeStream *stream, char *buf, size_t len)
{
	GMimeStreamFilter *filter = (GMimeStreamFilter *) stream;
	struct _GMimeStreamFilterPrivate *p = filter->priv;
	struct _filter *f;
	size_t presize;
	char *buffer;
	size_t n;
	
	p->last_was_read = FALSE;
	
	buffer = buf;
	n = len;
	
	f = p->filters;
	presize = 0;
	while (f) {
		g_mime_filter_filter (f->filter, buffer, n, presize, &buffer, &n, &presize);
		
		f = f->next;
	}
	
	if (g_mime_stream_write (filter->source, buffer, n) != n)
		return -1;
	
	/* return 'len' because that's what our caller expects */
	return len;
}

static int
stream_flush (GMimeStream *stream)
{
	GMimeStreamFilter *filter = (GMimeStreamFilter *) stream;
	struct _GMimeStreamFilterPrivate *p = filter->priv;
	size_t len, presize;
	struct _filter *f;
	char *buffer;
	
	if (p->last_was_read) {
		/* no-op */
		return 0;
	}
	
	buffer = "";
	len = 0;
	presize = 0;
	f = p->filters;
	
	while (f) {
		g_mime_filter_complete (f->filter, buffer, len, presize, &buffer, &len, &presize);
		
		f = f->next;
	}
	
	if (len > 0 && g_mime_stream_write (filter->source, buffer, len) == -1)
		return -1;
	
	return g_mime_stream_flush (filter->source);
}

static int
stream_close (GMimeStream *stream)
{
	GMimeStreamFilter *filter = (GMimeStreamFilter *) stream;
	struct _GMimeStreamFilterPrivate *p = filter->priv;
	
	if (!p->last_was_read) {
		stream_flush (stream);
	}
	
	return g_mime_stream_close (filter->source);
}

static gboolean
stream_eos (GMimeStream *stream)
{
	GMimeStreamFilter *filter = (GMimeStreamFilter *) stream;
	struct _GMimeStreamFilterPrivate *p = filter->priv;
	
	if (p->filteredlen > 0)
		return FALSE;
	
	return g_mime_stream_eos (filter->source);
}

static int
stream_reset (GMimeStream *stream)
{
	GMimeStreamFilter *filter = (GMimeStreamFilter *) stream;
	struct _GMimeStreamFilterPrivate *p = filter->priv;
	struct _filter *f;
	
	p->filteredlen = 0;
	
	/* and reset filters */
	f = p->filters;
	while (f) {
		g_mime_filter_reset (f->filter);
		f = f->next;
	}
	
	return g_mime_stream_reset (filter->source);
}

static off_t
stream_seek (GMimeStream *stream, off_t offset, GMimeSeekWhence whence)
{
	/*GMimeStreamFilter *filter = (GMimeStreamFilter *) stream;*/
	
	return -1;
}

static off_t
stream_tell (GMimeStream *stream)
{
	return -1;
}

static gssize
stream_length (GMimeStream *stream)
{
	return stream->bound_end - stream->bound_start;
}

static GMimeStream *
stream_substream (GMimeStream *stream, off_t start, off_t end)
{
	GMimeStreamFilter *filter = (GMimeStreamFilter *) stream;
	GMimeStreamFilter *sub;
	/*struct _filter *f, *fn;*/
	
	sub = g_object_new (GMIME_TYPE_STREAM_FILTER, NULL, NULL);
	sub->source = filter->source;
	g_mime_stream_ref (sub->source);
	
	if (filter->priv->filters) {
		struct _filter *f, *sn, *s = NULL;
		
		f = filter->priv->filters;
		while (f) {
			sn = g_new (struct _filter, 1);
			sn->filter = g_mime_filter_copy (f->filter);
			sn->id = f->id;
			
			if (s) {
				s->next = sn;
				s = sn;
			} else {
				s = sub->priv->filters = sn;
			}
			
			f = f->next;
		}
		
		s->next = NULL;
		
		sub->priv->filterid = filter->priv->filterid;
	}
	
	g_mime_stream_construct (GMIME_STREAM (filter),
				 sub->source->bound_start,
				 sub->source->bound_end);
	
	return GMIME_STREAM (sub);
}


/**
 * g_mime_stream_filter_new_with_stream:
 * @stream: source stream
 *
 * Creates a new GMimeStreamFilter object using @stream as the source
 * stream.
 *
 * Returns a new filter stream with @stream as its source.
 **/
GMimeStream *
g_mime_stream_filter_new_with_stream (GMimeStream *stream)
{
	GMimeStreamFilter *filter;
	
	g_return_val_if_fail (GMIME_IS_STREAM (stream), NULL);
	
	filter = g_object_new (GMIME_TYPE_STREAM_FILTER, NULL, NULL);
	filter->source = stream;
	g_mime_stream_ref (stream);
	
	g_mime_stream_construct (GMIME_STREAM (filter),
				 stream->bound_start,
				 stream->bound_end);
	
	return GMIME_STREAM (filter);
}


/**
 * g_mime_stream_filter_add:
 * @fstream: filter stream
 * @filter: filter
 *
 * Adds a @filter to @fstream.
 *
 * Returns an id for the filter.
 **/
int
g_mime_stream_filter_add (GMimeStreamFilter *fstream, GMimeFilter *filter)
{
	struct _GMimeStreamFilterPrivate *p;
	struct _filter *f, *fn;
	
	g_return_val_if_fail (GMIME_IS_STREAM_FILTER (fstream), -1);
	g_return_val_if_fail (filter != NULL, -1);
	
	p = fstream->priv;
	
	fn = g_new (struct _filter, 1);
	fn->next = NULL;
	fn->filter = filter;
	fn->id = p->filterid++;
	
	f = (struct _filter *) &p->filters;
	while (f->next)
		f = f->next;
	
	f->next = fn;
	fn->next = NULL;
	
	return fn->id;
}


/**
 * g_mime_stream_filter_remove:
 * @fstream: filter stream
 * @id: filter id
 *
 * Removed a filter from the stream based on the id (as returned from
 * filter_add).
 **/
void
g_mime_stream_filter_remove (GMimeStreamFilter *fstream, int id)
{
	struct _GMimeStreamFilterPrivate *p;
	struct _filter *f, *fn;
	
	g_return_if_fail (GMIME_IS_STREAM_FILTER (fstream));
	
	p = fstream->priv;
	
	if (id == -1)
		return;
	
	f = (struct _filter *) &p->filters;
	while (f && f->next) {
		fn = f->next;
		if (fn->id == id) {
			f->next = fn->next;
			g_mime_filter_destroy (fn->filter);
			g_free (fn);
		}
		f = f->next;
	}
}
